import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { BesarBayarComponentsPage, BesarBayarDeleteDialog, BesarBayarUpdatePage } from './besar-bayar.page-object';

const expect = chai.expect;

describe('BesarBayar e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let besarBayarComponentsPage: BesarBayarComponentsPage;
  let besarBayarUpdatePage: BesarBayarUpdatePage;
  let besarBayarDeleteDialog: BesarBayarDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load BesarBayars', async () => {
    await navBarPage.goToEntity('besar-bayar');
    besarBayarComponentsPage = new BesarBayarComponentsPage();
    await browser.wait(ec.visibilityOf(besarBayarComponentsPage.title), 5000);
    expect(await besarBayarComponentsPage.getTitle()).to.eq('sisteminformasiakademikApp.besarBayar.home.title');
    await browser.wait(ec.or(ec.visibilityOf(besarBayarComponentsPage.entities), ec.visibilityOf(besarBayarComponentsPage.noResult)), 1000);
  });

  it('should load create BesarBayar page', async () => {
    await besarBayarComponentsPage.clickOnCreateButton();
    besarBayarUpdatePage = new BesarBayarUpdatePage();
    expect(await besarBayarUpdatePage.getPageTitle()).to.eq('sisteminformasiakademikApp.besarBayar.home.createOrEditLabel');
    await besarBayarUpdatePage.cancel();
  });

  it('should create and save BesarBayars', async () => {
    const nbButtonsBeforeCreate = await besarBayarComponentsPage.countDeleteButtons();

    await besarBayarComponentsPage.clickOnCreateButton();

    await promise.all([
      besarBayarUpdatePage.setNominalInput('5'),
      besarBayarUpdatePage.kelasSelectLastOption(),
      besarBayarUpdatePage.jenisBayarSelectLastOption()
    ]);

    expect(await besarBayarUpdatePage.getNominalInput()).to.eq('5', 'Expected nominal value to be equals to 5');

    await besarBayarUpdatePage.save();
    expect(await besarBayarUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await besarBayarComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last BesarBayar', async () => {
    const nbButtonsBeforeDelete = await besarBayarComponentsPage.countDeleteButtons();
    await besarBayarComponentsPage.clickOnLastDeleteButton();

    besarBayarDeleteDialog = new BesarBayarDeleteDialog();
    expect(await besarBayarDeleteDialog.getDialogTitle()).to.eq('sisteminformasiakademikApp.besarBayar.delete.question');
    await besarBayarDeleteDialog.clickOnConfirmButton();

    expect(await besarBayarComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
