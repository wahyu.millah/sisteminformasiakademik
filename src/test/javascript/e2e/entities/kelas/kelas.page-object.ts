import { element, by, ElementFinder } from 'protractor';

export class KelasComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-kelas div table .btn-danger'));
  title = element.all(by.css('jhi-kelas div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class KelasUpdatePage {
  pageTitle = element(by.id('jhi-kelas-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  namaInput = element(by.id('field_nama'));
  jenjangSelect = element(by.id('field_jenjang'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNamaInput(nama: string): Promise<void> {
    await this.namaInput.sendKeys(nama);
  }

  async getNamaInput(): Promise<string> {
    return await this.namaInput.getAttribute('value');
  }

  async setJenjangSelect(jenjang: string): Promise<void> {
    await this.jenjangSelect.sendKeys(jenjang);
  }

  async getJenjangSelect(): Promise<string> {
    return await this.jenjangSelect.element(by.css('option:checked')).getText();
  }

  async jenjangSelectLastOption(): Promise<void> {
    await this.jenjangSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class KelasDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-kelas-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-kelas'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
