import { element, by, ElementFinder } from 'protractor';

export class JenisPembayaranComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-jenis-pembayaran div table .btn-danger'));
  title = element.all(by.css('jhi-jenis-pembayaran div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class JenisPembayaranUpdatePage {
  pageTitle = element(by.id('jhi-jenis-pembayaran-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  jenisInput = element(by.id('field_jenis'));
  metodeInput = element(by.id('field_metode'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setJenisInput(jenis: string): Promise<void> {
    await this.jenisInput.sendKeys(jenis);
  }

  async getJenisInput(): Promise<string> {
    return await this.jenisInput.getAttribute('value');
  }

  async setMetodeInput(metode: string): Promise<void> {
    await this.metodeInput.sendKeys(metode);
  }

  async getMetodeInput(): Promise<string> {
    return await this.metodeInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class JenisPembayaranDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-jenisPembayaran-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-jenisPembayaran'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
