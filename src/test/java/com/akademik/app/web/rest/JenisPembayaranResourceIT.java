package com.akademik.app.web.rest;

import com.akademik.app.SisteminformasiakademikApp;
import com.akademik.app.domain.JenisPembayaran;
import com.akademik.app.repository.JenisPembayaranRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link JenisPembayaranResource} REST controller.
 */
@SpringBootTest(classes = SisteminformasiakademikApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class JenisPembayaranResourceIT {

    private static final String DEFAULT_JENIS = "AAAAAAAAAA";
    private static final String UPDATED_JENIS = "BBBBBBBBBB";

    private static final String DEFAULT_METODE = "AAAAAAAAAA";
    private static final String UPDATED_METODE = "BBBBBBBBBB";

    @Autowired
    private JenisPembayaranRepository jenisPembayaranRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restJenisPembayaranMockMvc;

    private JenisPembayaran jenisPembayaran;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JenisPembayaran createEntity(EntityManager em) {
        JenisPembayaran jenisPembayaran = new JenisPembayaran()
            .jenis(DEFAULT_JENIS)
            .metode(DEFAULT_METODE);
        return jenisPembayaran;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JenisPembayaran createUpdatedEntity(EntityManager em) {
        JenisPembayaran jenisPembayaran = new JenisPembayaran()
            .jenis(UPDATED_JENIS)
            .metode(UPDATED_METODE);
        return jenisPembayaran;
    }

    @BeforeEach
    public void initTest() {
        jenisPembayaran = createEntity(em);
    }

    @Test
    @Transactional
    public void createJenisPembayaran() throws Exception {
        int databaseSizeBeforeCreate = jenisPembayaranRepository.findAll().size();
        // Create the JenisPembayaran
        restJenisPembayaranMockMvc.perform(post("/api/jenis-pembayarans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jenisPembayaran)))
            .andExpect(status().isCreated());

        // Validate the JenisPembayaran in the database
        List<JenisPembayaran> jenisPembayaranList = jenisPembayaranRepository.findAll();
        assertThat(jenisPembayaranList).hasSize(databaseSizeBeforeCreate + 1);
        JenisPembayaran testJenisPembayaran = jenisPembayaranList.get(jenisPembayaranList.size() - 1);
        assertThat(testJenisPembayaran.getJenis()).isEqualTo(DEFAULT_JENIS);
        assertThat(testJenisPembayaran.getMetode()).isEqualTo(DEFAULT_METODE);
    }

    @Test
    @Transactional
    public void createJenisPembayaranWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jenisPembayaranRepository.findAll().size();

        // Create the JenisPembayaran with an existing ID
        jenisPembayaran.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJenisPembayaranMockMvc.perform(post("/api/jenis-pembayarans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jenisPembayaran)))
            .andExpect(status().isBadRequest());

        // Validate the JenisPembayaran in the database
        List<JenisPembayaran> jenisPembayaranList = jenisPembayaranRepository.findAll();
        assertThat(jenisPembayaranList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllJenisPembayarans() throws Exception {
        // Initialize the database
        jenisPembayaranRepository.saveAndFlush(jenisPembayaran);

        // Get all the jenisPembayaranList
        restJenisPembayaranMockMvc.perform(get("/api/jenis-pembayarans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jenisPembayaran.getId().intValue())))
            .andExpect(jsonPath("$.[*].jenis").value(hasItem(DEFAULT_JENIS)))
            .andExpect(jsonPath("$.[*].metode").value(hasItem(DEFAULT_METODE)));
    }
    
    @Test
    @Transactional
    public void getJenisPembayaran() throws Exception {
        // Initialize the database
        jenisPembayaranRepository.saveAndFlush(jenisPembayaran);

        // Get the jenisPembayaran
        restJenisPembayaranMockMvc.perform(get("/api/jenis-pembayarans/{id}", jenisPembayaran.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(jenisPembayaran.getId().intValue()))
            .andExpect(jsonPath("$.jenis").value(DEFAULT_JENIS))
            .andExpect(jsonPath("$.metode").value(DEFAULT_METODE));
    }
    @Test
    @Transactional
    public void getNonExistingJenisPembayaran() throws Exception {
        // Get the jenisPembayaran
        restJenisPembayaranMockMvc.perform(get("/api/jenis-pembayarans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJenisPembayaran() throws Exception {
        // Initialize the database
        jenisPembayaranRepository.saveAndFlush(jenisPembayaran);

        int databaseSizeBeforeUpdate = jenisPembayaranRepository.findAll().size();

        // Update the jenisPembayaran
        JenisPembayaran updatedJenisPembayaran = jenisPembayaranRepository.findById(jenisPembayaran.getId()).get();
        // Disconnect from session so that the updates on updatedJenisPembayaran are not directly saved in db
        em.detach(updatedJenisPembayaran);
        updatedJenisPembayaran
            .jenis(UPDATED_JENIS)
            .metode(UPDATED_METODE);

        restJenisPembayaranMockMvc.perform(put("/api/jenis-pembayarans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedJenisPembayaran)))
            .andExpect(status().isOk());

        // Validate the JenisPembayaran in the database
        List<JenisPembayaran> jenisPembayaranList = jenisPembayaranRepository.findAll();
        assertThat(jenisPembayaranList).hasSize(databaseSizeBeforeUpdate);
        JenisPembayaran testJenisPembayaran = jenisPembayaranList.get(jenisPembayaranList.size() - 1);
        assertThat(testJenisPembayaran.getJenis()).isEqualTo(UPDATED_JENIS);
        assertThat(testJenisPembayaran.getMetode()).isEqualTo(UPDATED_METODE);
    }

    @Test
    @Transactional
    public void updateNonExistingJenisPembayaran() throws Exception {
        int databaseSizeBeforeUpdate = jenisPembayaranRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJenisPembayaranMockMvc.perform(put("/api/jenis-pembayarans")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(jenisPembayaran)))
            .andExpect(status().isBadRequest());

        // Validate the JenisPembayaran in the database
        List<JenisPembayaran> jenisPembayaranList = jenisPembayaranRepository.findAll();
        assertThat(jenisPembayaranList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteJenisPembayaran() throws Exception {
        // Initialize the database
        jenisPembayaranRepository.saveAndFlush(jenisPembayaran);

        int databaseSizeBeforeDelete = jenisPembayaranRepository.findAll().size();

        // Delete the jenisPembayaran
        restJenisPembayaranMockMvc.perform(delete("/api/jenis-pembayarans/{id}", jenisPembayaran.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<JenisPembayaran> jenisPembayaranList = jenisPembayaranRepository.findAll();
        assertThat(jenisPembayaranList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
