package com.akademik.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.akademik.app.web.rest.TestUtil;

public class JenisPembayaranTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JenisPembayaran.class);
        JenisPembayaran jenisPembayaran1 = new JenisPembayaran();
        jenisPembayaran1.setId(1L);
        JenisPembayaran jenisPembayaran2 = new JenisPembayaran();
        jenisPembayaran2.setId(jenisPembayaran1.getId());
        assertThat(jenisPembayaran1).isEqualTo(jenisPembayaran2);
        jenisPembayaran2.setId(2L);
        assertThat(jenisPembayaran1).isNotEqualTo(jenisPembayaran2);
        jenisPembayaran1.setId(null);
        assertThat(jenisPembayaran1).isNotEqualTo(jenisPembayaran2);
    }
}
