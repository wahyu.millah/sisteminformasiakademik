package com.akademik.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.akademik.app.web.rest.TestUtil;

public class BesarBayarTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BesarBayar.class);
        BesarBayar besarBayar1 = new BesarBayar();
        besarBayar1.setId(1L);
        BesarBayar besarBayar2 = new BesarBayar();
        besarBayar2.setId(besarBayar1.getId());
        assertThat(besarBayar1).isEqualTo(besarBayar2);
        besarBayar2.setId(2L);
        assertThat(besarBayar1).isNotEqualTo(besarBayar2);
        besarBayar1.setId(null);
        assertThat(besarBayar1).isNotEqualTo(besarBayar2);
    }
}
