package com.akademik.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.akademik.app.web.rest.TestUtil;

public class KelasTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Kelas.class);
        Kelas kelas1 = new Kelas();
        kelas1.setId(1L);
        Kelas kelas2 = new Kelas();
        kelas2.setId(kelas1.getId());
        assertThat(kelas1).isEqualTo(kelas2);
        kelas2.setId(2L);
        assertThat(kelas1).isNotEqualTo(kelas2);
        kelas1.setId(null);
        assertThat(kelas1).isNotEqualTo(kelas2);
    }
}
