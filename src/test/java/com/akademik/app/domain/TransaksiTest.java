package com.akademik.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.akademik.app.web.rest.TestUtil;

public class TransaksiTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Transaksi.class);
        Transaksi transaksi1 = new Transaksi();
        transaksi1.setId(1L);
        Transaksi transaksi2 = new Transaksi();
        transaksi2.setId(transaksi1.getId());
        assertThat(transaksi1).isEqualTo(transaksi2);
        transaksi2.setId(2L);
        assertThat(transaksi1).isNotEqualTo(transaksi2);
        transaksi1.setId(null);
        assertThat(transaksi1).isNotEqualTo(transaksi2);
    }
}
