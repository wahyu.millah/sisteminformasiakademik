import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IKelas, Kelas } from 'app/shared/model/kelas.model';
import { GenerateService } from './generate.service';
import { GenerateComponent } from './generate.component';

@Injectable({ providedIn: 'root' })
export class GenerateResolve implements Resolve<IKelas> {
  constructor(private service: GenerateService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IKelas> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((kelas: HttpResponse<Kelas>) => {
          if (kelas.body) {
            return of(kelas.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Kelas());
  }
}

export const generateRoute: Routes = [
  {
    path: '',
    component: GenerateComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sisteminformasiakademikApp.kelas.home.titleGenerate'
    },
    canActivate: [UserRouteAccessService]
  }
];
