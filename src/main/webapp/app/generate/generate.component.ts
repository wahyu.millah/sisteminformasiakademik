import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IKelas } from 'app/shared/model/kelas.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { GenerateService } from './generate.service';
import { JenisPembayaranService } from 'app/entities/jenis-pembayaran/jenis-pembayaran.service';
import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-generate',
  templateUrl: './generate.component.html'
})
export class GenerateComponent implements OnInit, OnDestroy {
  kelas?: IKelas[];
  isSaving = false;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  jenispembayarans: IJenisPembayaran[] = [];
  searchOption!: string;

  editForm = this.fb.group({
    date: [null, [Validators.required]],
    sistem: [null, [Validators.required]],
    jenisBayar: [null, [Validators.required]],
    sistemBayar: [null],
    besarPerBulan: [null],
    tempo: [''],
    redaksi: ['']
  });

  constructor(
    protected generateService: GenerateService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected jenisPembayaranService: JenisPembayaranService,
    private fb: FormBuilder,
    private translate: TranslateService
  ) {
    this.jenisPembayaranService
      .query({ size: 500 })
      .subscribe((res: HttpResponse<IJenisPembayaran[]>) => (this.jenispembayarans = res.body || []));
  }

  loadPage(): void {}

  ngOnInit(): void {
    this.jenisPembayaranService
      .query({ size: 500 })
      .subscribe((res: HttpResponse<IJenisPembayaran[]>) => (this.jenispembayarans = res.body || []));
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  generate(): void {
    this.isSaving = true;
    const date = this.editForm.get(['date'])!.value;
    const tagihan = this.editForm.get(['jenisBayar'])!.value;
    const bulanan = this.editForm.get(['sistem'])!.value;
    const tempo = this.editForm.get(['tempo'])!.value;
    const redaksi = this.editForm.get(['redaksi'])!.value;
    this.subscribeToSaveResponse(this.generateService.generate(date, tagihan, bulanan, tempo, redaksi));
  }

  cleardate(): void {
    this.editForm.patchValue({
      tempo: ''
    });
  }

  clear(): void {
    this.editForm.patchValue({
      date: null,
      jenisBayar: null,
      sistem: null,
      sistemBayar: null,
      perbulan: null,
      tempo: '',
      redaksi: ''
    });
  }

  optionSearch(): void {
    this.searchOption = this.editForm.get(['sistemBayar'])!.value;
  }

  registerChangeInKelas(): void {
    this.eventSubscriber = this.eventManager.subscribe('generateListModification', () => this.loadPage());
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKelas>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast('employeeListModification');
    this.clear();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  save(): void {
    Swal.fire({
      icon: 'info',
      text: ' Simpan Generate Tagihan ?',
      showCancelButton: false,
      confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
      cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.generate();
      }
    });
  }
}
