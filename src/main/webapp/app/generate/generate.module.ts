import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { GenerateComponent } from './generate.component';
import { generateRoute } from './generate.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(generateRoute)],
  declarations: [GenerateComponent]
})
export class SisteminformasiakademikGenerateModule {}
