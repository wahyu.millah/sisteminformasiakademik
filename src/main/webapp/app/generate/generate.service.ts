import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IKelas } from 'app/shared/model/kelas.model';

type EntityResponseType = HttpResponse<IKelas>;

@Injectable({ providedIn: 'root' })
export class GenerateService {
  public resourceUrl = SERVER_API_URL + 'api/tagihans';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IKelas>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  generate(date: string, tagihan: string, bulanan: string, tempo: string, redaksi: string): Observable<any> {
    const params = new HttpParams()
      .set('date', date)
      .set('tagihan', tagihan)
      .set('bulanan', bulanan)
      .set('tempo', tempo)
      .set('redaksi', redaksi);
    return this.http.post(`${this.resourceUrl}/bulanan`, params, { observe: 'response' });
  }
}
