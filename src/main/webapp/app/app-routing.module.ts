import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { sidebarRoute } from './layouts';

const LAYOUT_ROUTES = [navbarRoute, sidebarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: ['ROLE_ADMIN', 'ROLE_TU']
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule)
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule)
        },
        {
          path: 'generate',
          loadChildren: () => import('./generate/generate.module').then(m => m.SisteminformasiakademikGenerateModule)
        },
        {
          path: 'channel',
          loadChildren: () => import('./channel/channel.module').then(m => m.SisteminformasiakademikChannelModule)
        },
        {
          path: 'laporan',
          loadChildren: () => import('./laporan/generate/laporan.module').then(m => m.SisteminformasiakademikLaporanModule)
        },
        {
          path: 'other',
          loadChildren: () => import('./laporan/other/other.module').then(m => m.SisteminformasiakademikOtherModule)
        },
        {
          path: 'bukukontrol',
          loadChildren: () => import('./laporan/bukukontrol/bukukontrol.module').then(m => m.SisteminformasiakademikBukuKontrolModule)
        },
        {
          path: 'transaksiharian',
          loadChildren: () =>
            import('./laporan/transaksiharian/transaksiharian.module').then(m => m.SisteminformasiakademikTransaksiHarianModule)
        },
        {
          path: 'transaksimingguan',
          loadChildren: () =>
            import('./laporan/transaksimingguan/transaksimingguan.module').then(m => m.SisteminformasiakademikTransaksiMingguanModule)
        },
        {
          path: 'transaksiharianitem',
          loadChildren: () =>
            import('./laporan/transaksiharianitem/transaksiharianitem.module').then(m => m.SisteminformasiakademikTransaksiHarianItemModule)
        },
        {
          path: 'riwayatpembayaran',
          loadChildren: () =>
            import('./laporan/riwayatpembayaran/riwayatpembayaran.module').then(m => m.SisteminformasiakademikRiwayatPembayaranModule)
        },
        ...LAYOUT_ROUTES
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    )
  ],
  exports: [RouterModule]
})
export class SisteminformasiakademikAppRoutingModule {}
