import { Injectable } from '@angular/core';
import { AccountService } from 'app/core';

export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  translate: string;
  icontype: string;
  collapse?: string;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  ab: string;
  type?: string;
  translate: string;
  title: string;
}

@Injectable({ providedIn: 'root' })
export class MenuService {
  constructor(private accountService: AccountService) {}

  getMenuItems(): RouteInfo[] {
    if (this.accountService.hasAnyAuthority(['ROLE_ADMIN'])) {
      return [
        { path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
        {
          path: '/',
          title: 'Entities',
          type: 'sub',
          translate: 'entities.main',
          icontype: 'list_alt',
          collapse: 'entities',
          children: [
            { path: 'tagihan', title: 'Tagihan', ab: 'TA', translate: 'entities.tagihan' },
            { path: 'transaksi', title: 'Transaksi', ab: 'TR', translate: 'entities.transaksi' },
            { path: 'siswa', title: 'Siswa', ab: 'SW', translate: 'entities.siswa' },
            { path: 'kelas', title: 'Kelas', ab: 'KL', translate: 'entities.kelas' },
            { path: 'besar-bayar', title: 'Besar Pembayaran', ab: 'BB', translate: 'entities.besarBayar' },
            { path: 'jenis-pembayaran', title: 'Jenis Pembayaran', ab: 'Jp', translate: 'entities.jenisPembayaran' }
          ]
        },
        {
          path: '/',
          title: 'Administration',
          type: 'sub',
          translate: 'entities.admin',
          icontype: 'list_alt',
          collapse: 'admin',
          children: [
            {
              path: 'admin/user-management',
              title: 'User Management',
              ab: 'UM',
              translate: 'admin.userManagement'
            },
            { path: 'admin/jhi-metrics', title: 'Metrics', ab: 'MT', translate: 'admin.metrics' },
            { path: 'admin/jhi-health', title: 'Health', ab: 'HE', translate: 'admin.health' },
            {
              path: 'admin/jhi-configuration',
              title: 'Configuration',
              ab: 'CO',
              translate: 'admin.configuration'
            },
            { path: 'admin/audits', title: 'Audits', ab: 'AU', translate: 'admin.audits' },
            { path: 'admin/logs', title: 'Logs', ab: 'LO', translate: 'admin.logs' },
            { path: 'admin/docs', title: 'API', ab: 'DO', translate: 'admin.apidocs' }
          ]
        },
        { path: '/hajat', title: 'Hajat', type: 'link', translate: 'home', icontype: 'dashboard' }
      ];
    } else if (this.accountService.hasAnyAuthority(['ROLE_TU'])) {
      return [
        { path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
        { path: 'tagihan', title: 'Tagihan', type: 'link', translate: 'entities.tagihan', icontype: 'payment' },
        { path: 'transaksi', title: 'Transaksi', type: 'link', translate: 'entities.transaksi', icontype: 'dashboard' },
        //        { path: 'generate', title: 'Generate', type: 'link', translate: 'entities.generate', icontype: 'content_copy' },
        {
          path: '/',
          title: 'Daftar',
          type: 'sub',
          translate: 'entities.setting',
          icontype: 'settings',
          collapse: 'entities',
          children: [
            //            {path: 'admin/user-management',title: 'User Management',ab: 'UM',translate: 'admin.userManagement'},
            { path: 'siswa', title: 'Siswa', ab: 'SW', translate: 'entities.siswa' },
            { path: 'kelas', title: 'Kelas', ab: 'KL', translate: 'entities.kelas' },
            { path: 'besar-bayar', title: 'Besar Pembayaran', ab: 'BB', translate: 'entities.besarBayar' },
            { path: 'jenis-pembayaran', title: 'Jenis Pembayaran', ab: 'Jp', translate: 'entities.jenisPembayaran' },
            { path: 'channel', title: 'Akun Bank', ab: 'AB', translate: 'entities.channel' },
            { path: 'generate', title: 'Generate', ab: 'GT', translate: 'entities.generate' }
          ]
        },
        {
          path: '/',
          title: 'Laporan',
          type: 'sub',
          translate: 'entities.laporan',
          icontype: 'list_alt',
          collapse: 'laporan',
          children: [
            //            { path: 'bukukontrol', title: 'Buku Kontrol', ab: 'BK', translate: 'entities.bukukontrol' },
            //            { path: 'other', title: 'Laporan Lainnya', ab: 'DL', translate: 'entities.download' },
            { path: 'laporan', title: 'Laporan Syahriah', ab: 'LS', translate: 'entities.lsyahriah' },
            { path: 'transaksiharian', title: 'Laporan Harian', ab: 'LH', translate: 'entities.lapharian' },
            { path: 'transaksimingguan', title: 'Laporan Mingguan', ab: 'LM', translate: 'entities.lapmingguan' },
            { path: 'transaksiharianitem', title: 'Laporan Item', ab: 'LI', translate: 'entities.lapitem' },
            { path: 'riwayatpembayaran', title: 'Riwayat Pembayaran', ab: 'RP', translate: 'entities.histori' }
          ]
        }
      ];
    } else if (this.accountService.hasAnyAuthority(['ROLE_WALI'])) {
      return [
        { path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
        { path: 'tagihan', title: 'Tagihan', type: 'link', translate: 'entities.tagihan', icontype: 'payment' }
      ];
    } else if (this.accountService.hasAnyAuthority(['ROLE_KETUA'])) {
      return [
        { path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
        { path: 'tagihan', title: 'Tagihan', type: 'link', translate: 'entities.tagihan', icontype: 'payment' },
        {
          path: '/',
          title: 'Laporan',
          type: 'sub',
          translate: 'entities.laporan',
          icontype: 'list_alt',
          collapse: 'laporan',
          children: [
            { path: 'laporan', title: 'Laporan Syahriah', ab: 'LS', translate: 'entities.lsyahriah' },
            { path: 'other', title: 'Laporan Lainnya', ab: 'DL', translate: 'entities.download' }
          ]
        }
      ];
    } else if (this.accountService.hasAnyAuthority(['ROLE_LOUNDRY'])) {
      return [
        { path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
        { path: 'tagihan', title: 'Tagihan', type: 'link', translate: 'entities.tagihan', icontype: 'payment' },
        { path: 'siswa', title: 'siswa', type: 'link', translate: 'entities.siswa', icontype: 'people_alt' }
      ];
    } else {
      return [];
    }
  }
}
