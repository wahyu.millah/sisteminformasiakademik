import { Route } from '@angular/router';

import { LoginComponent } from 'app/layouts/login/login.component';

export const loginRoute: Route = {
  path: '',
  component: LoginComponent,
  outlet: 'login'
};
