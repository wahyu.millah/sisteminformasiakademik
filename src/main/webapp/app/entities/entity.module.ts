import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'user-management',
        loadChildren: () => import('app/admin/user-management/user-management.module').then(m => m.UserManagementModule),
        data: {
          pageTitle: 'userManagement.home.title'
        }
      },
      {
        path: 'siswa',
        loadChildren: () => import('./siswa/siswa.module').then(m => m.SisteminformasiakademikSiswaModule)
      },
      {
        path: 'kelas',
        loadChildren: () => import('./kelas/kelas.module').then(m => m.SisteminformasiakademikKelasModule)
      },
      {
        path: 'jenis-pembayaran',
        loadChildren: () => import('./jenis-pembayaran/jenis-pembayaran.module').then(m => m.SisteminformasiakademikJenisPembayaranModule)
      },
      {
        path: 'besar-bayar',
        loadChildren: () => import('./besar-bayar/besar-bayar.module').then(m => m.SisteminformasiakademikBesarBayarModule)
      },
      {
        path: 'tagihan',
        loadChildren: () => import('./tagihan/tagihan.module').then(m => m.SisteminformasiakademikTagihanModule)
      },
      {
        path: 'transaksi',
        loadChildren: () => import('./transaksi/transaksi.module').then(m => m.SisteminformasiakademikTransaksiModule)
      },
      {
        path: 'hajat',
        loadChildren: () => import('./hajat/hajat.module').then(m => m.UndanganHajatModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class SisteminformasiakademikEntityModule {}
