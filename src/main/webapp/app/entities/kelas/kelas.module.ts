import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { KelasComponent } from './kelas.component';
import { KelasDetailComponent } from './kelas-detail.component';
import { KelasUpdateComponent } from './kelas-update.component';
import { KelasDeleteDialogComponent } from './kelas-delete-dialog.component';
import { kelasRoute } from './kelas.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(kelasRoute)],
  declarations: [KelasComponent, KelasDetailComponent, KelasUpdateComponent, KelasDeleteDialogComponent],
  entryComponents: [KelasDeleteDialogComponent]
})
export class SisteminformasiakademikKelasModule {}
