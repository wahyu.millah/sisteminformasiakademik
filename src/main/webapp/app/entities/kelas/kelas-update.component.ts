import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IKelas, Kelas } from 'app/shared/model/kelas.model';
import { KelasService } from './kelas.service';

@Component({
  selector: 'jhi-kelas-update',
  templateUrl: './kelas-update.component.html'
})
export class KelasUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nama: [null, [Validators.required]],
    jenjang: []
  });

  constructor(protected kelasService: KelasService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ kelas }) => {
      this.updateForm(kelas);
    });
  }

  updateForm(kelas: IKelas): void {
    this.editForm.patchValue({
      id: kelas.id,
      nama: kelas.nama,
      jenjang: kelas.jenjang
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const kelas = this.createFromForm();
    if (kelas.id !== undefined) {
      this.subscribeToSaveResponse(this.kelasService.update(kelas));
    } else {
      this.subscribeToSaveResponse(this.kelasService.create(kelas));
    }
  }

  private createFromForm(): IKelas {
    return {
      ...new Kelas(),
      id: this.editForm.get(['id'])!.value,
      nama: this.editForm.get(['nama'])!.value,
      jenjang: this.editForm.get(['jenjang'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKelas>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
