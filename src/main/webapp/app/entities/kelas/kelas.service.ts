import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IKelas } from 'app/shared/model/kelas.model';

type EntityResponseType = HttpResponse<IKelas>;
type EntityArrayResponseType = HttpResponse<IKelas[]>;

@Injectable({ providedIn: 'root' })
export class KelasService {
  public resourceUrl = SERVER_API_URL + 'api/kelas';

  constructor(protected http: HttpClient) {}

  create(kelas: IKelas): Observable<EntityResponseType> {
    return this.http.post<IKelas>(this.resourceUrl, kelas, { observe: 'response' });
  }

  update(kelas: IKelas): Observable<EntityResponseType> {
    return this.http.put<IKelas>(this.resourceUrl, kelas, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IKelas>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IKelas[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
