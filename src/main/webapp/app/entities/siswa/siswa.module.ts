import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { SiswaComponent } from './siswa.component';
import { SiswaDetailComponent } from './siswa-detail.component';
import { SiswaUpdateComponent } from './siswa-update.component';
import { SiswaDeleteDialogComponent } from './siswa-delete-dialog.component';
import { siswaRoute } from './siswa.route';
import { SiswaUpdateDialogComponent } from './siswa-update-dialog.component';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(siswaRoute)],
  declarations: [SiswaComponent, SiswaDetailComponent, SiswaUpdateComponent, SiswaDeleteDialogComponent, SiswaUpdateDialogComponent],
  entryComponents: [SiswaDeleteDialogComponent, SiswaUpdateDialogComponent]
})
export class SisteminformasiakademikSiswaModule {}
