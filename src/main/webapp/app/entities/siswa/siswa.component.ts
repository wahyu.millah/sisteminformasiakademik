import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ISiswa } from 'app/shared/model/siswa.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { SiswaService } from './siswa.service';
import { SiswaDeleteDialogComponent } from './siswa-delete-dialog.component';
import { FormBuilder } from '@angular/forms';
import { KelasService } from '../kelas/kelas.service';
import { IKelas } from 'app/shared/model/kelas.model';
import { MatDialog } from '@angular/material';
import { SiswaUpdateDialogComponent } from './siswa-update-dialog.component';
import { ISiswaDTO } from 'app/shared/model/siswaDTO.model';

@Component({
  selector: 'jhi-siswa',
  templateUrl: './siswa.component.html',
  styleUrls: ['siswa.scss']
})
export class SiswaComponent implements OnInit, OnDestroy {
  isSaving = false;
  siswas?: ISiswa[];
  kelas?: IKelas[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  skey!: string;
  svalue!: string;
  svalueKelas!: string;
  currentSearch!: string;
  reverse: any;

  editForm = this.fb.group({
    pNis: [],
    pSiswa: [],
    pOptions: 1,
    pKelas: []
  });

  constructor(
    protected kelasService: KelasService,
    protected siswaService: SiswaService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

    if (this.currentSearch) {
      console.log('data default');
      this.searchOption = 1;
      this.siswaService
        .query({
          page: this.page - 1,
          query: this.currentSearch,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<ISiswa[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
          () => this.onError()
        );

      this.kelasService.query().subscribe((res: HttpResponse<IKelas[]>) => {
        this.kelas = res.body ? res.body : [];
      });
    } else {
      this.siswaService
        .query({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<ISiswa[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
          () => this.onError()
        );
    }
  }

  search(query: string): void {
    if (!query) {
      return this.clear();
    }
    this.page = 0;
    this.currentSearch = query;
    this.router.navigate([
      '/siswa',
      {
        search: this.currentSearch,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  clear(): void {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/tagihan',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInSiswas();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ISiswa): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInSiswas(): void {
    this.eventSubscriber = this.eventManager.subscribe('siswaListModification', () => this.loadPage());
  }

  delete(siswa: ISiswa): void {
    const modalRef = this.modalService.open(SiswaDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.siswa = siswa;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  beasiswa(id: number, type: boolean): void {
    if (type === true) {
      console.log('updte1' + id);
      this.subscribeToSaveResponse(this.siswaService.beasiswa(id, 'true'));
    }
    if (type === false) {
      console.log('updte2' + id);
      this.subscribeToSaveResponse(this.siswaService.beasiswa(id, 'false'));
    }
  }

  loundry(id: number, type: boolean): void {
    if (type === true) {
      console.log('updte1' + id);
      this.subscribeToSaveResponse(this.siswaService.loundry(id, 'true'));
    }
    if (type === false) {
      console.log('updte2' + id);
      this.subscribeToSaveResponse(this.siswaService.loundry(id, 'false'));
    }
  }

  editSiswa(siswadto: ISiswaDTO): void {
    const modalRef = this.modalService.open(SiswaUpdateDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.updateForm(siswadto, 2);
  }

  create(): void {
    const modalRef = this.modalService.open(SiswaUpdateDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.new(1);
  }

  protected onSuccess(data: ISiswa[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/siswa'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.siswas = data ? data : [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISiswa>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.loadPage();
    this.eventManager.broadcast('siswaListModification');
    this.clear();
  }

  protected onSaveError(): void {
    this.isSaving = false;
    this.loadPage();
    this.clear();
  }
}
