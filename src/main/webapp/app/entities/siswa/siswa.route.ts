import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ISiswa, Siswa } from 'app/shared/model/siswa.model';
import { SiswaService } from './siswa.service';
import { SiswaComponent } from './siswa.component';
import { SiswaDetailComponent } from './siswa-detail.component';
import { SiswaUpdateComponent } from './siswa-update.component';

@Injectable({ providedIn: 'root' })
export class SiswaResolve implements Resolve<ISiswa> {
  constructor(private service: SiswaService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISiswa> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((siswa: HttpResponse<Siswa>) => {
          if (siswa.body) {
            return of(siswa.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Siswa());
  }
}

export const siswaRoute: Routes = [
  {
    path: '',
    component: SiswaComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sisteminformasiakademikApp.siswa.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SiswaDetailComponent,
    resolve: {
      siswa: SiswaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.siswa.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SiswaUpdateComponent,
    resolve: {
      siswa: SiswaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.siswa.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SiswaUpdateComponent,
    resolve: {
      siswa: SiswaResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.siswa.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
