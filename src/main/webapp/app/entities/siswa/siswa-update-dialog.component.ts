import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { FormBuilder, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { ISiswaDTO } from 'app/shared/model/siswaDTO.model';
import { Siswa } from 'app/shared/model/siswa.model';
import { SiswaService } from './siswa.service';
import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from '../kelas/kelas.service';
import { AccountService } from 'app/core';
import Swal from 'sweetalert2';

@Component({
  templateUrl: './siswa-update-dialog.component.html',
  styleUrls: ['siswa.scss']
})
export class SiswaUpdateDialogComponent implements OnInit {
  isSaving = false;
  private tabSet!: NgbTabset;
  set!: number;
  pages: string[] = ['1', '2', '3'];

  kelas: IKelas[] = [];

  @ViewChild(NgbTabset, { static: false })
  set content(content: NgbTabset) {
    this.tabSet = content;
  }

  editForm = this.fb.group({
    siswaId: [],
    nis: [null, [Validators.required]],
    nama: [],
    alamat: [null, [Validators.required]],
    jenisKelamin: [null, [Validators.required]],
    tempatLahir: [null, [Validators.required]],
    tanggalLahir: [null, [Validators.required]],
    waliMurid: [null, [Validators.required]],
    noTelephone: [null, [Validators.required]],
    tahunAjaran: [null, [Validators.required]],
    beasiswa: [null, [Validators.required]],
    loundry: [null, [Validators.required]],
    jenisBeasiswa: [],
    jmlBulan: [],
    kelas: [null, [Validators.required]],
    userId: [],
    firstName: [],
    lastName: [],
    login: [],
    password: []
  });

  constructor(
    protected siswaService: SiswaService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder,
    protected modalService: NgbModal,
    public dialog: MatDialog,
    private accountService: AccountService,
    private translate: TranslateService,
    protected kelasService: KelasService
  ) {}

  ngOnInit(): void {
    this.kelasService.query().subscribe((res: HttpResponse<IKelas[]>) => (this.kelas = res.body || []));
    this.isSaving = false;
  }

  updateForm(siswadto: ISiswaDTO, id: number): void {
    this.set = id;
    this.editForm.patchValue({
      userId: siswadto.userId,
      siswaId: siswadto.siswaId,
      nis: siswadto.nis,
      nama: siswadto.nama,
      alamat: siswadto.alamat,
      jenisKelamin: siswadto.jenisKelamin,
      tempatLahir: siswadto.tempatLahir,
      tanggalLahir: siswadto.tanggalLahir,
      waliMurid: siswadto.waliMurid,
      noTelephone: siswadto.noTelephone,
      tahunAjaran: siswadto.tahunAjaran,
      beasiswa: siswadto.beasiswa,
      loundry: siswadto.loundry,
      jenisBeasiswa: siswadto.jenisBeasiswa,
      jmlBulan: siswadto.jmlBulan,
      kelas: siswadto.kelas,
      firstName: siswadto.firstName,
      lastName: siswadto.lastName,
      login: siswadto.login,
      password: siswadto.password
    });
  }

  save(): void {
    this.isSaving = true;
    const siswa = this.createForm();
    console.log(siswa);
    if (siswa.siswaId !== null) {
      this._notifupdate(siswa);
    } else {
      this._notifsave(siswa);
    }
  }

  next(tabSet: any, id: number): void {
    const page = this.pages;
    setTimeout(function(): void {
      tabSet.select(page[id]);
    });
  }

  new(id: number): void {
    this.set = id;
  }

  private createForm(): ISiswaDTO {
    return {
      ...new Siswa(),
      siswaId: this.editForm.get(['siswaId'])!.value,
      nis: this.editForm.get(['nis'])!.value,
      nama: this.editForm.get(['nama'])!.value,
      alamat: this.editForm.get(['alamat'])!.value,
      jenisKelamin: this.editForm.get(['jenisKelamin'])!.value,
      tempatLahir: this.editForm.get(['tempatLahir'])!.value,
      tanggalLahir: this.editForm.get(['tanggalLahir'])!.value,
      waliMurid: this.editForm.get(['waliMurid'])!.value,
      noTelephone: this.editForm.get(['noTelephone'])!.value,
      tahunAjaran: this.editForm.get(['tahunAjaran'])!.value,
      beasiswa: this.editForm.get(['beasiswa'])!.value,
      loundry: this.editForm.get(['loundry'])!.value,
      jenisBeasiswa: this.editForm.get(['jenisBeasiswa'])!.value,
      jmlBulan: this.editForm.get(['jmlBulan'])!.value,
      kelas: this.editForm.get(['kelas'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      login: this.editForm.get(['login'])!.value,
      password: this.editForm.get(['password'])!.value
    };
  }

  _notifsave(siswa: any): void {
    Swal.fire({
      icon: 'info',
      text: ' Simpan data siswa baru ?',
      showCancelButton: false,
      confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
      cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.subscribeToSaveResponse(this.siswaService.create(siswa));
      } else {
        this.isSaving = false;
      }
    });
  }

  _notifupdate(siswa: any): void {
    Swal.fire({
      icon: 'info',
      text: 'Simpan perubahan data siswa ?',
      showCancelButton: false,
      confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
      cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.subscribeToSaveResponse(this.siswaService.update(siswa));
      } else {
        this.isSaving = false;
      }
    });
  }

  clear(): void {
    this.activeModal.dismiss();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISiswaDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast('siswaListModification');
    this.clear();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ISiswaDTO): any {
    return item.siswaId;
  }

  protected onDeleteSuccess(): void {
    this.eventManager.broadcast('tagihanListModification');
  }

  protected onDeleteError(response: any): void {
    console.log(response);
  }

  getImageUrl(): any {
    return this.accountService.getImageUrl();
  }
}
