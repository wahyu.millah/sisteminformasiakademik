import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISiswaDTO } from 'app/shared/model/siswaDTO.model';
import { SiswaService } from './siswa.service';

@Component({
  templateUrl: './siswa-delete-dialog.component.html'
})
export class SiswaDeleteDialogComponent {
  siswa?: ISiswaDTO;

  constructor(protected siswaService: SiswaService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    console.log(id);
    this.siswaService.delete(id).subscribe(() => {
      this.eventManager.broadcast('siswaListModification');
      this.activeModal.close();
    });
  }
}
