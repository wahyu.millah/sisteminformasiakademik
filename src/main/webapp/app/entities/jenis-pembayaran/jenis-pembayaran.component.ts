import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { JenisPembayaranService } from './jenis-pembayaran.service';
import { JenisPembayaranDeleteDialogComponent } from './jenis-pembayaran-delete-dialog.component';

@Component({
  selector: 'jhi-jenis-pembayaran',
  templateUrl: './jenis-pembayaran.component.html'
})
export class JenisPembayaranComponent implements OnInit, OnDestroy {
  jenisPembayarans: IJenisPembayaran[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected jenisPembayaranService: JenisPembayaranService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.jenisPembayarans = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.jenisPembayaranService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IJenisPembayaran[]>) => this.paginateJenisPembayarans(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.jenisPembayarans = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJenisPembayarans();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJenisPembayaran): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJenisPembayarans(): void {
    this.eventSubscriber = this.eventManager.subscribe('jenisPembayaranListModification', () => this.reset());
  }

  delete(jenisPembayaran: IJenisPembayaran): void {
    const modalRef = this.modalService.open(JenisPembayaranDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.jenisPembayaran = jenisPembayaran;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateJenisPembayarans(data: IJenisPembayaran[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.jenisPembayarans.push(data[i]);
      }
    }
  }
}
