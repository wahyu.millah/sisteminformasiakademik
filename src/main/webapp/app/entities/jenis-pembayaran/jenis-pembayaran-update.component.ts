import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IJenisPembayaran, JenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { JenisPembayaranService } from './jenis-pembayaran.service';

@Component({
  selector: 'jhi-jenis-pembayaran-update',
  templateUrl: './jenis-pembayaran-update.component.html'
})
export class JenisPembayaranUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    jenis: [],
    metode: [],
    keterangan: []
  });

  constructor(
    protected jenisPembayaranService: JenisPembayaranService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jenisPembayaran }) => {
      this.updateForm(jenisPembayaran);
    });
  }

  updateForm(jenisPembayaran: IJenisPembayaran): void {
    this.editForm.patchValue({
      id: jenisPembayaran.id,
      jenis: jenisPembayaran.jenis,
      metode: jenisPembayaran.metode,
      keterangan: jenisPembayaran.keterangan
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const jenisPembayaran = this.createFromForm();
    if (jenisPembayaran.id !== undefined) {
      this.subscribeToSaveResponse(this.jenisPembayaranService.update(jenisPembayaran));
    } else {
      this.subscribeToSaveResponse(this.jenisPembayaranService.create(jenisPembayaran));
    }
  }

  private createFromForm(): IJenisPembayaran {
    return {
      ...new JenisPembayaran(),
      id: this.editForm.get(['id'])!.value,
      jenis: this.editForm.get(['jenis'])!.value,
      metode: this.editForm.get(['metode'])!.value,
      keterangan: this.editForm.get(['keterangan'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJenisPembayaran>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
