import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';

@Component({
  selector: 'jhi-jenis-pembayaran-detail',
  templateUrl: './jenis-pembayaran-detail.component.html'
})
export class JenisPembayaranDetailComponent implements OnInit {
  jenisPembayaran: IJenisPembayaran | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ jenisPembayaran }) => (this.jenisPembayaran = jenisPembayaran));
  }

  previousState(): void {
    window.history.back();
  }
}
