import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';

type EntityResponseType = HttpResponse<IJenisPembayaran>;
type EntityArrayResponseType = HttpResponse<IJenisPembayaran[]>;

@Injectable({ providedIn: 'root' })
export class JenisPembayaranService {
  public resourceUrl = SERVER_API_URL + 'api/jenis-pembayarans';

  constructor(protected http: HttpClient) {}

  create(jenisPembayaran: IJenisPembayaran): Observable<EntityResponseType> {
    return this.http.post<IJenisPembayaran>(this.resourceUrl, jenisPembayaran, { observe: 'response' });
  }

  update(jenisPembayaran: IJenisPembayaran): Observable<EntityResponseType> {
    return this.http.put<IJenisPembayaran>(this.resourceUrl, jenisPembayaran, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IJenisPembayaran>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IJenisPembayaran[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
