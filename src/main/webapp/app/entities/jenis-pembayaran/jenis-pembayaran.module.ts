import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { JenisPembayaranComponent } from './jenis-pembayaran.component';
import { JenisPembayaranDetailComponent } from './jenis-pembayaran-detail.component';
import { JenisPembayaranUpdateComponent } from './jenis-pembayaran-update.component';
import { JenisPembayaranDeleteDialogComponent } from './jenis-pembayaran-delete-dialog.component';
import { jenisPembayaranRoute } from './jenis-pembayaran.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(jenisPembayaranRoute)],
  declarations: [
    JenisPembayaranComponent,
    JenisPembayaranDetailComponent,
    JenisPembayaranUpdateComponent,
    JenisPembayaranDeleteDialogComponent
  ],
  entryComponents: [JenisPembayaranDeleteDialogComponent]
})
export class SisteminformasiakademikJenisPembayaranModule {}
