import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBesarBayar } from 'app/shared/model/besar-bayar.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { BesarBayarService } from './besar-bayar.service';
import { BesarBayarDeleteDialogComponent } from './besar-bayar-delete-dialog.component';
import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from '../kelas/kelas.service';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'jhi-besar-bayar',
  templateUrl: './besar-bayar.component.html'
})
export class BesarBayarComponent implements OnInit, OnDestroy {
  besarBayars?: IBesarBayar[];
  kelas?: IKelas[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  currentSearch!: string;
  reverse: any;
  totalItems = 0;
  routeData: any;
  previousPage: number | undefined;

  editForm = this.fb.group({
    kelas: []
  });

  constructor(
    protected besarBayarService: BesarBayarService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks,
    private fb: FormBuilder,
    protected router: Router,
    protected kelasService: KelasService,
    protected activatedRoute: ActivatedRoute
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  reset(): void {
    this.page = 0;
    this.besarBayars = [];
  }

  loadPage(page?: number): void {
    console.log('page:' + page);
    const pageToLoad: number = page ? page : this.page;
    if (this.currentSearch) {
      console.log('data default');
      this.besarBayarService
        .query({
          page: this.page - 1,
          query: this.currentSearch,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IBesarBayar[]>) => this.paginateBesarBayars(res.body, res.headers, pageToLoad));
    } else {
      console.log('not default');
      this.besarBayarService
        .query({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IBesarBayar[]>) => this.paginateBesarBayars(res.body, res.headers, pageToLoad));
    }
  }

  ngOnInit(): void {
    this.loadPage();
    this.registerChangeInBesarBayars();
    this.kelasService.query().subscribe((res: HttpResponse<IKelas[]>) => {
      this.kelas = res.body ? res.body : [];
    });
  }

  search(query: string): void {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.router.navigate([
      '/besar-bayar',
      {
        search: this.currentSearch,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  clear(): void {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/besar-bayar',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBesarBayar): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBesarBayars(): void {
    this.eventSubscriber = this.eventManager.subscribe('besarBayarListModification', () => this.loadPage());
  }

  delete(besarBayar: IBesarBayar): void {
    const modalRef = this.modalService.open(BesarBayarDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.besarBayar = besarBayar;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateBesarBayars(data: IBesarBayar[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/besar-bayar'], {
      queryParams: {
        page: this.page,
        query: this.currentSearch,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.besarBayars = data ? data : [];
  }
}
