import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBesarBayar, BesarBayar } from 'app/shared/model/besar-bayar.model';
import { BesarBayarService } from './besar-bayar.service';
import { BesarBayarComponent } from './besar-bayar.component';
import { BesarBayarDetailComponent } from './besar-bayar-detail.component';
import { BesarBayarUpdateComponent } from './besar-bayar-update.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

@Injectable({ providedIn: 'root' })
export class BesarBayarResolve implements Resolve<IBesarBayar> {
  constructor(private service: BesarBayarService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBesarBayar> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((besarBayar: HttpResponse<BesarBayar>) => {
          if (besarBayar.body) {
            return of(besarBayar.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BesarBayar());
  }
}

export const besarBayarRoute: Routes = [
  {
    path: '',
    component: BesarBayarComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.besarBayar.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BesarBayarDetailComponent,
    resolve: {
      besarBayar: BesarBayarResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.besarBayar.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BesarBayarUpdateComponent,
    resolve: {
      besarBayar: BesarBayarResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.besarBayar.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BesarBayarUpdateComponent,
    resolve: {
      besarBayar: BesarBayarResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.besarBayar.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
