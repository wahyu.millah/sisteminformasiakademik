import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { BesarBayarComponent } from './besar-bayar.component';
import { BesarBayarDetailComponent } from './besar-bayar-detail.component';
import { BesarBayarUpdateComponent } from './besar-bayar-update.component';
import { BesarBayarDeleteDialogComponent } from './besar-bayar-delete-dialog.component';
import { besarBayarRoute } from './besar-bayar.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(besarBayarRoute)],
  declarations: [BesarBayarComponent, BesarBayarDetailComponent, BesarBayarUpdateComponent, BesarBayarDeleteDialogComponent],
  entryComponents: [BesarBayarDeleteDialogComponent]
})
export class SisteminformasiakademikBesarBayarModule {}
