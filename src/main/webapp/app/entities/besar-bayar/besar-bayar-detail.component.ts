import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBesarBayar } from 'app/shared/model/besar-bayar.model';

@Component({
  selector: 'jhi-besar-bayar-detail',
  templateUrl: './besar-bayar-detail.component.html'
})
export class BesarBayarDetailComponent implements OnInit {
  besarBayar: IBesarBayar | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ besarBayar }) => (this.besarBayar = besarBayar));
  }

  previousState(): void {
    window.history.back();
  }
}
