import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBesarBayar, BesarBayar } from 'app/shared/model/besar-bayar.model';
import { BesarBayarService } from './besar-bayar.service';
import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from 'app/entities/kelas/kelas.service';
import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { JenisPembayaranService } from 'app/entities/jenis-pembayaran/jenis-pembayaran.service';

type SelectableEntity = IKelas | IJenisPembayaran;

@Component({
  selector: 'jhi-besar-bayar-update',
  templateUrl: './besar-bayar-update.component.html',
  styleUrls: ['besarbayar.scss']
})
export class BesarBayarUpdateComponent implements OnInit {
  isSaving = false;
  kelas: IKelas[] = [];
  jenispembayarans: IJenisPembayaran[] = [];

  editForm = this.fb.group({
    id: [],
    nominal: [],
    kelas: [],
    jenisBayar: []
  });

  constructor(
    protected besarBayarService: BesarBayarService,
    protected kelasService: KelasService,
    protected jenisPembayaranService: JenisPembayaranService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ besarBayar }) => {
      this.updateForm(besarBayar);

      this.kelasService.query().subscribe((res: HttpResponse<IKelas[]>) => (this.kelas = res.body || []));

      this.jenisPembayaranService
        .query({
          size: 100
        })
        .subscribe((res: HttpResponse<IJenisPembayaran[]>) => (this.jenispembayarans = res.body || []));
    });
  }

  updateForm(besarBayar: IBesarBayar): void {
    this.editForm.patchValue({
      id: besarBayar.id,
      nominal: besarBayar.nominal,
      kelas: besarBayar.kelas,
      jenisBayar: besarBayar.jenisBayar
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const besarBayar = this.createFromForm();
    if (besarBayar.id !== undefined) {
      this.subscribeToSaveResponse(this.besarBayarService.update(besarBayar));
    } else {
      this.subscribeToSaveResponse(this.besarBayarService.create(besarBayar));
    }
  }

  private createFromForm(): IBesarBayar {
    return {
      ...new BesarBayar(),
      id: this.editForm.get(['id'])!.value,
      nominal: this.editForm.get(['nominal'])!.value,
      kelas: this.editForm.get(['kelas'])!.value,
      jenisBayar: this.editForm.get(['jenisBayar'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBesarBayar>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
