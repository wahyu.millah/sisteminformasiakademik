import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IHajat } from 'app/shared/model/hajat.model';

type EntityResponseType = HttpResponse<IHajat>;
type EntityArrayResponseType = HttpResponse<IHajat[]>;

@Injectable({ providedIn: 'root' })
export class HajatService {
  public resourceUrl = SERVER_API_URL + 'api/hajats';

  constructor(protected http: HttpClient) {}

  create(hajat: IHajat): Observable<EntityResponseType> {
    return this.http.post<IHajat>(this.resourceUrl, hajat, { observe: 'response' });
  }

  update(hajat: IHajat): Observable<EntityResponseType> {
    return this.http.put<IHajat>(this.resourceUrl, hajat, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IHajat>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IHajat[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
