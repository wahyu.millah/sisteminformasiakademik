import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IHajat, Hajat } from 'app/shared/model/hajat.model';
import { HajatService } from './hajat.service';
import { HajatComponent } from './hajat.component';
import { HajatDetailComponent } from './hajat-detail.component';
import { HajatUpdateComponent } from './hajat-update.component';

@Injectable({ providedIn: 'root' })
export class HajatResolve implements Resolve<IHajat> {
  constructor(private service: HajatService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHajat> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((hajat: HttpResponse<Hajat>) => {
          if (hajat.body) {
            return of(hajat.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Hajat());
  }
}

export const hajatRoute: Routes = [
  {
    path: '',
    component: HajatComponent,
    data: {
      authorities: [],
      pageTitle: 'Hajats'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view/:nama',
    component: HajatDetailComponent,
    resolve: {
      hajat: HajatResolve
    },
    data: {
      authorities: [],
      pageTitle: 'Hajats'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HajatUpdateComponent,
    resolve: {
      hajat: HajatResolve
    },
    data: {
      authorities: [],
      pageTitle: 'Hajats'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HajatUpdateComponent,
    resolve: {
      hajat: HajatResolve
    },
    data: {
      authorities: [],
      pageTitle: 'Hajats'
    },
    canActivate: [UserRouteAccessService]
  }
];
