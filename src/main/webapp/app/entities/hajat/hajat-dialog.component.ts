import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHajat } from 'app/shared/model/hajat.model';
import { HajatService } from './hajat.service';

@Component({
  templateUrl: './hajat-dialog.component.html',
  styleUrls: ['hajat.scss']
})
export class HajatDialogComponent {
  hajat?: IHajat;
  nama?: String;

  constructor(protected hajatService: HajatService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.hajatService.delete(id).subscribe(() => {
      this.eventManager.broadcast('hajatListModification');
      this.activeModal.close();
    });
  }
}
