import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHajat } from 'app/shared/model/hajat.model';
import { HajatDialogComponent } from './hajat-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { trigger, transition, animate, style, state } from '@angular/animations';

@Component({
  selector: 'jhi-hajat-detail',
  templateUrl: './hajat-detail.component.html',
  styleUrls: ['hajat.scss'],
  animations: [trigger('fade', [state('void', style({ opacity: 0 })), transition('void <=> *', [animate('4s ease-in-out')])])]
})
export class HajatDetailComponent implements OnInit {
  hajat!: IHajat;
  nama: String | any;
  page: String | any;

  counter = 0;
  slideItems = [
    { src: '/content/images/jhipster_family_member_0_head-192.png', title: 'Title 1' },
    { src: '/content/images/jhipster_family_member_1_head-192.png', title: 'Title 2' },
    { src: '/content/images/jhipster_family_member_2_head-384.png', title: 'Title 3' }
  ];
  imagesBasic = [
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(117).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(117).jpg',
      description: 'Image 1'
    },
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(98).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(98).jpg',
      description: 'Image 2'
    },
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(131).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(131).jpg',
      description: 'Image 3'
    },
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(123).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(123).jpg',
      description: 'Image 4'
    },
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(118).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(118).jpg',
      description: 'Image 5'
    },
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(128).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(128).jpg',
      description: 'Image 6'
    },
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(132).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(132).jpg',
      description: 'Image 7'
    },
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(115).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(115).jpg',
      description: 'Image 8'
    },
    {
      img: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(133).jpg',
      thumb: 'https://mdbootstrap.com/img/Photos/Horizontal/Nature/12-col/img%20(133).jpg',
      description: 'Image 9'
    }
  ];

  constructor(protected activatedRoute: ActivatedRoute, protected modalService: NgbModal) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ hajat }) => {
      this.hajat = hajat;
    });
    this.activatedRoute.params.subscribe(params => {
      this.nama = params['nama'];
    });

    this.show(this.hajat, this.nama);
    this.page = 'Home';
    this.showSlides();
  }

  previousState(): void {
    window.history.back();
  }

  openPage(page: String): void {
    this.page = page;
  }

  show(hajat: IHajat, nama: String): void {
    const modalRef = this.modalService.open(HajatDialogComponent, { size: 'lg', backdrop: 'static', centered: true });
    modalRef.componentInstance.hajat = hajat;
    modalRef.componentInstance.nama = nama;
  }

  showSlides(): void {
    let i = 0;
    // eslint-disable-next-line no-console

    for (i = 0; i < this.slideItems.length; i++) {
      setTimeout(() => {
        this.counter += 1;
        if (this.counter === this.slideItems.length) {
          this.counter = 0;
          i = 0;
          this.showSlides();
        }
      }, 5000 * (i + 1));
    }
  }
}
