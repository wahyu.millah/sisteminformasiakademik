import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IHajat } from 'app/shared/model/hajat.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HajatService } from './hajat.service';
import { HajatDeleteDialogComponent } from './hajat-delete-dialog.component';

@Component({
  selector: 'jhi-hajat',
  templateUrl: './hajat.component.html'
})
export class HajatComponent implements OnInit, OnDestroy {
  hajats: IHajat[];
  eventSubscriber?: Subscription;
  itemsPerPage: number;
  links: any;
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(
    protected hajatService: HajatService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected parseLinks: JhiParseLinks
  ) {
    this.hajats = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.hajatService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IHajat[]>) => this.paginateHajats(res.body, res.headers));
  }

  reset(): void {
    this.page = 0;
    this.hajats = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInHajats();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IHajat): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInHajats(): void {
    this.eventSubscriber = this.eventManager.subscribe('hajatListModification', () => this.reset());
  }

  delete(hajat: IHajat): void {
    const modalRef = this.modalService.open(HajatDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.hajat = hajat;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateHajats(data: IHajat[] | null, headers: HttpHeaders): void {
    const headersLink = headers.get('link');
    this.links = this.parseLinks.parse(headersLink ? headersLink : '');
    if (data) {
      for (let i = 0; i < data.length; i++) {
        this.hajats.push(data[i]);
      }
    }
  }
}
