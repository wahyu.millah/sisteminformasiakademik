import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IHajat, Hajat } from 'app/shared/model/hajat.model';
import { HajatService } from './hajat.service';

@Component({
  selector: 'jhi-hajat-update',
  templateUrl: './hajat-update.component.html'
})
export class HajatUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    pengantinPria: [],
    imagePria: [],
    imageWanita: [],
    pengantinWanita: [],
    waliPria: [],
    waliWanita: [],
    tglAkad: [],
    hariAkad: [],
    jamAkad: [],
    lokasiAkad: [],
    lokasiAkadMap: [],
    tglResepsi: [],
    hariResepsi: [],
    jamMulaiResepsi: [],
    jamSelesai: [],
    lokasiResepsi: [],
    lokasiResepsiMap: [],
    catatan: [],
    image1: [],
    image2: [],
    image3: [],
    image4: [],
    image5: [],
    image6: [],
    image7: [],
    image8: [],
    image9: [],
    image10: [],
    history1: [],
    history2: [],
    history3: [],
    hstory4: []
  });

  constructor(protected hajatService: HajatService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ hajat }) => {
      this.updateForm(hajat);
    });
  }

  updateForm(hajat: IHajat): void {
    this.editForm.patchValue({
      id: hajat.id,
      pengantinPria: hajat.pengantinPria,
      imagePria: hajat.imagePria,
      imageWanita: hajat.imageWanita,
      pengantinWanita: hajat.pengantinWanita,
      waliPria: hajat.waliPria,
      waliWanita: hajat.waliWanita,
      tglAkad: hajat.tglAkad,
      hariAkad: hajat.hariAkad,
      jamAkad: hajat.jamAkad,
      lokasiAkad: hajat.lokasiAkad,
      lokasiAkadMap: hajat.lokasiAkadMap,
      tglResepsi: hajat.tglResepsi,
      hariResepsi: hajat.hariResepsi,
      jamMulaiResepsi: hajat.jamMulaiResepsi,
      jamSelesai: hajat.jamSelesai,
      lokasiResepsi: hajat.lokasiResepsi,
      lokasiResepsiMap: hajat.lokasiResepsiMap,
      catatan: hajat.catatan,
      image1: hajat.image1,
      image2: hajat.image2,
      image3: hajat.image3,
      image4: hajat.image4,
      image5: hajat.image5,
      image6: hajat.image6,
      image7: hajat.image7,
      image8: hajat.image8,
      image9: hajat.image9,
      image10: hajat.image10,
      history1: hajat.history1,
      history2: hajat.history2,
      history3: hajat.history3,
      hstory4: hajat.hstory4
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const hajat = this.createFromForm();
    if (hajat.id !== undefined) {
      this.subscribeToSaveResponse(this.hajatService.update(hajat));
    } else {
      this.subscribeToSaveResponse(this.hajatService.create(hajat));
    }
  }

  private createFromForm(): IHajat {
    return {
      ...new Hajat(),
      id: this.editForm.get(['id'])!.value,
      pengantinPria: this.editForm.get(['pengantinPria'])!.value,
      imagePria: this.editForm.get(['imagePria'])!.value,
      imageWanita: this.editForm.get(['imageWanita'])!.value,
      pengantinWanita: this.editForm.get(['pengantinWanita'])!.value,
      waliPria: this.editForm.get(['waliPria'])!.value,
      waliWanita: this.editForm.get(['waliWanita'])!.value,
      tglAkad: this.editForm.get(['tglAkad'])!.value,
      hariAkad: this.editForm.get(['hariAkad'])!.value,
      jamAkad: this.editForm.get(['jamAkad'])!.value,
      lokasiAkad: this.editForm.get(['lokasiAkad'])!.value,
      lokasiAkadMap: this.editForm.get(['lokasiAkadMap'])!.value,
      tglResepsi: this.editForm.get(['tglResepsi'])!.value,
      hariResepsi: this.editForm.get(['hariResepsi'])!.value,
      jamMulaiResepsi: this.editForm.get(['jamMulaiResepsi'])!.value,
      jamSelesai: this.editForm.get(['jamSelesai'])!.value,
      lokasiResepsi: this.editForm.get(['lokasiResepsi'])!.value,
      lokasiResepsiMap: this.editForm.get(['lokasiResepsiMap'])!.value,
      catatan: this.editForm.get(['catatan'])!.value,
      image1: this.editForm.get(['image1'])!.value,
      image2: this.editForm.get(['image2'])!.value,
      image3: this.editForm.get(['image3'])!.value,
      image4: this.editForm.get(['image4'])!.value,
      image5: this.editForm.get(['image5'])!.value,
      image6: this.editForm.get(['image6'])!.value,
      image7: this.editForm.get(['image7'])!.value,
      image8: this.editForm.get(['image8'])!.value,
      image9: this.editForm.get(['image9'])!.value,
      image10: this.editForm.get(['image10'])!.value,
      history1: this.editForm.get(['history1'])!.value,
      history2: this.editForm.get(['history2'])!.value,
      history3: this.editForm.get(['history3'])!.value,
      hstory4: this.editForm.get(['hstory4'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHajat>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
