import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { HajatComponent } from './hajat.component';
import { HajatDetailComponent } from './hajat-detail.component';
import { HajatUpdateComponent } from './hajat-update.component';
import { HajatDeleteDialogComponent } from './hajat-delete-dialog.component';
import { hajatRoute } from './hajat.route';
import { HajatDialogComponent } from './hajat-dialog.component';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(hajatRoute)],
  declarations: [HajatComponent, HajatDetailComponent, HajatUpdateComponent, HajatDeleteDialogComponent, HajatDialogComponent],
  entryComponents: [HajatDeleteDialogComponent, HajatDialogComponent]
})
export class UndanganHajatModule {}
