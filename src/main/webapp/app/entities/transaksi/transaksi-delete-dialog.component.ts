import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITransaksi } from 'app/shared/model/transaksi.model';
import { TransaksiService } from './transaksi.service';

@Component({
  templateUrl: './transaksi-delete-dialog.component.html'
})
export class TransaksiDeleteDialogComponent {
  transaksi?: ITransaksi;

  constructor(protected transaksiService: TransaksiService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.transaksiService.delete(id).subscribe(() => {
      this.eventManager.broadcast('transaksiListModification');
      this.activeModal.close();
    });
  }
}
