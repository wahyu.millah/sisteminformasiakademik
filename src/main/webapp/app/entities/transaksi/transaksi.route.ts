import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ITransaksi, Transaksi } from 'app/shared/model/transaksi.model';
import { TransaksiService } from './transaksi.service';
import { TransaksiComponent } from './transaksi.component';
import { TransaksiDetailComponent } from './transaksi-detail.component';
import { TransaksiUpdateComponent } from './transaksi-update.component';

@Injectable({ providedIn: 'root' })
export class TransaksiResolve implements Resolve<ITransaksi> {
  constructor(private service: TransaksiService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITransaksi> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((transaksi: HttpResponse<Transaksi>) => {
          if (transaksi.body) {
            return of(transaksi.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Transaksi());
  }
}

export const transaksiRoute: Routes = [
  {
    path: '',
    component: TransaksiComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sisteminformasiakademikApp.transaksi.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TransaksiDetailComponent,
    resolve: {
      transaksi: TransaksiResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.transaksi.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TransaksiUpdateComponent,
    resolve: {
      transaksi: TransaksiResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.transaksi.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TransaksiUpdateComponent,
    resolve: {
      transaksi: TransaksiResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.transaksi.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
