import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITransaksi } from 'app/shared/model/transaksi.model';

@Component({
  selector: 'jhi-transaksi-detail',
  templateUrl: './transaksi-detail.component.html'
})
export class TransaksiDetailComponent implements OnInit {
  transaksi: ITransaksi | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ transaksi }) => (this.transaksi = transaksi));
  }

  previousState(): void {
    window.history.back();
  }
}
