import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITransaksi } from 'app/shared/model/transaksi.model';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';

type EntityResponseType = HttpResponse<ITransaksi>;
type EntityArrayResponseType = HttpResponse<ITransaksi[]>;
type EntityResponseTypeDTO = HttpResponse<ITagihanDTO>;
type EntityArrayResponseTypeDTO = HttpResponse<ITagihanDTO[]>;

@Injectable({ providedIn: 'root' })
export class TransaksiService {
  public resourceUrl = SERVER_API_URL + 'api/transaksis';
  public resourceUrladd = SERVER_API_URL + 'api/transaksis/add';
  public resourceUrladdMulti = SERVER_API_URL + 'api/transaksis/addMulti';
  public resourceUrlmulti = SERVER_API_URL + 'api/transaksis/multi';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITransaksi>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findByid(va: any): Observable<EntityResponseTypeDTO> {
    return this.http
      .get<ITagihanDTO>(`${this.resourceUrladd}/${va}`, { observe: 'response' })
      .pipe(map((res: EntityResponseTypeDTO) => this.convertDateFromServerDTO(res)));
  }

  findMulti(va: any): Observable<EntityResponseTypeDTO> {
    return this.http
      .get<ITagihanDTO>(`${this.resourceUrladdMulti}/${va}`, { observe: 'response' })
      .pipe(map((res: EntityResponseTypeDTO) => this.convertDateFromServerDTO(res)));
  }

  query(req?: any): Observable<EntityArrayResponseTypeDTO> {
    const options = createRequestOption(req);
    return this.http
      .get<ITagihanDTO[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseTypeDTO) => this.convertDateArrayFromServerDTO(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  createTransaksi(transaksi: ITagihanDTO): Observable<EntityResponseTypeDTO> {
    return this.http
      .post<ITagihanDTO>(this.resourceUrladd, transaksi, { observe: 'response' })
      .pipe(map((res: EntityResponseTypeDTO) => this.convertDateFromServerDTO(res)));
  }

  createTransaksiTransfer(transaksi: ITagihanDTO, va: string): Observable<EntityResponseTypeDTO> {
    return this.http
      .post<ITagihanDTO>(`${this.resourceUrladd}/${va}`, transaksi, { observe: 'response' })
      .pipe(map((res: EntityResponseTypeDTO) => this.convertDateFromServerDTO(res)));
  }

  multi(idTagihan: number[], va: string): Observable<EntityResponseTypeDTO> {
    return this.http
      .post<ITagihanDTO>(`${this.resourceUrlmulti}/${va}`, idTagihan, { observe: 'response' })
      .pipe(map((res: EntityResponseTypeDTO) => this.convertDateFromServerDTO(res)));
  }

  create(transaksi: ITransaksi): Observable<EntityResponseType> {
    return this.http
      .post<ITransaksi>(this.resourceUrladd, transaksi, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(transaksi: ITransaksi): Observable<EntityResponseType> {
    return this.http
      .post<ITransaksi>(`${this.resourceUrladd}/`, transaksi, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  protected convertDateFromClient(transaksi: ITransaksi): ITransaksi {
    const copy: ITransaksi = Object.assign({}, transaksi, {
      tanggal: transaksi.tanggal && transaksi.tanggal.isValid() ? transaksi.tanggal.toJSON() : undefined,
      updateOn: transaksi.updateOn && transaksi.updateOn.isValid() ? transaksi.updateOn.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.tanggal = res.body.tanggal ? moment(res.body.tanggal) : undefined;
      res.body.updateOn = res.body.updateOn ? moment(res.body.updateOn) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((transaksi: ITransaksi) => {
        transaksi.tanggal = transaksi.tanggal ? moment(transaksi.tanggal) : undefined;
        transaksi.updateOn = transaksi.updateOn ? moment(transaksi.updateOn) : undefined;
      });
    }
    return res;
  }

  protected convertDateArrayFromServerDTO(res: EntityArrayResponseTypeDTO): EntityArrayResponseTypeDTO {
    if (res.body) {
      res.body.forEach((transaksi: ITagihanDTO) => {
        transaksi.tanggal = transaksi.tanggal ? moment(transaksi.tanggal) : undefined;
        transaksi.updateOn = transaksi.updateOn ? moment(transaksi.updateOn) : undefined;
      });
    }
    return res;
  }

  protected convertDateFromServerDTO(res: EntityResponseTypeDTO): EntityResponseTypeDTO {
    if (res.body) {
      res.body.tanggal = res.body.tanggal ? moment(res.body.tanggal) : undefined;
      res.body.updateOn = res.body.updateOn ? moment(res.body.updateOn) : undefined;
    }
    return res;
  }
}
