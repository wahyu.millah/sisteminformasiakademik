import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TransaksiService } from './transaksi.service';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-transaksi',
  templateUrl: './transaksi.component.html',
  styleUrls: ['transaksi.scss']
})
export class TransaksiComponent implements OnInit, OnDestroy {
  isSaving = false;
  transaksis?: ITagihanDTO[] | null = null;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  previousPage: number | undefined;
  currentSearch!: string;
  routeData: any;
  reverse: any;

  constructor(
    protected transaksiService: TransaksiService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    private translate: TranslateService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page || this.ngbPaginationPage;
    if (this.currentSearch) {
      this.transaksiService
        .query({
          page: this.page - 1,
          query: this.currentSearch,
          size: this.itemsPerPage,
          sort: ''
        })
        .subscribe((res: HttpResponse<ITagihanDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
      return;
    }
    this.transaksiService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: ''
      })
      .subscribe((res: HttpResponse<ITagihanDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }

  search(query: string): void {
    if (!query) {
      return this.clear();
    }
    this.page = 0;
    this.currentSearch = query;
    this.router.navigate([
      '/transaksi',
      {
        search: this.currentSearch,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  transition(): void {
    this.router.navigate(['./'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.loadPage();
  }

  ngOnInit(): void {
    this.loadPage();
    this.registerChangeInTransaksis();
  }

  previousState(): void {
    window.history.back();
  }

  clear(): void {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/transaksi',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  confirmDelete(id: number): void {
    Swal.fire({
      type: 'warning',
      title: this.translate.instant('sisteminformasiakademikApp.tagihan.delete.title'),
      showCancelButton: true,
      confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
      cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.transaksiService.delete(id).subscribe(
          (res: HttpResponse<ITagihanDTO>) => this.onDeleteSuccess(res),
          (res: HttpErrorResponse) => this.onDeleteError(res)
        );
      }
    });
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITagihanDTO): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTransaksis(): void {
    this.eventSubscriber = this.eventManager.subscribe('transaksiListModification', () => this.loadPage());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  private onSuccess(users: ITagihanDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/transaksi'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.transaksis = users;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onDeleteSuccess(response: any): void {
    console.log(response);
    this.loadPage();
  }

  protected onDeleteError(response: any): void {
    console.log(response);
  }
}
