import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { TagihanComponent } from './tagihan.component';
import { TagihanDetailComponent } from './tagihan-detail.component';
import { TagihanUpdateComponent } from './tagihan-update.component';
import { TagihanDeleteDialogComponent } from './tagihan-delete-dialog.component';
import { tagihanRoute } from './tagihan.route';
import { TagihanDetailDialogComponent } from './tagihan-detail-dialog.component';
import { ProsesTransferComponent } from './proses-transfer.component';
import { CaraBayarComponent } from './cara-bayar.component';
import { TagihanDetailSiswaComponent } from './tagihan-detail-siswa.component';
import { TagihanUpdateDialogComponent } from './tagihan-update-dialog.component';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(tagihanRoute)],
  declarations: [
    TagihanComponent,
    TagihanDetailComponent,
    TagihanUpdateComponent,
    TagihanDeleteDialogComponent,
    TagihanDetailDialogComponent,
    ProsesTransferComponent,
    CaraBayarComponent,
    TagihanDetailSiswaComponent,
    TagihanUpdateDialogComponent
  ],
  entryComponents: [
    CaraBayarComponent,
    TagihanDeleteDialogComponent,
    TagihanDetailDialogComponent,
    ProsesTransferComponent,
    TagihanDetailSiswaComponent,
    TagihanUpdateDialogComponent
  ]
})
export class SisteminformasiakademikTagihanModule {}
