import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TagihanService } from './tagihan.service';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';

@Component({
  templateUrl: './cara-bayar.component.html'
})
export class CaraBayarComponent {
  tagihanDTO?: ITagihanDTO | any;
  bank?: string;
  va?: string;

  editForm = this.fb.group({
    va: [{ value: null, disabled: true }],
    expiredDate: [{ value: null, disabled: true }],
    nominalPembayaran: [{ value: null, disabled: true }],
    jenisPembayaran: [{ value: null, disabled: true }]
  });

  constructor(
    protected tagihanService: TagihanService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  resultTransaksi(tagihan: ITagihanDTO | any): void {
    const dateString = tagihan.expiredDate;
    const newDate = new Date(dateString);

    this.editForm.patchValue({
      va: tagihan.virtualAccount,
      expiredDate: newDate,
      nominalPembayaran: tagihan.nominalPembayaran,
      jenisPembayaran: tagihan.jenisPembayaran
    });
    if (tagihan.bank === 'va_muamalat') {
      this.bank = 'MUAMALAT';
    } else if (tagihan.bank === 'va_finpay') {
      this.bank = 'FINPAY';
    }
    this.va = tagihan.virtualAccount;
  }

  confirmDelete(id: number): void {
    this.tagihanService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tagihanListModification');
      this.activeModal.close();
    });
  }
}
