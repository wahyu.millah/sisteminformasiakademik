import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITagihan } from 'app/shared/model/tagihan.model';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';
import { TagihanService } from './tagihan.service';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TransaksiService } from '../transaksi/transaksi.service';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: './tagihan-update-dialog.component.html',
  styleUrls: ['tagihan.scss']
})
export class TagihanUpdateDialogComponent implements OnInit {
  isSaving = false;
  tagihan?: ITagihan;

  editForm = this.fb.group({
    tglBayar: [null, [Validators.required]],
    nominalPembayaran: [],
    namaSiswa: [],
    nis: [],
    kelas: [],
    jenisPembayaran: [],
    idTagihan: [],
    virtualAccount: []
  });

  constructor(
    protected transaksiService: TransaksiService,
    protected tagihanService: TagihanService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder,
    protected modalService: NgbModal,
    public dialog: MatDialog,
    private translate: TranslateService
  ) {}

  ngOnInit(): void {
    this.isSaving = false;
    this.editForm.get('tglBayar')!.setValue(null);
  }

  save(id: number): void {
    console.log('tgl:' + this.editForm.get('tglBayar')!.value);
    if (this.editForm.get('tglBayar')!.value !== null) {
      this.subscribeToSaveResponse(this.tagihanService.updatetgl(id, this.editForm.get('tglBayar')!.value));
    }
    this.dialog.closeAll();
    this.eventManager.broadcast('tagihanListSiswaModification');
  }

  clear(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.tagihanService.delete(id).subscribe(() => {
      this.activeModal.close();
    });
  }

  updateForm(tagihan: ITagihanDTO): void {
    this.isSaving = false;
    console.log('tgl bayar:' + tagihan.tglBayar);
    this.editForm.patchValue({
      namaSiswa: tagihan.namaSiswa,
      nis: tagihan.nis,
      kelas: tagihan.kelas,
      jenisPembayaran: tagihan.jenisPembayaran,
      nominalPembayaran: tagihan.nominalPembayaran,
      idTagihan: tagihan.idTagihan,
      tglBayar: tagihan.tglBayar,
      virtualAccount: tagihan.virtualAccount
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITagihanDTO): any {
    return item.id;
  }

  protected onDeleteSuccess(): void {
    this.eventManager.broadcast('tagihanListModification');
  }

  protected onDeleteError(response: any): void {
    console.log(response);
  }
}
