import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITagihan } from 'app/shared/model/tagihan.model';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';
import { IChannel } from 'app/shared/model/channel.model';
import { ITahunAjaran } from 'app/shared/model/tahun-ajaran.model';
import { ISiswa } from 'app/shared/model/siswa.model';

type EntityResponseTypeSiswa = HttpResponse<ISiswa>;
type EntityResponseType = HttpResponse<ITagihan>;
type EntityArrayResponseType = HttpResponse<ITagihan[]>;
type EntityArrayResponseTypeChannel = HttpResponse<IChannel[]>;
type EntityResponseTypeDTO = HttpResponse<ITagihanDTO>;
type EntityArrayResponseTypeDTO = HttpResponse<ITagihanDTO[]>;
type EntityArrayResponseTypeTahunAjaran = HttpResponse<ITahunAjaran[]>;

@Injectable({ providedIn: 'root' })
export class TagihanService {
  public resourceUrlSiswa1 = SERVER_API_URL + 'api/siswas';
  public resourceUrl = SERVER_API_URL + 'api/tagihans';
  public resourceUrlupdate = SERVER_API_URL + 'api/tagihans/update';
  public resourceUrlSiswa = SERVER_API_URL + 'api/tagihans/siswa';
  public resourceUrladd = SERVER_API_URL + 'api/transaksis/add';
  public resourceUrlChannel = SERVER_API_URL + 'api/channel';
  public resourceUrlTa = SERVER_API_URL + 'api/tahun-ajaran';

  constructor(protected http: HttpClient) {}

  create(tagihan: ITagihan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tagihan);
    return this.http
      .post<ITagihan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(tagihan: ITagihan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tagihan);
    return this.http
      .put<ITagihan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITagihan>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find2(id: number): Observable<EntityResponseTypeSiswa> {
    return this.http
      .get<ISiswa>(`${this.resourceUrlSiswa1}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseTypeSiswa) => this.convertDateFromServerSiswa(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITagihan[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  queryChannel(req?: any): Observable<EntityArrayResponseTypeChannel> {
    const options = createRequestOption(req);
    return this.http.get<IChannel[]>(this.resourceUrlChannel, { params: options, observe: 'response' });
  }

  queryTa(req?: any): Observable<EntityArrayResponseTypeTahunAjaran> {
    const options = createRequestOption(req);
    return this.http.get<ITahunAjaran[]>(this.resourceUrlTa, { params: options, observe: 'response' });
  }

  detail(siswaId: string): Observable<EntityArrayResponseType> {
    let body = new HttpParams();
    body = body.set('siswaId', siswaId);
    console.log(body);
    return this.http
      .post<ITagihan[]>(this.resourceUrlSiswa, body, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  detail2(siswaId: string, ta: string, req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    let body = new HttpParams();
    body = body.set('siswaId', siswaId);
    body = body.set('ta', ta);
    console.log(body);
    return this.http
      .post<ITagihan[]>(this.resourceUrlSiswa, body, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  detail3(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITagihan[]>(this.resourceUrlSiswa, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  createTransaksiTransfer(transaksi: ITagihanDTO, va: string): Observable<EntityResponseTypeDTO> {
    return this.http
      .post<ITagihanDTO>(`${this.resourceUrladd}/${va}`, transaksi, { observe: 'response' })
      .pipe(map((res: EntityResponseTypeDTO) => this.convertDateFromServerDTO(res)));
  }

  createTransaksi(transaksi: ITagihanDTO): Observable<EntityResponseTypeDTO> {
    return this.http
      .post<ITagihanDTO>(this.resourceUrladd, transaksi, { observe: 'response' })
      .pipe(map((res: EntityResponseTypeDTO) => this.convertDateFromServerDTO(res)));
  }

  updatetgl(id: number, tgl: string): Observable<HttpResponse<{}>> {
    console.log('edit servis note :' + id + tgl);
    return this.http
      .put(`${this.resourceUrlupdate}/${id}`, tgl, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  protected convertDateFromClient(tagihan: ITagihan): ITagihan {
    const copy: ITagihan = Object.assign({}, tagihan, {
      tanggal: tagihan.tanggal && tagihan.tanggal.isValid() ? tagihan.tanggal.toJSON() : undefined,
      jatuhTempo: tagihan.jatuhTempo && tagihan.jatuhTempo.isValid() ? tagihan.jatuhTempo.toJSON() : undefined,
      tanggalBayar: tagihan.tanggalBayar && tagihan.tanggalBayar.isValid() ? tagihan.tanggalBayar.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.tanggal = res.body.tanggal ? moment(res.body.tanggal) : undefined;
      res.body.jatuhTempo = res.body.jatuhTempo ? moment(res.body.jatuhTempo) : undefined;
      res.body.tanggalBayar = res.body.tanggalBayar ? moment(res.body.tanggalBayar) : undefined;
    }
    return res;
  }

  protected convertDateFromServerSiswa(res: EntityResponseTypeSiswa): EntityResponseTypeSiswa {
    if (res.body) {
      console.log(res.body);
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((tagihan: ITagihan) => {
        tagihan.tanggal = tagihan.tanggal ? moment(tagihan.tanggal) : undefined;
        tagihan.jatuhTempo = tagihan.jatuhTempo ? moment(tagihan.jatuhTempo) : undefined;
        tagihan.tanggalBayar = tagihan.tanggalBayar ? moment(tagihan.tanggalBayar) : undefined;
      });
    }
    return res;
  }

  protected convertDateArrayFromServerDTO(res: EntityArrayResponseTypeDTO): EntityArrayResponseTypeDTO {
    if (res.body) {
      res.body.forEach((transaksi: ITagihanDTO) => {
        transaksi.tanggal = transaksi.tanggal ? moment(transaksi.tanggal) : undefined;
        transaksi.updateOn = transaksi.updateOn ? moment(transaksi.updateOn) : undefined;
      });
    }
    return res;
  }

  protected convertDateFromServerDTO(res: EntityResponseTypeDTO): EntityResponseTypeDTO {
    if (res.body) {
      res.body.tanggal = res.body.tanggal ? moment(res.body.tanggal) : undefined;
      res.body.updateOn = res.body.updateOn ? moment(res.body.updateOn) : undefined;
    }
    return res;
  }
}
