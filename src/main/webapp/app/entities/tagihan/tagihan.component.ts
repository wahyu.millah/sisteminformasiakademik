import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog } from '@angular/material/dialog';

import { ITagihan } from 'app/shared/model/tagihan.model';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';
import Swal from 'sweetalert2';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TagihanService } from './tagihan.service';
import { TagihanDeleteDialogComponent } from './tagihan-delete-dialog.component';
import { TagihanDetailDialogComponent } from './tagihan-detail-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { TransaksiService } from '../transaksi/transaksi.service';
import { ProsesTransferComponent } from './proses-transfer.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TagihanDetailSiswaComponent } from './tagihan-detail-siswa.component';

@Component({
  selector: 'jhi-tagihan',
  templateUrl: './tagihan.component.html'
})
export class TagihanComponent implements OnInit, OnDestroy {
  isSaving = false;
  tagihans?: ITagihan[] | any;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  skey!: string;
  svalue!: string;
  previousPage: number | undefined;
  currentSearch!: string;
  routeData: any;
  reverse: any;
  statusMulti?: boolean;
  idTagihan: ITagihan[] = [];
  idMulti: number[] = [];
  porsesMulti?: boolean;

  constructor(
    protected transaksiService: TransaksiService,
    protected tagihanService: TagihanService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected jhiAlertService: JhiAlertService,
    private translate: TranslateService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadPage(page?: number): void {
    this.statusMulti = false;
    console.log('loadPage: ' + page);
    const pageToLoad: number = page || this.ngbPaginationPage;
    if (this.currentSearch) {
      this.tagihanService
        .query({
          page: this.page - 1,
          query: this.currentSearch,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihan[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
      return;
    } else {
      this.tagihanService
        .query({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ITagihan[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
    }
  }

  search(query: string): void {
    if (!query) {
      return this.clear();
    }
    this.page = 0;
    this.currentSearch = query;
    this.router.navigate([
      '/tagihan',
      {
        search: this.currentSearch,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  previousState(): void {
    window.history.back();
  }

  ngOnInit(): void {
    this.loadPage();
    this.registerChangeInTagihans();
    console.log('total item:' + this.totalItems);
  }

  selectMulti(): void {
    this.statusMulti = true;
  }

  cancelMulti(): void {
    this.statusMulti = false;
    this.idTagihan.length = 0;
    this.idMulti.length = 0;
  }

  selectTagihan(item: ITagihan, type: boolean): void {
    if (item.idTagihan !== undefined) {
      if (type === true) {
        this.idTagihan.push(item);
        this.idMulti.push(item.idTagihan);
      }

      if (type !== true) {
        const index = this.idTagihan.indexOf(item);
        const indexId = this.idMulti.indexOf(item.idTagihan);
        if (index > -1) {
          this.idTagihan.splice(index, 1);
          this.idMulti.splice(indexId, 1);
        }
      }
      console.log(this.idTagihan.length);
      console.log(this.idMulti.length);
      console.log(this.disButMulti());
    }
  }

  disButMulti(): boolean {
    const test = this.idMulti.length > 1;
    return test;
  }

  multiTagihan(): void {
    const modalRef = this.dialog.open(TagihanDetailDialogComponent, {
      panelClass: 'dialog-responsive'
    });
    modalRef.componentInstance.idMulti = this.idMulti;
    modalRef.componentInstance.tagihans = this.idTagihan;
  }

  detail(id: string, tagihan: ITagihan): void {
    console.log(id);
    const modalRef = this.dialog.open(TagihanDetailSiswaComponent, {
      height: '600px',
      width: '1000px'
    });
    this.tagihanService.detail(id).subscribe((res: HttpResponse<ITagihan[]>) => {
      modalRef.componentInstance.tagihans = res.body;
      modalRef.componentInstance.tagihan = tagihan;
    });
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITagihan): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTagihans(): void {
    this.eventSubscriber = this.eventManager.subscribe('tagihanListModification', () => this.loadPage());
  }

  delete(tagihan: ITagihan): void {
    const modalRef = this.modalService.open(TagihanDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tagihan = tagihan;
  }

  detailTransaksi(tagihan: ITagihan): void {
    const modalRef = this.dialog.open(TagihanDetailDialogComponent, {
      panelClass: 'dialog-responsive'
    });
    modalRef.componentInstance.tagihan = tagihan;
    modalRef.componentInstance.prosesTagihan(tagihan);
    this.ngOnInit();
    this.loadPage();

    modalRef.afterClosed().subscribe(result => {
      console.log('result:', result);
      //		this.openSnackBar ('Permintaan kode virtual account berhasil','');
      //	    this.resultTransaksi(tagihan.idTagihan);
    });
  }

  //  openSnackBar(message: string, action: string): any {
  //    this._snackBar.open(message, action, {
  //      duration: 3000,
  //    });
  //  }

  resultTransaksi(va: string | undefined): void {
    console.log('id:' + va);
    const modalRef = this.dialog.open(ProsesTransferComponent, {
      panelClass: 'dialog-responsive'
    });
    this.transaksiService.findByid(va).subscribe((res: HttpResponse<ITagihanDTO>) => {
      modalRef.componentInstance.resultTransaksi(res.body);
    });
  }

  resultTransaksiMulti(va: number | undefined): void {
    console.log('id:' + va);
    const modalRef = this.dialog.open(ProsesTransferComponent, {
      panelClass: 'dialog-responsive'
    });
    this.transaksiService.findMulti(va).subscribe((res: HttpResponse<ITagihanDTO>) => {
      modalRef.componentInstance.resultTransaksi(res.body);
    });
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  transition(): void {
    this.router.navigate(['/tagihan'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
  }

  clear(): void {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/tagihan',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  confirmDelete(id: number): void {
    Swal.fire({
      type: 'warning',
      title: this.translate.instant('sisteminformasiakademikApp.tagihan.delete.title'),
      showCancelButton: true,
      confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
      cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.tagihanService.delete(id).subscribe(
          (res: HttpResponse<ITagihan>) => this.onDeleteSuccess(res),
          (res: HttpErrorResponse) => this.onDeleteError(res)
        );
      }
    });
  }

  private onSuccess(users: ITagihan[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/tagihan'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.tagihans = users;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.loadPage();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  protected onDeleteSuccess(response: any): void {
    console.log('result:', response);
    this.loadPage();
  }

  protected onDeleteError(response?: any): void {
    console.log('result:', response);
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihan>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
}
