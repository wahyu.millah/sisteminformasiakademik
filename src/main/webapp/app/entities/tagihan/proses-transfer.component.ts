import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TagihanService } from './tagihan.service';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { CaraBayarComponent } from './cara-bayar.component';
import * as moment from 'moment';
import { DATE_TIME } from 'app/shared/constants/input.constants';
import { IChannel } from 'app/shared/model/channel.model';
import { HttpResponse } from '@angular/common/http';

@Component({
  templateUrl: './proses-transfer.component.html'
})
export class ProsesTransferComponent implements OnInit {
  tagihanDTO?: ITagihanDTO | any;
  bank?: string;
  va?: string;
  tagihans: ITagihanDTO[] = [];
  multiTagihan?: number;
  total = 0;
  channel: IChannel[] = [];
  adminPermata?: number;
  adminBni?: number;
  adminMuamalat?: number;
  adminCimb?: number;
  adminFinpay?: number;
  adminMandiri?: number;
  totadminPermata?: number;
  totadminBni?: number;
  totadminMuamalat?: number;
  totadminCimb?: number;
  totadminFinpay?: number;
  totadminMandiri?: number;
  jenisPembayaran?: string;
  nominalPembayaran?: number;
  totalMulti?: number;

  editForm = this.fb.group({
    va: [{ value: null, disabled: true }],
    expiredDate: [{ value: null, disabled: true }],
    nominalPembayaran: [{ value: null, disabled: true }],
    jenisPembayaran: [{ value: null, disabled: true }]
  });

  constructor(
    protected tagihanService: TagihanService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.tagihanService.queryChannel().subscribe((res: HttpResponse<IChannel[]>) => {
      if (res.body) this.channel = res.body;
    });
  }

  ngOnInit(): void {
    this.tagihanService.queryChannel().subscribe((res: HttpResponse<IChannel[]>) => {
      if (res.body) this.channel = res.body;
      this.channel.map(item => {
        if (item.name === 'PERMATA') {
          this.adminPermata = item.fee;
          if (this.adminPermata && this.nominalPembayaran) {
            this.totadminPermata = this.adminPermata + this.nominalPembayaran;
          }
        }
        if (item.name === 'BNI') {
          this.adminBni = item.fee;
          if (this.adminBni && this.nominalPembayaran) {
            this.totadminBni = this.adminBni + this.nominalPembayaran;
          }
        }
        if (item.name === 'MUAMALAT') {
          this.adminMuamalat = item.fee;
          if (this.adminMuamalat && this.nominalPembayaran) {
            this.totadminMuamalat = this.adminMuamalat + this.nominalPembayaran;
          }
        }
        if (item.name === 'CIMB') {
          this.adminCimb = item.fee;
          if (this.adminCimb && this.nominalPembayaran) {
            this.totadminCimb = this.adminCimb + this.nominalPembayaran;
          }
        }
        if (item.name === 'FINPAY') {
          this.adminFinpay = item.fee;
          if (this.adminFinpay && this.nominalPembayaran) {
            this.totadminFinpay = this.adminFinpay + this.nominalPembayaran;
          }
        }
        if (item.name === 'MANDIRI') {
          this.adminMandiri = item.fee;
          if (this.adminMandiri && this.nominalPembayaran) {
            this.totadminMandiri = this.adminMandiri + this.nominalPembayaran;
          }
        }
      });
    });
  }

  clear(): void {
    this.activeModal.dismiss();
  }

  resultTransaksi(tagihan: ITagihanDTO | any): void {
    const dateString = moment(tagihan.expiredDate);
    this.multiTagihan = tagihan.typeTagihan;

    this.editForm.patchValue({
      va: tagihan.virtualAccount,
      expiredDate: dateString.format(DATE_TIME),
      nominalPembayaran: tagihan.nominalPembayaran,
      jenisPembayaran: tagihan.nomorTagihan
    });
    this.total = tagihan.jmlTagihan;
    this.tagihans = tagihan.multiTagihan;
    this.totalMulti = tagihan.jmlBayar;
    if (tagihan.bank === 'va_muamalat') {
      this.bank = 'MUAMALAT';
    } else if (tagihan.bank === 'va_finpay') {
      this.bank = 'FINPAY';
    } else if (tagihan.bank === 'va_permata') {
      this.bank = 'PERMATA';
    } else if (tagihan.bank === 'va_bca') {
      this.bank = 'BCA';
    } else if (tagihan.bank === 'va_mandiri') {
      this.bank = 'MANDIRI';
    } else if (tagihan.bank === 'va_bni') {
      this.bank = 'BNI';
    } else if (tagihan.bank === 'va_cimb') {
      this.bank = 'NIAGA';
    }
    this.va = tagihan.virtualAccount;
    this.jenisPembayaran = tagihan.nomorTagihan;
    this.nominalPembayaran = tagihan.nominalPembayaran;
  }

  confirmDelete(id: number): void {
    this.tagihanService.delete(id).subscribe(() => {
      this.eventManager.broadcast('tagihanListModification');
      this.activeModal.close();
    });
  }

  caraPembayaran(): void {
    const modalRef = this.dialog.open(CaraBayarComponent, {
      height: '600px',
      width: '600px'
    });
    modalRef.componentInstance.va = this.va;
    modalRef.componentInstance.bank = this.bank;
  }
}
