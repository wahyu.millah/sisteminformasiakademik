import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import Swal from 'sweetalert2';
import { ITagihan } from 'app/shared/model/tagihan.model';
import { ITagihanDTO, TagihanDTO } from 'app/shared/model/tagihanDTO.model';
import { TagihanService } from './tagihan.service';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TransaksiService } from '../transaksi/transaksi.service';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { IChannel } from 'app/shared/model/channel.model';

@Component({
  templateUrl: './tagihan-detail-dialog.component.html',
  styleUrls: ['tagihan.scss']
})
export class TagihanDetailDialogComponent implements OnInit {
  isSaving = false;
  tagihan?: ITagihan;
  tagihans: ITagihan[] = [];
  channel: IChannel[] = [];
  searchOption!: string;
  tagihanAwal!: number;
  jmlBayar!: number;
  sisaTagihan!: number;
  idTagihan: number | undefined;
  selectedValue!: string;
  va!: string;
  total = 0;
  idMulti: number[] = [];
  activePermata = false;
  activeCimb = false;
  activeBni = false;
  activeMandiri = false;
  activeFinpay = false;
  activeMuamalat = false;

  editForm = this.fb.group({
    pOptions: 'TRANSFER',
    caraBayar: 'CASH',
    va: [null, [Validators.required]],
    jmlBayar: [],
    sisaTagihan: [{ value: null, disabled: true }],
    nominalPembayaran: [{ value: null, disabled: true }],
    jenisPembayaran: [{ value: null, disabled: true }],
    kelsaAjaran: [{ value: null, disabled: true }],
    harusBayar: [{ value: null, disabled: true }]
  });

  constructor(
    protected transaksiService: TransaksiService,
    protected tagihanService: TagihanService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    private fb: FormBuilder,
    protected modalService: NgbModal,
    public dialog: MatDialog,
    private translate: TranslateService
  ) {
    this.tagihanService.queryChannel().subscribe((res: HttpResponse<IChannel[]>) => {
      if (res.body) this.channel = res.body;
    });
    this.activatedChannel();
  }

  ngOnInit(): void {
    this.tagihans.map(item => {
      if (item.nominalPembayaran) this.total = this.total + item.nominalPembayaran;
    });

    this.tagihanService.queryChannel().subscribe((res: HttpResponse<IChannel[]>) => {
      if (res.body) this.channel = res.body;
      this.channel.map(item => {
        if (item.name === 'PERMATA' && item.status === 'ACTIVE') this.activePermata = true;
        if (item.name === 'BNI' && item.status === 'ACTIVE') this.activeBni = true;
        if (item.name === 'MUAMALAT' && item.status === 'ACTIVE') this.activeMuamalat = true;
        if (item.name === 'CIMB' && item.status === 'ACTIVE') this.activeCimb = true;
        if (item.name === 'FINPAY' && item.status === 'ACTIVE') this.activeFinpay = true;
        if (item.name === 'MANDIRI' && item.status === 'ACTIVE') this.activeMandiri = true;
      });
    });
    this.activatedChannel();
    console.log('channel:' + this.activePermata);
  }

  activatedChannel(): void {
    this.channel.map(item => {
      console.log(item);
      if (item.name === 'PERMATA' && item.status === 'ACTIVE') this.activePermata = true;
    });
  }

  prosesTagihan(tagihan: ITagihan): void {
    this.editForm.patchValue({
      nominalPembayaran: tagihan.nominalPembayaran,
      jenisPembayaran: tagihan.jenisPembayaran,
      kelsaAjaran: tagihan.kelas + '-' + tagihan.tglTagihan,
      harusBayar: tagihan.harusBayar
    });
    this.idTagihan = tagihan.idTagihan;
  }

  save(): void {
    this.isSaving = true;
    const tagihan = this.createFromForm();
    this.subscribeToSaveResponse(this.transaksiService.createTransaksi(tagihan));
    this.notifTunai();
    this.dialog.closeAll();
  }

  transfer(): void {
    this.isSaving = true;
    const tagihan = this.createFromForm();
    this.subscribeToSaveResponse(this.transaksiService.createTransaksiTransfer(tagihan, this.va));
    this.notif();
    this.dialog.closeAll();
  }

  multiTransfer(): void {
    this.isSaving = true;
    console.log('multi tagihan');
    this.subscribeToSaveResponse(this.transaksiService.multi(this.idMulti, this.va));
    this.notif();
    this.dialog.closeAll();
  }

  notif(): void {
    Swal.fire({
      icon: 'success',
      text: 'Virtual Account berhasil dibuat, Silahkan lakukan pembayaran !',
      showCancelButton: false,
      confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
      cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.onDeleteSuccess();
      }
    });
  }

  notifTunai(): void {
    Swal.fire({
      icon: 'success',
      text: 'Pembayaran berhasil dilakukan !',
      showCancelButton: false,
      confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
      cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.onDeleteSuccess();
      }
    });
  }

  private createFromForm(): ITagihanDTO {
    return {
      ...new TagihanDTO(),
      jenisPembayaran: this.editForm.get(['jenisPembayaran'])!.value,
      nominalPembayaran: this.editForm.get(['nominalPembayaran'])!.value,
      harusBayar: this.editForm.get(['harusBayar'])!.value,
      metodeTransaksi: this.editForm.get(['pOptions'])!.value,
      caraBayar: this.editForm.get(['caraBayar'])!.value,
      jmlBayar: this.editForm.get(['jmlBayar'])!.value,
      jmlSisa: this.editForm.get(['sisaTagihan'])!.value,
      idTagihan: this.idTagihan
    };
  }

  clear(): void {
    this.activeModal.dismiss();
  }

  sisa(): void {
    this.jmlBayar = this.editForm.get(['jmlBayar'])!.value;
    this.tagihanAwal = this.editForm.get(['harusBayar'])!.value;
    this.sisaTagihan = this.tagihanAwal - this.jmlBayar;

    this.editForm.patchValue({
      sisaTagihan: this.sisaTagihan
    });
  }

  optionSearch(): void {
    this.searchOption = this.editForm.get(['pOptions'])!.value;
  }

  confirmDelete(id: number): void {
    this.tagihanService.delete(id).subscribe(() => {
      this.activeModal.close();
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihanDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ITagihanDTO): any {
    return item.id;
  }

  protected onDeleteSuccess(): void {
    this.eventManager.broadcast('tagihanListSiswaModification');
  }

  protected onDeleteError(response: any): void {
    console.log(response);
  }
}
