import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITagihan } from 'app/shared/model/tagihan.model';

@Component({
  selector: 'jhi-tagihan-detail',
  templateUrl: './tagihan-detail.component.html'
})
export class TagihanDetailComponent implements OnInit {
  tagihan: ITagihan | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tagihan }) => {
      this.tagihan = tagihan;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
