import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
// import { ITagihan, Tagihan } from 'app/shared/model/tagihan.model';
// import { TagihanService } from './tagihan.service';
import { TagihanComponent } from './tagihan.component';
import { TagihanDetailSiswaComponent } from './tagihan-detail-siswa.component';
import { ISiswa, Siswa } from 'app/shared/model/siswa.model';
import { SiswaService } from '../siswa/siswa.service';

@Injectable({ providedIn: 'root' })
export class TagihanResolve implements Resolve<ISiswa> {
  constructor(private service: SiswaService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ISiswa> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((siswa: HttpResponse<Siswa>) => {
          if (siswa.body) {
            return of(siswa.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Siswa());
  }
}

export const tagihanRoute: Routes = [
  {
    path: '',
    component: TagihanComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sisteminformasiakademikApp.tagihan.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TagihanDetailSiswaComponent,
    resolve: {
      siswa: TagihanResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'sisteminformasiakademikApp.besarBayar.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
