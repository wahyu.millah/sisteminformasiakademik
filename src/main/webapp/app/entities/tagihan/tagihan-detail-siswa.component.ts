import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager, JhiAlertService, JhiParseLinks } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog } from '@angular/material/dialog';

import { ITagihan } from 'app/shared/model/tagihan.model';
import { ITagihanDTO } from 'app/shared/model/tagihanDTO.model';
import { ITahunAjaran } from 'app/shared/model/tahun-ajaran.model';
import Swal from 'sweetalert2';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TagihanService } from './tagihan.service';
import { TagihanDeleteDialogComponent } from './tagihan-delete-dialog.component';
import { TagihanDetailDialogComponent } from './tagihan-detail-dialog.component';
import { TagihanUpdateDialogComponent } from './tagihan-update-dialog.component';
import { TranslateService } from '@ngx-translate/core';
import { TransaksiService } from '../transaksi/transaksi.service';
import { ProsesTransferComponent } from './proses-transfer.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder } from '@angular/forms';
import { ISiswa } from 'app/shared/model/siswa.model';

@Component({
  selector: 'jhi-tagihan-detail-siswa',
  templateUrl: './tagihan-detail-siswa.component.html'
})
export class TagihanDetailSiswaComponent implements OnInit, OnDestroy {
  siswa: ISiswa | null = null;
  isSaving = false;
  tagihans?: ITagihan[] | any;
  tahunAjarans?: ITahunAjaran[] | any;
  tagihan?: ITagihan;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = 10000;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  skey!: string;
  svalue!: string;
  previousPage: number | undefined;
  currentSearch!: string;
  routeData: any;
  reverse: any;
  links: any;
  idSiswa!: string | undefined;
  ta!: string | undefined;

  editForm = this.fb.group({
    tahunAjaran: [null]
  });

  constructor(
    protected transaksiService: TransaksiService,
    protected tagihanService: TagihanService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected jhiAlertService: JhiAlertService,
    private translate: TranslateService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    protected parseLinks: JhiParseLinks
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0
    };
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    console.log('pageToLoad: ' + pageToLoad);
    if (this.currentSearch) {
      console.log('data default');
      this.tagihanService
        .detail3({
          page: this.page - 1,
          siswaId: this.idSiswa,
          ta: this.ta,
          size: this.itemsPerPage
        })
        .subscribe((res: HttpResponse<ITagihan[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
    } else {
      this.tagihanService
        .detail3({
          page: this.page - 1,
          siswaId: this.idSiswa,
          ta: this.ta,
          size: this.itemsPerPage
        })
        .subscribe((res: HttpResponse<ITagihan[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
    }
  }

  previousState(): void {
    this.router.navigate(['/tagihan']);
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ siswa }) => {
      this.siswa = siswa;
      this.idSiswa = siswa.id;
    });

    this.loadPage();
    this.registerChangeInTagihans();

    this.tagihanService.queryTa({ size: 500 }).subscribe((res: HttpResponse<ITahunAjaran[]>) => (this.tahunAjarans = res.body || []));
  }

  detail(id: number): void {
    console.log(id);
    const modalRef = this.dialog.open(ProsesTransferComponent, {
      height: '600px',
      width: '570px'
    });
    this.transaksiService.findByid(id).subscribe((res: HttpResponse<ITagihanDTO>) => {
      modalRef.componentInstance.resultTransaksi(res.body);
    });
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITagihan): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTagihans(): void {
    this.eventSubscriber = this.eventManager.subscribe('tagihanListSiswaModification', () => this.loadPage());
  }

  delete(tagihan: ITagihan): void {
    const modalRef = this.modalService.open(TagihanDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.tagihan = tagihan;
  }

  detailTransaksi(tagihan: ITagihan): void {
    const modalRef = this.dialog.open(TagihanDetailDialogComponent, {
      height: '650px',
      width: '600px'
    });
    modalRef.componentInstance.tagihan = tagihan;
    modalRef.componentInstance.prosesTagihan(tagihan);
    this.ngOnInit();
    this.loadPage();

    modalRef.afterClosed().subscribe(result => {
      console.log('result:', result);
      //		this.openSnackBar ('Permintaan kode virtual account berhasil','');
      //	    this.resultTransaksi(tagihan.idTagihan);
    });
  }

  resultTransaksi(id: number | undefined): void {
    console.log('va:' + id);
    const modalRef = this.dialog.open(ProsesTransferComponent, {
      height: '600px',
      width: '570px'
    });
    this.transaksiService.findByid(id).subscribe((res: HttpResponse<ITagihanDTO>) => {
      modalRef.componentInstance.resultTransaksi(res.body);
    });
  }

  edit(tagihan: ITagihan): void {
    const modalRef = this.dialog.open(TagihanUpdateDialogComponent, {
      panelClass: 'dialog-responsive'
    });
    modalRef.componentInstance.tagihan = tagihan;
    modalRef.componentInstance.updateForm(tagihan);
  }

  transition(): void {
    console.log('ta: ' + this.editForm.get(['tahunAjaran'])!.value);
    this.router.navigate(['/tagihan/' + this.idSiswa + '/edit'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: {
        page: this.page
        //       sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
  }

  clear(): void {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/tagihan/' + this.idSiswa + '/edit',
      {
        page: this.page
        //       sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  confirmDelete(id: number): void {
    Swal.fire({
      type: 'warning',
      title: this.translate.instant('sisteminformasiakademikApp.tagihan.delete.title'),
      showCancelButton: true,
      confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
      cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    } as any).then(result => {
      if (result.value) {
        this.tagihanService.delete(id).subscribe(
          (res: HttpResponse<ITagihan>) => this.onDeleteSuccess(res),
          (res: HttpErrorResponse) => this.onDeleteError(res)
        );
      }
    });
  }

  private onSuccess(users: ITagihan[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/tagihan/' + this.idSiswa + '/edit'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage
        //        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.tagihans = users;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.loadPage();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  protected onDeleteSuccess(response: any): void {
    console.log(response);
    this.loadPage();
  }

  loadData(): void {
    this.ta = this.editForm.get(['tahunAjaran'])!.value;
    this.loadPage();
  }

  protected onDeleteError(response: any): void {
    console.log(response);
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITagihan>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
}
