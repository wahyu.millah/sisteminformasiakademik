import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { ChannelComponent } from './channel.component';
import { channelRoute } from './channel.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(channelRoute)],
  declarations: [ChannelComponent]
})
export class SisteminformasiakademikChannelModule {}
