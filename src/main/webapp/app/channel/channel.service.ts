import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IChannel } from 'app/shared/model/channel.model';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';

type EntityResponseType = HttpResponse<IChannel>;
type EntityArrayResponseType = HttpResponse<IChannel[]>;

@Injectable({ providedIn: 'root' })
export class ChannelService {
  public resourceUrl = SERVER_API_URL + 'api/channel';
  public resourceUrlChannel = SERVER_API_URL + 'api/channel/update';
  constructor(protected http: HttpClient) {}

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IChannel[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IChannel>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  approve(channel: IChannel, tipe: boolean): Observable<EntityResponseType> {
    return this.http
      .put<IChannel>(this.resourceUrlChannel + '/' + tipe, channel, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((channel: IChannel) => {
        console.log(channel);
      });
    }
    return res;
  }
}
