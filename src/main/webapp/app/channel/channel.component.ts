import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IKelas } from 'app/shared/model/kelas.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { FormBuilder } from '@angular/forms';
import { ChannelService } from './channel.service';
import { IChannel } from 'app/shared/model/channel.model';

@Component({
  selector: 'jhi-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['channel.scss']
})
export class ChannelComponent implements OnInit, OnDestroy {
  channel?: IChannel[];
  isSaving = false;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  status?: false;

  constructor(
    protected channelService: ChannelService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    private fb: FormBuilder
  ) {}

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    this.channelService
      .query({
        page: this.page - 1,
        size: this.itemsPerPage
        //          sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IChannel[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  ngOnInit(): void {
    this.loadPage();
  }

  change(item: IChannel, type: boolean): void {
    console.log(type);
    this.subscribeToSaveResponse(this.channelService.approve(item, type));
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  protected onSuccess(data: IChannel[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/channel'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.channel = data ? data : [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  registerChangeInKelas(): void {
    this.eventSubscriber = this.eventManager.subscribe('generateListModification', () => this.loadPage());
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IKelas>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast('employeeListModification');
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
