import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';

import { MetricsComponent } from './metrics.component';

import { metricsRoute } from './metrics.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild([metricsRoute])],
  declarations: [MetricsComponent]
})
export class MetricsModule {}
