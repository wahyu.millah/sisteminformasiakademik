import { Moment } from 'moment';
import { StatusBayar } from './enumerations/status-bayar.model';

export interface IRiwayatPembayaranDTO {
  id?: number;
  namaSiswa?: string;
  nis?: string;
  kelas?: string;
  ta?: string;
  jenisPembayaran?: string;
  nominalPembayaran?: number;
  tglBayar?: Moment;
  statusTagihan?: StatusBayar;
  total?: number;
}

export class ReportDTO implements IRiwayatPembayaranDTO {
  constructor(
    public id?: number,
    public namaSiswa?: string,
    public nis?: string,
    public kelas?: string,
    public ta?: string,
    public jenisPembayaran?: string,
    public nominalPembayaran?: number,
    public tglBayar?: Moment,
    public statusTagihan?: StatusBayar,
    public total?: number
  ) {}
}
