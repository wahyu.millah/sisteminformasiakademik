export interface ITahunAjaran {
  id?: number;
  nama?: string;
}

export class TahunAjaran implements ITahunAjaran {
  constructor(public id?: number, public nama?: string) {}
}
