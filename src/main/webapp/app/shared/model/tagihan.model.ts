import { Moment } from 'moment';
import { IBesarBayar } from 'app/shared/model/besar-bayar.model';
import { StatusBayar } from 'app/shared/model/enumerations/status-bayar.model';

export interface ITagihan {
  id?: number;
  nomor?: string;
  tanggal?: Moment;
  total?: number;
  jatuhTempo?: Moment;
  tanggalBayar?: Moment;
  status?: StatusBayar;
  besarBayar?: IBesarBayar;
  namaSiswa?: string;
  nis?: string;
  kelas?: string;
  jenisPembayaran?: string;
  nominalPembayaran?: number;
  idTagihan?: number;
  nomorTagihan?: string;
  tglTagihan?: string;
  tglJatuhTempo?: Moment;
  tglBayar?: Moment;
  statusTagihan?: StatusBayar;
  harusBayar?: number;
  metodeBayar?: string;
  virtualAccount?: string;
  typeTagihan?: string;
  idSiswa?: number;
  siswaId?: number;
}

export class Tagihan implements ITagihan {
  constructor(
    public id?: number,
    public nomor?: string,
    public tanggal?: Moment,
    public total?: number,
    public jatuhTempo?: Moment,
    public tanggalBayar?: Moment,
    public status?: StatusBayar,
    public besarBayar?: IBesarBayar,
    public namaSiswa?: string,
    public nis?: string,
    public kelas?: string,
    public jenisPembayaran?: string,
    public nominalPembayaran?: number,
    public idTagihan?: number,
    public nomorTagihan?: string,
    public tglTagihan?: string,
    public tglJatuhTempo?: Moment,
    public tglBayar?: Moment,
    public statusTagihan?: StatusBayar,
    public harusBayar?: number,
    public metodeBayar?: string,
    public virtualAccount?: string,
    public typeTagihan?: string,
    public idSiswa?: number,
    public siswaId?: number
  ) {}
}
