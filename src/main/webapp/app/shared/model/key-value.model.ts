export interface IKeyValueString {
  key: string;
  value: string;
}

export const createHourKeyValue = (start: string, end: string, intervalMinute: number): IKeyValueString[] => {
  const selections: IKeyValueString[] = [];
  let _start = parseFloat(start.replace(':', '.'));
  let _end = parseFloat(end.replace(':', '.'));
  if (_start > _end) _end += 24;

  const _interval = intervalMinute / 100;

  while (_end >= _start) {
    const dec = (_start - Math.floor(_start)).toFixed(2);

    if (dec === '0.60') _start = Math.floor(_start) + 1;
    const _e = _start > 23.59 ? _start - 24 : _start;
    const e = (_e < 10 ? '0' : '') + Math.floor(_e).toString() + ':' + (dec === '0.60' ? '00' : dec.slice(2, 4));
    selections.push({ key: e, value: e });

    _start = _start + _interval;
  }

  return selections;
};

export const createHourKeyValue2 = (start: string, range: number, intervalMinute: number): IKeyValueString[] => {
  const selections: IKeyValueString[] = [];
  let _start = parseFloat(start.replace(':', '.'));
  let _end = _start + range;
  if (_end > 24) _end = 24.0;

  if (_start > _end) {
    _start = _end;
    _end = parseFloat(start.replace(':', '.'));
  }

  const _interval = intervalMinute / 100;

  while (_end >= _start) {
    const dec = (_start - Math.floor(_start)).toFixed(2);

    if (dec === '0.60') _start = Math.floor(_start) + 1;

    const e = (_start < 10 ? '0' : '') + Math.floor(_start).toString() + ':' + (dec === '0.60' ? '00' : dec.slice(2, 4));
    selections.push({ key: e, value: e });

    _start = _start + _interval;
  }

  return selections;
};
