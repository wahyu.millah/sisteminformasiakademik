import { Moment } from 'moment';
import { ITagihan } from 'app/shared/model/tagihan.model';
import { MetodeTransaksi } from 'app/shared/model/enumerations/metode-transaksi.model';
import { Bank } from 'app/shared/model/enumerations/bank.model';
import { StatusTransaksi } from 'app/shared/model/enumerations/status-transaksi.model';

export interface ITransaksi {
  id?: number;
  nomor?: string;
  tanggal?: Moment;
  metode?: MetodeTransaksi;
  bank?: Bank;
  nomorRekening?: string;
  virtualAccount?: string;
  status?: StatusTransaksi;
  updateBy?: string;
  updateOn?: Moment;
  tagihan?: ITagihan;
}

export class Transaksi implements ITransaksi {
  constructor(
    public id?: number,
    public nomor?: string,
    public tanggal?: Moment,
    public metode?: MetodeTransaksi,
    public bank?: Bank,
    public nomorRekening?: string,
    public virtualAccount?: string,
    public status?: StatusTransaksi,
    public updateBy?: string,
    public updateOn?: Moment,
    public tagihan?: ITagihan
  ) {}
}
