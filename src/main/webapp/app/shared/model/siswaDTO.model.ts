import { Moment } from 'moment';
import { IKelas } from 'app/shared/model/kelas.model';
import { JenisKelamin } from 'app/shared/model/enumerations/jenis-kelamin.model';

export interface ISiswaDTO {
  siswaId?: number;
  nis?: string;
  nama?: string;
  alamat?: string;
  jenisKelamin?: JenisKelamin;
  tempatLahir?: string;
  tanggalLahir?: Moment;
  waliMurid?: string;
  noTelephone?: string;
  tahunAjaran?: string;
  kelas?: IKelas;
  beasiswa?: boolean;
  loundry?: boolean;
  jenisBeasiswa?: string;
  jmlBulan?: number;
  login?: string;
  firstName?: string;
  lastName?: string;
  userId?: number;
  password?: string;
}

export class SiswaDTO implements ISiswaDTO {
  constructor(
    public id?: number,
    public nis?: string,
    public nama?: string,
    public alamat?: string,
    public jenisKelamin?: JenisKelamin,
    public tempatLahir?: string,
    public tanggalLahir?: Moment,
    public waliMurid?: string,
    public noTelephone?: string,
    public tahunAjaran?: string,
    public kelas?: IKelas,
    public beasiswa?: boolean,
    public loundry?: boolean,
    public jenisBeasiswa?: string,
    public jmlBulan?: number,
    public login?: string,
    public firstName?: string,
    public lastName?: string,
    public userId?: number,
    public password?: string
  ) {}
}
