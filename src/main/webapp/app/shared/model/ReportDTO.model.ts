import { Moment } from 'moment';
import { StatusBayar } from './enumerations/status-bayar.model';

export interface IReportDTO {
  id?: number;
  namaSiswa?: string;
  nis?: string;
  kelas?: string;
  ta?: string;
  jenisPembayaran?: string;
  nominalPembayaran?: number;
  tglBayar?: Moment;
  statusTagihan?: StatusBayar;
  metodeBayar?: string;
  bank?: string;
  virtualAccount?: string;
  updateBy?: string;
  total?: number;
}

export class ReportDTO implements IReportDTO {
  constructor(
    public id?: number,
    public namaSiswa?: string,
    public nis?: string,
    public kelas?: string,
    public ta?: string,
    public jenisPembayaran?: string,
    public nominalPembayaran?: number,
    public tglBayar?: Moment,
    public statusTagihan?: StatusBayar,
    public metodeBayar?: string,
    public bank?: string,
    public virtualAccount?: string,
    public updateBy?: string,
    public total?: number
  ) {}
}
