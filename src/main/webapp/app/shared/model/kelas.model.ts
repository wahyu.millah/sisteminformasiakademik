import { Jenjang } from 'app/shared/model/enumerations/jenjang.model';

export interface IKelas {
  id?: number;
  nama?: string;
  jenjang?: Jenjang;
}

export class Kelas implements IKelas {
  constructor(public id?: number, public nama?: string, public jenjang?: Jenjang) {}
}
