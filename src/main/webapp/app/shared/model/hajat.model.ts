export interface IHajat {
  id?: number;
  pengantinPria?: string;
  imagePria?: string;
  imageWanita?: string;
  pengantinWanita?: string;
  waliPria?: string;
  waliWanita?: string;
  tglAkad?: string;
  hariAkad?: string;
  jamAkad?: string;
  lokasiAkad?: string;
  lokasiAkadMap?: string;
  tglResepsi?: string;
  hariResepsi?: string;
  jamMulaiResepsi?: string;
  jamSelesai?: string;
  lokasiResepsi?: string;
  lokasiResepsiMap?: string;
  catatan?: string;
  image1?: string;
  image2?: string;
  image3?: string;
  image4?: string;
  image5?: string;
  image6?: string;
  image7?: string;
  image8?: string;
  image9?: string;
  image10?: string;
  history1?: string;
  history2?: string;
  history3?: string;
  hstory4?: string;
}

export class Hajat implements IHajat {
  constructor(
    public id?: number,
    public pengantinPria?: string,
    public imagePria?: string,
    public imageWanita?: string,
    public pengantinWanita?: string,
    public waliPria?: string,
    public waliWanita?: string,
    public tglAkad?: string,
    public hariAkad?: string,
    public jamAkad?: string,
    public lokasiAkad?: string,
    public lokasiAkadMap?: string,
    public tglResepsi?: string,
    public hariResepsi?: string,
    public jamMulaiResepsi?: string,
    public jamSelesai?: string,
    public lokasiResepsi?: string,
    public lokasiResepsiMap?: string,
    public catatan?: string,
    public image1?: string,
    public image2?: string,
    public image3?: string,
    public image4?: string,
    public image5?: string,
    public image6?: string,
    public image7?: string,
    public image8?: string,
    public image9?: string,
    public image10?: string,
    public history1?: string,
    public history2?: string,
    public history3?: string,
    public hstory4?: string
  ) {}
}
