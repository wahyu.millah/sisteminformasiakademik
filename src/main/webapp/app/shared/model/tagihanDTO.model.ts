import { Moment } from 'moment';
import { IBesarBayar } from 'app/shared/model/besar-bayar.model';
import { StatusBayar } from 'app/shared/model/enumerations/status-bayar.model';
import { CaraBayar } from './enumerations/cara-bayar.model';
import { StatusTransaksi } from './enumerations/status-transaksi.model';

export interface ITagihanDTO {
  idTransaksi?: number;
  nomorTransaksi?: string;
  tanggalTransaksi?: Moment;
  metodeTransaksi?: string;
  bank?: string;
  nomorRekening?: string;
  virtualAccount?: string;
  statusTransaksi?: StatusTransaksi;
  updateBy?: string;
  updateOn?: Moment;
  jmlTagihan?: number;
  jmlBayar?: string;
  jmlSisa?: number;
  caraBayar?: CaraBayar;
  id?: number;
  nomor?: string;
  tanggal?: Moment;
  total?: number;
  jatuhTempo?: Moment;
  tanggalBayar?: Moment;
  status?: StatusBayar;
  besarBayar?: IBesarBayar;
  namaSiswa?: string;
  nis?: string;
  kelas?: string;
  jenisPembayaran?: string;
  nominalPembayaran?: number;
  idTagihan?: number;
  nomorTagihan?: string;
  tglTagihan?: string;
  tglJatuhTempo?: Moment;
  tglBayar?: Moment;
  statusTagihan?: StatusBayar;
  harusBayar?: number;
  metodeBayar?: string;
  expiredDate?: string;
  transactionId?: string;
  typeTagihan?: string;
  multiTagihan?: [];
}

export class TagihanDTO implements ITagihanDTO {
  constructor(
    public idTransaksi?: number,
    public nomorTransaksi?: string,
    public tanggalTransaksi?: Moment,
    public metodeTransaksi?: string,
    public bank?: string,
    public nomorRekening?: string,
    public virtualAccount?: string,
    public statusTransaksi?: StatusTransaksi,
    public updateBy?: string,
    public updateOn?: Moment,
    public jmlTagihan?: number,
    public jmlBayar?: string,
    public jmlSisa?: number,
    public caraBayar?: CaraBayar,
    public id?: number,
    public nomor?: string,
    public tanggal?: Moment,
    public total?: number,
    public jatuhTempo?: Moment,
    public tanggalBayar?: Moment,
    public status?: StatusBayar,
    public besarBayar?: IBesarBayar,
    public namaSiswa?: string,
    public nis?: string,
    public kelas?: string,
    public jenisPembayaran?: string,
    public nominalPembayaran?: number,
    public idTagihan?: number,
    public nomorTagihan?: string,
    public tglTagihan?: string,
    public tglJatuhTempo?: Moment,
    public tglBayar?: Moment,
    public statusTagihan?: StatusBayar,
    public harusBayar?: number,
    public metodeBayar?: string,
    public expiredDate?: string,
    public transactionId?: string,
    public typeTagihan?: string,
    public multiTagihan?: []
  ) {}
}
