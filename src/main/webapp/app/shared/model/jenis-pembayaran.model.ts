export interface IJenisPembayaran {
  id?: number;
  jenis?: string;
  metode?: string;
  keterangan?: string;
}

export class JenisPembayaran implements IJenisPembayaran {
  constructor(public id?: number, public jenis?: string, public metode?: string, public keterangan?: string) {}
}
