export interface IChannel {
  id?: number;
  name?: string;
  status?: string;
  fee?: number;
  va?: string;
  secretKey?: string;
}

export class Channel implements IChannel {
  constructor(
    public id?: number,
    public name?: string,
    public status?: string,
    public fee?: number,
    public va?: string,
    public secretKey?: string
  ) {}
}
