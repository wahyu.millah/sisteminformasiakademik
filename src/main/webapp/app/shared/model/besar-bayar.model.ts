import { IKelas } from 'app/shared/model/kelas.model';
import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';

export interface IBesarBayar {
  id?: number;
  nominal?: number;
  kelas?: IKelas;
  jenisBayar?: IJenisPembayaran;
  perbulan?: number;
  cicilan?: boolean;
}

export class BesarBayar implements IBesarBayar {
  constructor(
    public id?: number,
    public nominal?: number,
    public kelas?: IKelas,
    public jenisBayar?: IJenisPembayaran,
    public perbulan?: number,
    public cicilan?: boolean
  ) {}
}
