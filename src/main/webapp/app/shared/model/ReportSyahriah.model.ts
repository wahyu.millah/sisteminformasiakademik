import { Moment } from 'moment';

export interface IReportSyahriah {
  id?: number;
  nis?: string;
  nama?: string;
  tahunAjaran?: string;
  kelas?: string;
  januari?: boolean;
  februari?: boolean;
  maret?: boolean;
  april?: boolean;
  mei?: boolean;
  juni?: boolean;
  juli?: boolean;
  agustus?: boolean;
  september?: boolean;
  oktober?: boolean;
  november?: boolean;
  desember?: boolean;
  januariOn?: Moment;
  februariOn?: Moment;
  maretOn?: Moment;
  aprilOn?: Moment;
  meiOn?: Moment;
  juniOn?: Moment;
  juliOn?: Moment;
  agustusOn?: Moment;
  septemberOn?: Moment;
  oktoberOn?: Moment;
  novemberOn?: Moment;
  desemberOn?: Moment;
}

export class ReportSyahriah implements IReportSyahriah {
  constructor(
    public id?: number,
    public nis?: string,
    public nama?: string,
    public tahunAjaran?: string,
    public kelas?: string,
    public januari?: boolean,
    public februari?: boolean,
    public maret?: boolean,
    public april?: boolean,
    public mei?: boolean,
    public juni?: boolean,
    public juli?: boolean,
    public agustus?: boolean,
    public september?: boolean,
    public oktober?: boolean,
    public november?: boolean,
    public desember?: boolean,
    public januariOn?: Moment,
    public februariOn?: Moment,
    public maretOn?: Moment,
    public aprilOn?: Moment,
    public meiOn?: Moment,
    public juniOn?: Moment,
    public juliOn?: Moment,
    public agustusOn?: Moment,
    public septemberOn?: Moment,
    public oktoberOn?: Moment,
    public novemberOn?: Moment,
    public desemberOn?: Moment
  ) {}
}
