import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IReportSyahriah } from 'app/shared/model/ReportSyahriah.model';
import { map } from 'rxjs/operators';

type BlobResponseType = HttpResponse<Blob>;
type EntityArrayResponseType = HttpResponse<IReportSyahriah[]>;
type EntityResponseType = HttpResponse<IReportSyahriah>;

@Injectable({ providedIn: 'root' })
export class LaporanService {
  public resourceUrl = SERVER_API_URL + 'api/tagihans';
  public resourceUrlReport = SERVER_API_URL + 'api/report-syahriah';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IReportSyahriah>(`${this.resourceUrlReport}/${id}`, { observe: 'response' });
  }

  report(req?: any): Observable<BlobResponseType> {
    const options = createRequestOption(req);
    return this.http.get(`${this.resourceUrl}/report`, { params: options, observe: 'response', responseType: 'blob' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IReportSyahriah[]>(this.resourceUrlReport, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((r: IReportSyahriah) => {
        r.januariOn = r.januariOn ? moment(r.januariOn) : undefined;
        r.februariOn = r.februariOn ? moment(r.februariOn) : undefined;
        r.maretOn = r.maretOn ? moment(r.maretOn) : undefined;
        r.aprilOn = r.aprilOn ? moment(r.aprilOn) : undefined;
        r.meiOn = r.meiOn ? moment(r.meiOn) : undefined;
        r.juniOn = r.juniOn ? moment(r.juniOn) : undefined;
        r.juliOn = r.juliOn ? moment(r.juliOn) : undefined;
        r.agustusOn = r.agustusOn ? moment(r.agustusOn) : undefined;
        r.septemberOn = r.septemberOn ? moment(r.septemberOn) : undefined;
        r.oktoberOn = r.oktoberOn ? moment(r.oktoberOn) : undefined;
        r.novemberOn = r.novemberOn ? moment(r.novemberOn) : undefined;
        r.desemberOn = r.desemberOn ? moment(r.desemberOn) : undefined;
      });
    }
    return res;
  }
}
