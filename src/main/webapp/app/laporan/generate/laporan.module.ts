import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { LaporanComponent } from './laporan.component';
import { laporanRoute } from './laporan.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(laporanRoute)],
  declarations: [LaporanComponent]
})
export class SisteminformasiakademikLaporanModule {}
