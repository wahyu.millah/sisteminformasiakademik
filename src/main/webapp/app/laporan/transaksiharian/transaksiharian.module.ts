import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { TransaksiHarianComponent } from './transaksiharian.component';
import { transaksiHarianRoute } from './transaksiharian.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(transaksiHarianRoute)],
  declarations: [TransaksiHarianComponent]
})
export class SisteminformasiakademikTransaksiHarianModule {}
