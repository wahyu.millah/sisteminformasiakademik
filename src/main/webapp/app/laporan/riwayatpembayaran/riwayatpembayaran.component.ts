import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { RiwayatPembayaranService } from './riwayatpembayaran.service';
import { JenisPembayaranService } from 'app/entities/jenis-pembayaran/jenis-pembayaran.service';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { IRiwayatPembayaranDTO } from 'app/shared/model/RiwayatPembayaran.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription, Observable } from 'rxjs';
import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from 'app/entities/kelas/kelas.service';
import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { ITahunAjaran } from 'app/shared/model/tahun-ajaran.model';
// import * as FileSaver from 'file-saver';
import { SiswaService } from 'app/entities/siswa/siswa.service';
import { IKeyValueString } from 'app/shared/model/key-value.model';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'jhi-riwayatpembayaran',
  templateUrl: './riwayatpembayaran.component.html'
})
export class RiwayatPembayaranComponent implements OnInit {
  kelas!: IKelas[];
  report?: IRiwayatPembayaranDTO[] | any;
  jenispembayarans: IJenisPembayaran[] = [];
  tahunAjarans: ITahunAjaran[] = [];
  isSaving = false;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  previousPage: number | undefined;
  currentSearch!: string;
  routeData: any;
  reverse: any;
  ta?: any;
  siswa?: any;
  status?: any;
  totTransaksiVa!: number | 0;
  totTransaksiTu!: number | 0;
  siswaList: IKeyValueString[] = [];
  params?: any;

  editForm = this.fb.group({
    ta: [],
    status: []
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private fb: FormBuilder,
    protected laporanService: RiwayatPembayaranService,
    protected siswaService: SiswaService,
    protected jenisPembayaranService: JenisPembayaranService,
    protected kelasService: KelasService,
    private translate: TranslateService
  ) {
    this.siswaService.getKeyValueList('').subscribe((res: HttpResponse<IKeyValueString[]>) => {
      if (res.body) this.siswaList = res.body;
      console.log(this.siswaList);
    });
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

    this.laporanService
      .query({
        page: this.page - 1,
        ta: this.editForm.get(['ta'])!.value,
        siswa: this.siswa,
        status: this.editForm.get(['status'])!.value,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<IRiwayatPembayaranDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }

  typeAheadFormatter = (item: IKeyValueString) => item.value;

  typeAheadSearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => (term.length < 2 ? [] : this.siswaList.filter(v => v.value.toLowerCase().includes(term.toLowerCase())).slice(0, 10)))
    );

  total(data: any): void {
    let jmlVa = 0;
    data.forEach(function(value: any): void {
      jmlVa += value.nominalPembayaran;
    });
    this.totTransaksiVa = jmlVa;
  }

  onSelectSiswa(item: any): void {
    this.siswa = item.key;
    this.ngbPaginationPage = 1;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  search(): void {
    if (this.siswa !== undefined && this.editForm.get(['ta'])!.value !== null && this.editForm.get(['status'])!.value !== null) {
      this.router.navigate([
        '/riwayatpembayaran',
        {
          ta: this.editForm.get(['ta'])!.value,
          siswa: this.siswa,
          status: this.editForm.get(['status'])!.value,
          page: this.page,
          sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }
      ]);
      this.loadPage();
    } else {
      Swal.fire({
        icon: 'info',
        text: ' Nama siswa, status dan tahun ajaran harus terisi !!!',
        showCancelButton: false,
        //  confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
        //  cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      } as any).then(result => {
        console.log(result);
      });
    }
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
    });
    this.registerChangeInReportSyahriah();
  }

  clear(): void {
    console.log('clear...');
    this.editForm.patchValue({
      siswa: null
    });
    this.params.siswa = null;
  }

  trackId(index: number, item: IRiwayatPembayaranDTO): number {
    return item.id!;
  }

  registerChangeInReportSyahriah(): void {
    this.eventSubscriber = this.eventManager.subscribe('reportListModification', () => this.loadPage());
  }

  download(): void {
    if (this.siswa !== undefined && this.editForm.get(['ta'])!.value !== null && this.editForm.get(['status'])!.value !== null) {
      Swal.fire({
        icon: 'info',
        text: 'Download Surat Tagihan, hanya yang status tagihan selain LUNAS',
        showCancelButton: true,
        confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
        cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      } as any).then(result => {
        if (result.value) {
          this.prosesdownload();
        }
      });
    } else {
      Swal.fire({
        icon: 'info',
        text: ' Nama siswa, status dan tahun ajaran harus terisi !!!',
        showCancelButton: false,
        //  confirmButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonyes'),
        //  cancelButtonText: this.translate.instant('sisteminformasiakademikApp.tagihan.buttonno'),
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      } as any).then(result => {
        console.log(result);
      });
    }
  }

  prosesdownload(): void {
    window.open('/api/riwayat-pembayaran/list/download/' + this.editForm.get(['ta'])!.value + '/' + this.siswa);
    /* this.laporanService.report(this.siswa, this.editForm.get(['ta'])!.value).subscribe(res => {
		  const fileURL = URL.createObjectURL(res);
		  window.open(fileURL, '_blank');
   		 console.log('fileURL :'+ fileURL);
		});*/
    //   window.open('/api/riwayat-pembayaran/list/download/' + this.siswa + '/' + this.editForm.get(['ta'])!.value);
  }

  protected onTot(data: IRiwayatPembayaranDTO[] | null): void {
    this.total(data);
  }

  protected onSuccess(data: IRiwayatPembayaranDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/riwayatpembayaran'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.report = data;
    this.total(data);
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRiwayatPembayaranDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.eventManager.broadcast('reportSyahriahListModification');
    this.clear();
  }

  protected onSaveError(): void {
    this.isSaving = false;
    this.clear();
  }
}
