import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITahunAjaran } from 'app/shared/model/tahun-ajaran.model';
import { map } from 'rxjs/operators';
import { IRiwayatPembayaranDTO } from 'app/shared/model/RiwayatPembayaran.model';

type BlobResponseType = HttpResponse<Blob>;
type EntityArrayResponseType = HttpResponse<IRiwayatPembayaranDTO[]>;
type EntityArrayResponseType2 = HttpResponse<ITahunAjaran[]>;
type EntityResponseType = HttpResponse<IRiwayatPembayaranDTO>;

@Injectable({ providedIn: 'root' })
export class RiwayatPembayaranService {
  public resourceUrlReport = SERVER_API_URL + 'api/riwayat-pembayaran/list';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRiwayatPembayaranDTO>(`${this.resourceUrlReport}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    console.log('params:' + options);
    return this.http
      .get<IRiwayatPembayaranDTO[]>(this.resourceUrlReport, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  /* report(req?: any): Observable<BlobResponseType> {
    const options = createRequestOption(req);
    return this.http.get(`${this.resourceUrlReport}/download`, { params: options, observe: 'response', responseType: 'blob' });
  }*/

  report(siswaId?: any, ta?: any): any {
    console.log(siswaId, ta);
    const requestUrl = `${this.resourceUrlReport}/download/${ta}/${siswaId}`;
    return this.http.get(requestUrl, { observe: 'response' }).pipe(
      map((res: any) => {
        return new Blob([res.body], { type: 'application/pdf' });
      })
    );
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((r: IRiwayatPembayaranDTO) => {
        r.tglBayar = r.tglBayar ? moment(r.tglBayar) : undefined;
        if (r.total) r.total += Number(r.nominalPembayaran);
      });
    }
    return res;
  }

  protected convertDateArrayFromServer2(res: EntityArrayResponseType2): EntityArrayResponseType2 {
    if (res.body) {
      console.log(res);
    }
    return res;
  }
}
