import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { RiwayatPembayaranComponent } from './riwayatpembayaran.component';
import { riwayatPembayaranRoute } from './riwayatpembayaran.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(riwayatPembayaranRoute)],
  declarations: [RiwayatPembayaranComponent]
})
export class SisteminformasiakademikRiwayatPembayaranModule {}
