import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { BukuKontrolComponent } from './bukukontrol.component';
import { bukuKontrolRoute } from './bukukontrol.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(bukuKontrolRoute)],
  declarations: [BukuKontrolComponent]
})
export class SisteminformasiakademikBukuKontrolModule {}
