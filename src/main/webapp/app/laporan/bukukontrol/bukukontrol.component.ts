import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BukuKontrolService } from './bukukontrol.service';
import { JenisPembayaranService } from 'app/entities/jenis-pembayaran/jenis-pembayaran.service';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { IReportSyahriah } from 'app/shared/model/ReportSyahriah.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription, Observable } from 'rxjs';
import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from 'app/entities/kelas/kelas.service';
import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { ITahunAjaran } from 'app/shared/model/tahun-ajaran.model';

@Component({
  selector: 'jhi-bukukontrol',
  templateUrl: './bukukontrol.component.html'
})
export class BukuKontrolComponent implements OnInit {
  kelas!: IKelas[];
  reportSyahriah?: IReportSyahriah[] | any;
  jenispembayarans: IJenisPembayaran[] = [];
  tahunAjarans: ITahunAjaran[] = [];
  isSaving = false;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  previousPage: number | undefined;
  currentSearch!: string;
  routeData: any;
  reverse: any;
  status?: boolean;

  editForm = this.fb.group({
    kelas: [],
    jenisBayar: [],
    tahunAjaran: []
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private fb: FormBuilder,
    protected laporanService: BukuKontrolService,
    protected jenisPembayaranService: JenisPembayaranService,
    protected kelasService: KelasService
  ) {
    this.laporanService
      .queryTa({
        size: 1000
      })
      .subscribe((res: HttpResponse<ITahunAjaran[]>) => (this.tahunAjarans = res.body || []));
    this.jenisPembayaranService
      .query({
        size: 1000
      })
      .subscribe((res: HttpResponse<IJenisPembayaran[]>) => (this.jenispembayarans = res.body || []));
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });

    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

    if (this.currentSearch) {
      this.laporanService
        .query({
          page: this.page - 1,
          query: this.currentSearch,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IReportSyahriah[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
    } else {
      console.log('data default');
      this.laporanService
        .query({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IReportSyahriah[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
    }
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  search(query: string): void {
    if (!query) {
      return this.clear();
    }
    this.page = 0;
    this.currentSearch = query;
    this.router.navigate([
      '/laporan',
      {
        search: this.currentSearch,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInReportSyahriah();
    this.kelasService.query().subscribe((res: HttpResponse<IKelas[]>) => {
      this.kelas = res.body ? res.body : [];
    });
  }

  clear(): void {
    this.page = 0;
    this.currentSearch = '';
    this.router.navigate([
      '/laporan',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  trackId(index: number, item: IReportSyahriah): number {
    return item.id!;
  }

  registerChangeInReportSyahriah(): void {
    this.eventSubscriber = this.eventManager.subscribe('bukuKontrolListModification', () => this.loadPage());
  }

  protected onSuccess(data: IReportSyahriah[] | null, headers: HttpHeaders, page: number): void {
    console.log(data);
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/bukukontrol'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.reportSyahriah = data;
    console.log(data);
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReportSyahriah>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.loadPage();
    this.eventManager.broadcast('reportSyahriahListModification');
    this.clear();
  }

  protected onSaveError(): void {
    this.isSaving = false;
    this.loadPage();
    this.clear();
  }
}
