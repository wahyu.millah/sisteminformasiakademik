import { Routes, Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { BukuKontrolComponent } from './bukukontrol.component';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Injectable } from '@angular/core';
import { IReportSyahriah, ReportSyahriah } from 'app/shared/model/ReportSyahriah.model';
import { BukuKontrolService } from './bukukontrol.service';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class LaporanSyahriahResolve implements Resolve<IReportSyahriah> {
  constructor(private service: BukuKontrolService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IReportSyahriah> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((besarBayar: HttpResponse<ReportSyahriah>) => {
          if (besarBayar.body) {
            return of(besarBayar.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ReportSyahriah());
  }
}

export const bukuKontrolRoute: Routes = [
  {
    path: '',
    component: BukuKontrolComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sisteminformasiakademikApp.kelas.home.bukukontrol'
    },
    canActivate: [UserRouteAccessService]
  }
];
