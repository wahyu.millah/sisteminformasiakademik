import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { TransaksiMingguanComponent } from './transaksimingguan.component';
import { transaksiMingguanRoute } from './transaksimingguan.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(transaksiMingguanRoute)],
  declarations: [TransaksiMingguanComponent]
})
export class SisteminformasiakademikTransaksiMingguanModule {}
