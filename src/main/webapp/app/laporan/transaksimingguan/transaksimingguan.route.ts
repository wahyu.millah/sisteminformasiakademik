import { Routes, Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { TransaksiMingguanComponent } from './transaksimingguan.component';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Injectable } from '@angular/core';
import { IReportSyahriah, ReportSyahriah } from 'app/shared/model/ReportSyahriah.model';
import { TransaksiMingguanService } from './transaksimingguan.service';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class TransaksiMingguanResolve implements Resolve<IReportSyahriah> {
  constructor(private service: TransaksiMingguanService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IReportSyahriah> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((besarBayar: HttpResponse<ReportSyahriah>) => {
          if (besarBayar.body) {
            return of(besarBayar.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ReportSyahriah());
  }
}

export const transaksiMingguanRoute: Routes = [
  {
    path: '',
    component: TransaksiMingguanComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sisteminformasiakademikApp.kelas.home.lapmingguan'
    },
    canActivate: [UserRouteAccessService]
  }
];
