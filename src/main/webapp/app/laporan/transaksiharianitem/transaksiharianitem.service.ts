import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITahunAjaran } from 'app/shared/model/tahun-ajaran.model';
import { map } from 'rxjs/operators';
import { IReportDTO } from 'app/shared/model/ReportDTO.model';

type BlobResponseType = HttpResponse<Blob>;
type EntityArrayResponseType = HttpResponse<IReportDTO[]>;
type EntityArrayResponseType2 = HttpResponse<ITahunAjaran[]>;
type EntityResponseType = HttpResponse<IReportDTO>;

@Injectable({ providedIn: 'root' })
export class TransaksiHarianItemService {
  public resourceUrlReport = SERVER_API_URL + 'api/report-item';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IReportDTO>(`${this.resourceUrlReport}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IReportDTO[]>(this.resourceUrlReport, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  queryList(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IReportDTO[]>(`${this.resourceUrlReport}/list`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  report(req?: any): Observable<BlobResponseType> {
    const options = createRequestOption(req);
    return this.http.get(`${this.resourceUrlReport}/download`, { params: options, observe: 'response', responseType: 'blob' });
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((r: IReportDTO) => {
        r.tglBayar = r.tglBayar ? moment(r.tglBayar) : undefined;
        if (r.total) r.total += Number(r.nominalPembayaran);
      });
    }
    return res;
  }

  protected convertDateArrayFromServer2(res: EntityArrayResponseType2): EntityArrayResponseType2 {
    if (res.body) {
      console.log(res);
    }
    return res;
  }
}
