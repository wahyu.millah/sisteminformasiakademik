import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TransaksiHarianItemService } from './transaksiharianitem.service';
import { JenisPembayaranService } from 'app/entities/jenis-pembayaran/jenis-pembayaran.service';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { IReportDTO } from 'app/shared/model/ReportDTO.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { JhiEventManager } from 'ng-jhipster';
import { Subscription, Observable } from 'rxjs';
import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from 'app/entities/kelas/kelas.service';
import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { ITahunAjaran } from 'app/shared/model/tahun-ajaran.model';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'jhi-transaksiharianitem',
  templateUrl: './transaksiharianitem.component.html'
})
export class TransaksiHarianItemComponent implements OnInit {
  kelas!: IKelas[];
  report?: IReportDTO[] | any;
  jenispembayarans: IJenisPembayaran[] = [];
  tahunAjarans: ITahunAjaran[] = [];
  isSaving = false;
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  searchOption!: number;
  previousPage: number | undefined;
  startDate!: string;
  endDate!: string;
  routeData: any;
  reverse: any;
  status?: boolean;
  date?: any;
  totTransaksi!: number | 0;

  editForm = this.fb.group({
    date: []
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    private fb: FormBuilder,
    protected laporanService: TransaksiHarianItemService,
    protected jenisPembayaranService: JenisPembayaranService,
    protected kelasService: KelasService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });

    this.startDate =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';

    this.endDate =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ? this.activatedRoute.snapshot.params['search'] : '';
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    console.log('load:' + this.startDate, this.endDate);

    if (this.startDate) {
      this.laporanService
        .query({
          page: this.page - 1,
          startDate: this.startDate,
          endDate: this.endDate,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IReportDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
    } else {
      this.laporanService
        .query({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IReportDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
    }
    this.laporanService
      .queryList({
        startDate: this.startDate,
        endDate: this.endDate
      })
      .subscribe((res: HttpResponse<IReportDTO[]>) => this.total(res.body));
  }

  total(data: any): void {
    let jml = 0;
    data.forEach(function(value: any): void {
      jml += value.nominalPembayaran;
    });
    this.totTransaksi = jml;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  search(start: string, end: string): void {
    console.log(start);
    if (!start || !end) {
      return this.clear();
    }
    this.startDate = start;
    this.endDate = end;
    this.router.navigate([
      '/transaksiharianitem',
      {
        startDate: this.startDate,
        endDate: this.endDate,
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
    this.startDate = start;
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInReportSyahriah();
  }

  clear(): void {
    this.page = 0;
    this.startDate = '';
    this.endDate = '';
    this.router.navigate([
      '/transaksiharianitem',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadPage();
  }

  trackId(index: number, item: IReportDTO): number {
    return item.id!;
  }

  registerChangeInReportSyahriah(): void {
    this.eventSubscriber = this.eventManager.subscribe('reportListModification', () => this.loadPage());
  }

  download(): void {
    this.laporanService.report({ startDate: this.startDate, endDate: this.endDate }).subscribe(res => {
      if (res.body) {
        const _filename = res.headers.get('Content-Disposition');
        const filename = _filename ? _filename.substring(_filename.lastIndexOf('=') + 1) : 'filename=download.xlsx';
        FileSaver.saveAs(res.body, filename);
      }
    });
  }

  protected onTot(data: IReportDTO[] | null): void {
    this.total(data);
  }

  protected onSuccess(data: IReportDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/transaksiharianitem'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.report = data;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReportDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.loadPage();
    this.eventManager.broadcast('reportSyahriahListModification');
    this.clear();
  }

  protected onSaveError(): void {
    this.isSaving = false;
    this.loadPage();
    this.clear();
  }
}
