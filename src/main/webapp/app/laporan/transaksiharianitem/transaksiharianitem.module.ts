import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { TransaksiHarianItemComponent } from './transaksiharianitem.component';
import { transaksiHarianItemRoute } from './transaksiharianitem.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(transaksiHarianItemRoute)],
  declarations: [TransaksiHarianItemComponent]
})
export class SisteminformasiakademikTransaksiHarianItemModule {}
