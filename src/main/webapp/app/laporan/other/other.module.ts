import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasiakademikSharedModule } from 'app/shared/shared.module';
import { OtherComponent } from './other.component';
import { otherRoute } from './other.route';

@NgModule({
  imports: [SisteminformasiakademikSharedModule, RouterModule.forChild(otherRoute)],
  declarations: [OtherComponent]
})
export class SisteminformasiakademikOtherModule {}
