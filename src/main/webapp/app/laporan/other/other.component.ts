import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import * as FileSaver from 'file-saver';
import { OtherService } from './other.service';
import { JenisPembayaranService } from 'app/entities/jenis-pembayaran/jenis-pembayaran.service';
import { HttpResponse } from '@angular/common/http';
import { IJenisPembayaran } from 'app/shared/model/jenis-pembayaran.model';
import { IKelas } from 'app/shared/model/kelas.model';
import { KelasService } from 'app/entities/kelas/kelas.service';

@Component({
  selector: 'jhi-laporan',
  templateUrl: './other.component.html'
})
export class OtherComponent implements OnInit {
  isSaving = false;
  jenispembayarans: IJenisPembayaran[] = [];
  kelas: IKelas[] = [];
  ta: any;
  kls: any;
  jenis: any;
  nis: any;

  editForm = this.fb.group({
    ta: [],
    jenisBayar: [],
    nis: [],
    kelas: []
  });

  constructor(
    private fb: FormBuilder,
    protected laporanService: OtherService,
    protected jenisPembayaranService: JenisPembayaranService,
    protected kelasService: KelasService
  ) {
    this.jenisPembayaranService
      .query({
        size: 1000
      })
      .subscribe((res: HttpResponse<IJenisPembayaran[]>) => (this.jenispembayarans = res.body || []));
    this.kelasService.query().subscribe((res: HttpResponse<IKelas[]>) => (this.kelas = res.body || []));
  }

  ngOnInit(): void {
    this.jenisPembayaranService.query().subscribe((res: HttpResponse<IJenisPembayaran[]>) => (this.jenispembayarans = res.body || []));
    this.kelasService.query().subscribe((res: HttpResponse<IKelas[]>) => (this.kelas = res.body || []));
  }

  startDate(event: any): void {
    console.log('date:' + event.target.value);
  }

  downloadPerSiswa(): void {
    this.nis = this.editForm.get(['nis'])!.value;
    if (this.nis === null) this.nis = 'ALL';
    console.log('generate per siswa');
    this.laporanService.report({ ta: 'ALL', nis: this.nis, jenis: 'ALL', kelas: 'ALL' }).subscribe(res => {
      if (res.body) {
        const _filename = res.headers.get('Content-Disposition');
        const filename = _filename ? _filename.substring(_filename.lastIndexOf('=') + 1) : 'filename=download.xlsx';
        FileSaver.saveAs(res.body, filename);
      }
    });
  }

  download(): void {
    this.nis = this.editForm.get(['nis'])!.value;
    if (this.nis === null) this.nis = 'ALL';
    this.ta = this.editForm.get(['ta'])!.value;
    if (this.ta === null) this.ta = 'ALL';
    this.kls = this.editForm.get(['kelas'])!.value;
    if (this.kls === null) this.kls = 'ALL';
    this.jenis = this.editForm.get(['jenisBayar'])!.value;
    if (this.jenis === null) this.jenis = 'ALL';
    console.log('param:' + this.ta + ',' + this.nis + ',' + this.jenis + ',' + this.kls);
    this.laporanService.report({ ta: this.ta, nis: this.nis, jenis: this.jenis, kelas: this.kls }).subscribe(res => {
      if (res.body) {
        const _filename = res.headers.get('Content-Disposition');
        const filename = _filename ? _filename.substring(_filename.lastIndexOf('=') + 1) : 'filename=download.xlsx';
        FileSaver.saveAs(res.body, filename);
      }
    });
  }
}
