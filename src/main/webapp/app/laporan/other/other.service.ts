import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';

type BlobResponseType = HttpResponse<Blob>;

@Injectable({ providedIn: 'root' })
export class OtherService {
  public resourceUrl = SERVER_API_URL + 'api/report-syahriah';

  constructor(protected http: HttpClient) {}

  report(req?: any): Observable<BlobResponseType> {
    const options = createRequestOption(req);
    return this.http.get(`${this.resourceUrl}/download`, { params: options, observe: 'response', responseType: 'blob' });
  }
}
