import { Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { OtherComponent } from './other.component';

export const otherRoute: Routes = [
  {
    path: '',
    component: OtherComponent,
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'sisteminformasiakademikApp.kelas.home.laporan'
    },
    canActivate: [UserRouteAccessService]
  }
];
