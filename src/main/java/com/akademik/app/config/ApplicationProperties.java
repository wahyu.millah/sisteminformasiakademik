package com.akademik.app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Sisteminformasiakademik.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
	private final Directory directory = new Directory();
	private final Payment payment = new Payment();
	
    public Directory getDirectory() {
		return directory;
	}

	public Payment getPayment() {
		return payment;
	}

	public static class Directory {
    	private String base;

		public String getBase() {
			return base;
		}

		public void setBase(String base) {
			this.base = base;
		}

		public String getUpload() {
			return this.base + "foto_profil" + "/";
		}
		
		public String getTemplate() {
			return this.base + "template" + "/";
		}
    }
    
    public static  class Payment {
		private String url;
        private String merchantid;
        private String apikey;
        private String secretkeymuammalat;
        private String secretkeyfinpay;
        private String secretkeypermata;
        private String secretkeybni;
        private String secretkeymandiri;
        private String secretkeybca;
        private String secretkeyniaga;
        private String secretkeybri;
        private String host;
        private Long expired_time;
        private String secretUnboundId;
        private String hashKey;
		public Long getExpired_time() {
			return expired_time;
		}
		public void setExpired_time(Long expired_time) {
			this.expired_time = expired_time;
		}
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getMerchantid() {
			return merchantid;
		}
		public void setMerchantid(String merchantid) {
			this.merchantid = merchantid;
		}
		public String getApikey() {
			return apikey;
		}
		public void setApikey(String apikey) {
			this.apikey = apikey;
		}
		public String getSecretkeymuammalat() {
			return secretkeymuammalat;
		}
		public void setSecretkeymuammalat(String secretkeymuammalat) {
			this.secretkeymuammalat = secretkeymuammalat;
		}
		public String getSecretkeyfinpay() {
			return secretkeyfinpay;
		}
		public void setSecretkeyfinpay(String secretkeyfinpay) {
			this.secretkeyfinpay = secretkeyfinpay;
		}
		public String getSecretkeypermata() {
			return secretkeypermata;
		}
		public void setSecretkeypermata(String secretkeypermata) {
			this.secretkeypermata = secretkeypermata;
		}
		public String getSecretkeybni() {
			return secretkeybni;
		}
		public void setSecretkeybni(String secretkeybni) {
			this.secretkeybni = secretkeybni;
		}
		public String getSecretkeymandiri() {
			return secretkeymandiri;
		}
		public void setSecretkeymandiri(String secretkeymandiri) {
			this.secretkeymandiri = secretkeymandiri;
		}
		public String getSecretkeybca() {
			return secretkeybca;
		}
		public void setSecretkeybca(String secretkeybca) {
			this.secretkeybca = secretkeybca;
		}
		public String getSecretkeyniaga() {
			return secretkeyniaga;
		}
		public void setSecretkeyniaga(String secretkeyniaga) {
			this.secretkeyniaga = secretkeyniaga;
		}
        public String getSecretkeybri() {
			return secretkeybri;
		}
		public void setSecretkeybri(String secretkeybri) {
			this.secretkeybri = secretkeybri;
		}
		public String getSecretUnboundId() {
			return secretUnboundId;
		}
		public void setSecretUnboundId(String secretUnboundId) {
			this.secretUnboundId = secretUnboundId;
		}
		public String getHashKey() {
			return hashKey;
		}
		public void setHashKey(String hashKey) {
			this.hashKey = hashKey;
		}
    }
}
