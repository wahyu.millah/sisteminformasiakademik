package com.akademik.app.web.rest.mobile;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.service.dto.ProfilDTO;
import com.akademik.app.web.rest.SiswaResource;

@RestController
@RequestMapping("/api")
@Transactional
public class AccountMobileResource {

    private final Logger log = LoggerFactory.getLogger(SiswaResource.class);

    private final SiswaRepository siswaRepository;

    public AccountMobileResource(SiswaRepository siswaRepository) {
        this.siswaRepository = siswaRepository;
    }

    @GetMapping("/mobile/profil")
    public ProfilDTO getSiswa() {
		log.debug("REST request to siswa mobile by id : {}");
    	
		ProfilDTO result = siswaRepository.userProfil((long) 9).get();
        return result;
    }

}
