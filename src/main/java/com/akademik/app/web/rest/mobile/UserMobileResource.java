package com.akademik.app.web.rest.mobile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.activation.FileTypeMap;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.akademik.app.config.ApplicationProperties;
import com.akademik.app.domain.Authority;
import com.akademik.app.domain.PhotoProfil;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.User;
import com.akademik.app.repository.PhotoProfilRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.UserService;
import com.akademik.app.web.rest.SiswaResource;

import io.github.jhipster.web.util.HeaderUtil;

@RestController
@RequestMapping("/api")
@Transactional
public class UserMobileResource {

    private final Logger log = LoggerFactory.getLogger(UserMobileResource.class);

    private final UserRepository userRepository;

    private final SiswaRepository siswaRepository;

    private final PhotoProfilRepository photoProfilRepository;
    
    private final ApplicationProperties applicationProperties;

    private final UserService userService;
	
	public UserMobileResource(UserService userService, PhotoProfilRepository photoProfilRepository, SiswaRepository siswaRepository, UserRepository userRepository, ApplicationProperties applicationProperties) {
		this.userRepository = userRepository;
		this.applicationProperties= applicationProperties;
		this.siswaRepository = siswaRepository;
		this.photoProfilRepository = photoProfilRepository;
		this.userService = userService;
	}
	
	@RequestMapping(value = "/user/upload/{id}", method = RequestMethod.POST, 
			consumes = { MediaType.MULTIPART_FORM_DATA_VALUE},
	        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> uploadFile(
    		@PathVariable Long id, @RequestParam(value = "file", required = false) MultipartFile file) throws IOException {
		log.debug("REST Request to upload {},{}",id, file);
		
		BufferedOutputStream stream = null;
    	try {
        	User user = userRepository.findById(id).get();
        	Siswa siswa = siswaRepository.findOneByUser(user);
        	if(user != null) {
        		String directory = applicationProperties.getDirectory().getUpload() + siswa.getKelas().getNama();
        		File dir = new File(directory);
        		if(!dir.exists()) dir.mkdir();
        		
        		File output = new File(dir, siswa.getNis() + ".jpg");
        		user.setImageUrl(output.getAbsolutePath());
        		userRepository.save(user);
                byte[] bytes = file.getBytes();
                if (siswa.getPhotoProfil() != null) {
                	PhotoProfil photo = photoProfilRepository.findById(siswa.getPhotoProfil().getId()).get();
		        		photo.setPhoto(bytes);
		        		photo.setPhotoContentType("png/jpg");
		        		photoProfilRepository.save(photo);
		        		siswa.setPhotoProfil(photo);
		        		siswaRepository.save(siswa);
                } else {
	        		PhotoProfil photo = new PhotoProfil();
		        		photo.setPhoto(bytes);
		        		photo.setPhotoContentType("image/jpeg");
		        		photoProfilRepository.save(photo);
		        		siswa.setPhotoProfil(photo);
		        		siswaRepository.save(siswa);
	        		}
                stream = new BufferedOutputStream(new FileOutputStream(output));
                stream.write(bytes);
                stream.close();
                return ResponseEntity.ok().body(user);
        	}
        } catch (Exception e) {
        	if(stream != null) try { stream.close(); } catch(Exception ex) {}
            log.error("UserMobileResource.uploadFile failed caused by " + e.getMessage(), e);
            return ResponseEntity.badRequest().headers(HeaderUtil.createAlert("User", id.toString(), e.getMessage())).body(null);
        }
    	
    	return null;
	}

	@RequestMapping(value = "/user/previewphoto/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_PDF_VALUE,
			MediaType.ALL_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE })
	@ResponseBody
	public FileSystemResource previewBerkas(@PathVariable Long id) {
		try {
			log.debug("prview photo user :{} " + id);

        	User user = userRepository.findById(id).get();

			String filePath = user.getImageUrl();

			File file = new File(filePath);

			return new FileSystemResource(file);
		} catch (Exception e) {
			log.error("FileResource.previewBerkasFile failed caused by " + e.getMessage(), e);
			throw e;
		}
	}
	
    @GetMapping(value = "/user/role")
    public String checkRole() {
		String login = SecurityUtils.getCurrentUserLogin().get();
        log.debug("login by:{}", login);
		User user = userRepository.findOneByLogin(login).get();
        log.debug("REST request to role");
        List<String> result = user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toList());
        String rest = null;
        for (String string : result) {
			if(!"ROLE_USER".equals(string)) rest = string;
		}
        log.debug("role:{}", rest);
        return rest;
    }
	
    @GetMapping(value = "/user/cekLogin")
    public Boolean checkLoginExisting(@RequestParam String login) {
        log.debug("Cek user: {}", login);
    	Boolean result = false;
		if (userRepository.findOneByLogin(login.toLowerCase()).isPresent()) {
			result = true;
		}
        return result;
    }
}
