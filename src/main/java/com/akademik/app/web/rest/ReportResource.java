package com.akademik.app.web.rest;

import java.io.File;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.akademik.app.domain.ReportSyahriah;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.repository.ReportSyahriahRepository;
import com.akademik.app.service.ReportService;
import com.akademik.app.service.SuratPenagihanService;
import com.akademik.app.service.TagihanService;
import com.akademik.app.service.dto.ReportDTO;
import com.akademik.app.service.dto.ReportSyahriahDTO;
import com.akademik.app.service.dto.RiwayatPembayaranDTO;
import com.akademik.app.util.DateUtils;

import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;


/**
 * REST controller for managing {@link com.akademik.app.domain.Transaksi}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ReportResource {

    private final Logger log = LoggerFactory.getLogger(ReportResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReportSyahriahRepository reportSyahriahRepository;

    private final TagihanService tagihanService;

    private final ReportService reportService;
    
    private final SuratPenagihanService suratPenagihanService;
    
	public ReportResource(SuratPenagihanService suratPenagihanService, ReportService reportService, TagihanService tagihanService, ReportSyahriahRepository reportSyahriahRepository) {
		this.reportSyahriahRepository = reportSyahriahRepository;
		this.tagihanService = tagihanService;
		this.reportService = reportService;
		this.suratPenagihanService = suratPenagihanService;
	}

    @GetMapping("/report-syahriah")
    public ResponseEntity<List<ReportSyahriahDTO>> getAllReportSyahriah(
			@RequestParam(value = "query", required = false) String query,
			Pageable pageable) {
        log.debug("REST request to get a page of Report Syahriah");
        
        Page<ReportSyahriahDTO> page = null;
        if (query != null) {
            log.debug("cari siswa by nis:{}",query);
            page = reportSyahriahRepository.findByNameSiswaDto(query, pageable);
    	} else {
        	log.debug("find All");
        	page = reportSyahriahRepository.findAllSyahriah(pageable);
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/report-syahriah/{id}")
    public ResponseEntity<ReportSyahriah> getBesarBayar(@PathVariable Long id) {
        log.debug("REST request to get BesarBayar : {}", id);
        Optional<ReportSyahriah> besarBayar = reportSyahriahRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(besarBayar);
    }
    
    /* ----Employee Performance Export To Excel---- */
    @RequestMapping(value = "/report-syahriah/download",
    		method = RequestMethod.GET,
            produces = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    @ResponseBody
    public ResponseEntity<byte[]> getReport(
    		@RequestParam(value = "kelas", required = false) String kelasId,
    		@RequestParam(value = "ta", required = false) String ta,
    		@RequestParam(value = "nis", required = false) String nis,
    		@RequestParam(value = "jenis", required = false) String jenis) throws URISyntaxException {
            log.debug("REST request to get rekap pembayaran ta:{} nis:{} jenis:{}", ta, nis, jenis);
            
            try {
            	String fileName = "RekapPembayaran.xlsx";
            	byte[] result = null;
            	 if (jenis.equals("SYAHRIAH")) {
             		result = tagihanService.buildReportSyahriah(kelasId, nis, ta);
            	 } else {
             		result = tagihanService.buildReportPembayaran(kelasId, nis, jenis, ta);
            	 }
	            if(result != null) {
	                return ResponseEntity.ok()
	                .header("Content-Disposition", "attachment; filename=" + fileName)
	                .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
	                .contentLength(result.length)
	                .body(result);
                } else {
                    return ResponseEntity.badRequest().body(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
      }

    @GetMapping("/report-syahriah/all")
    public ResponseEntity<List<ReportDTO>> getAllReport(
    		@RequestParam(value = "kelas", required = false) String kelas,
    		@RequestParam(value = "ta", required = false) String ta,
    		@RequestParam(value = "nis", required = false) String nis,
    		@RequestParam(value = "jenis", required = false) String jenis,
			Pageable pageable) {
        log.debug("REST request to get a page of Report Syahriah");
        
        Page<ReportDTO> page = reportService.reportAll(kelas, ta, nis, jenis, pageable);
        
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/report-harian")
    public ResponseEntity<List<ReportDTO>> getAllHarian(
			@RequestParam(value = "date", required = false) String date,
			Pageable pageable) {
        log.debug("REST request to get a page of Report Harian :{}",date);
        
        LocalDate pDate = ((date != null && !date.isEmpty()) ? DateUtils.parseDate(date, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
    	Instant startDate = pDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant endDate = startDate.plusSeconds(86000);
    	
        Page<ReportDTO> page = reportSyahriahRepository.generateReportByDate(startDate, endDate, pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/report-mingguan")
    public ResponseEntity<List<ReportDTO>> getAllMingguan(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			Pageable pageable) {
        log.debug("REST request to get a page of Report Mingguan start:{} s/d end:{}",startDate, endDate);
        
        LocalDate pStart = ((startDate != null && !startDate.isEmpty()) ? DateUtils.parseDate(startDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
        LocalDate pEnd = ((endDate != null && !endDate.isEmpty()) ? DateUtils.parseDate(endDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now().plusDays(1));
    	Instant _startDate = pStart.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant _endDate = pEnd.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant();
    	
        Page<ReportDTO> page = reportSyahriahRepository.generateReportByDate(_startDate, _endDate, pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/report-item")
    public ResponseEntity<List<ReportDTO>> getAllHarianItem(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			Pageable pageable) {
        log.debug("REST request to get a page of Report item start:{} s/d end:{}",startDate, endDate);
        
        LocalDate pStart = ((startDate != null && !startDate.isEmpty()) ? DateUtils.parseDate(startDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
        LocalDate pEnd = ((endDate != null && !endDate.isEmpty()) ? DateUtils.parseDate(endDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now().plusDays(1));
    	Instant _startDate = pStart.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant _endDate = pEnd.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant();
    	
        Page<ReportDTO> page = reportSyahriahRepository.generateReportByDateAndItem(_startDate, _endDate, pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/report-item/list")
    public List<ReportDTO> getAllHarianItem2(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			Pageable pageable) {
        log.debug("REST request to get a page of Report item start:{} s/d end:{}",startDate, endDate);
        
        LocalDate pStart = ((startDate != null && !startDate.isEmpty()) ? DateUtils.parseDate(startDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
        LocalDate pEnd = ((endDate != null && !endDate.isEmpty()) ? DateUtils.parseDate(endDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now().plusDays(1));
    	Instant _startDate = pStart.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant _endDate = pEnd.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant();
    	
        List<ReportDTO> page = reportSyahriahRepository.generateReportByDateAndItem2(_startDate,_endDate);
        
        return page;
    }

    @GetMapping("/report-harian/list")
    public List<ReportDTO> getAllHarian2(
			@RequestParam(value = "date", required = false) String date) {
        log.debug("REST request to get a page of Report Harian : {}", date);
        
        LocalDate pDate = ((date != null && !date.isEmpty()) ? DateUtils.parseDate(date, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
    	Instant startDate = pDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant endDate = startDate.plusSeconds(86000);
    	
        List<ReportDTO> page = reportSyahriahRepository.generateReportByDate2(startDate, endDate);
        
        return page;
    }

    @GetMapping("/report-mingguan/list")
    public List<ReportDTO> getAllMingguan2(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate) {
        log.debug("REST request to get a page of Report Mingguan list start:{} s/d end:{}",startDate, endDate);
        
        LocalDate pStart = ((startDate != null && !startDate.isEmpty()) ? DateUtils.parseDate(startDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
        LocalDate pEnd = ((endDate != null && !endDate.isEmpty()) ? DateUtils.parseDate(endDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now().plusDays(1));
    	Instant _startDate = pStart.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant _endDate = pEnd.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant();
    	
        List<ReportDTO> page = reportSyahriahRepository.generateReportByDate2(_startDate, _endDate);
        
        return page;
    }

    @GetMapping("/riwayat-pembayaran/list")
    public ResponseEntity<List<RiwayatPembayaranDTO>> getRiwayatPembayaran(
			@RequestParam(value = "siswa", required = false) String siswa,
			@RequestParam(value = "ta", required = false) String ta,
			@RequestParam(value = "status", required = false) String status,
			Pageable pageable) {
        log.debug("REST request to get a page of Report riwayat-pembayaran list siswa:{}, ta:{}, status :{}",siswa, ta, status);
        
        String thnAjaran = ta.replace('-','/');
        
        Page<RiwayatPembayaranDTO> page = reportSyahriahRepository.generateReportHistory(Long.parseLong(siswa), thnAjaran, StatusBayar.valueOf(status), pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @RequestMapping(value = "/report-harian/download",
    		method = RequestMethod.GET,
            produces = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    @ResponseBody
    public ResponseEntity<byte[]> getReportHarian(
			@RequestParam(value = "date", required = false) String date) throws URISyntaxException {
            log.debug("REST request to getReportHarian tanggal:{}", date);
            String pDate = date != null ? date: LocalDate.now().toString();
            try {
            	String fileName = "DAILYREPORT "+ pDate + ".xlsx"; 
                log.debug("fileName:{}", fileName);
            	byte[] result = null;
            	if (date !=null || !date.isEmpty() || "".equals(date)) {
            		result = reportService.buildReportTransaksiHarian(date);
            	}
	            if(result != null) {
	                return ResponseEntity.ok()
	                .header("Content-Disposition", "attachment; filename=" + fileName)
	                .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
	                .contentLength(result.length)
	                .body(result);
                } else {
                    return ResponseEntity.badRequest().body(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
      }
    
    @RequestMapping(value = "/report-mingguan/download",
    		method = RequestMethod.GET,
            produces = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    @ResponseBody
    public ResponseEntity<byte[]> getReportMingguan(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate
			) throws URISyntaxException {
            log.debug("REST request to getReportHarian tanggal {} s/d {}", startDate, endDate);
            String pStart = startDate != null ? startDate: LocalDate.now().toString();
            String pEnd = endDate != null ? endDate: LocalDate.now().toString();
            try {
            	String fileName = "WEEKLYREPORT_"+ pStart +"_s/d_"+pEnd +".xlsx"; 
                log.debug("fileName:{}", fileName);
            	byte[] result = reportService.buildReportTransaksiMingguan(startDate, endDate);
	            if(result != null) {
	                return ResponseEntity.ok()
	                .header("Content-Disposition", "attachment; filename=" + fileName)
	                .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
	                .contentLength(result.length)
	                .body(result);
                } else {
                    return ResponseEntity.badRequest().body(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
      }
    
    @RequestMapping(value = "/report-item/download",
    		method = RequestMethod.GET,
            produces = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    @ResponseBody
    public ResponseEntity<byte[]> getReportItem(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate
				) throws URISyntaxException {
            log.debug("REST request to getReportItem tanggal {} s/d {}", startDate, endDate);
            String pStart = startDate != null ? startDate: LocalDate.now().toString();
            String pEnd = endDate != null ? endDate: LocalDate.now().toString();
            try {
            	String fileName = "ITEMREPORT_"+ pStart +"_s/d_"+pEnd +".xlsx";  
                log.debug("fileName:{}", fileName);
            	byte[] result = reportService.buildReportTransaksiHarianItem(startDate, endDate);
	            if(result != null) {
	                return ResponseEntity.ok()
	                .header("Content-Disposition", "attachment; filename=" + fileName)
	                .contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
	                .contentLength(result.length)
	                .body(result);
                } else {
                    return ResponseEntity.badRequest().body(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
      }


    @RequestMapping(value = "/riwayat-pembayaran/list/download/{ta}/{siswa}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public FileSystemResource downloadtagihan(
			@PathVariable(value = "siswa", required = false) String siswa,
			@PathVariable(value = "ta", required = false) String ta) throws Exception {
        log.debug("REST request to downloadtagihan siswa: {} ta:{}", siswa, ta);
        
        String filePath = suratPenagihanService.generateSuratTagihan(siswa, ta);
        
        log.debug("filePath: {}", filePath);

        File file = new File(filePath);
        if(!file.exists()) {
            return null;
        }
        
        return new FileSystemResource(new File(filePath));
    }
}
