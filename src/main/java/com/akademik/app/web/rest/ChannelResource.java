package com.akademik.app.web.rest;

import com.akademik.app.domain.Channel;
import com.akademik.app.domain.enumeration.StatusBank;
import com.akademik.app.repository.ChannelRepository;
import com.akademik.app.service.PaymentService;
import com.akademik.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.akademik.app.domain.Channel}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ChannelResource {

    private final Logger log = LoggerFactory.getLogger(ChannelResource.class);

    private static final String ENTITY_NAME = "channel";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChannelRepository channelRepository;

    private final PaymentService paymentService;

    public ChannelResource(ChannelRepository channelRepository,PaymentService paymentService) {
        this.channelRepository = channelRepository;
        this.paymentService = paymentService;
    }

    /**
     * {@code POST  /channel} : Create a new Channel.
     *
     * @param channel the channel to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new channel, or with status {@code 400 (Bad Request)} if the channel has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/channel")
    public ResponseEntity<Channel> createChannel(@Valid @RequestBody Channel channel) throws URISyntaxException {
        log.debug("REST request to save Kelas : {}", channel);
        if (channel.getId() != null) {
            throw new BadRequestAlertException("A new channel cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Channel result = channelRepository.save(channel);
        return ResponseEntity.created(new URI("/api/channel/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /channel} : Updates an existing channel.
     *
     * @param channel the channel to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated channel,
     * or with status {@code 400 (Bad Request)} if the channel is not valid,
     * or with status {@code 500 (Internal Server Error)} if the channel couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/channel")
    public ResponseEntity<Channel> updateChannel(@Valid @RequestBody Channel channel) throws URISyntaxException {
        log.debug("REST request to update channel : {}", channel);
        if (channel.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Channel result = channelRepository.save(channel);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, channel.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /channel} : get all the channel.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of channel in body.
     */
    @GetMapping("/channel")
    public ResponseEntity<List<Channel>> getAllChannel(Pageable pageable) {
        log.debug("REST request to get a page of Channel");
        Page<Channel> page = channelRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/channel/status")
    public ResponseEntity<List<Channel>> getAllChannelAktive(Pageable pageable) {
        log.debug("REST request to get a page of Channel");
        Page<Channel> page = channelRepository.findByStatus(StatusBank.ACTIVE, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /channel/:id} : get the "id" channel.
     *
     * @param id the id of the channel to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the channel, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/channel/{id}")
    public ResponseEntity<Channel> getChannel(@PathVariable Long id) {
        log.debug("REST request to get Kelas : {}", id);
        Optional<Channel> channel = channelRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(channel);
    }

    /**
     * {@code DELETE  /channel/:id} : delete the "id" channel.
     *
     * @param id the id of the channel to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/channel/{id}")
    public ResponseEntity<Void> deleteChannel(@PathVariable Long id) {
        log.debug("REST request to delete channel : {}", id);
        channelRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    
    @PutMapping("/channel/update/{tipe}")
    public ResponseEntity<Integer> approve(@PathVariable Boolean tipe, @RequestBody Channel channel) throws URISyntaxException {
        log.debug("REST request to update {} channel : {}", tipe, channel);
        
        Channel result = channelRepository.findById(channel.getId()).get();
        if (tipe) result.setStatus(StatusBank.ACTIVE);
        if (!tipe) result.setStatus(StatusBank.INACTIVE);
        channelRepository.save(result);

        return new ResponseEntity<Integer>(result.getId().intValue(), HttpStatus.OK);
    }
    
    @PutMapping("/channel/active/{id}")
    public ResponseEntity<Integer> update(@PathVariable String id) throws URISyntaxException {
        log.debug("REST request to update id: {}", id);
        
        Channel result = channelRepository.findById(Long.parseLong(id)).get();
        if (StatusBank.ACTIVE.equals(result.getStatus())) {
        	result.setStatus(StatusBank.INACTIVE);
    	} else {
        	result.setStatus(StatusBank.ACTIVE);
    	}
        channelRepository.save(result);

        return new ResponseEntity<Integer>(result.getId().intValue(), HttpStatus.OK);
    }
}
