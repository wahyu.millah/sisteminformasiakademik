package com.akademik.app.web.rest.errors;

public class TransaksiAlreadyUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public TransaksiAlreadyUsedException() {
        super(ErrorConstants.VA_ALREADY_PAYMENT, "Virtual Account can't duplicates!", "va");
    }
}
