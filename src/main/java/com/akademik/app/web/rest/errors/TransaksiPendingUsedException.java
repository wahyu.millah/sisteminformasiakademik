package com.akademik.app.web.rest.errors;

public class TransaksiPendingUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public TransaksiPendingUsedException() {
        super(ErrorConstants.VA_PENDING, "Virtual Account pending!", "va_pending");
    }
}
