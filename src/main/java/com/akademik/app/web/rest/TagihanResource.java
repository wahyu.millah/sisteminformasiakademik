package com.akademik.app.web.rest;

import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.repository.KelasRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.AuthoritiesConstants;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.ReadingExcelService;
import com.akademik.app.service.TagihanService;
import com.akademik.app.service.dto.TagihanDTO;
import com.akademik.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * REST controller for managing {@link com.akademik.app.domain.Tagihan}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TagihanResource {

    private final Logger log = LoggerFactory.getLogger(TagihanResource.class);

    private static final String ENTITY_NAME = "tagihan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TagihanRepository tagihanRepository;

    private final TagihanService tagihanService;

    private final UserRepository userRepository;

    private final SiswaRepository siswaRepository;

    private final KelasRepository kelasRepository;

    private final ReadingExcelService readingExcelService;

    public TagihanResource(ReadingExcelService readingExcelService, KelasRepository kelasRepository, SiswaRepository siswaRepository, TagihanRepository tagihanRepository, TagihanService tagihanService, UserRepository userRepository) {
        this.tagihanRepository = tagihanRepository;
        this.tagihanService = tagihanService;
        this.userRepository = userRepository;
        this.siswaRepository = siswaRepository;
        this.kelasRepository = kelasRepository;
        this.readingExcelService= readingExcelService;
    }

    /**
     * {@code POST  /tagihans} : Create a new tagihan.
     *
     * @param tagihan the tagihan to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tagihan, or with status {@code 400 (Bad Request)} if the tagihan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tagihans")
    public ResponseEntity<Tagihan> createTagihan(@Valid @RequestBody Tagihan tagihan) throws URISyntaxException {
        log.debug("REST request to save Tagihan : {}", tagihan);
        if (tagihan.getId() != null) {
            throw new BadRequestAlertException("A new tagihan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tagihan result = tagihanRepository.save(tagihan);
        return ResponseEntity.created(new URI("/api/tagihans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tagihans} : Updates an existing tagihan.
     *
     * @param tagihan the tagihan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tagihan,
     * or with status {@code 400 (Bad Request)} if the tagihan is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tagihan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tagihans")
    public ResponseEntity<Tagihan> updateTagihan(@Valid @RequestBody Tagihan tagihan) throws URISyntaxException {
        log.debug("REST request to update Tagihan : {}", tagihan);
        if (tagihan.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Tagihan result = tagihanRepository.save(tagihan);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tagihan.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tagihans} : get all the tagihans.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tagihans in body.
     */
//    @GetMapping("/tagihans")
//    public ResponseEntity<List<Tagihan>> getAllTagihans(Pageable pageable) {
//        log.debug("REST request to get a page of Tagihans");
//        Page<Tagihan> page = tagihanRepository.findAll(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }

    /**
     * {@code GET  /tagihans/:id} : get the "id" tagihan.
     *
     * @param id the id of the tagihan to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tagihan, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tagihans/{id}")
    public ResponseEntity<Tagihan> getTagihan(@PathVariable Long id) {
        log.debug("REST request to get Tagihan : {}", id);
        Optional<Tagihan> tagihan = tagihanRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tagihan);
    }

    /**
     * {@code DELETE  /tagihans/:id} : delete the "id" tagihan.
     *
     * @param id the id of the tagihan to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tagihans/{id}")
    public ResponseEntity<Void> deleteTagihan(@PathVariable String id) {
        log.debug("REST request to delete Tagihan : {}", id);
        tagihanRepository.deleteById(Long.valueOf(id));
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    
    @DeleteMapping("/tagihans/hapus")
    public ResponseEntity<Void> hapusTagihan(@RequestParam String id) {
        log.debug("REST request to delete Tagihan : {}", id);
        tagihanRepository.deleteById(Long.valueOf(id));
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /tagihans} : get all the tagihans.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tagihans in body.
     */
    @GetMapping("/tagihans/dailyreport")
    public ResponseEntity<List<TagihanDTO>> getTagihan(
			@RequestParam(value = "date", required = false) String date,
			Pageable pageable){
        log.debug("REST request to get a page of Tagihan daily report");
		Page<TagihanDTO> page = null;
		
		if (date != null) {
			page = tagihanRepository.findTagihanByDateAndStatus(Instant.now(),StatusBayar.LUNAS, pageable);
		} else {
			page = tagihanRepository.findTagihanByDateAndStatus(Instant.now(),StatusBayar.LUNAS, pageable);
		}
		
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/tagihans")
    public ResponseEntity<List<TagihanDTO>> getAllTagihans(
			@RequestParam(value = "query", required = false) String query,
			@RequestParam(value = "kelasId", required = false) String kelasId,
			Pageable pageable) {
        log.debug("REST request to get a page of Tagihans");
		
		String login = SecurityUtils.getCurrentUserLogin().get();
		Optional<User> user = userRepository.findOneByLogin(login);
		Siswa siswa = siswaRepository.findOneByUser(user.get());
		Kelas kelas = kelasRepository.findByNama("VII").get();

		Page<TagihanDTO> page = null;
		if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.TU)) {
	        log.debug("login by TU");
	        if(query != null) {
	        	page = tagihanRepository.findAllTagihanByGroupSiswaId(query, pageable);
	        } else {
	        	page = tagihanRepository.findAllTagihanByGroupKelas(kelas.getId(), pageable);
//	        	page = tagihanRepository.findAllTagihan( pageable);
	        }
		} else if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.WALIMURID)){
	        log.debug("login by wali murid");
	        page = tagihanRepository.findAllTagihanByLogin(siswa.getId(), pageable);
		} else if(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.LOUNDRY)) {
	        log.debug("login by loundry account");
			if (query != null) {
	        	page = tagihanRepository.findAllTagihanByGroupSiswaIdLoundry(query, pageable);
			} else {
	        	page = tagihanRepository.findAllTagihanByGroupSiswaLoundry(pageable);
			}
		} else {
	        log.debug("login by Admin");
			page = tagihanRepository.findAllTagihan( pageable);
		}
		
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/tagihans/siswa")
    public ResponseEntity<List<TagihanDTO>> getEmployeId(
    		@RequestParam(value = "siswaId", required = false) String siswaId,
    		@RequestParam(value = "ta", required = false) String ta,
    		Pageable pageable) {
        log.debug("REST request to get tagihan by id siswa : {} tahunAjaran: {}", siswaId, ta);
        
        Page<TagihanDTO> page = null;
		if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.TU)) {
			if (ta != null && !"undefined".equals(ta)) {
				page = tagihanRepository.findAllTagihanByLoginAndTa(Long.parseLong(siswaId), ta, pageable);
			} else if( "undefined".equals(ta)) {
				page = tagihanRepository.findAllTagihanByLogin(Long.parseLong(siswaId), pageable);
			} else {
				page = tagihanRepository.findAllTagihanByLogin(Long.parseLong(siswaId), pageable);
			}
		} else {
			if (siswaId != null) {
				page = tagihanRepository.findAllTagihanByLoginAndLoundry(Long.parseLong(siswaId), pageable);
			} else {
				page = tagihanRepository.findAllTagihanByLoginAndLoundry(null, pageable);
			}
		}

    	HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @PostMapping("/tagihans/bulanan")
	public Tagihan tagihanBulanan(
			@RequestParam(value = "date", required = false) String pDate,
			@RequestParam(value = "tagihan", required = false) String pTagihan,
			@RequestParam(value = "bulanan", required = false) Boolean bulanan,
			@RequestParam(value = "tempo", required = false) String tempo,
			@RequestParam(value = "redaksi", required = false) String redaksi
			) throws URISyntaxException{
		log.debug("REST request to search Tagihan date...{}, {}, {}, {}, {}", pDate, pTagihan, bulanan, tempo, redaksi);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm", Locale.getDefault());
		DateTimeFormatter format2 = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.getDefault());
		LocalDate date = LocalDate.parse(pDate,pDate.contains("T")? format : format2);
		
		Tagihan tagihan = tagihanService.createMounthlyTagihan(date, pTagihan, bulanan, tempo, redaksi);
    	
        System.out.println(ResponseEntity.ok().body(tagihan));
		return tagihan;
    }
    
    @PutMapping("/tagihans/generatepartial/{id}")
	public ResponseEntity<Integer> generateTagihanPartial(@PathVariable Long id) throws URISyntaxException{
		log.debug("REST request to search Tagihan ..." );
    	
		int tagihan = tagihanService.createTagihanPartial(id);

        return new ResponseEntity<Integer>(tagihan, HttpStatus.OK);
    }
    
    @PutMapping("/tagihans/update/{id}")
    public ResponseEntity<Integer> update(@PathVariable Long id, @RequestBody(required = false) String tanggal) {
        log.debug("REST request to update tanggal : {} :{}", id, tanggal);
        
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm", Locale.getDefault());
		LocalDate date = LocalDate.parse(tanggal, format);
		Instant tglBayar = date.atStartOfDay(ZoneId.systemDefault()).toInstant();
        
        int result = tagihanRepository.updateNote(tglBayar, id);
        
        return new ResponseEntity<Integer>(result, HttpStatus.OK);
    }
    
//    public String convert(String pDate) {
//    	
//    	DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
//        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
//        LocalDate date = LocalDate.parse(pDate, inputFormatter);
//        String formattedDate = outputFormatter.format(date);
//        System.out.println(formattedDate);
//    	
//		return formattedDate;
//    }
}
