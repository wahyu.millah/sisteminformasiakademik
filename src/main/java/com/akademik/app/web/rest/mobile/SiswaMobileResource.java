package com.akademik.app.web.rest.mobile;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.PhotoProfil;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.User;
import com.akademik.app.repository.KelasRepository;
import com.akademik.app.repository.PhotoProfilRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.dto.ProfilDTO;
import com.akademik.app.service.dto.SiswaDTO;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
@Transactional
public class SiswaMobileResource {

    private final Logger log = LoggerFactory.getLogger(SiswaMobileResource.class);

    private final SiswaRepository siswaRepository;

    private final KelasRepository kelasRepository;

    private final PhotoProfilRepository photoProfilRepository;
    
    public SiswaMobileResource(PhotoProfilRepository photoProfilRepository, SiswaRepository siswaRepository, KelasRepository kelasRepository){
    	this.siswaRepository = siswaRepository;
    	this.kelasRepository = kelasRepository;
    	this.photoProfilRepository = photoProfilRepository;
    }

    @GetMapping("/mobile/siswa")
    public ResponseEntity<List<SiswaDTO>> getAllSiswas(
			@RequestParam(value = "query", required = false) String query,
			@RequestParam(value = "kelas", required = false) String kelas,
			Pageable pageable) {
        log.debug("REST request to get a page of Siswas");
        Kelas kls = null;
		if (kelas!=null) {
			kls = kelasRepository.findByNama(kelas).get();
		} else {
			kls = kelasRepository.findByNama("VII").get();
		}
        
        Page<SiswaDTO> page = null;
        if (query != null) {
            log.debug("cari siswa by nis:{}",query);
            page = siswaRepository.findByNameSiswaMobile(query, pageable);
    	} else {
            	page = siswaRepository.findAllSiswaMobile(kls.getId(),pageable);
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/siswa/detail")
	public ProfilDTO profil(
			@RequestParam(value = "id", required = false) String id,
			Pageable pageable){
		log.debug("REST request to siswa by Login..");
    	
		ProfilDTO result = siswaRepository.findByIdSiswaMobile(Long.parseLong(id));
		if (result != null) {
		log.debug("result.."+ result);
		Optional<Siswa> siswa = siswaRepository.findById(result.getSiswaId());
		log.debug("siswa.."+ siswa);
		if (siswa.isPresent()) {
			if (siswa.get().getPhotoProfil() != null ) {
				PhotoProfil photo = photoProfilRepository.findById(siswa.get().getPhotoProfil().getId()).get();
				result.setPhoto(photo.getPhoto());
				result.setPhotoContentType(photo.getPhotoContentType());
				siswa.get().setPhotoProfil(photo);
				siswaRepository.save(siswa.get());
				}
			}
		}
        return result;
    }

}
