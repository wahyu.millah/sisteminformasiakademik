package com.akademik.app.web.rest;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.akademik.app.domain.Message;
import com.akademik.app.domain.Notification;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.TahunAjaran;
import com.akademik.app.domain.TokenNotifikasi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;
import com.akademik.app.repository.MessageRepository;
import com.akademik.app.repository.NotificationRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.BlashNotifikasiService;
import com.akademik.app.service.NotificationService;
import com.akademik.app.service.dto.MessageDTO;
import com.akademik.app.service.dto.NotificationDTO;
import com.akademik.app.service.dto.TagihanDTO;

import io.github.jhipster.web.util.PaginationUtil;


/**
 * REST controller for managing {@link com.akademik.app.domain.Transaksi}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class NotificationResource {

    private final Logger log = LoggerFactory.getLogger(NotificationResource.class);

    private static final String ENTITY_NAME = "notification";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NotificationRepository notificationRepository;
    
    private final NotificationService notificationService;

    private final MessageRepository messageRepository;

    private final UserRepository userRepository;
    
    private final BlashNotifikasiService blashNotifikasiService;
    
    public NotificationResource(BlashNotifikasiService blashNotifikasiService, UserRepository userRepository, MessageRepository messageRepository, NotificationService notificationService, NotificationRepository notificationRepository) {
    	this.notificationRepository = notificationRepository;
    	this.notificationService = notificationService;
    	this.messageRepository = messageRepository;
    	this.userRepository = userRepository;
    	this.blashNotifikasiService = blashNotifikasiService;
    }
    
    @PostMapping(value = "/notification/siraajulummah", produces = MediaType.APPLICATION_JSON_VALUE)
    public Notification create(@RequestBody NotificationDTO notificationDTO) {
		log.debug("result:{}",notificationDTO);
		
		Notification result = notificationService.save(notificationDTO);
    	return result;
    }
    
    @PostMapping(value = "/notification/token", produces = MediaType.APPLICATION_JSON_VALUE)
    public TokenNotifikasi saveToken(
			@RequestParam(value = "token", required = false) String token) {
		log.debug("token device:{}",token);
		
		TokenNotifikasi result = notificationService.saveToken(token);
    	return result;
    }
    
    @PostMapping(value = "/notification/mobile/message/read", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> readingMessage(
    		@RequestParam(value = "id", required = false) String id) {
        log.debug("REST request to readingMessage id:{}", id);
        
        Message message = messageRepository.findById(Long.parseLong(id)).get();
        message.setStatus(NotificationStatus.READ);
        messageRepository.save(message);
        
        return new ResponseEntity<Message>(message, HttpStatus.OK);
    }
    
    @PostMapping(value = "/notification/sendNotifikasi", produces = MediaType.APPLICATION_JSON_VALUE)
    public void sendNotifikasi(
			@RequestParam(value = "to", required = false) String to,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "body", required = false) String body) {
		log.debug("token device:{}",title, body);
		notificationService.sendNotifikasi(to, body, title);
    }
    
    @GetMapping("/notification/mobile/message")
    public ResponseEntity<List<MessageDTO>> getAllByLogin(Pageable pageable) {
        log.debug("REST request to get a page of notifikasi");
		
		String login = SecurityUtils.getCurrentUserLogin().get();
        log.debug("login by:{}", login);
		Optional<User> user = userRepository.findOneByLogin(login);
        
        Page<MessageDTO> page = messageRepository.findByUserAndChannel(user.get().getId(), NotificationChannel.FIREBASE, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @PostMapping(value = "/notification/sendAll")
    public void sendBlashAll(
			@RequestParam(value = "content", required = false) String content,
			@RequestParam(value = "titel", required = false) String titel) {
		log.debug("REST request to send Blash All:{}", content);
		blashNotifikasiService.sendToAllNotif(content, titel);
    }
}
