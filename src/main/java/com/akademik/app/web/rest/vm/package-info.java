/**
 * View Models used by Spring MVC REST controllers.
 */
package com.akademik.app.web.rest.vm;
