package com.akademik.app.web.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akademik.app.domain.CaraPembayaran;
import com.akademik.app.domain.Channel;
import com.akademik.app.repository.CaraPembayaranRepository;
import com.akademik.app.repository.ChannelRepository;


@RestController
@RequestMapping("/api")
@Transactional
public class CaraPembayaranResource {

    private final Logger log = LoggerFactory.getLogger(TagihanResource.class);
    
    private final CaraPembayaranRepository caraPembayaranRepository;
    
    private final ChannelRepository channelRepository;
    
    public CaraPembayaranResource(ChannelRepository channelRepository, CaraPembayaranRepository caraPembayaranRepository) {
    	this.caraPembayaranRepository = caraPembayaranRepository;
    	this.channelRepository = channelRepository;
    }    
    
    @GetMapping("/cara-pembayaran/{bank}")
	public List<CaraPembayaran> getCaraPembayaranByChannel(@PathVariable String bank) {
	  log.debug("REST request to get a page of getCaraPembayaranByChannel id:{}", bank);
	  
	  Channel channel = channelRepository.findByVa(bank.toUpperCase());
	  List<CaraPembayaran> result = caraPembayaranRepository.findByChannel(channel);;
	  return result;
	}

}
