package com.akademik.app.web.rest;

import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.TahunAjaran;
import com.akademik.app.repository.KelasRepository;
import com.akademik.app.repository.TahunAjaranRepository;
import com.akademik.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.akademik.app.domain.Kelas}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TahunAjaranResource {

    private final Logger log = LoggerFactory.getLogger(TahunAjaranResource.class);

    private static final String ENTITY_NAME = "tahunAjaran";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TahunAjaranRepository tahunAjaranRepository;

    public TahunAjaranResource(TahunAjaranRepository tahunAjaranRepository) {
        this.tahunAjaranRepository = tahunAjaranRepository;
    }

    /**
     * {@code POST  /kelas} : Create a new kelas.
     *
     * @param kelas the kelas to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kelas, or with status {@code 400 (Bad Request)} if the kelas has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tahun-ajaran")
    public ResponseEntity<TahunAjaran> createTahunAjaran(@Valid @RequestBody TahunAjaran tahunAjaran) throws URISyntaxException {
        log.debug("REST request to save Tahun Ajaran : {}", tahunAjaran);
        if (tahunAjaran.getId() != null) {
            throw new BadRequestAlertException("A new kelas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TahunAjaran result = tahunAjaranRepository.save(tahunAjaran);
        return ResponseEntity.created(new URI("/api/tahun-ajaran/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kelas} : Updates an existing kelas.
     *
     * @param kelas the kelas to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kelas,
     * or with status {@code 400 (Bad Request)} if the kelas is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kelas couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tahun-ajaran")
    public ResponseEntity<TahunAjaran> updateTahunAjaran(@Valid @RequestBody TahunAjaran tahunAjaran) throws URISyntaxException {
        log.debug("REST request to update Tahun Ajaran : {}", tahunAjaran);
        if (tahunAjaran.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TahunAjaran result = tahunAjaranRepository.save(tahunAjaran);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tahunAjaran.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /kelas} : get all the kelas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kelas in body.
     */
    @GetMapping("/tahun-ajaran")
    public ResponseEntity<List<TahunAjaran>> getAllTahunAjaran(Pageable pageable) {
        log.debug("REST request to get a page of Tahun Ajaran");
        Page<TahunAjaran> page = tahunAjaranRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kelas/:id} : get the "id" kelas.
     *
     * @param id the id of the kelas to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kelas, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tahun-ajaran/{id}")
    public ResponseEntity<TahunAjaran> getTahunAjaran(@PathVariable Long id) {
        log.debug("REST request to get Tahun Ajaran : {}", id);
        Optional<TahunAjaran> tahunAjaran = tahunAjaranRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tahunAjaran);
    }

    /**
     * {@code DELETE  /kelas/:id} : delete the "id" kelas.
     *
     * @param id the id of the kelas to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tahun-ajaran/{id}")
    public ResponseEntity<Void> deleteTahunAjaran(@PathVariable Long id) {
        log.debug("REST request to delete Tahun Ajaran : {}", id);
        tahunAjaranRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
