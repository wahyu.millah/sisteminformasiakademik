package com.akademik.app.web.rest;

import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.PhotoProfil;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.JenisKelamin;
import com.akademik.app.repository.KelasRepository;
import com.akademik.app.repository.PhotoProfilRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.ReadingExcelService;
import com.akademik.app.service.SiswaService;
import com.akademik.app.service.dto.KeyValueDTO;
import com.akademik.app.service.dto.ProfilDTO;
import com.akademik.app.service.dto.SiswaDTO;
import com.akademik.app.util.DateUtils;
import com.akademik.app.web.rest.errors.BadRequestAlertException;
import com.akademik.app.web.rest.errors.LoginAlreadyUsedException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing {@link com.akademik.app.domain.Siswa}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class SiswaResource {

    private final Logger log = LoggerFactory.getLogger(SiswaResource.class);

    private static final String ENTITY_NAME = "siswa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhotoProfilRepository photoProfilRepository;

    private final SiswaRepository siswaRepository;

    private final UserRepository userRepository;

    private final SiswaService siswaService;

    private final KelasRepository kelasRepository;

    private final CacheManager cacheManager;

    private final PasswordEncoder passwordEncoder;

    private final ReadingExcelService readingExcelService;

    public SiswaResource(ReadingExcelService readingExcelService, PasswordEncoder passwordEncoder, CacheManager cacheManager, PhotoProfilRepository photoProfilRepository, KelasRepository kelasRepository,SiswaService siswaService, SiswaRepository siswaRepository, UserRepository userRepository) {
        this.siswaRepository = siswaRepository;
        this.userRepository = userRepository;
        this.siswaService =  siswaService;
        this.kelasRepository =  kelasRepository;
        this.photoProfilRepository = photoProfilRepository;
        this.cacheManager = cacheManager;
        this.passwordEncoder = passwordEncoder;
        this.readingExcelService= readingExcelService;
    }

    /**
     * {@code POST  /siswas} : Create a new siswa.
     *
     * @param siswa the siswa to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new siswa, or with status {@code 400 (Bad Request)} if the siswa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/siswas")
    public ResponseEntity<Siswa> createSiswa(@Valid @RequestBody SiswaDTO siswadto) throws URISyntaxException {
        log.debug("REST request to save Siswa : {}", siswadto);
        if (siswadto.getUserId() != null) {
            throw new BadRequestAlertException("A new siswa cannot already have an ID", ENTITY_NAME, "idexists");
        } else if (userRepository.findOneByLogin(siswadto.getNoTelephone()).isPresent()) {
            throw new LoginAlreadyUsedException();
        } else {
            Siswa result = siswaService.create(siswadto);
            return ResponseEntity.created(new URI("/api/siswas/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
        }
    }

    /**
     * {@code PUT  /siswas} : Updates an existing siswa.
     *
     * @param siswa the siswa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated siswa,
     * or with status {@code 400 (Bad Request)} if the siswa is not valid,
     * or with status {@code 500 (Internal Server Error)} if the siswa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/siswas")
    public ResponseEntity<Siswa> updateSiswa(@Valid @RequestBody SiswaDTO siswaDTO) throws URISyntaxException {
        log.debug("REST request to update Siswa : {}", siswaDTO);
        if (siswaDTO.getSiswaId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Siswa result = siswaService.update(siswaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, siswaDTO.getSiswaId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /siswas} : get all the siswas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of siswas in body.
     */

    @GetMapping("/siswas")
    public ResponseEntity<List<SiswaDTO>> getAllSiswas(
			@RequestParam(value = "query", required = false) String query,
			@RequestParam(value = "kelas", required = false) String kelas,
			Pageable pageable) {
        log.debug("REST request to get a page of Siswas");
        Kelas kls = null;
		if (kelas!=null) {
			kls = kelasRepository.findByNama(kelas).get();
		} else {
			kls = kelasRepository.findByNama("VII").get();
		}
        
        Page<SiswaDTO> page = null;
        if (query != null) {
            log.debug("cari siswa by nis:{}",query);
            page = siswaRepository.findByNameSiswaDto(query, pageable);
    	} else {
            	page = siswaRepository.findAllSiswaAndKelas(kls.getId(),pageable);
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /siswas/:id} : get the "id" siswa.
     *
     * @param id the id of the siswa to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the siswa, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/siswas/{id}")
    public ResponseEntity<Siswa> getSiswa(@PathVariable Long id) {
        log.debug("REST request to get Siswa : {}", id);
        Optional<Siswa> siswa = siswaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(siswa);
    }

    /**
     * {@code DELETE  /siswas/:id} : delete the "id" siswa.
     *
     * @param id the id of the siswa to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @PutMapping("/siswas/{id}")
    public ResponseEntity<Void> deleteSiswa(@PathVariable Long id) {
        log.debug("REST request to reset password Siswa : {}", id);
        User user = userRepository.findById(id).get();
        	user.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
        	siswaService.clearUserCaches(user);
        userRepository.save(user);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/siswas/profil")
	public ProfilDTO profil(){
		log.debug("REST request to siswa by Login..");
		
		String login = SecurityUtils.getCurrentUserLogin().get();
		log.debug("Login.."+ login);
		User user = userRepository.findOneByLogin(login).get();
		log.debug("user.."+ user.getId());
    	
		ProfilDTO result = siswaRepository.userProfil(user.getId()).get();
		if (result != null) {
		log.debug("result.."+ result);
		Siswa siswa = siswaRepository.findById(result.getSiswaId()).get();
		log.debug("siswa.."+ siswa);
		if (siswa.getPhotoProfil() != null) {
			PhotoProfil photo = photoProfilRepository.findById(siswa.getPhotoProfil().getId()).get();
			result.setPhoto(photo.getPhoto());
			result.setPhotoContentType(photo.getPhotoContentType());
			siswa.setPhotoProfil(photo);
			siswaRepository.save(siswa);
		}
		}
        return result;
    }
    
    @GetMapping("/siswas/profilMobile")
	public ProfilDTO profilMobile(@RequestParam String idSiswa){
		log.debug("REST request to siswa by Login..");
		
		Siswa siswa = siswaRepository.findById(Long.parseLong(idSiswa)).get();
		
		ProfilDTO result = siswaRepository.userProfil(siswa.getUser().getId()).get();
		if (result != null) {
		log.debug("result.."+ result);
		log.debug("siswa.."+ siswa);
			if (siswa.getPhotoProfil() != null) {
				PhotoProfil photo = photoProfilRepository.findById(siswa.getPhotoProfil().getId()).get();
				result.setPhoto(photo.getPhoto());
				result.setPhotoContentType(photo.getPhotoContentType());
				siswa.setPhotoProfil(photo);
				siswaRepository.save(siswa);
				}
		}
        return result;
    }
    
    @PostMapping("/siswa/update")
    public ResponseEntity<ProfilDTO> updateSiswa(
    		@RequestParam String idSiswa,
    		@RequestParam String key,
    		@RequestParam String value) {
        log.debug("REST request to update siswa:{}, key:{}, value:{}", idSiswa, key, value);
        
        Siswa siswa = siswaRepository.getOne(Long.parseLong(idSiswa));
		User user = userRepository.findById(siswa.getUser().getId()).get();
		
        if (key != null && "nama".equals(key)) siswa.setNama(value);
        if (key != null && "nis".equals(key)) siswa.setNis(value);
        if (key != null && "alamat".equals(key)) siswa.setAlamat(value);
        if (key != null && "madrasahAsal".equals(key)) siswa.setMadrasahAsal(value);
        if (key != null && "kelasMasuk".equals(key)) siswa.setKelasMasuk(value);
        if (key != null && "namaAyah".equals(key)) siswa.setNamaAyah(value);
        if (key != null && "pekerjaanAyah".equals(key)) siswa.setPekerjaanAyah(value);
        if (key != null && "namaIbu".equals(key)) siswa.setNamaIbu(value);
        if (key != null && "pekerjaanIbu".equals(key)) siswa.setPekerjaanIbu(value);
        if (key != null && "alamatOrangtua".equals(key)) siswa.setAlamatOrangtua(value);
        if (key != null && "noTelephone".equals(key)) siswa.setNoTelephone(value);
        if (key != null && "namaWali".equals(key)) siswa.setNamaWali(value);
        if (key != null && "alamatWali".equals(key)) siswa.setAlamatWali(value);
        if (key != null && "nomorTelpWali".equals(key)) siswa.setNomorTelpWali(value);
        if (key != null && "pekerjaanWali".equals(key)) siswa.setPekerjaanWali(value);
        if (key != null && "tempatLahir".equals(key)) {
        	if (value !=null && !"".equals(value))									
            	siswa.setTempatLahir(value);
        	};
        if (key != null && "jenisKelamin".equals(key)) siswa.setJenisKelamin((value.equals(JenisKelamin.LAKILAKI.toString()) ? JenisKelamin.LAKILAKI : JenisKelamin.PEREMPUAN));
        if (key != null && "anakKe".equals(key)) siswa.setAnakKe(value);
        if (key != null && "agama".equals(key)) siswa.setAgama(value);
        if (key != null && "kelasMasuk".equals(key)) siswa.setKelasMasuk(value);
        if (key != null && "tglMasuk".equals(key)) {
        	if (value != null && !"".equals(value)) {
            	LocalDate tglMasuk = DateUtils.parseDate(value, DateUtils.FORMATTER_ISO_LOCAL_DATE_V2);
                log.debug("tglMasuk : {}", tglMasuk.plusDays(1));
            	siswa.setTglMasuk(tglMasuk.plusDays(1));	
        	};
        }
        if (key != null && "tanggalLahir".equals(key)) {
        	if (value != null && !"".equals(value)) {
            	LocalDate tglLahir = DateUtils.parseDate(value, DateUtils.FORMATTER_ISO_LOCAL_DATE_V2);
            	Instant tglLhr = tglLahir.atStartOfDay(ZoneId.systemDefault()).toInstant();
            	siswa.setTanggalLahir(tglLhr);
        	}	
        }
        if (key != null && "beasiswa".equals(key)) siswa.setBeasiswa(Boolean.parseBoolean(value));
        if (key != null && "loundry".equals(key)) siswa.setLoundry(Boolean.parseBoolean(value));
        if (key != null && "status".equals(key) && "true".equals(value)) siswa.setStatus("AKTIF");
        if (key != null && "status".equals(key) && "false".equals(value)) siswa.setStatus("NONAKTIF");

        this.clearUserCaches(user);
        if (key != null && "login".equals(key)) user.setLogin(value);
        if (key != null && "password".equals(key)) user.setPassword(passwordEncoder.encode(value));
        this.clearUserCaches(user);
    	
		ProfilDTO result = siswaRepository.userProfil(user.getId()).get();

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, idSiswa))
            .body(result);
    }
    
    @PutMapping("/siswa/import")
    public void test(
			@RequestParam(value = "file", required = false) String file) throws IOException {
    	readingExcelService.reading(file);        
    }
    


    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) {
            Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
        }
    }
    
    @GetMapping("/siswas/keyvalue")
    public List<KeyValueDTO> getEmployeeKeyValue() {
        log.debug("REST request to get all Employees key-value");
        return  siswaRepository.findSiswaKeyValueAll();
    }
}
