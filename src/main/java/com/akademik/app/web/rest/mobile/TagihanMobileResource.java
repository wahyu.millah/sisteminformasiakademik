package com.akademik.app.web.rest.mobile;

import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.akademik.app.domain.BesarBayar;
import com.akademik.app.domain.Channel;
import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.TahunAjaran;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.StatusBank;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.repository.ApplicationConfigRepository;
import com.akademik.app.repository.BesarBayarRepository;
import com.akademik.app.repository.KelasRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.repository.TahunAjaranRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.SlackInhookService;
import com.akademik.app.service.TransaksiService;
import com.akademik.app.service.dto.JenisPembayaranDTO;
import com.akademik.app.service.dto.TagihanDTO;
import com.akademik.app.util.LogUtils;
import com.akademik.app.web.rest.SiswaResource;

import io.github.jhipster.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
@Transactional
public class TagihanMobileResource {

    private final Logger log = LoggerFactory.getLogger(TagihanMobileResource.class);

    private final TagihanRepository tagihanRepository;

    private final UserRepository userRepository;

    private final SiswaRepository siswaRepository;

    private final TransaksiService transaksiService;

    private final TahunAjaranRepository tahunAjaranRepository;

    private final BesarBayarRepository besarBayarRepository;

    private final ApplicationConfigRepository applicationConfigRepository;
    
    private final SlackInhookService slackService;

    public TagihanMobileResource(ApplicationConfigRepository applicationConfigRepository, SlackInhookService slackService, BesarBayarRepository besarBayarRepository,
    		TahunAjaranRepository tahunAjaranRepository, TransaksiService transaksiService, SiswaRepository siswaRepository, UserRepository userRepository, TagihanRepository tagihanRepository) {
        this.tagihanRepository = tagihanRepository;
        this.userRepository = userRepository;
        this.siswaRepository = siswaRepository;
        this.transaksiService = transaksiService;
        this.besarBayarRepository = besarBayarRepository;
        this.tahunAjaranRepository = tahunAjaranRepository;
        this.applicationConfigRepository = applicationConfigRepository;
        this.slackService = slackService;
    }
    
    // list tagihan
    @GetMapping("/mobile/tagihan")
    public ResponseEntity<List<TagihanDTO>> getAllTagihans(@RequestParam String ta,
			Pageable pageable) {
        log.debug("REST request to get a page of Tagihans:{}", ta);
        log.debug("defaultTahunAjaran on:{}", defaultTahunAjaran());
		
		String login = SecurityUtils.getCurrentUserLogin().get();
        log.debug("login by:{}", login);
		Optional<User> user = userRepository.findOneByLogin(login);
		Siswa siswa = siswaRepository.findOneByUser(user.get());
		
        List<StatusBayar> defaultStatusList = new ArrayList<StatusBayar>();
        defaultStatusList.add(StatusBayar.MENUNGGU);
        defaultStatusList.add(StatusBayar.BELUMBAYAR);
        TahunAjaran tahunAjaran = null;
        if (ta != null && !ta.equals("")) {
            tahunAjaran = tahunAjaranRepository.findById(Long.parseLong(ta)).get();
        }
        
        Page<TagihanDTO> page = tagihanRepository.findAllTagihanByLoginAndStatus(siswa.getId(), defaultStatusList, ta != null && !ta.equals("") ? tahunAjaran.getNama() :  defaultTahunAjaran(), pageable);;
		
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/mobile/tagihan-multi")
    public ResponseEntity<List<TagihanDTO>> getMulti(@RequestParam String ta,
			Pageable pageable) {
        log.debug("REST request to get a page of Tagihans:{}", ta);
        log.debug("defaultTahunAjaran on:{}", defaultTahunAjaran());
		
		String login = SecurityUtils.getCurrentUserLogin().get();
        log.debug("login by:{}", login);
		Optional<User> user = userRepository.findOneByLogin(login);
		Siswa siswa = siswaRepository.findOneByUser(user.get());
		
        List<StatusBayar> defaultStatusList = new ArrayList<StatusBayar>();
        defaultStatusList.add(StatusBayar.BELUMBAYAR);
        TahunAjaran tahunAjaran = null;
        if (ta != null && !ta.equals("")) {
            tahunAjaran = tahunAjaranRepository.findById(Long.parseLong(ta)).get();
        }
        
        Page<TagihanDTO> page = tagihanRepository.findAllTagihanByLoginAndStatus(siswa.getId(), defaultStatusList, ta != null && !ta.equals("") ? tahunAjaran.getNama() :  defaultTahunAjaran(), pageable);;
		
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    // list history tagihan    
    @GetMapping("/mobile/tagihan/history")
    public ResponseEntity<List<TagihanDTO>> getAllHitsoryTagihan(@RequestParam String ta,
			Pageable pageable) {
        log.debug("REST request to get a page of Tagihans:{}", ta);
        log.debug("defaultTahunAjaran on:{}", defaultTahunAjaran());
		
		String login = SecurityUtils.getCurrentUserLogin().get();
        log.debug("login by:{}", login);
		Optional<User> user = userRepository.findOneByLogin(login);
		Siswa siswa = siswaRepository.findOneByUser(user.get());
		
        List<StatusBayar> defaultStatusList = new ArrayList<StatusBayar>();
        defaultStatusList.add(StatusBayar.LUNAS);
        defaultStatusList.add(StatusBayar.BEASISWA);

        TahunAjaran tahunAjaran = null;
        if (ta != null && !ta.equals("")) {
            tahunAjaran = tahunAjaranRepository.findById(Long.parseLong(ta)).get();
        }
	
		Page<TagihanDTO> page = tagihanRepository.findAllTagihanByLoginAndStatus(siswa.getId(), defaultStatusList,  ta != null && !ta.equals("") ? tahunAjaran.getNama() :  defaultTahunAjaran(), pageable);
		
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/mobile/tagihan/siswa")
    public List<TagihanDTO> getAllTagihans(@RequestParam String siswaid, @RequestParam String ta) {
        log.debug("REST request to get a page of Tagihans:{},{}", siswaid, ta);
		
		Siswa siswa = siswaRepository.findById(Long.parseLong(siswaid)).get();
		
        TahunAjaran tahunAjaran = null;
        if (ta != null && !ta.equals("")) {
            tahunAjaran = tahunAjaranRepository.findById(Long.parseLong(ta)).get();
        }
        
        List<TagihanDTO> result = tagihanRepository.findAllTagihanBySiswaId(siswa.getId(), ta != null && !ta.equals("") ? tahunAjaran.getNama() :  defaultTahunAjaran());

        log.debug("result tagihan:{}", result.size());
        return result;
    }
    
    @GetMapping("/mobile/tagihans/{id}")
    public Optional<TagihanDTO> getDetailTagihanNotLunas(
    		@PathVariable Long id) {
        log.debug("REST request to get a page of Tagihans id:{}", id);
	
		Optional<TagihanDTO> result = tagihanRepository.findDetailTagihanByLoginAndStatusNotLunas(id);
        return result;
    }
    
    // online payment
    @PostMapping("/mobile/create-va/{type}/{id}/{va}")
    public Transaksi createTransaksiTransfer(
    		@PathVariable String type,
    		@PathVariable String id,
    		@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to create va id tagihan : {} va :{} type: {}", id, va, type);
        
        TagihanDTO tagihanDTO = null;
        Transaksi transaksi = null;
        
        if ("single".equals(type)) {
            if (id != null) {
                tagihanDTO = tagihanRepository.findTagihanById(Long.parseLong(id)).get();
            }
            transaksi = transaksiService.createTransaksiMobile(tagihanDTO, va);
        } else {
        	String[] idTagihan = id.substring(1, id.length()).split(",");
        	transaksi = transaksiService.createMultiTransaksi(idTagihan, va);
        }
        
        StringBuilder sb = new StringBuilder();
		sb.append("\n* Create Transaksi *");
		sb.append("\nnama      " + transaksi.getUpdateBy());
		sb.append("\nva       " + transaksi.getVirtualAccount());
		sb.append("\nstatus     " + transaksi.getStatus());
		sb.append("\nbayar     " + transaksi.getJmlBayar());
		sb.append("\nbank     " + va);

		LogUtils.debug(sb.toString(), log, slackService);
        
        return transaksi;
    }
    
    // online payment
    @PostMapping("/mobile/create-va/{id}/{va}")
    public Transaksi createTransaksiMultiTransfer(
    		@PathVariable String id,
    		@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to create va id tagihan : {} va :{}", id, va);
        
        TagihanDTO tagihanDTO = null;
        if (id != null) {
            tagihanDTO = tagihanRepository.findTagihanById(Long.parseLong(id)).get();
        }
        
        Transaksi transaksi =  transaksiService.createTransaksiMobile(tagihanDTO, va);

        StringBuilder sb = new StringBuilder();
		sb.append("\n* Create Transaksi *");
		sb.append("\nnama      " + transaksi.getUpdateBy());
		sb.append("\nva       " + transaksi.getVirtualAccount());
		sb.append("\nstatus    " + transaksi.getStatus());
		sb.append("\nbayar     " + transaksi.getJmlBayar());
		sb.append("\nbank     " + va);

		LogUtils.debug(sb.toString(), log, slackService);
        return transaksi;
    }
    
    public String defaultTahunAjaran() {
        
        LocalDate now = LocalDate.now();
        int nowMonth = now.getMonthValue();
        int nowYear = now.getYear();
    	
        String ta = null;
        if (nowMonth < 7) {
        	ta = String.valueOf(nowYear-1)+"/"+String.valueOf(nowYear);
        } else {
        	ta = String.valueOf(nowYear)+"/"+String.valueOf(nowYear + 1);
        }
    	
    	return ta;
    }
    
    @GetMapping("/mobile/tagihans/besar-bayars")
    public List<JenisPembayaranDTO> getAllBesarBayars(@RequestParam String siswaid) {
        log.debug("REST request to get a page of BesarBayars:{}", siswaid);
        Kelas kelas = siswaRepository.findById(Long.parseLong(siswaid)).get().getKelas();
        List<JenisPembayaranDTO> result = besarBayarRepository.findAllJenisPembayaran(kelas.getId());;
        
        return result;
    }
    
    @GetMapping("/mobile/tagihans/jenis-bayar")
    public List<JenisPembayaranDTO> getAllJenisBayar() {
        log.debug("REST request to get a page of jenis bayar");
        List<JenisPembayaranDTO> result = besarBayarRepository.findAllJenisPembayaran2();;
        
        return result;
    }

    
    @PutMapping("/mobile/tagihans/update")
    public Tagihan update(@RequestParam String id, @RequestParam String status) {
        log.debug("REST request to update:{}", id);
        
        Tagihan tagihan = tagihanRepository.findById(Long.parseLong(id)).get();
        	tagihan.setStatus(StatusBayar.valueOf(status));
        	tagihanRepository.save(tagihan);
        
        return tagihan;
    }
    
    @PutMapping("/mobile/tagihans/createOne")
    public Tagihan createOne(@Valid @RequestBody  JenisPembayaranDTO jenisPembayaranDTO) {
        log.debug("REST request to createOne:{}", jenisPembayaranDTO);
        Siswa siswa = siswaRepository.findById(jenisPembayaranDTO.getSiswaId()).get();
        BesarBayar bb = besarBayarRepository.findById(jenisPembayaranDTO.getBesarBayarId()).get();
        String item = null;
        
        Tagihan tagihan = new Tagihan();
        	tagihan.setHarusBayar(jenisPembayaranDTO.getNominal());
        	tagihan.setSiswa(siswa);
        	tagihan.setTotal(jenisPembayaranDTO.getPembayaran());
        	tagihan.setTahunAjaran(siswa.getTahunAjaran());
        	tagihan.setStatus(StatusBayar.BELUMBAYAR);
        	tagihan.setBesarBayar(bb);
        	tagihan.setNomor("SU"+System.currentTimeMillis());
        	tagihan.setIsPublish(0);
			if(jenisPembayaranDTO.getPembayaran().contains("SYAHRIAH")) {
				item = jenisPembayaranDTO.getPembayaran().split("-")[0];
			} else if(jenisPembayaranDTO.getPembayaran().contains("LOUNDRY")) {
				item = jenisPembayaranDTO.getPembayaran().split("-")[0];
			} else {
				item = jenisPembayaranDTO.getPembayaran();
			}
			tagihan.setItemTagihan(item);
        	tagihanRepository.save(tagihan);

            StringBuilder sb = new StringBuilder();
    		sb.append("\n* Create Tagihan *");
    		sb.append("\nnama      " + tagihan.getTotal());
    		sb.append("\nva       " + tagihan.getHarusBayar());
    		sb.append("\nsiswa     " + siswa.getNama());
    		sb.append("\nkelas     " + siswa.getKelas().getNama());

    		LogUtils.debug(sb.toString(), log, slackService);
        
        return tagihan;
    }
    
    @PostMapping("/mobile/tagihans/updatemulti/{id}")
    public ResponseEntity<Integer> multi(@PathVariable String id) throws URISyntaxException {
        log.debug("REST request to multi {}", id);
        
        Tagihan result = tagihanRepository.findById(Long.parseLong(id)).get();
        if (result.getMulti()) {
        	result.setMulti(false);
        } else {
        	result.setMulti(true);
        }

        return new ResponseEntity<Integer>(result.getId().intValue(), HttpStatus.OK);
    }
    
    @GetMapping("/mobile/tagihan/multi")
    public ResponseEntity<List<TagihanDTO>> getAllMulti(@RequestParam String ta,
			Pageable pageable) {
        log.debug("REST request to get a page of Tagihans:{}", ta);
        log.debug("defaultTahunAjaran on:{}", defaultTahunAjaran());
		
		String login = SecurityUtils.getCurrentUserLogin().get();
        log.debug("login by:{}", login);
		Optional<User> user = userRepository.findOneByLogin(login);
		Siswa siswa = siswaRepository.findOneByUser(user.get());
		
        List<StatusBayar> defaultStatusList = new ArrayList<StatusBayar>();
        defaultStatusList.add(StatusBayar.BELUMBAYAR);
        TahunAjaran tahunAjaran = null;
        if (ta != null && !ta.equals("")) {
            tahunAjaran = tahunAjaranRepository.findById(Long.parseLong(ta)).get();
        }
        
        Page<TagihanDTO> page = tagihanRepository.findAllMulti(siswa.getId(), defaultStatusList, ta != null && !ta.equals("") ? tahunAjaran.getNama() :  defaultTahunAjaran(), pageable);;
		
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/mobile/tagihan/btmulti")
    public Boolean activeButtonMulti() {
        
        Boolean button = Boolean.parseBoolean(applicationConfigRepository.findByCategoryAndName("MOBILE_ACTION", "BUTTON_MULTI").getValue());
    	
    	return button;
    }

}
