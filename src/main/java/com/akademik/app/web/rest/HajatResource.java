package com.akademik.app.web.rest;

import com.akademik.app.domain.Hajat;
import com.akademik.app.repository.HajatRepository;
import com.akademik.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional; 
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.app.undangan.domain.Hajat}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class HajatResource {

    private final Logger log = LoggerFactory.getLogger(HajatResource.class);

    private static final String ENTITY_NAME = "hajat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HajatRepository hajatRepository;

    public HajatResource(HajatRepository hajatRepository) {
        this.hajatRepository = hajatRepository;
    }

    /**
     * {@code POST  /hajats} : Create a new hajat.
     *
     * @param hajat the hajat to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hajat, or with status {@code 400 (Bad Request)} if the hajat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hajats")
    public ResponseEntity<Hajat> createHajat(@RequestBody Hajat hajat) throws URISyntaxException {
        log.debug("REST request to save Hajat : {}", hajat);
        if (hajat.getId() != null) {
            throw new BadRequestAlertException("A new hajat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Hajat result = hajatRepository.save(hajat);
        return ResponseEntity.created(new URI("/api/hajats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /hajats} : Updates an existing hajat.
     *
     * @param hajat the hajat to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hajat,
     * or with status {@code 400 (Bad Request)} if the hajat is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hajat couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hajats")
    public ResponseEntity<Hajat> updateHajat(@RequestBody Hajat hajat) throws URISyntaxException {
        log.debug("REST request to update Hajat : {}", hajat);
        if (hajat.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Hajat result = hajatRepository.save(hajat);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, hajat.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /hajats} : get all the hajats.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hajats in body.
     */
    @GetMapping("/hajats")
    public ResponseEntity<List<Hajat>> getAllHajats(Pageable pageable) {
        log.debug("REST request to get a page of Hajats");
        Page<Hajat> page = hajatRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /hajats/:id} : get the "id" hajat.
     *
     * @param id the id of the hajat to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hajat, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hajats/{id}")
    public ResponseEntity<Hajat> getHajat(@PathVariable Long id) {
        log.debug("REST request to get Hajat : {}", id);
        Optional<Hajat> hajat = hajatRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(hajat);
    }

    /**
     * {@code DELETE  /hajats/:id} : delete the "id" hajat.
     *
     * @param id the id of the hajat to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hajats/{id}")
    public ResponseEntity<Void> deleteHajat(@PathVariable Long id) {
        log.debug("REST request to delete Hajat : {}", id);
        hajatRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
