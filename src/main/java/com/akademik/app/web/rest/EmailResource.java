package com.akademik.app.web.rest;

import java.time.Instant;
import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akademik.app.domain.Notification;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.repository.BesarBayarRepository;
import com.akademik.app.repository.NotificationRepository;
import com.akademik.app.service.KwitansiService;
import com.akademik.app.service.MailService;
import com.akademik.app.service.NotificationService;
import com.akademik.app.service.SuratPenagihanService;
import com.akademik.app.service.dto.NotificationDTO;

/**
 * REST controller for managing {@link com.akademik.app.domain.Transaksi}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class EmailResource {

    private final Logger log = LoggerFactory.getLogger(EmailResource.class);

    private static final String ENTITY_NAME = "email";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    
    private final MailService mailService;
    
    private final KwitansiService kwitansiService;
    
    private final SuratPenagihanService suratPenagihanService;
    
    public EmailResource(SuratPenagihanService suratPenagihanService,MailService mailService, KwitansiService kwitansiService) {
    	this.mailService = mailService;
    	this.kwitansiService = kwitansiService;
    	this.suratPenagihanService = suratPenagihanService;
    }
    
    @PostMapping(value = "/email/send", produces = MediaType.APPLICATION_JSON_VALUE)
    public void send() {
		log.debug("Rest Send Email Api");
		
		mailService.sendEmailFromTemplateSukses("wahyu.millah@gmail.com", "Test Email", null, null, null, "paidDate", "mail/transaksiSukses");
    }
    
    @PostMapping(value = "/email/kwitansi", produces = MediaType.APPLICATION_JSON_VALUE)
    public void generate() {
		log.debug("Generate Kwitansi");
		
		kwitansiService.generateKwitansi();
    }
    
    @PostMapping(value = "/email/tagihan", produces = MediaType.APPLICATION_JSON_VALUE)
    public void generateSuratTagihan() {
		log.debug("Generate Kwitansi");
		
//		suratPenagihanService.generateKwitansi();
    }
}
