package com.akademik.app.web.rest;

import com.akademik.app.domain.JenisPembayaran;
import com.akademik.app.repository.JenisPembayaranRepository;
import com.akademik.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.akademik.app.domain.JenisPembayaran}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class JenisPembayaranResource {

    private final Logger log = LoggerFactory.getLogger(JenisPembayaranResource.class);

    private static final String ENTITY_NAME = "jenisPembayaran";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JenisPembayaranRepository jenisPembayaranRepository;

    public JenisPembayaranResource(JenisPembayaranRepository jenisPembayaranRepository) {
        this.jenisPembayaranRepository = jenisPembayaranRepository;
    }

    /**
     * {@code POST  /jenis-pembayarans} : Create a new jenisPembayaran.
     *
     * @param jenisPembayaran the jenisPembayaran to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new jenisPembayaran, or with status {@code 400 (Bad Request)} if the jenisPembayaran has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/jenis-pembayarans")
    public ResponseEntity<JenisPembayaran> createJenisPembayaran(@RequestBody JenisPembayaran jenisPembayaran) throws URISyntaxException {
        log.debug("REST request to save JenisPembayaran : {}", jenisPembayaran);
        if (jenisPembayaran.getId() != null) {
            throw new BadRequestAlertException("A new jenisPembayaran cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JenisPembayaran result = jenisPembayaranRepository.save(jenisPembayaran);
        return ResponseEntity.created(new URI("/api/jenis-pembayarans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /jenis-pembayarans} : Updates an existing jenisPembayaran.
     *
     * @param jenisPembayaran the jenisPembayaran to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated jenisPembayaran,
     * or with status {@code 400 (Bad Request)} if the jenisPembayaran is not valid,
     * or with status {@code 500 (Internal Server Error)} if the jenisPembayaran couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/jenis-pembayarans")
    public ResponseEntity<JenisPembayaran> updateJenisPembayaran(@RequestBody JenisPembayaran jenisPembayaran) throws URISyntaxException {
        log.debug("REST request to update JenisPembayaran : {}", jenisPembayaran);
        if (jenisPembayaran.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JenisPembayaran result = jenisPembayaranRepository.save(jenisPembayaran);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, jenisPembayaran.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /jenis-pembayarans} : get all the jenisPembayarans.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of jenisPembayarans in body.
     */
    @GetMapping("/jenis-pembayarans")
    public ResponseEntity<List<JenisPembayaran>> getAllJenisPembayarans(Pageable pageable) {
        log.debug("REST request to get a page of JenisPembayarans");
        Page<JenisPembayaran> page = jenisPembayaranRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /jenis-pembayarans/:id} : get the "id" jenisPembayaran.
     *
     * @param id the id of the jenisPembayaran to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the jenisPembayaran, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/jenis-pembayarans/{id}")
    public ResponseEntity<JenisPembayaran> getJenisPembayaran(@PathVariable Long id) {
        log.debug("REST request to get JenisPembayaran : {}", id);
        Optional<JenisPembayaran> jenisPembayaran = jenisPembayaranRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jenisPembayaran);
    }

    /**
     * {@code DELETE  /jenis-pembayarans/:id} : delete the "id" jenisPembayaran.
     *
     * @param id the id of the jenisPembayaran to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/jenis-pembayarans/{id}")
    public ResponseEntity<Void> deleteJenisPembayaran(@PathVariable Long id) {
        log.debug("REST request to delete JenisPembayaran : {}", id);
        jenisPembayaranRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
