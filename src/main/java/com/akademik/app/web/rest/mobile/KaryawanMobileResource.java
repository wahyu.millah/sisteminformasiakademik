package com.akademik.app.web.rest.mobile;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akademik.app.domain.Karyawan;
import com.akademik.app.domain.PhotoProfil;
import com.akademik.app.domain.User;
import com.akademik.app.repository.KaryawanRepository;
import com.akademik.app.repository.PhotoProfilRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.dto.ProfilDTO;

@RestController
@RequestMapping("/api")
@Transactional
public class KaryawanMobileResource {

    private final Logger log = LoggerFactory.getLogger(KaryawanMobileResource.class);

    private static final String ENTITY_NAME = "karyawan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PhotoProfilRepository photoProfilRepository;

    private final UserRepository userRepository;

    private final KaryawanRepository karyawanRepository;
    
    public KaryawanMobileResource(UserRepository userRepository, PhotoProfilRepository photoProfilRepository, KaryawanRepository karyawanRepository) {        
	    this.userRepository = userRepository;
	    this.photoProfilRepository = photoProfilRepository;
	    this.karyawanRepository = karyawanRepository;
    }
    
    @GetMapping("/karyawan/profil")
	public ProfilDTO profil(){
		log.debug("REST request to siswa by karyawan..");
		
		String login = SecurityUtils.getCurrentUserLogin().get();
		log.debug("Login.."+ login);
		User user = userRepository.findOneByLogin(login).get();
		log.debug("user.."+ user.getId());
    	
		Optional<ProfilDTO> result = karyawanRepository.profil(user.getId());
			if (result != null && result.isPresent()) {
			log.debug("result.."+ result);
			Karyawan karyawan = karyawanRepository.findById(result.get().getKaryawanId()).get();
				log.debug("karyawan.."+ karyawan);
				if (karyawan.getPhotoProfil() != null) {
					PhotoProfil photo = photoProfilRepository.findById(karyawan.getPhotoProfil().getId()).get();
					result.get().setPhoto(photo.getPhoto());
					result.get().setPhotoContentType(photo.getPhotoContentType());
					karyawan.setPhotoProfil(photo);
					karyawanRepository.save(karyawan);
				}
			}
        return result.get();
    }

}
