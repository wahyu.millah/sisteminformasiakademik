package com.akademik.app.web.rest;

import com.akademik.app.domain.Channel;
import com.akademik.app.domain.HistoryTransaction;
import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.domain.enumeration.StatusTransaksi;
import com.akademik.app.repository.ChannelRepository;
import com.akademik.app.repository.HistoryTransactionRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.repository.TransaksiRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.PaymentService;
import com.akademik.app.service.TransaksiService;
import com.akademik.app.service.dto.PaymentResponseDTO;
import com.akademik.app.service.dto.SiswaDTO;
import com.akademik.app.service.dto.TagihanDTO;
import com.akademik.app.service.dto.TransaksiDTO;
import com.akademik.app.util.DateUtils;
import com.akademik.app.web.rest.errors.BadRequestAlertException;
import com.akademik.app.web.rest.errors.TransaksiAlreadyUsedException;
import com.akademik.app.web.rest.errors.TransaksiPendingUsedException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

/**
 * REST controller for managing {@link com.akademik.app.domain.Transaksi}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TransaksiResource {

    private final Logger log = LoggerFactory.getLogger(TransaksiResource.class);

    private static final String ENTITY_NAME = "transaksi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TransaksiRepository transaksiRepository;

    private final UserRepository userRepository;

    private final SiswaRepository siswaRepository;

    private final TagihanRepository tagihanRepository;

    private final PaymentService paymentService;

    private final TransaksiService transaksiService;

    private final ChannelRepository chennelRepository;

    private final HistoryTransactionRepository historyTransactionRepository;

    public TransaksiResource(SiswaRepository siswaRepository, HistoryTransactionRepository historyTransactionRepository,ChannelRepository chennelRepository, TransaksiService transaksiService, PaymentService paymentService, TransaksiRepository transaksiRepository, UserRepository userRepository, TagihanRepository tagihanRepository) {
        this.transaksiRepository = transaksiRepository;
        this.userRepository = userRepository;
        this.tagihanRepository = tagihanRepository;
        this.paymentService = paymentService;
        this.transaksiService = transaksiService;
        this.chennelRepository = chennelRepository;
        this.historyTransactionRepository = historyTransactionRepository;
        this.siswaRepository = siswaRepository;
    }

    /**
     * {@code POST  /transaksis} : Create a new transaksi.
     *
     * @param transaksi the transaksi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transaksi, or with status {@code 400 (Bad Request)} if the transaksi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transaksis")
    public ResponseEntity<Transaksi> createTransaksi(@RequestBody Transaksi transaksi) throws URISyntaxException {
        log.debug("REST request to save Transaksi : {}", transaksi);
        if (transaksi.getId() != null) {
            throw new BadRequestAlertException("A new transaksi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Transaksi result = transaksiRepository.save(transaksi);
        return ResponseEntity.created(new URI("/api/transaksis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /transaksis} : Updates an existing transaksi.
     *
     * @param transaksi the transaksi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transaksi,
     * or with status {@code 400 (Bad Request)} if the transaksi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transaksi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/transaksis")
    public ResponseEntity<Transaksi> updateTransaksi(@RequestBody Transaksi transaksi) throws URISyntaxException {
        log.debug("REST request to update Transaksi : {}", transaksi);
        if (transaksi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Transaksi result = transaksiRepository.save(transaksi);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transaksi.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /transaksis} : get all the transaksis.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transaksis in body.
     */
//    @GetMapping("/transaksis")
//    public ResponseEntity<List<Transaksi>> getAllTransaksis(Pageable pageable) {
//        log.debug("REST request to get a page of Transaksis");
//        Page<Transaksi> page = transaksiRepository.findAll(pageable);
//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
//        return ResponseEntity.ok().headers(headers).body(page.getContent());
//    }
    @GetMapping("/transaksis")
    public ResponseEntity<List<TagihanDTO>> getAllTransaksis(
			@RequestParam(value = "query", required = false) String query,
			Pageable pageable) {
        log.debug("REST request to get a page of Transaksis");
        
        LocalDate startDate = LocalDate.now().withDayOfMonth(1);
        ZonedDateTime st = startDate.atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime ed = st.plusMonths(1);
        
		Page<TagihanDTO> page = null;
		
        if(query != null) {
            log.debug("cari nis:{}",query);
        	page = tagihanRepository.findTransaksiByNis(query, pageable);
        } else {
        	page = tagihanRepository.findAllTransaksi(st.toInstant(), ed.toInstant(), pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/transaksis/add/{va}")
    public ResponseEntity<TagihanDTO> getTransaksiDTO(@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to get Transaksi : {}", va);
        Optional<TagihanDTO> transaksi = tagihanRepository.findTransaksiByVa(va);
        String[] bank = transaksi.get().getBank().split("_");
        Channel ch = chennelRepository.findByName(bank[1].toUpperCase());
        transaksi.get().setBiayaAdmin(ch.getFee());
        transaksi.get().setJmlBayar(transaksi.get().getJmlBayar() + ch.getFee());
        return ResponseUtil.wrapOrNotFound(transaksi);
    }
    
    @GetMapping("/transaksis/addMulti/{va}")
    public ResponseEntity<TagihanDTO> getMultiTagihan(@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to get Transaksi : {}", va);
        Optional<TagihanDTO> transaksi = tagihanRepository.findByTransaksiMultiByVa(va);
        log.debug("REST request to get test : {}", transaksi);
        List<Tagihan> multiTagihan = tagihanRepository.findByTransaksiId(transaksi.get().getIdTransaksi());
        log.debug("multiTagihan: {}", multiTagihan.size());
        transaksi.get().setMultiTagihan(multiTagihan);
        if (multiTagihan.size() > 0) transaksi.get().setTypeTagihan(1);
        return ResponseUtil.wrapOrNotFound(transaksi);
    }

    /**
     * {@code GET  /transaksis/:id} : get the "id" transaksi.
     *
     * @param id the id of the transaksi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transaksi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transaksis/{id}")
    public ResponseEntity<Transaksi> getTransaksi(@PathVariable Long id) {
        log.debug("REST request to get Transaksi : {}", id);
        Optional<Transaksi> transaksi = transaksiRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(transaksi);
    }

    /**
     * {@code DELETE  /transaksis/:id} : delete the "id" transaksi.
     *
     * @param id the id of the transaksi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transaksis/{id}")
    public ResponseEntity<Void> deleteTransaksi(@PathVariable Long id) {
        log.debug("REST request to delete Transaksi : {}", id);
        transaksiRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    
    // manual payment
    @PostMapping("/transaksis/add")
    public ResponseEntity<Transaksi> createTransaksiTagihan(@Valid @RequestBody TagihanDTO tagihanDTO) throws URISyntaxException {
        log.debug("REST request to save transaksi : {}", tagihanDTO);
        if (tagihanDTO.getIdTransaksi() != null) {
            throw new BadRequestAlertException("A new guestBook cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tagihan tagihan = tagihanRepository.findById(tagihanDTO.getIdTagihan()).get();
        
        Long currentMilis = System.currentTimeMillis();
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();
	        Transaksi tr = new Transaksi();
			        tr.setUpdateOn(Instant.now());
			        tr.setUpdateBy(user.getFirstName()+" "+user.getLastName());
			        tr.setNomor("SU"+currentMilis.toString());
			        tr.setJmlTagiha(tagihanDTO.getNominalPembayaran());
			        tr.setStatus(StatusTransaksi.SUKSES);
			        tr.setTanggal(Instant.now());
			        tr.setMetode(tagihanDTO.getMetodeTransaksi());
			        tr.setCaraBayar(tagihanDTO.getCaraBayar());
			        
			        if (!tagihan.getBesarBayar().getJenisBayar().getMetode().equals("ANGSURAN")) {
			            log.debug("non angusran");
			            log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
			            tr.setJmlSisa(Long.valueOf(0));
			        	tagihan.setStatus(StatusBayar.LUNAS);
			            tr.setJmlBayar(tagihanDTO.getNominalPembayaran());
			            log.debug("jml sisa transaksi: {}", tr.getJmlSisa());
			        } else {
			            log.debug("angusran");
			            log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
			            tr.setJmlSisa(tagihanDTO.getJmlSisa());
			            tr.setJmlBayar(tagihanDTO.getJmlBayar());
			            log.debug("jml sisa transaksi: {}", tr.getJmlSisa());
			            if ( tr.getJmlSisa() > 0) tagihan.setStatus(StatusBayar.ANGSUR);
			            if (tr.getJmlSisa() == 0) tagihan.setStatus(StatusBayar.LUNAS);
			        }
			        
	        tagihan.setTanggalBayar(Instant.now());
	        tagihan.setReportSubmitted(false);
//	        tagihan.setHarusBayar(tr.getJmlSisa());
	        transaksiRepository.save(tr);
	        tagihan.setTransaksi(tr);
	        tagihanRepository.save(tagihan);
        
        return ResponseEntity.created(new URI("/api/transaksis/" + tr.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, tr.getId().toString()))
            .body(tr);
    }
    
    // online payment
    @PostMapping("/transaksis/add/{va}")
    public void createTransaksiTransfer(
    		@Valid @RequestBody TagihanDTO tagihanDTO,
    		@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to save transaksi : {}", tagihanDTO.getIdTagihan(), va);
        
        transaksiService.createTransaksi(tagihanDTO, va);
    }
    
    // online payment
    @PostMapping("/transaksis/multi/{va}")
    public void createMultiPayment(
    		@Valid @RequestBody String[] idTagihan,
    		@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to save multi tagihan : {}", va);
        
        transaksiService.createMultiTransaksi(idTagihan, va);
    }
    
    @GetMapping("/transaksis/cek/{va}")
    public void checkStatusVa(
    		@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to cek status va : {}", va);
        
        Transaksi transaksi = transaksiRepository.findByVirtualAccount(va);
        log.debug("transaksi : {}", transaksi);
        log.debug("transaksi.getTransactionId() : {}", transaksi.getTransactionId());
        
        paymentService.statusPaymentV2(transaksi.getTransactionId());
    }
    
    @GetMapping("/transaksis/cancel/{va}")
    public void cancelVa(
    		@PathVariable String va) throws URISyntaxException {
        log.debug("REST request to cek status va : {}", va);
        
        Transaksi transaksi = transaksiRepository.findByVirtualAccount(va);
        log.debug("transaksi : {}", transaksi);
        log.debug("transaksi.getTransactionId() : {}", transaksi.getTransactionId());
        
        paymentService.cancelPaymentV2(transaksi.getTransactionId());
    }

	@GetMapping("/transaksis/mcp/all")
	public ResponseEntity<Void> scanTransactionAll() {
		log.debug("REST request to scanTransactionAll");

		paymentService.allPayment();

		return new ResponseEntity<>(HttpStatus.OK);

	}

    @GetMapping("/transaksis/search-va")
    public TransaksiDTO getTransaction(
			@RequestParam(value = "query", required = false) String query) {
        log.debug("REST request to get a page of getTransaction by virtual account:{}", query);
        
        Transaksi transaksi = transaksiRepository.findByVirtualAccount(query);
        HistoryTransaction historyTransaction = historyTransactionRepository.findByTransaksiId(transaksi.getId());
        Optional<Siswa> siswa = siswaRepository.findById(historyTransaction.getSiswaId());
        PaymentResponseDTO resultMCP = paymentService.statusPaymentV2(transaksi.getTransactionId());
        
        TransaksiDTO result = new TransaksiDTO();
        	result.setIdTransaksi(transaksi.getId());
        	result.setStatusTransaksi(transaksi.getStatus().name());
        	result.setVirtualAccount(transaksi.getVirtualAccount());
        	result.setStatusTransaksiMCP(resultMCP.getStatus());
        	result.setTransactionId(transaksi.getTransactionId());
        	result.setNamaSiswa(siswa.get().getNama());
        	result.setTglBayar(resultMCP.getPaid_date());
        return result;
    }

    @PutMapping("/transaksis/update/{va}")
    public TransaksiDTO updateTransaksi(@PathVariable String va) {
        log.debug("REST request to get a page of updateTransaksi by virtual account:{}", va);
        
        Transaksi transaksi = transaksiRepository.findByVirtualAccount(va);
        HistoryTransaction historyTransaction = historyTransactionRepository.findByTransaksiId(transaksi.getId());
        Optional<Siswa> siswa = siswaRepository.findById(historyTransaction.getSiswaId());
        PaymentResponseDTO resultMCP = paymentService.statusPaymentV2(transaksi.getTransactionId());
        
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();
        
        if(historyTransaction.getTagihanId().contains(",")) {
        	String[] ids = historyTransaction.getTagihanId().split(",");
        	for (String id : ids) {
				Tagihan tagihan = tagihanRepository.findById(Long.parseLong(id)).get();
					if("SUCCESS".equals(resultMCP.getStatus()) || "PAID".equals(resultMCP.getStatus())) {
						tagihan.setStatus(StatusBayar.LUNAS);
					}
					tagihan.setVirtualAccount(resultMCP.getPayment_code());
					tagihan.setType(1);
					tagihan.setMulti(true);
					tagihan.setTransaksi(transaksi);
					tagihan.setReportSubmitted(false);
//					tagihan.setTanggalBayar(DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(resultMCP.getPaid_date())));
			        tagihanRepository.save(tagihan);
			}
        } else {
			Tagihan tagihan = tagihanRepository.findById(Long.parseLong(historyTransaction.getTagihanId())).get();
        	if("SUCCESS".equals(resultMCP.getStatus()) || "PAID".equals(resultMCP.getStatus())) {
				tagihan.setStatus(StatusBayar.LUNAS);
				tagihan.setVirtualAccount(resultMCP.getPayment_code());
				tagihan.setMulti(false);
				tagihan.setType(0);
//				tagihan.setTanggalBayar(DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(resultMCP.getPaid_date())));
	    		tagihan.setTransaksi(transaksi);
	    		tagihan.setReportSubmitted(false);
	            tagihanRepository.save(tagihan);
			}
        }
        transaksi.setStatus(StatusTransaksi.SUKSES);
        transaksi.setUpdateOn(Instant.now());
        transaksi.setUpdateBy(user.getFirstName());
        
        TransaksiDTO result = new TransaksiDTO();
        	result.setIdTransaksi(transaksi.getId());
        	result.setStatusTransaksi(transaksi.getStatus().name());
        	result.setVirtualAccount(transaksi.getVirtualAccount());
        	result.setStatusTransaksiMCP(resultMCP.getStatus());
        	result.setTransactionId(transaksi.getTransactionId());
        	result.setNamaSiswa(siswa.get().getNama());
        	result.setTglBayar(resultMCP.getPaid_date());
        return result;
    }
	
	final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	public static Instant parseScanDate(String str) {
		LocalDateTime dateTime = null;
		if( str !=null) {
			dateTime = LocalDateTime.parse(str, formatter);
		}
		return dateTime.atZone(ZoneId.systemDefault()).toInstant();
	}
}
