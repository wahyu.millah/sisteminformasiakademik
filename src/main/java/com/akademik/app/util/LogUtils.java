package com.akademik.app.util;

import org.slf4j.Logger;

import com.akademik.app.service.SlackInhookService;

public class LogUtils {
	
	private final static String NO_CONTENT = "(No content to log)";

	private final static String LOG_PREFIX = "Debug:\n";
	
	public static void debug(Object o, Logger log, SlackInhookService slack) {
		if(o == null) {
			log.debug(NO_CONTENT);
		} else {
			log.debug(LOG_PREFIX + o.toString());
			if(slack != null) {
				slack.sendMessageAsync(o.toString());
			}
		}
	}
	
	public static void debug(Object o, Logger log) {
		debug(o, log, null);
	}
}
