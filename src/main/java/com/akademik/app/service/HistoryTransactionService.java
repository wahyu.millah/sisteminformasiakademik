package com.akademik.app.service;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.akademik.app.domain.Channel;
import com.akademik.app.domain.HistoryTransaction;
import com.akademik.app.domain.User;
import com.akademik.app.repository.ChannelRepository;
import com.akademik.app.repository.HistoryTransactionRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;

@Service
public class HistoryTransactionService {

	private static final Logger log = LoggerFactory.getLogger(HistoryTransactionService.class);
	
    private final UserRepository userRepository;
	
    private final ChannelRepository channelRepository;
    
    private final HistoryTransactionRepository historyTransactionRepository;
    
    public HistoryTransactionService(ChannelRepository channelRepository, UserRepository userRepository, HistoryTransactionRepository historyTransactionRepository) {
    	this.userRepository = userRepository;
    	this.historyTransactionRepository = historyTransactionRepository;
    	this.channelRepository = channelRepository;
    }
	
	public void recordHostoryTransaction(Long transaksiId, String tagihanId, String channel, Long siswaId, String jenis, Long nominal) {
		log.debug("Record trasaction...");
        
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();
        
        Channel ch = channelRepository.findByVa(channel.toUpperCase());
		
		HistoryTransaction historyTransaction = new HistoryTransaction();
		historyTransaction.setTransaksiId(transaksiId);
		historyTransaction.setTagihanId(tagihanId);
		historyTransaction.setChannelId(ch.getId());
		historyTransaction.setSiswaId(siswaId);
		historyTransaction.setJenis(jenis);
		historyTransaction.setNominal(nominal);
		historyTransaction.setTransactionBy(user.getFirstName());
		historyTransaction.setTransactionOn(LocalDateTime.now());
		historyTransactionRepository.save(historyTransaction);
	}

}
