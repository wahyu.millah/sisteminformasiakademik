package com.akademik.app.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.ApplicationConfig;
import com.akademik.app.repository.ApplicationConfigRepository;
import com.akademik.app.service.dto.ResponDevicesWaDTO;
import com.akademik.app.service.dto.WaResponDTO;

@Service
@Transactional
public class WaService {

	private static final Logger log = LoggerFactory.getLogger(WaService.class);

    private final ApplicationConfigRepository applicationConfigRepository;

	public WaService(ApplicationConfigRepository applicationConfigRepository) {
		this.applicationConfigRepository = applicationConfigRepository;
	}

	public WaResponDTO sendWhatsapp(String to, String content) {
    	log.info("Request to sendMessage to receiver:{}", to);
    	
    	WaResponDTO response = null;
    	
    	if (content != null) {
    		BufferedReader in = null;
            try {
            	
            	int connectTimeout = 20000;
            	int readTimeout = 20000;
            	
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("to", to);
                jsonObject.put("content", content);
                jsonObject.put("instances_id", applicationConfigRepository.findByCategoryAndName("WA","instance_id").getValue());

                String postData = jsonObject.toString();

                URL url = new URL(applicationConfigRepository.findByCategoryAndName("WA","wa_url").getValue());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("x-access-key", applicationConfigRepository.findByCategoryAndName("WA","x-access-key").getValue());
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");

                con.setConnectTimeout(connectTimeout);
                con.setReadTimeout(readTimeout);
                con.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(postData);
                wr.flush();
                wr.close();

    			int responseCode = con.getResponseCode();
    			
    			if(responseCode == HttpStatus.OK.value()) {
    				log.debug("Post parameters : " + postData);
    				log.debug("Response Code : " + responseCode);
    				
    				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        			
        			String line;
        			StringBuffer sb = new StringBuffer();
        			while ((line = in.readLine()) != null) {
        				sb.append(line);
        			}
        			
        			response = parseJson(sb.toString());
    			} else  {
    				log.debug("Post parameters : " + postData);
    				log.debug("Response Code : " + responseCode);
    			}
            } catch (Exception e) {
                log.error("Failed sendMessage caused by {}", e.getMessage(), e);
            } finally {
    			if(in != null) {
    				try {
    					in.close();
    				} catch(Exception e) {}
    			}
    		}
    		
    	}
		
		return response;
	}

	public WaResponDTO sendWhatsapp2(String to, String content) {
    	log.info("Request to sendMessage to receiver:{}", to);
    	
    	WaResponDTO response = null;
    	Boolean connect = diviceConnect() != "disconnected" ? true : false ;
    	
    	if (content != null && connect) {
    		BufferedReader in = null;
            try {
            	
            	int connectTimeout = 20000;
            	int readTimeout = 20000;
            	
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("phone_number", to);
                jsonObject.put("message", content);
                jsonObject.put("device_id", "siraajul");
                jsonObject.put("message_type", "text");

                String postData = jsonObject.toString();

                URL url = new URL("https://api.kirimwa.id/v1/messages");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.setRequestProperty("Authorization", "Bearer " + "5vJgdQnTTqPIDxBhYaRxB8DfwHZ54UPrBrz6xxE8trqhPVxl9d7QhKr6RMZt1+jD-wahyu");

                con.setConnectTimeout(connectTimeout);
                con.setReadTimeout(readTimeout);
                con.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(postData);
                wr.flush();
                wr.close();

    			int responseCode = con.getResponseCode();
    			
    			if(responseCode == HttpStatus.OK.value()) {
    				log.debug("Post parameters : " + postData);
    				log.debug("Response Code : " + responseCode);
    				
    				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        			
        			String line;
        			StringBuffer sb = new StringBuffer();
        			while ((line = in.readLine()) != null) {
        				sb.append(line);
        			}
        			
        			response = parseJson2(sb.toString());
    			} else  {
    				log.debug("Post parameters : " + postData);
    				log.debug("Response Code : " + responseCode);
    			}
            } catch (Exception e) {
                log.error("Failed sendMessage caused by {}", e.getMessage(), e);
            } finally {
    			if(in != null) {
    				try {
    					in.close();
    				} catch(Exception e) {}
    			}
    		}
    		
    	}
		
		return response;
	}
	
	private static WaResponDTO parseJson2(String json) throws Exception {
		WaResponDTO resp = new WaResponDTO();
		resp.setSuccess(false);
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject obj = new JSONObject(json);
				resp.setSuccess(obj.getBoolean("success"));
				
				if(resp.getSuccess()) {
					resp.setMessage(obj.getString("message"));
				} else {
					resp.setSuccess(obj.getBoolean("success"));
					resp.setMessage(obj.getString("message"));
				}
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
	
	private static WaResponDTO parseJson(String json) throws Exception {
		WaResponDTO resp = new WaResponDTO();
		resp.setSuccess(false);
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject obj = new JSONObject(json);
				resp.setSuccess(obj.getBoolean("success"));
				
				if(resp.getSuccess()) {
					resp.setMessage(obj.getString("message"));
				} else {
					resp.setSuccess(obj.getBoolean("success"));
					resp.setMessage(obj.getString("message"));
				}
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
	
	private static ResponDevicesWaDTO parseJson3(String json) throws Exception {
		ResponDevicesWaDTO resp = new ResponDevicesWaDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject obj = new JSONObject(json);
				resp.setId(obj.getString("id"));
				resp.setCreate_at(obj.getString("created_at"));
				resp.setConnected_at(obj.getString("connected_at"));
				resp.setDisconnected_at(obj.getString("disconnected_at"));
				resp.setDisconnected_reason(obj.getString("disconnected_reason"));
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
	
	public String diviceConnect () {
    	
    	ResponDevicesWaDTO response = null;
    	
    		BufferedReader in = null;
            try {
            	
            	int connectTimeout = 20000;
            	int readTimeout = 20000;

                URL url = new URL("https://api.kirimwa.id/v1/devices/siraajul");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.setRequestProperty("Authorization", "Bearer " + "5vJgdQnTTqPIDxBhYaRxB8DfwHZ54UPrBrz6xxE8trqhPVxl9d7QhKr6RMZt1+jD-wahyu");

                con.setConnectTimeout(connectTimeout);
                con.setReadTimeout(readTimeout);
                con.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.flush();
                wr.close();

    			int responseCode = con.getResponseCode();
    			
    			if(responseCode == HttpStatus.OK.value()) {
    				log.debug("Response Code : " + responseCode);
    				
    				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        			
        			String line;
        			StringBuffer sb = new StringBuffer();
        			while ((line = in.readLine()) != null) {
        				sb.append(line);
        			}
        			
        			response = parseJson3(sb.toString());
    			} else  {
    				log.debug("Response Code : " + responseCode);
    			}
            } catch (Exception e) {
                log.error("Failed sendMessage caused by {}", e.getMessage(), e);
            } finally {
    			if(in != null) {
    				try {
    					in.close();
    				} catch(Exception e) {}
    			}
            }
		
		return response.getStatus();
	}
}

