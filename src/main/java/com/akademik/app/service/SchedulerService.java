package com.akademik.app.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.Notification;
import com.akademik.app.domain.ReportSyahriah;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.TokenNotifikasi;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.domain.enumeration.StatusTransaksi;
import com.akademik.app.repository.ApplicationConfigRepository;
import com.akademik.app.repository.NotificationRepository;
import com.akademik.app.repository.ReportSyahriahRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.repository.TokenNotifikasiRepository;
import com.akademik.app.repository.TransaksiRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.service.dto.NotificationDTO;
import com.akademik.app.service.dto.PaymentResponseDTO;
import com.akademik.app.util.DateUtils;
import com.akademik.app.util.LogUtils;
import com.akademik.app.util.NumberUtils;
import com.akademik.app.web.rest.util.CommonResponse;

@Service
@Transactional
public class SchedulerService {

	private static final Logger log = LoggerFactory.getLogger(SchedulerService.class);
    
    private final TransaksiRepository transaksiRepository;
    
    private final NotificationRepository notificationRepository;
    
    private final TagihanService tagihanService;
    
    private final PaymentService paymentService;
    
    private final TagihanRepository tagihanRepository;
    
    private final ReportSyahriahRepository reportSyahriahRepository;

    private final MessageService messageService;

    private final MailService mailService;

    private final ApplicationConfigRepository applicationConfigRepository;

    private final TokenNotifikasiRepository tokenNotifikasiRepository;

    private final UserRepository userRepository;
    
    private final SlackInhookService slackService;

	public SchedulerService (SlackInhookService slackService, UserRepository userRepository, TokenNotifikasiRepository tokenNotifikasiRepository, ApplicationConfigRepository applicationConfigRepository, MailService mailService, MessageService messageService, PaymentService paymentService, ReportSyahriahRepository reportSyahriahRepository, TagihanService tagihanService, TransaksiRepository transaksiRepository, NotificationRepository notificationRepository, TagihanRepository tagihanRepository) {
		this.transaksiRepository = transaksiRepository;
		this.notificationRepository = notificationRepository;
		this.tagihanService = tagihanService;
		this.tagihanRepository = tagihanRepository;
		this.reportSyahriahRepository = reportSyahriahRepository;
		this.paymentService = paymentService;
		this.messageService = messageService;
		this.mailService = mailService;
        this.applicationConfigRepository = applicationConfigRepository;
        this.tokenNotifikasiRepository = tokenNotifikasiRepository;
        this.userRepository = userRepository;
        this.slackService = slackService;
	}
	
    @Scheduled(cron = "0 0 10 1 * ?")
	public void SchedulerTagihanBulanan(){
		log.debug("..."); 
		LocalDate date = LocalDate.now();
		if(Boolean.parseBoolean(applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "SYAHRIAH").getValue().toLowerCase()))tagihanService.createMounthlyTagihan(date,"SYAHRIAH",true, null, null);
		if(Boolean.parseBoolean(applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "LOUNDRY").getValue().toLowerCase()))tagihanService.createMounthlyTagihan(date,"LOUNDRY",true, null, null);
//		tagihanService.createMounthlyTagihan(date,"QURBAN",true);
	}

    @Scheduled(cron = "0 * * ? * *")
	public void updateStatusTagihan() {
    	try {
    		List<Notification> notifications = notificationRepository.findByStatusUpdate(false);
    		Transaksi transaksi = null;
    		
    		for (Notification notification : notifications) {
    			if (transaksiRepository.findById(Long.valueOf(notification.getOrder_id())).isPresent()) {
    				transaksi = transaksiRepository.findById(Long.valueOf(notification.getOrder_id())).get();
	        		
	        		if (notification.getStatus().equals("SUCCESS")) {
	        			log.debug("notif sukses..:{}", notification.getOrder_id());
	        			
	        			transaksi.setStatus(StatusTransaksi.SUKSES);
	        			transaksi.setUpdateOn(Instant.now());;
	            		List<Tagihan> tagihans = tagihanRepository.findByTransaksiId(Long.valueOf(notification.getOrder_id()));
	            		
	        			for (Tagihan tagihan : tagihans) {
	            			log.debug("tagihan:{}", tagihan.getId());
	            			tagihan.setStatus(StatusBayar.LUNAS);
	            			tagihan.setTanggalBayar(Instant.now());;
						}
	        			notification.setStatusUpdate(true);
	        		} else if (notification.getStatus().equals("EXPIRED")) {
	        			log.debug("notif EXPIRED..:{}", notification.getOrder_id());
	        			
	        			transaksi.setStatus(StatusTransaksi.EXPIRED);
	        			List<Tagihan> tagihans = tagihanRepository.findByTransaksiId(Long.valueOf(notification.getOrder_id()));
	        			for (Tagihan tagihan : tagihans) {
	            			log.debug("tagihan:{}", tagihan.getId());
	            			if (tagihan.getStatus().equals(StatusBayar.MENUNGGU)) {
	            				tagihan.setStatus(StatusBayar.BELUMBAYAR);
	            				tagihan.setVirtualAccount(null);
	            				tagihan.setTanggalBayar(null);
	            				tagihan.setTransaksi(null);
	            				tagihanRepository.save(tagihan);
	            			}
						}
	        			notification.setStatusUpdate(true);
	        		} 
    			} else {
        			log.debug("Order Id Not Found in Table Transaksi");
    			}
			}
			
		} catch (Exception e) { log.error("Error on updateStatusTagihan() caused by " + e.getMessage(), e); }
	}

//    @Scheduled(cron = "0 * * ? * *")
	public void ReportUpdate(){
    	System.out.println("report");
		log.debug("Report Syahriah Update Scheduler . . .");
//		List<Tagihan> tagihans = tagihanRepository.findByTotal("SYAHRIAH", StatusBayar.LUNAS, false);findByStatusAndReportSubmitted
		List<Tagihan> tagihans = tagihanRepository.findByStatusAndReportSubmitted(StatusBayar.LUNAS, false);
		log.debug("report data length: {}",tagihans.size());
		
		for (Tagihan t : tagihans) {
			if (reportSyahriahRepository.findBySiswa(t.getSiswa()) != null) {
				ReportSyahriah r  = reportSyahriahRepository.findBySiswa(t.getSiswa());
				r.setSiswa(t.getSiswa());
				r.setTahunAjaran(t.getTahunAjaran());
				if (t.getTotal().contains("SYAHRIAH")) {
					if (t.getTotal().equals("SYAHRIAH-JULY")) {
						r.setJuli(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setJuliOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-AUGUST")) {
						r.setAgustus(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setAgustusOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-SEPTEMBER")) {
						r.setSeptember(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setSeptemberOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-OCTOBER")) {
						r.setOktober(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setOktoberOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-NOVEMBER")) {
						r.setNovember(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setNovemberOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-DECEMBER")) {
						r.setDesember(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setDesemberOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-JANUARY")) {
						r.setJanuari(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setJanuariOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-FEBRUARY")) {
						r.setFebruari(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setFebruariOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-MARCH")) {
						r.setMaret(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setMaretOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-APRIL")) {
						r.setApril(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setAprilOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-MAY")) {
						r.setMei(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setMeiOn(t.getTanggalBayar());
					};
					if (t.getTotal().equals("SYAHRIAH-JUNE")) {
						r.setJuni(t.getHarusBayar());
						if(t.getTanggalBayar() != null) r.setJuniOn(t.getTanggalBayar());
					};
					
				} else {
					
				}
		    	reportSyahriahRepository.save(r);
			} else {
				log.debug("new record . . .");
				ReportSyahriah newR = new ReportSyahriah();
				newR.setSiswa(t.getSiswa());
				newR.setTahunAjaran(t.getTahunAjaran());
				if (t.getTotal().equals("SYAHRIAH-JULY")) {
					newR.setJuli(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setJuliOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-AUGUST")) {
					newR.setAgustus(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setAgustusOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-SEPTEMBER")) {
					newR.setSeptember(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setSeptemberOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-OCTOBER")) {
					newR.setOktober(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setOktoberOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-NOVEMBER")) {
					newR.setNovember(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setNovemberOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-DECEMBER")) {
					newR.setDesember(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setDesemberOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-JANUARY")) {
					newR.setJanuari(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setJanuariOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-FEBRUARY")) {
					newR.setFebruari(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setFebruariOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-MARCH")) {
					newR.setMaret(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setMaretOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-APRIL")) {
					newR.setApril(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setAprilOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-MAY")) {
					newR.setMei(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setMeiOn(t.getTanggalBayar());
				};
				if (t.getTotal().equals("SYAHRIAH-JUNE")) {
					newR.setJuni(t.getHarusBayar());
					if(t.getTanggalBayar() != null) newR.setJuniOn(t.getTanggalBayar());
				};
		    	reportSyahriahRepository.save(newR);
			}
			t.setReportSubmitted(true);
			tagihanRepository.save(t);
		}

	}
    
    public void resetInvalidTagihan(Tagihan tagihan) {
		tagihan.setStatus(StatusBayar.BELUMBAYAR);
		tagihan.setVirtualAccount(null);
		tagihan.setTanggalBayar(null);
		tagihan.setTransaksi(null);
    	
		tagihanRepository.save(tagihan);
    }
    
    @Scheduled(cron = "0 * * ? * *")
	public void UpdateStatus(){
		log.debug("Update Statusm cheking start . . .");

        List<Tagihan> listtagihanCek = tagihanRepository.findByStatus(StatusBayar.MENUNGGU);
        
        if (listtagihanCek != null) {
    		log.debug("Pengecekan virtual account sejumlah :{}", listtagihanCek.size());
    		for (Tagihan tagihan : listtagihanCek) {
					if (tagihan.getVirtualAccount() != null) {

			            Siswa siswa = tagihan.getSiswa();
			            User user = userRepository.findById(siswa.getUser().getId()).get();
						Transaksi transaksi = transaksiRepository.findById(tagihan.getTransaksi().getId()).get();
						PaymentResponseDTO respon = null;
						
						try {
							if (transaksi.getTransactionId() != null) {
								respon =  paymentService.statusPaymentV2(transaksi.getTransactionId());
							} else {
								resetInvalidTagihan(tagihan);
								continue;
							}
						} catch (Exception e) {
							log.error("Error update status tagihan : {}", e);
							continue;
						}
						
						String content = null;
						String body = null;
						String title = null;
			            String bank = transaksi.getBank().toUpperCase().split("_")[1];
			            String utkBayar = tagihan.getTotal();
			            String total = "Rp." + NumberUtils.format(transaksi.getJmlBayar(), NumberUtils.FORMATTER_LONG_EN);
			            String paidTime = DateUtils.formatZonedDateTime(ZonedDateTime.parse(Instant.now().toString()), DateUtils.FORMATTER_ID_LOCAL_DATETIME);

				        String service = "Transaksi Pembayaran";
				        String parameter = "Siswa: " + siswa.getNama() +", " + "Bayar: " + utkBayar;
//				        CommonResponse response = new CommonResponse(service, parameter);

	        			log.debug("respon:{}", respon);
	        			if (respon != null) {
							if (respon.getStatus().equals("PAID")) {
			        			log.debug("Result success by va:{}", transaksi.getVirtualAccount());
			        			
			        			transaksi.setStatus(StatusTransaksi.SUKSES);
			        			transaksi.setUpdateOn(Instant.now());
		            			tagihan.setStatus(StatusBayar.LUNAS);
		            			tagihan.setTanggalBayar(Instant.now());
		            			
		        				tagihanRepository.save(tagihan);
		        				transaksiRepository.save(transaksi);
		            			save(respon);

								content = "*Transaksi Berhasil*" +
										"\n\nAssalamu'alaikum wr. wb. " +
										"\nBerikut informasi transaksi :" +
										"\nNama	    : " + siswa.getNama() +
										"\nNis		    : " + siswa.getNis() +
										"\nBayar      : " + total +
										"\nUntuk      : " + utkBayar +
										"\nKode VA : " +"*"+ transaksi.getVirtualAccount() +"*"+
										"\nBANK      : " + bank +
										"\nPaid        : " + paidTime +
										"\n\nTerima kasih atas transaksi yang dilakukan." +
										"\n\nWassalammu'alaikum wr. wb." +
										"\n_Admin Siraajul Ummah._";
								
//								mailService.sendEmailFromTemplateSukses(
//										applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "EMAIL_SUPPORT").getValue(),"Successful Transaction Notification",
//										tagihan, transaksi, siswa, DateUtils.formatZonedDateTime(ZonedDateTime.parse(Instant.now().toString()), DateUtils.FORMATTER_ID_LOCAL_DATETIME),
//										"mail/transaksiSukses");
								
								title = "Transaksi Berhasil";
						        body = "Virtual account " +transaksi.getVirtualAccount()+ " untuk pembayaran [" + utkBayar+ "] dengan Bank [" + bank + "] Total bayar ["+ total +"] berhasil dibayarkan." +
						          "Terima kasih sudah melakukan pembayaran dengan tepat waktu";
				    	        
				    	        //slack report
				    			StringBuilder sb = new StringBuilder();
				    			sb.append("\n* Detail Transaksi *");
				    			sb.append("\nNama       " + siswa.getNama());
				    			sb.append("\nNis         " + siswa.getNis());
				    			sb.append("\nBayar      " + total);
				    			sb.append("\nUntuk      " + utkBayar);
				    			sb.append("\nKode Va   " + transaksi.getVirtualAccount());
				    			sb.append("\nBank       " + bank);
				    			sb.append("\nStatus     " + "SUCCESS");
				    			sb.append("\nPaid ON   " + paidTime);
						        
								LogUtils.debug(sb.toString(), log, slackService);
//					            if (siswa.getNoTelephone() != null && siswa.getNoTelephone().length() > 0) {
//					            	String[] tlpon = siswa.getNoTelephone().split("/");
//					            	for (String string : tlpon) {
//					            		String tlp = string.trim().replace("-","");
//							            messageService.newMessage(
//							            		NotificationChannel.WA, content,
//							            		null, Instant.now(), Instant.now(), Instant.now(),
//							            		tlp, "SYSTEM", NotificationStatus.NEW, "NOTIFIKASI", user);
//									}
//					            }
							
							} else if (respon.getStatus().equals("EXPIRED")){
					            String exp = DateUtils.formatZonedDateTime(ZonedDateTime.parse(respon.getExpired_time()), DateUtils.FORMATTER_ID_LOCAL_DATETIME);
			        			log.debug("Result EXPIRED..:{}", transaksi.getVirtualAccount());
			        			
			        			transaksi.setStatus(StatusTransaksi.EXPIRED);
			        			
		            			if (tagihan.getStatus().equals(StatusBayar.MENUNGGU)) {
		            				tagihan.setStatus(StatusBayar.BELUMBAYAR);
		            				tagihan.setVirtualAccount(null);
		            				tagihan.setTanggalBayar(null);
		            				tagihan.setTransaksi(null);
		            				
		            				tagihanRepository.save(tagihan);
		            				transaksiRepository.save(transaksi);
		                			save(respon);
		                			
		                			content = "*Transaksi Expired*" +
		    								"\n\nAssalamu'alaikum wr. wb. " +
		    								"\nBerikut informasi transaksi :" +
		    								"\nNama	    : " + siswa.getNama() +
		    								"\nNis		    : " + siswa.getNis() +
		    								"\nBayar      : " + total +
		    								"\nUntuk      : " + utkBayar +
		    								"\nKode VA : " +"*"+ transaksi.getVirtualAccount() +"*"+
		    								"\nBANK      : " + bank +
		    								"\nExpired   : " + exp +
		    								"\n\nMasuk ke aplikasi dan buat virtual account kembali untuk melunasi pembayaran anda." +
		    								"\n\nWassalammu'alaikum wr. wb." +
		    								"\n_Admin Siraajul Ummah._";
		                			
									title = "Virtual Account Expired";
							        body = "Virtual account " +transaksi.getVirtualAccount()+ " untuk pembayaran [" + utkBayar+ "] dengan Bank [" + bank + "] Total bayar ["+ total +"] telah kedaluarsa." +
									          "Silahkan masuk kembali ke aplikasi untuk membuat virtual account baru";
					    	        
					    	        //slack report
					    			StringBuilder sb = new StringBuilder();
					    			sb.append("\n* Detail Transaksi *");
					    			sb.append("\nNama      " + siswa.getNama());
					    			sb.append("\nNis       " + siswa.getNis());
					    			sb.append("\nBayar     " + total);
					    			sb.append("\nUntuk     " + utkBayar);
					    			sb.append("\nKode Va   " + transaksi.getVirtualAccount());
					    			sb.append("\nBank      " + bank);
					    			sb.append("\nStatus    " + "EXPIRED");
					    			sb.append("\nExp On    " + exp);
							        
//									LogUtils.debug(sb.toString(), log, slackService);
		            			}
						} else if (respon.getStatus().equals("CANCELED")) {
							if (tagihan.getStatus().equals(StatusBayar.MENUNGGU)) {
	            				tagihan.setStatus(StatusBayar.BELUMBAYAR);
	            				tagihan.setVirtualAccount(null);
	            				tagihan.setTanggalBayar(null);
	            				tagihan.setTransaksi(null);
	            				
	            				tagihanRepository.save(tagihan);
	            				transaksiRepository.save(transaksi);
	                			save(respon);
	                			
	                			content = "*Transaksi Expired*" +
	    								"\n\nAssalamu'alaikum wr. wb. " +
	    								"\nBerikut informasi transaksi :" +
	    								"\nNama	    : " + siswa.getNama() +
	    								"\nNis		    : " + siswa.getNis() +
	    								"\nBayar      : " + total +
	    								"\nUntuk      : " + utkBayar +
	    								"\nKode VA : " +"*"+ transaksi.getVirtualAccount() +"*"+
	    								"\nBANK      : " + bank +
//	    								"\nExpired   : " + exp +
	    								"\n\nMasuk ke aplikasi dan buat virtual account kembali untuk melunasi pembayaran anda." +
	    								"\n\nWassalammu'alaikum wr. wb." +
	    								"\n_Admin Siraajul Ummah._";
	                			
								title = "Virtual Account Expired";
						        body = "Virtual account " +transaksi.getVirtualAccount()+ " untuk pembayaran [" + utkBayar+ "] dengan Bank [" + bank + "] Total bayar ["+ total +"] telah kedaluarsa." +
								          "Silahkan masuk kembali ke aplikasi untuk membuat virtual account baru";
				    	        
				    	        //slack report
				    			StringBuilder sb = new StringBuilder();
				    			sb.append("\n* Detail Transaksi *");
				    			sb.append("\nNama      " + siswa.getNama());
				    			sb.append("\nNis       " + siswa.getNis());
				    			sb.append("\nBayar     " + total);
				    			sb.append("\nUntuk     " + utkBayar);
				    			sb.append("\nKode Va   " + transaksi.getVirtualAccount());
				    			sb.append("\nBank      " + bank);
				    			sb.append("\nStatus    " + "CANCELED");
//				    			sb.append("\nExp On    " + exp);
//								LogUtils.debug(sb.toString(), log, slackService);
	            			}
						} else {
				    		log.debug("virtual account null tagihan id:{}", tagihan.getId());
			    	        
			    	        //slack report
			    			StringBuilder sb = new StringBuilder();
			    			sb.append("\n* Detail Transaksi *");
			    			sb.append("\nNama      " + siswa.getNama());
			    			sb.append("\nNis       " + siswa.getNis());
			    			sb.append("\nBayar     " + total);
			    			sb.append("\nUntuk     " + utkBayar);
			    			sb.append("\nKode Va   " + transaksi.getVirtualAccount());
			    			sb.append("\nBank      " + bank);
			    			sb.append("\nStatus    " + respon.getStatus()!= null? respon.getStatus(): "FAILED");
					        
//							LogUtils.debug(sb.toString(), log, slackService);
						}
	        		} else {
	        			continue;
	        		}
		            
		            if(tokenNotifikasiRepository.findBySiswaId(siswa.getId()) != null && body != null) {
				          TokenNotifikasi tokenNotifikasi = tokenNotifikasiRepository.findBySiswaId(siswa.getId());
				          
				          messageService.newMessage(
				        		  NotificationChannel.FIREBASE, body,
				        		  null, Instant.now(), Instant.now(), Instant.now(),
				        		  tokenNotifikasi.getTokenNotifikasi(), "SYSTEM", NotificationStatus.NEW, title != null? title : "NOTIFIKASI", user);
		            } 
				} else {
					tagihan.setStatus(StatusBayar.BELUMBAYAR);
    				tagihanRepository.save(tagihan);
    				//slack report
	    			StringBuilder sb = new StringBuilder();
	    			sb.append("\n* Reset Transaksi *");
	    			sb.append("\nid       " + tagihan.getId());
	    			sb.append("\nnama     " + tagihan.getSiswa().getNama());
	    			sb.append("\nnis      " + tagihan.getSiswa().getNis());
	    			sb.append("\nbayar    " + tagihan.getTotal());
	    			sb.append("\nstatus   " + "RESET");
					
//					LogUtils.debug(sb.toString(), log, slackService);
				}
    		}
        }
    }
    
    public Notification save(PaymentResponseDTO respon) {
		log.debug("Save notifikasi from API...");

		Notification notification = new Notification();
		notification.setAmount(Long.valueOf(respon.getAmount()));
		notification.setTransaction_id(respon.getTransaction_id());
		notification.setPayment_code(respon.getPayment_code());
		notification.setCustomer_info(respon.getCustomer_info());
		notification.setPayment_info(respon.getPayment_info());
		notification.setStatus(respon.getStatus());
		notification.setOrder_id(respon.getOrder_id().toString());
		notification.setMerchant_id(respon.getMerchant_id());
		notification.setPayment_channel(respon.getPayment_channel());
		notification.setTransaction_time(DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(respon.getTransaction_time())));
		notification.setExpired_date(respon.getExpired_time() != null ?
				DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(respon.getExpired_time())): null);
//		notification.setPaid_date(respon.getPaid_date() != null ?
//				DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(respon.getPaid_date())): null);
		notification.setNotifOn(Instant.now());
		notification.setStatusUpdate(false);
		notificationRepository.save(notification);
		
		return notification; 
    }

}
