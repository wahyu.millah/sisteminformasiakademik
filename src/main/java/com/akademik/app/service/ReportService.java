package com.akademik.app.service;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.enumeration.MetodeTransaksi;
import com.akademik.app.repository.ReportSyahriahRepository;
import com.akademik.app.service.dto.CellObject;
import com.akademik.app.service.dto.ReportDTO;
import com.akademik.app.service.dto.ReportSyahriahDTO;
import com.akademik.app.util.DateUtils;
import com.akademik.app.util.ExcelUtil;

@Service
@Transactional
public class ReportService {

	private static final Logger log = LoggerFactory.getLogger(ReportService.class);

    private final ReportSyahriahRepository reportSyahriahRepository;
    
	public ReportService(ReportSyahriahRepository reportSyahriahRepository) {
		this.reportSyahriahRepository = reportSyahriahRepository;
	}
    
    public Page<ReportDTO> reportAll(String kelas, String ta, String nis, String jenis, Pageable pageable) {
    	log.debug("Service page report DTO with:{},{},{},{}", kelas, ta,  nis, jenis);
    	if (!jenis.equals("ALL")) {
        	log.debug("jenis");
        	Page<ReportDTO> page = reportSyahriahRepository.generateReportByJenis(Long.valueOf(jenis), pageable);
    		return page;
    	} else {
        	Page<ReportDTO> page = reportSyahriahRepository.generateReportByJenis(Long.valueOf(jenis), pageable);
        	return page;
    	}
    }

	public byte[] buildReportTransaksiHarianItem(String start, String end) {
    	log.debug("buildReportTransaksiHarianItem {} s/d {}", start, end);
        
        LocalDate pDate = ((start != null && !start.isEmpty()) ? DateUtils.parseDate(start, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
        LocalDate pEnd = ((end != null && !end.isEmpty()) ? DateUtils.parseDate(end, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
    	Instant startDate = pDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant endDate = (end != null && !end.isEmpty()) ? pEnd.atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant() : startDate.plusSeconds(86000);
    	
        ByteArrayOutputStream outputStream = null;
    	List<ReportDTO> report = reportSyahriahRepository.generateReportByDateAndItem2(startDate, endDate);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("HARIAN");

        CellStyle headerTextStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, null);
        CellStyle resultTextStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, true, "###,###");
        CellStyle tbDateHeaderStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, "dd-MM-yyyy");
        CellStyle thStyle = ExcelUtil.createStyle(workbook, IndexedColors.LIGHT_YELLOW.getIndex(), true, true, true, null);
        CellStyle tbStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, null);
        CellStyle tbCenterStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, null);
        CellStyle tbNumberStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, true, "###,###");
        thStyle.setWrapText(true);

        int rownum = 0;
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "LAPORAN PENERIMAAN KEUANGAN PER ITEM", headerTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Tanggal Transaksi :", tbDateHeaderStyle)}, sheet);
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(1, "TGL AWAL", tbDateHeaderStyle),
                new CellObject(2, pDate, tbDateHeaderStyle)}, sheet);
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(1, "TGL AKHIR", tbDateHeaderStyle),
                new CellObject(2, pEnd, tbDateHeaderStyle)}, sheet);
        
        rownum++;
        
        sheet.setColumnWidth(0, 1000);
        sheet.setColumnWidth(1, 6000);
        sheet.setColumnWidth(2, 3500);

        ExcelUtil.createRow(rownum++, new CellObject[] {
            new CellObject(0, "No", thStyle),
            new CellObject(1, "Item Pembayaran", thStyle),
            new CellObject(2, "Jumlah", thStyle)}, sheet, (short)300);
        
        Long jml = 0L;
        int no = 0;
        for(ReportDTO r : report) {
            ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, ++no, tbStyle),
                new CellObject(1, r.getItem(), tbStyle),
                new CellObject(2, r.getNominalPembayaran(), tbNumberStyle)}, sheet);
            	jml += r.getNominalPembayaran();
    	}

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(1, "TOTAL", headerTextStyle),
                new CellObject(2, jml, resultTextStyle)}, sheet);
        
        try {
            outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);

            return outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
                if(outputStream != null)
                    outputStream.close();
            } catch (Exception e) {}
        }

        return null;
    }

	public byte[] buildReportTransaksiMingguan(String start, String end) {
    	log.debug("buildReportTransaksiHarian {} s/d {}", start, end);
        
        LocalDate pDate = ((start != null && !start.isEmpty()) ? DateUtils.parseDate(start, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
        LocalDate pEnd = ((end != null && !end.isEmpty()) ? DateUtils.parseDate(end, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
    	Instant startDate = pDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant endDate = (end != null && !end.isEmpty()) ? pEnd.atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant() : startDate.plusSeconds(86000);
    	
        ByteArrayOutputStream outputStream = null;
        log.debug("param:{},{}",startDate,endDate);
    	List<ReportDTO> report = reportSyahriahRepository.generateReportByDate2(startDate, endDate);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("MINGGUAN");

        CellStyle headerTextStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, null);
        CellStyle resultTextStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, true, "###,###");
        CellStyle tbDateHeaderStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, "dd MMMM yyyy");
        CellStyle thStyle = ExcelUtil.createStyle(workbook, IndexedColors.LIGHT_YELLOW.getIndex(), true, true, true, null);
        CellStyle tbStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, null);
        CellStyle tbDateStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, "dd-MM-yyyy");
        CellStyle tbCenterStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, null);
        CellStyle tbNumberStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, true, "###,###");
        thStyle.setWrapText(true);

        int rownum = 0;
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "LAPORAN PENERIMAAN KEUANGAN", headerTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Tanggal Transaksi :", tbDateHeaderStyle)}, sheet);
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(1, "TGL AWAL", tbDateHeaderStyle),
                new CellObject(2, pDate, tbDateHeaderStyle)}, sheet);
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(1, "TGL AKHIR", tbDateHeaderStyle),
                new CellObject(2, pEnd, tbDateHeaderStyle)}, sheet);
        
        rownum++;
        
        sheet.setColumnWidth(0, 1000);
        sheet.setColumnWidth(1, 3500);
        sheet.setColumnWidth(2, 6000);
        sheet.setColumnWidth(3, 1500);
        sheet.setColumnWidth(4, 5500);
        sheet.setColumnWidth(5, 3500);
        sheet.setColumnWidth(6, 3500);

        ExcelUtil.createRow(rownum++, new CellObject[] {
            new CellObject(0, "No", thStyle),
            new CellObject(1, "Tanggal", thStyle),
            new CellObject(2, "Nama Siswa", thStyle),
            new CellObject(3, "Kelas", thStyle),
            new CellObject(4, "Item Pembayaran", thStyle),
            new CellObject(5, "TUNAI", thStyle),
            new CellObject(6, "Transfer VA", thStyle)}, sheet, (short)300);
        
        Long jmlVa = 0L;
        Long jmlTunai = 0L;
        int no = 0;
        for(ReportDTO r : report) {
            String date = DateUtils.format(r.getTglBayar(), "dd-MM-yyyy").toString();
            ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, ++no, tbStyle),
                new CellObject(1, date, tbDateStyle),
                new CellObject(2, r.getNamaSiswa(), tbStyle),
                new CellObject(3, r.getKelas(), tbCenterStyle),
                new CellObject(4, r.getJenisPembayaran(), tbStyle),
                new CellObject(5, (r.getMetodeBayar().equals(MetodeTransaksi.TUNAI))? r.getNominalPembayaran():0, tbNumberStyle),
                new CellObject(6, (r.getMetodeBayar().equals(MetodeTransaksi.TRANSFER))? r.getNominalPembayaran():0, tbNumberStyle)}, sheet);
            if (r.getMetodeBayar().equals(MetodeTransaksi.TRANSFER)) {
            	jmlVa += r.getNominalPembayaran();
            } else {
            	jmlTunai += r.getNominalPembayaran();
            }
    	}

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(4, "Jumlah", headerTextStyle),
                new CellObject(5, jmlTunai, resultTextStyle),
                new CellObject(6, jmlVa, resultTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(4, "TOTAL", headerTextStyle),
                new CellObject(6, jmlVa + jmlTunai, resultTextStyle)}, sheet);
        
        try {
            outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);

            return outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
                if(outputStream != null)
                    outputStream.close();
            } catch (Exception e) {}
        }

        return null;
    }

	public byte[] buildReportTransaksiHarian(String date) {
    	log.debug("buildReportTransaksiHarian {}", date);
        
        LocalDate pDate = ((date != null && !date.isEmpty()) ? DateUtils.parseDate(date, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now());
    	Instant startDate = pDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    	Instant endDate = startDate.plusSeconds(86000);
    	
        ByteArrayOutputStream outputStream = null;
    	List<ReportDTO> report = reportSyahriahRepository.generateReportByDate2(startDate, endDate);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("HARIAN");

        CellStyle headerTextStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, null);
        CellStyle resultTextStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, true, "###,###");
        CellStyle tbDateHeaderStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, "dd-MM-yyyy");
        CellStyle thStyle = ExcelUtil.createStyle(workbook, IndexedColors.LIGHT_YELLOW.getIndex(), true, true, true, null);
        CellStyle tbStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, null);
        CellStyle tbCenterStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, null);
        CellStyle tbNumberStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, true, "###,###");
        thStyle.setWrapText(true);

        int rownum = 0;
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "LAPORAN PENERIMAAN KEUANGAN HARIAN", headerTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Tanggal Transaksi " + pDate, tbDateHeaderStyle)}, sheet);
        
        rownum++;
        
        sheet.setColumnWidth(0, 1000);
        sheet.setColumnWidth(1, 6000);
        sheet.setColumnWidth(2, 1500);
        sheet.setColumnWidth(3, 5500);
        sheet.setColumnWidth(4, 3500);
        sheet.setColumnWidth(5, 3500);

        ExcelUtil.createRow(rownum++, new CellObject[] {
            new CellObject(0, "No", thStyle),
            new CellObject(1, "Nama Siswa", thStyle),
            new CellObject(2, "Kelas", thStyle),
            new CellObject(3, "Item Pembayaran", thStyle),
            new CellObject(4, "TUNAI", thStyle),
            new CellObject(5, "Transfer VA", thStyle)}, sheet, (short)300);
        
        Long jmlVa = 0L;
        Long jmlTunai = 0L;
        int no = 0;
        for(ReportDTO r : report) {
            ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, ++no, tbStyle),
                new CellObject(1, r.getNamaSiswa(), tbStyle),
                new CellObject(2, r.getKelas(), tbCenterStyle),
                new CellObject(3, r.getJenisPembayaran(), tbStyle),
                new CellObject(4, (r.getMetodeBayar().equals(MetodeTransaksi.TUNAI))? r.getNominalPembayaran():0, tbNumberStyle),
                new CellObject(5, (r.getMetodeBayar().equals(MetodeTransaksi.TRANSFER))? r.getNominalPembayaran():0, tbNumberStyle)}, sheet);
            if (r.getMetodeBayar().equals(MetodeTransaksi.TRANSFER)) {
            	jmlVa += r.getNominalPembayaran();
            } else {
            	jmlTunai += r.getNominalPembayaran();
            }
    	}

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(3, "Jumlah", headerTextStyle),
                new CellObject(4, jmlTunai, resultTextStyle),
                new CellObject(5, jmlVa, resultTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(3, "TOTAL", headerTextStyle),
                new CellObject(5, jmlVa + jmlTunai, resultTextStyle)}, sheet);
        
        try {
            outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);

            return outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
                if(outputStream != null)
                    outputStream.close();
            } catch (Exception e) {}
        }

        return null;
    }

}
