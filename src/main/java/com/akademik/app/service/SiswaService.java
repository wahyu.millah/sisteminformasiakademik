package com.akademik.app.service;

import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.Authority;
import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.User;
import com.akademik.app.repository.AuthorityRepository;
import com.akademik.app.repository.KelasRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.AuthoritiesConstants;
import com.akademik.app.service.dto.SiswaDTO;

import io.github.jhipster.security.RandomUtil;

@Service
@Transactional
public class SiswaService {

    private final Logger log = LoggerFactory.getLogger(SiswaService.class);

    private final SiswaRepository siswaRepository;

    private final KelasRepository kelasRepository;

    private final UserRepository userRepository;

    private final CacheManager cacheManager;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;
    
    public SiswaService(AuthorityRepository authorityRepository, PasswordEncoder passwordEncoder, CacheManager cacheManager, SiswaRepository siswaRepository, KelasRepository kelasRepository, UserRepository userRepository) {
    	this.siswaRepository = siswaRepository;
    	this.kelasRepository = kelasRepository;
    	this.userRepository = userRepository;
    	this.cacheManager = cacheManager;
    	this.passwordEncoder = passwordEncoder;
    	this.authorityRepository = authorityRepository;
    }

	public Siswa update(@Valid SiswaDTO siswaDTO) {
		 Siswa siswa = siswaRepository.findById(siswaDTO.getSiswaId()).get();
		 Kelas kelas = kelasRepository.findByNama(siswaDTO.getKelas()).get();
		 User user = userRepository.findById(siswaDTO.getUserId()).get();
		 
		 siswa.setNama(siswaDTO.getFirstName() + siswaDTO.getLastName());
		 siswa.setNis(siswaDTO.getNis());
		 siswa.setAlamat(siswaDTO.getAlamat());
		 siswa.setJenisKelamin(siswaDTO.getJenisKelamin());
		 siswa.setKelas(kelas);
		 siswa.setLoundry(siswaDTO.getLoundry());
		 siswa.setJmlBulan(siswaDTO.getJmlBulan());
		 siswa.setBeasiswa(siswaDTO.getBeasiswa());
		 siswa.setJenisBeasiswa(siswaDTO.getJenisBeasiswa());
		 siswa.setTahunAjaran(siswaDTO.getTahunAjaran());
		 siswa.setTanggalLahir(siswaDTO.getTanggalLahir());
		 siswa.setTempatLahir(siswaDTO.getTempatLahir());
		 siswa.setWaliMurid(siswaDTO.getWaliMurid());
		 siswa.setUser(user);
		 siswa.setNoTelephone(siswaDTO.getNoTelephone());
		 	user.setLogin(siswaDTO.getNoTelephone());
		 	user.setFirstName(siswaDTO.getFirstName());
		 	user.setLastName(siswaDTO.getLastName());
		 	user.setEmail(siswaDTO.getNis()+"@localhost.com");
		 	this.clearUserCaches(user);
		 	userRepository.save(user);
		 siswaRepository.save(siswa);
		 
		return siswa;
	}

	public Siswa create(@Valid SiswaDTO siswaDTO) {
		 Kelas kelas = kelasRepository.findByNama(siswaDTO.getKelas()).get();
		 
		 Siswa siswa = new Siswa();
		 siswa.setNama(siswaDTO.getFirstName()+ " " + siswaDTO.getLastName());
		 siswa.setNis(siswaDTO.getNis());
		 siswa.setAlamat(siswaDTO.getAlamat());
		 siswa.setJenisKelamin(siswaDTO.getJenisKelamin());
		 siswa.setKelas(kelas);
		 siswa.setStatus("AKTIF");
		 siswa.setLoundry(siswaDTO.getLoundry());
		 siswa.setJmlBulan(siswaDTO.getJmlBulan());
		 siswa.setBeasiswa(siswaDTO.getBeasiswa());
		 siswa.setJenisBeasiswa(siswaDTO.getJenisBeasiswa());
		 siswa.setTahunAjaran(siswaDTO.getTahunAjaran());
		 siswa.setTanggalLahir(siswaDTO.getTanggalLahir());
		 siswa.setTempatLahir(siswaDTO.getTempatLahir());
		 siswa.setWaliMurid(siswaDTO.getWaliMurid());
			 User user = new User();
			 siswa.setNoTelephone(siswaDTO.getNoTelephone());
			 	user.setLogin(siswaDTO.getNoTelephone());
			 	user.setFirstName(siswaDTO.getFirstName());
			 	user.setLastName(siswaDTO.getLastName());
		        user.setPassword("$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K");
		        user.setResetDate(Instant.now());
		        user.setActivated(true);
		        user.setLangKey("in");
		        user.setResetKey(RandomUtil.generateResetKey());
		        user.setActivationKey(RandomUtil.generateActivationKey());
			 	this.clearUserCaches(user);
			 	Set<Authority> authorities = new HashSet<>();
			 	authorityRepository.findById(AuthoritiesConstants.WALIMURID).ifPresent(authorities::add);
		        authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
			 	user.setAuthorities(authorities);
			 	userRepository.save(user);
			siswa.setUser(user);
		 siswaRepository.save(siswa);
		 
		return siswa;
	}


    public void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) {
            Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
        }
    }

}
