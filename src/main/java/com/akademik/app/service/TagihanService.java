package com.akademik.app.service;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.BesarBayar;
import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.TagihanCicilan;
import com.akademik.app.domain.TokenNotifikasi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.repository.BesarBayarRepository;
import com.akademik.app.repository.JenisPembayaranRepository;
import com.akademik.app.repository.KelasRepository;
import com.akademik.app.repository.ReportSyahriahRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TagihanCicilanRepository;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.repository.TokenNotifikasiRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.dto.CellObject;
import com.akademik.app.service.dto.ReportDTO;
import com.akademik.app.service.dto.ReportSyahriahDTO;
import com.akademik.app.util.DateUtil;
import com.akademik.app.util.ExcelUtil;

import antlr.StringUtils;
import javassist.expr.NewArray;

@Service
@Transactional
public class TagihanService {

    private final Logger log = LoggerFactory.getLogger(TagihanService.class);

    private final BesarBayarRepository besarBayarRepository;

    private final SiswaRepository siswaRepository;

    private final KelasRepository kelasRepository;

    private final TagihanRepository tagihanRepository;

    private final UserRepository userRepository;

    private final JenisPembayaranRepository jenisPembayaranRepository;
    
    private final ReportSyahriahRepository reportSyahriahRepository;

    private final TokenNotifikasiRepository tokenNotifikasiRepository;

    private final MessageService messageService;

    private final TagihanCicilanRepository tagihanCicilanRepository;

    public TagihanService(TagihanCicilanRepository tagihanCicilanRepository, MessageService messageService, TokenNotifikasiRepository tokenNotifikasiRepository, ReportSyahriahRepository reportSyahriahRepository, UserRepository userRepository, JenisPembayaranRepository jenisPembayaranRepository, KelasRepository kelasRepository, BesarBayarRepository besarBayarRepository, SiswaRepository siswaRepository, TagihanRepository tagihanRepository) {
    	this.besarBayarRepository = besarBayarRepository;
    	this.siswaRepository = siswaRepository;
    	this.tagihanRepository = tagihanRepository;
    	this.kelasRepository = kelasRepository;
    	this.jenisPembayaranRepository = jenisPembayaranRepository;
    	this.userRepository = userRepository;
    	this.reportSyahriahRepository = reportSyahriahRepository;
    	this.tokenNotifikasiRepository=tokenNotifikasiRepository;
    	this.messageService=messageService;
    	this.tagihanCicilanRepository=tagihanCicilanRepository;
    }

    public Tagihan createMounthlyTagihan(LocalDate jobDate, String pTagihan, boolean bulanan, boolean cicilan, String perbulan, String tempo, String redaksi) {
    	
    	if (cicilan) {
    		return createTagihanCicilan(jobDate, pTagihan, bulanan, cicilan, perbulan, tempo, redaksi);
    	} else {
    		return createMounthlyTagihan(jobDate, pTagihan, bulanan, tempo, redaksi);
    	}
    }
 // create tagihan dengan fiture cicilan   
    public Tagihan createTagihanCicilan(LocalDate jobDate, String pTagihan, boolean bulanan, boolean cicilan, String perbulan, String tempo, String redaksi) {
		log.debug("generate tagihan cicilan date {}, {}, {}, {}, {}, {}, {}", jobDate, pTagihan, bulanan, cicilan, perbulan, tempo, redaksi);
		    	
		    	List<BesarBayar> listBesarBayar = null;
		    	
		    	if (bulanan == true) {
		    		listBesarBayar = besarBayarRepository.findByJenisBayarMetodeAndJenisBayarJenis("BULANAN",pTagihan);
		    	} else {
		    		listBesarBayar = besarBayarRepository.findByJenisBayarMetodeAndJenisBayarJenis("KHUSUS",pTagihan);
		    	}
		    	
		    	List<Siswa> listSiswa = siswaRepository.findByStatus("AKTIF");
		    	log.debug("list siswa {}", listSiswa.size());
		    	log.debug("list listBesarBayar {}", listBesarBayar.size());
		    	
		    	for (BesarBayar besarBayar : listBesarBayar) { 
						for (Siswa siswa : listSiswa) {
							if (siswa.getKelas() == besarBayar.getKelas()) {
			    					log.debug("tagihan khusus cicilan :{}", besarBayar.getJenisBayar().getJenis());
			    					Long perbln = Long.parseLong(perbulan);
			    					createTagihan(besarBayar, siswa, pTagihan, jobDate, perbln, tempo, redaksi);
		    	    			}
							}
						}
    	return null;
    }
    
    
    public Tagihan createMounthlyTagihan(LocalDate jobDate, String pTagihan, boolean bulanan, String tempo, String redaksi) {
    	log.debug("generate tagihan date {}", jobDate);
    	
    	List<BesarBayar> listBesarBayar = null;
    	
    	if (bulanan == true) {
    		listBesarBayar = besarBayarRepository.findByJenisBayarMetodeAndJenisBayarJenis("BULANAN",pTagihan);
    	} else {
    		listBesarBayar = besarBayarRepository.findByJenisBayarMetodeAndJenisBayarJenis("KHUSUS",pTagihan);
    	}
    	
    	List<Siswa> listSiswa = siswaRepository.findByStatus("AKTIF");
    	log.debug("list siswa {}", listSiswa.size());
    	log.debug("list listBesarBayar {}", listBesarBayar.size());
    	
    	for (BesarBayar besarBayar : listBesarBayar) { 
				for (Siswa siswa : listSiswa) {
					if (siswa.getKelas() == besarBayar.getKelas()) {
	    				if (bulanan == true) {
		    					if(pTagihan.toUpperCase().equals(besarBayar.getJenisBayar().getJenis()) &&  siswa.getLoundry() != null && siswa.getLoundry() == true
		    						&& pTagihan.toUpperCase().equals("LOUNDRY")) {
		    						log.debug("tagihan1:{}",besarBayar.getJenisBayar().getJenis());
		    						createTagihan(besarBayar, siswa, pTagihan, jobDate, tempo, redaksi);
		    					}
		    					
		    					if(pTagihan.toUpperCase().equals(besarBayar.getJenisBayar().getJenis()) && pTagihan.toUpperCase().equals("QURBAN")) {
		    						log.debug("tagihan2:{}", besarBayar.getJenisBayar().getJenis());
		    						createTagihan(besarBayar, siswa, pTagihan, jobDate, tempo, redaksi);
		    					}
		    	    			
		    	    			if (pTagihan.toUpperCase().equals(besarBayar.getJenisBayar().getJenis()) && pTagihan.toUpperCase().equals("SYAHRIAH")) {
		    						log.debug("tagihan3:{}", besarBayar.getJenisBayar().getJenis());
		    						createTagihan(besarBayar, siswa, pTagihan, jobDate, tempo, redaksi);
		    	    			}
	    					} else if (bulanan != true) {
		    					log.debug("tagihan khusus:{}", besarBayar.getJenisBayar().getJenis());
		    					createTagihan(besarBayar, siswa, pTagihan, jobDate, tempo, redaksi);
	    					}
    	    			}
					}
				}
		return null;
    }
	
    public int createTagihanPartial(Long id) {
    	
    	BesarBayar besarBayar = besarBayarRepository.findById(id).get();
    	
    	Long currentMilis = System.currentTimeMillis();
    	List<Siswa> listSiswa = siswaRepository.findByKelasId(besarBayar.getKelas().getId());
    	
			for (Siswa siswa : listSiswa) {
				String jenis = besarBayar.getJenisBayar().getJenis();
				String item = null;
				if(jenis.contains("SYAHRIAH")) {
					item = jenis.split("-")[0];
				} else if(jenis.contains("LOUNDRY")) {
					item = jenis.split("-")[0];
				} else {
					item = jenis;
				}
				Tagihan tagihan = new Tagihan();
		       		tagihan.setNomor("SU"+currentMilis);
		       		tagihan.setTahunAjaran(siswa.getTahunAjaran());
		       		tagihan.setTotal(jenis);
		       		tagihan.setHarusBayar(besarBayar.getNominal());
		       		tagihan.setStatus(StatusBayar.BELUMBAYAR);
		       		tagihan.setSiswa(siswa);
		       		tagihan.setBesarBayar(besarBayar);
		       		tagihan.setMulti(false);
		       		tagihan.setItemTagihan(item);
		       		tagihanRepository.save(tagihan);
			   	   
		            if(tokenNotifikasiRepository.findBySiswaId(siswa.getId()) != null) {
				          TokenNotifikasi tokenNotifikasi = tokenNotifikasiRepository.findBySiswaId(siswa.getId());
				          String body = "Assalamu'alaikum wr wb \n\nKepada Wali Santri Pondok Pesantren Siraajul Ummah \nTagihan " + tagihan.getTotal() + " Untuk santri " + siswa.getNama() +
				        		  "sudah ada di sistem siraajul, silahkan lakukan pembayaran di sistem siraajul. \nTerima Kasih atas perhatiannya" +
				        		  "\n\nWassalamu'alaikum wr wb";
				          
				          messageService.newMessage(NotificationChannel.FIREBASE, body, null, Instant.now(), Instant.now(),
				        		  Instant.now(), tokenNotifikasi.getTokenNotifikasi(), "SYSTEM", NotificationStatus.NEW, "NOTIFIKASI", siswa.getUser());
		            }
			}	
    	
		besarBayar.setStatus("DONE");
		besarBayarRepository.save(besarBayar);
    	
		return 0;
    }
    
//    cicilan
    public void createTagihan (BesarBayar besarBayar, Siswa siswa, String pTagihan, LocalDate jobDate, Long perbulan, String tempo, String redaksi) {
    	Long currentMilis = System.currentTimeMillis();
    	LocalDate bulan = jobDate;
    	
		if (pTagihan.toUpperCase().equals(besarBayar.getJenisBayar().getJenis())) {
				Tagihan tagihan = new Tagihan();
			  			tagihan.setNomor("SU"+currentMilis);
			   			tagihan.setTahunAjaran(siswa.getTahunAjaran());
			   			tagihan.setStatus(StatusBayar.BELUMBAYAR);
			   			if(besarBayar.getJenisBayar().getKeterangan() !=null)
			   				tagihan.setKeterangan(besarBayar.getJenisBayar().getKeterangan());
			   			if (besarBayar.getJenisBayar().getMetode().equals("KHUSUS")) {
				   			tagihan.setTotal(besarBayar.getJenisBayar().getJenis());
			   			} else {
				   			tagihan.setTotal(besarBayar.getJenisBayar().getJenis() +"-"+ bulan.getMonth());
			   			}
			   			tagihan.setHarusBayar(besarBayar.getNominal());
			   			if (besarBayar.getJenisBayar().getJenis().equals("SYAHRIAH")) {
				   			if (siswa.getBeasiswa() != null && siswa.getBeasiswa() == true) {
					        	if (siswa.getJmlBulan() != null && siswa.getJmlBulan() != 0) {
					       			tagihan.setStatus(StatusBayar.BEASISWA);
					       			siswa.setJmlBulan(siswa.getJmlBulan()-1);
					       			if (siswa.getJmlBulan() == 0) siswa.setBeasiswa(false);
					       			siswaRepository.save(siswa);
					        	}
			        		} else {
					   			tagihan.setStatus(StatusBayar.BELUMBAYAR);
			        		}
			   			} else {
				   			tagihan.setStatus(StatusBayar.BELUMBAYAR);
			   			}
			   			tagihan.setSiswa(siswa);
			   			tagihan.setBesarBayar(besarBayar);
			   			tagihan.setIsPublish(0);
			   			tagihan.setMulti(false);
			   			tagihan.setItemTagihan(besarBayar.getJenisBayar().getJenis());
			   			tagihanRepository.save(tagihan);
			   	   
			   	   if (perbulan != null) {
				   		Long total = tagihan.getHarusBayar();
				   		Long blns =  total % perbulan;
				   		Long bln  = (total-blns)/perbulan;
				   		
				   		for(int i=0 ; i < bln; i++) {
					   		TagihanCicilan tagihanCicilan = new TagihanCicilan();
					   		tagihanCicilan.setJmlBayar(perbulan);
							int ke = i+1;
					   		tagihanCicilan.setNamaTagihan(tagihan.getTotal() + " Cicilan Ke: "+ ke);
					   		tagihanCicilan.setStatusBayar(StatusBayar.BELUMBAYAR.toString());
					   		tagihanCicilan.setTagihan(tagihan);
				   			tagihanCicilanRepository.save(tagihanCicilan);
					   		
				   		}
				   		if (blns > 0) {
					   		TagihanCicilan tagihanCicilan = new TagihanCicilan();
					   		tagihanCicilan.setJmlBayar(blns);
							Long ke = bln+1;
					   		tagihanCicilan.setNamaTagihan(tagihan.getTotal() + " Cicilan Ke: "+ ke);
					   		tagihanCicilan.setStatusBayar(StatusBayar.BELUMBAYAR.toString());
					   		tagihanCicilan.setTagihan(tagihan);
				   			tagihanCicilanRepository.save(tagihanCicilan);
				   		}
			   	   }
		   			
		   			String tgl = tempo != null && !"".equals(tempo) ? DateUtil.formatDate(LocalDate.parse(tempo), DateUtil.FORMATTER_DATE) : "10";
		   			String rdks = redaksi != null && !"".equals(redaksi)  ? "\n"+ redaksi : "";
		   			
		            if(tokenNotifikasiRepository.findBySiswaId(siswa.getId()) != null) {
				          TokenNotifikasi tokenNotifikasi = tokenNotifikasiRepository.findBySiswaId(siswa.getId());
				          String body = "Assalamu'alaikum wr wb \nKepada Wali Santri Ponpes Siraajul Ummah \nTagihan " + tagihan.getTotal() + " [" + siswa.getNama() + "] "+
				        		  " sudah ada di sistem siraajul, silahkan lakukan pembayaran sebelum tanggal "+ tgl + rdks +
//				        		  ". \nTerima Kasih atas perhatiannya" +
				        		  "\nWassalamu'alaikum wr wb";
				          
				          messageService.newMessage(NotificationChannel.FIREBASE, body, null, Instant.now(), Instant.now(),
				        		  Instant.now(), tokenNotifikasi.getTokenNotifikasi(), "SYSTEM", NotificationStatus.NEW, "NOTIFIKASI", siswa.getUser());
		            }  
        }
    	
    }
    
    public void createTagihan (BesarBayar besarBayar, Siswa siswa, String pTagihan, LocalDate jobDate, String tempo, String redaksi) {
    	Long currentMilis = System.currentTimeMillis();
    	LocalDate bulan = jobDate;
    	
		if (pTagihan.toUpperCase().equals(besarBayar.getJenisBayar().getJenis().toUpperCase())) {
				Tagihan tagihan = new Tagihan();
			  			tagihan.setNomor("SU"+currentMilis);
			   			tagihan.setTahunAjaran(siswa.getTahunAjaran());
			   			tagihan.setStatus(StatusBayar.BELUMBAYAR);
			   			if(besarBayar.getJenisBayar().getKeterangan() !=null)
			   				tagihan.setKeterangan(besarBayar.getJenisBayar().getKeterangan());
			   			if (besarBayar.getJenisBayar().getMetode().equals("KHUSUS")) {
				   			tagihan.setTotal(besarBayar.getJenisBayar().getJenis());
			   			} else {
				   			tagihan.setTotal(besarBayar.getJenisBayar().getJenis() +"-"+ bulan.getMonth());
			   			}
			   			
			   			tagihan.setHarusBayar(besarBayar.getNominal());
			   			if (besarBayar.getJenisBayar().getJenis().equals("SYAHRIAH")) {
				   			if (siswa.getBeasiswa() != null && siswa.getBeasiswa() == true) {
					        	if (siswa.getJmlBulan() != null && siswa.getJmlBulan() != 0) {
					       			tagihan.setStatus(StatusBayar.BEASISWA);
					       			siswa.setJmlBulan(siswa.getJmlBulan()-1);
					       			if (siswa.getJmlBulan() == 0) siswa.setBeasiswa(false);
					       			siswaRepository.save(siswa);
					        	}
			        		} else {
					   			tagihan.setStatus(StatusBayar.BELUMBAYAR);
			        		}
			   			} else {
				   			tagihan.setStatus(StatusBayar.BELUMBAYAR);
			   			}
			   			tagihan.setSiswa(siswa);
			   			tagihan.setBesarBayar(besarBayar);
			   			tagihan.setIsPublish(0);
			   			tagihan.setMulti(false);
			   			tagihan.setItemTagihan(besarBayar.getJenisBayar().getJenis());
		   				tagihan.setCicilan(false);
			   	   tagihanRepository.save(tagihan);

		   			if(besarBayar.getCicilan()) {
		   				if (besarBayar.getPerbulan() != null) {
					   		Long total = tagihan.getHarusBayar();
					   		Long blns =  total % besarBayar.getPerbulan();
					   		Long bln  = (total-blns)/besarBayar.getPerbulan();
					   		
					   		for(int i=0 ; i < bln; i++) {
						   		TagihanCicilan tagihanCicilan = new TagihanCicilan();
						   		tagihanCicilan.setJmlBayar(besarBayar.getPerbulan());
								int ke = i+1;
						   		tagihanCicilan.setNamaTagihan(tagihan.getTotal() + " Cicilan Ke: "+ ke);
						   		tagihanCicilan.setStatusBayar(StatusBayar.BELUMBAYAR.toString());
						   		tagihanCicilan.setTagihan(tagihan);
					   			tagihanCicilanRepository.save(tagihanCicilan);
						   		
					   		}
					   		if (blns > 0) {
						   		TagihanCicilan tagihanCicilan = new TagihanCicilan();
						   		tagihanCicilan.setJmlBayar(blns);
								Long ke = bln+1;
						   		tagihanCicilan.setNamaTagihan(tagihan.getTotal() + " Cicilan Ke: "+ ke);
						   		tagihanCicilan.setStatusBayar(StatusBayar.BELUMBAYAR.toString());
						   		tagihanCicilan.setTagihan(tagihan);
					   			tagihanCicilanRepository.save(tagihanCicilan);
					   		}
				   	   }
		   				tagihan.setCicilan(true);
		   				tagihanRepository.save(tagihan);
		   			}
		   			
		   			String tgl = tempo != null && !"".equals(tempo) ? DateUtil.formatDate(LocalDate.parse(tempo), DateUtil.FORMATTER_DATE) : "10";
		   			String rdks = redaksi != null && !"".equals(redaksi)  ? "\n"+ redaksi : "";
		   			
		            if(tokenNotifikasiRepository.findBySiswaId(siswa.getId()) != null) {
				          TokenNotifikasi tokenNotifikasi = tokenNotifikasiRepository.findBySiswaId(siswa.getId());
				          String body = "Assalamu'alaikum wr wb \nKepada Wali Santri Ponpes Siraajul Ummah \nTagihan " + tagihan.getTotal() + " [" + siswa.getNama() + "] "+
				        		  " sudah ada di sistem siraajul, silahkan lakukan pembayaran sebelum tanggal "+ tgl + rdks +
//				        		  ". \nTerima Kasih atas perhatiannya" +
				        		  "\nWassalamu'alaikum wr wb";
				          
				          messageService.newMessage(NotificationChannel.FIREBASE, body, null, Instant.now(), Instant.now(),
				        		  Instant.now(), tokenNotifikasi.getTokenNotifikasi(), "SYSTEM", NotificationStatus.NEW, "NOTIFIKASI", siswa.getUser());
		            } 
        }
    	
    }

	public byte[] buildReportPembayaran(String kelasId, String nis, String jenisTagihan, String ta) {
    	log.debug("buildReportPembayaran {},{},{}", kelasId, nis, jenisTagihan);
    	
        ByteArrayOutputStream outputStream = null;
        
		String login = SecurityUtils.getCurrentUserLogin().get();
		Optional<User> user = userRepository.findOneByLogin(login);
        
		List<ReportDTO> report = null;
		String kelas = null;
		Long jenis = null;
		Long siswaId = null;
		String siswa = null;
		
		if(!kelasId.equals("ALL")) {
			log.debug("kelasId:{}", kelasId);
			kelas = kelasRepository.findById(Long.parseLong(kelasId)).get().getNama();
		}
		
		if(!jenisTagihan.equals("ALL")) jenis = jenisPembayaranRepository.findByJenis(jenisTagihan).getId();
        boolean isTU = SecurityUtils.isCurrentUserInRole("ROLE_TU");
        boolean isWALISANTRI = SecurityUtils.isCurrentUserInRole("ROLE_WALI");
        
        if (isTU) {
        	if(!nis.equals("ALL")) siswa = siswaRepository.findByNis(nis).getNama();
        	if(!nis.equals("ALL")) siswaId = siswaRepository.findByNis(nis).getId();
        } else if (isWALISANTRI) {
        	siswaId = siswaRepository.findOneByUser(user.get()).getId();
        	if(!nis.equals("ALL")) siswa = siswaRepository.findById(siswaId).get().getNama();
        }
		
		if (!kelasId.equals("ALL") && !ta.equals("ALL") && !jenisTagihan.equals("ALL")) { // all
			report = tagihanRepository.generateReportByKelasAndJenisAndThnAjaran(Long.parseLong(kelasId), jenis, ta);
		} else if (!kelasId.equals("ALL") && !ta.equals("ALL") && jenisTagihan.equals("ALL")) { // kelas & ta
			report = tagihanRepository.generateReportByKelasAndThnAjaran(Long.parseLong(kelasId), ta);
		} else if (!kelasId.equals("ALL") && ta.equals("ALL") && jenisTagihan.equals("ALL")) {// kelas
			report = tagihanRepository.generateReportByKelas(Long.parseLong(kelasId));
		} else if (!kelasId.equals("ALL") && ta.equals("ALL") && !jenisTagihan.equals("ALL")) {// kelas & jenis
			report = tagihanRepository.generateReportByKelasAndJenis(Long.valueOf(kelasId), jenis);
		} else if (kelasId.equals("ALL") && !ta.equals("ALL") && !jenisTagihan.equals("ALL")) {// ta & jenis
			report = tagihanRepository.generateReportByJenisAndThnAjaran(jenis, ta);
		} else if (kelasId.equals("ALL") && !ta.equals("ALL") && jenisTagihan.equals("ALL")) {// ta
			report = tagihanRepository.generateReportByTahunAjaran(ta);
		} else if (kelasId.equals("ALL") && ta.equals("ALL") && !jenisTagihan.equals("ALL")) {// jenis
			report = tagihanRepository.generateReportByJenis(jenis);
		} else if (siswaId != null) {
			report = tagihanRepository.generateReportBySiswa(siswaId);
		} else {
			report = tagihanRepository.generateAllReport();
		}

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Rekap Pembayaran");

        CellStyle headerTextStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, null);
        CellStyle thStyle = ExcelUtil.createStyle(workbook, IndexedColors.LIGHT_YELLOW.getIndex(), true, true, true, null);
        CellStyle tbStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, null);
        CellStyle tbCenterStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, null);
        CellStyle tbDateStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "dd-MM-yyyy");
        thStyle.setWrapText(true);
        //CellStyle tbIntStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "0");
       //CellStyle tbDoubleStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "0.00");
        CellStyle commaStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "#,##0");

        int rownum = 0;
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "LAPORAN REKAP PEMBAYARAN", headerTextStyle)}, sheet);
        if (isTU) {
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Kelas", headerTextStyle),
                new CellObject(2, !kelasId.equals("ALL") ? kelas : "ALL KELAS", headerTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Jenis Pembayaran", headerTextStyle),
                new CellObject(2, !jenisTagihan.equals("ALL") ? jenisTagihan : "ALL TAGIHAN", headerTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Tahun Ajaran", headerTextStyle),
                new CellObject(2, !ta.equals("ALL") ? ta : "ALL TAHUN AJARAN", headerTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Siswa", headerTextStyle),
                new CellObject(2, siswaId != null ? siswa : "ALL SISWA", headerTextStyle)}, sheet);
        }
        
        if (isWALISANTRI) {

            ExcelUtil.createRow(rownum++, new CellObject[] {
                    new CellObject(0, "Siswa", headerTextStyle),
                    new CellObject(2, "" + siswaId != null ? siswa : "SEMUA", headerTextStyle)}, sheet);
        }
        rownum++;
        rownum++;
        
        sheet.setColumnWidth(0, 1000);
        sheet.setColumnWidth(1, 5000);
        sheet.setColumnWidth(2, 5000);
        sheet.setColumnWidth(3, 3500);
        sheet.setColumnWidth(4, 5000);
        sheet.setColumnWidth(5, 2700);
        sheet.setColumnWidth(6, 3500);
        sheet.setColumnWidth(7, 2700);
        sheet.setColumnWidth(8, 2700);
        sheet.setColumnWidth(9, 4000);

        ExcelUtil.createRow(rownum++, new CellObject[] {
            new CellObject(0, "No. ", thStyle),
            new CellObject(1, "Nis", thStyle),
            new CellObject(2, "Nama", thStyle),
            new CellObject(3, "Kelas-TA", thStyle),
            new CellObject(4, "Jenis", thStyle),
            new CellObject(5, "Jml Bayar (Rp)", thStyle),
            new CellObject(6, "Status", thStyle),
            new CellObject(7, "Tgl Bayar", thStyle),
            new CellObject(8, "Metode", thStyle)
//            new CellObject(9, "Dibayar oleh", thStyle)
            }, sheet);
        
        int no = 0;
        if (report != null) {
            for(ReportDTO r : report) {
            	LocalDate date =r.getTglBayar() != null ? r.getTglBayar().atZone(ZoneId.systemDefault()).toLocalDate(): null;
            	String metode = r.getMetodeBayar() != null ? r.getMetodeBayar().toString() : null;
                ExcelUtil.createRow(rownum++, new CellObject[] {
                    new CellObject(0, ++no, tbCenterStyle),
                    new CellObject(1, r.getNis(), tbStyle),
                    new CellObject(2, r.getNamaSiswa(), tbStyle),
                    new CellObject(3, r.getKelas()+"-"+r.getTa(), tbStyle),
                    new CellObject(4, r.getJenisPembayaran(), tbStyle),
                    new CellObject(5, r.getNominalPembayaran(), commaStyle),
                    new CellObject(6, r.getStatusTagihan().toString(), tbStyle),
                    new CellObject(7, date, tbDateStyle),
                    new CellObject(8, metode, tbStyle)
//                    new CellObject(9, r.getUpdateBy(), tbStyle)
                    }, sheet);
                }
        }
        
        try {
            outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);

            return outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
                if(outputStream != null)
                    outputStream.close();
            } catch (Exception e) {}
        }

        return null;
    }

	public byte[] buildReportSyahriah(String kelasId, String nis, String ta) {
    	log.debug("buildReportPembayaran Syahriah {},{},{}", kelasId, nis, ta);

        ByteArrayOutputStream outputStream = null;
    	List<ReportSyahriahDTO> report = null;
    	String kelas, siswa = null;
		Long siswaId = null;
		
		if(!kelasId.equals("ALL")) {
			log.debug("kelasId:{}", kelasId);
			kelas = kelasRepository.findById(Long.parseLong(kelasId)).get().getNama();
		}
    	
		if (!kelasId.equals("ALL") && ta.equals("ALL")) {
			report = reportSyahriahRepository.findByKelas(ta);
		} else if (kelasId.equals("ALL") && !ta.equals("ALL")) {
			report = reportSyahriahRepository.findByTa(ta);
		} else if (kelasId.equals("ALL") && ta.equals("ALL")) {
			report = reportSyahriahRepository.findAllSyahriah();
		} else {
			report = reportSyahriahRepository.findAllSyahriah();
		}

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("SYAHRIAH");

        CellStyle headerTextStyle = ExcelUtil.createStyle(workbook, 1, false, true, false, null);
        CellStyle thStyle = ExcelUtil.createStyle(workbook, IndexedColors.LIGHT_YELLOW.getIndex(), true, true, true, null);
        CellStyle tbStyle = ExcelUtil.createStyle(workbook, 1, true, false, false, null);
        CellStyle tbCenterStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, null);
        CellStyle tbGreenStyle = ExcelUtil.createStyle(workbook, IndexedColors.BRIGHT_GREEN.getIndex(), true, false, true, null);
        CellStyle tbDateStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "dd-MM-yyyy");
        CellStyle tbNumberStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "###,###");
        thStyle.setWrapText(true);
//        CellStyle tbBoldStyle = ExcelUtil.createStyle(workbook, 1, true, true, false, null);
//        CellStyle tbIntStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "0");
//        CellStyle tbDoubleStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "0.00");
//        CellStyle commaStyle = ExcelUtil.createStyle(workbook, 1, true, false, true, "#,##0");

        int rownum = 0;
        
        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "LAPORAN REKAP SYAHRIAH", headerTextStyle)}, sheet);
//        ExcelUtil.createRow(rownum++, new CellObject[] {
//                new CellObject(0, "Kelas", headerTextStyle),
//                new CellObject(2, !kelasId.equals("ALL") ? kelas : "ALL KELAS", headerTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Tahun Ajaran", headerTextStyle),
                new CellObject(2, !ta.equals("ALL") ? ta : "ALL TAHUN AJARAN", headerTextStyle)}, sheet);

        ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, "Siswa", headerTextStyle),
                new CellObject(2, siswaId != null ? siswa : "ALL SISWA", headerTextStyle)}, sheet);
        rownum++;
        rownum++;
        sheet.setColumnWidth(0, 1000);
        sheet.setColumnWidth(1, 5000);
        sheet.setColumnWidth(2, 5200);
        sheet.setColumnWidth(3, 1500);
        sheet.setColumnWidth(4, 2700);
        sheet.setColumnWidth(5, 2700);
        sheet.setColumnWidth(6, 2700);
        sheet.setColumnWidth(7, 2700);
        sheet.setColumnWidth(8, 2700);
        sheet.setColumnWidth(9, 2700);
        sheet.setColumnWidth(10, 2700);
        sheet.setColumnWidth(11, 2700);
        sheet.setColumnWidth(12, 2700);
        sheet.setColumnWidth(13, 2700);
        sheet.setColumnWidth(14, 2700);
        sheet.setColumnWidth(15, 2700);
        sheet.setColumnWidth(16, 2700);

        ExcelUtil.createRow(rownum++, new CellObject[] {
            new CellObject(0, "No", thStyle),
            new CellObject(1, "Siswa", thStyle),
            new CellObject(2, "Nis", thStyle),
            new CellObject(3, "Kelas", thStyle),
            new CellObject(4, "Th.Ajaran", thStyle),
            new CellObject(5, "Juli", thStyle),
            new CellObject(6, "Agustus", thStyle),
            new CellObject(7, "September", thStyle),
            new CellObject(8, "Oktober", thStyle),
            new CellObject(9, "November", thStyle),
            new CellObject(10, "Desember", thStyle),
            new CellObject(11, "Januari", thStyle),
            new CellObject(12, "Februari", thStyle),
            new CellObject(13, "Maret", thStyle),
            new CellObject(14, "April", thStyle),
            new CellObject(15, "Mei", thStyle),
            new CellObject(16, "Juni", thStyle)}, sheet, (short)300);
        
        int no = 0;
        for(ReportSyahriahDTO r : report) {
            ExcelUtil.createRow(rownum++, new CellObject[] {
                new CellObject(0, ++no, tbStyle),
                new CellObject(1, r.getNamaSiswa(), tbStyle),
                new CellObject(2, r.getNis(), tbCenterStyle),
                new CellObject(3, r.getKelas(), tbCenterStyle),
                new CellObject(4, r.getTahunAjaran(), tbCenterStyle),
                new CellObject(5, r.getJuli(), tbNumberStyle),
                new CellObject(6, r.getAgustus(), tbNumberStyle),
                new CellObject(7, r.getSeptember(), tbNumberStyle),
                new CellObject(8, r.getOktober(), tbNumberStyle),
                new CellObject(9, r.getNovember(), tbNumberStyle),
                new CellObject(10, r.getDesember(), tbNumberStyle),
                new CellObject(11, r.getJanuari(), tbNumberStyle),
                new CellObject(12, r.getFebruari(), tbNumberStyle),
                new CellObject(13, r.getMaret(), tbNumberStyle),
                new CellObject(14, r.getApril(), tbNumberStyle),
                new CellObject(15, r.getMei(), tbNumberStyle),
                new CellObject(16, r.getJuni(), tbNumberStyle)}, sheet);
            }
        
        try {
            outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);

            return outputStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
                if(outputStream != null)
                    outputStream.close();
            } catch (Exception e) {}
        }

        return null;
    }
}
