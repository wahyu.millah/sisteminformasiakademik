package com.akademik.app.service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.akademik.app.config.ApplicationProperties;
import com.akademik.app.domain.Surat;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.service.dto.ReportDTO;
import com.akademik.app.util.AddParagraph;
import com.akademik.app.util.DateUtils;
import com.akademik.app.util.NumberUtils;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.BaseFont;

@Service
public class SuratPenagihanService {

    private final Logger log = LoggerFactory.getLogger(SuratPenagihanService.class);
    
    private final TagihanRepository tagihanRepository;
    
    private final ApplicationProperties applicationProperties;
    
    private final CreateSuratService createSuratService;
     
    public SuratPenagihanService (CreateSuratService createSuratService, ApplicationProperties applicationProperties, TagihanRepository tagihanRepository) {
    	this.tagihanRepository = tagihanRepository;
    	this.applicationProperties = applicationProperties;
    	this.createSuratService = createSuratService;
    }

    public String generateSuratTagihan(String siswaId, String ta) throws IOException {
		log.debug("Generate Surat Tagihan...");
        File result = null;

		try {
			File template = new File(applicationProperties.getDirectory().getTemplate()+ "kop.pdf");
			
			Surat surat = createSuratService.create(siswaId);
	        String filePath = applicationProperties.getDirectory().getBase()+ "surattagihan/" + ta +"/" + surat.getSiswa().getKelas().getNama() +"/";
	        String thnAjaran = ta.replace('-', '/');
	        
	    	Map<Integer, String> bulan = new HashMap<>();
	    	bulan.put(1, "I");
	    	bulan.put(2, "II");
	    	bulan.put(3, "III");
	    	bulan.put(4, "IV");
	    	bulan.put(5, "V");
	    	bulan.put(6, "VI");
	    	bulan.put(7, "VII");
	    	bulan.put(8, "VIII");
	    	bulan.put(9, "IX");
	    	bulan.put(10, "X");
	    	bulan.put(11, "XI");
	    	bulan.put(12, "XII");
			
			String nomorSurat = surat.getUsedBy() + "." + surat.getNomor() + "/" + surat.getNama()+ "/" + bulan.get(surat.getTglKeluar().getMonthValue())+ "/" + surat.getTglKeluar().getYear();
			String alamat = surat.getSiswa().getAlamat() != null ? surat.getSiswa().getAlamat() :"";
			String ortu = surat.getSiswa().getWaliMurid() != null ? surat.getSiswa().getWaliMurid() : surat.getSiswa().getNamaAyah() != null ? surat.getSiswa().getNamaAyah() : "";
			
            PDDocument document = PDDocument.load(template);
			PDFont fontRegular = PDType1Font.HELVETICA;
//			BaseFont font = PDType1Font.HELVETICA.;
//            BaseFont b_font = BaseFont.createFont(fontFile, BaseFont.CP1257, BaseFont.EMBEDDED);
			PDFont fontBold = PDType1Font.HELVETICA_BOLD;
//            Font f_normal = new Font(font, 9, Font.NORMAL, BaseColor.BLACK);
			float fontSize = 9;
	        String letterDate = LocalDate.now().format(DateUtils.FORMATTER_ID_LOCAL_DATE);

			PDPage page = document.getPage(0);
			PDPageContentStream contentStream = new PDPageContentStream(document, page,
					PDPageContentStream.AppendMode.APPEND, true, true);
			
			appendPDFContentStream(contentStream, fontRegular, fontSize, 115,644, nomorSurat, false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 115,630, "Penting", false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 115,615, "-", false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 115,601, "Penagihan", false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 132,558, surat.getSiswa().getNama(), false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 248,470, thnAjaran, false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 190,455, surat.getSiswa().getNama(), false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 190,440, surat.getSiswa().getKelas().getNama() , false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 190,427, alamat, false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 190,412, ortu, false);
			appendPDFContentStream(contentStream, fontRegular, fontSize, 468,136, letterDate, false);

			int interval = -13;
			int firstY = 353;
			int int2 = 0;
			int int3 = 0;
			
			List<StatusBayar> defaultStatusList = new ArrayList<>();
			defaultStatusList.add(StatusBayar.BELUMBAYAR);
			defaultStatusList.add(StatusBayar.MENUNGGU);
			
			List<ReportDTO> data = tagihanRepository.generateReportByDateAndItem2(Long.parseLong(siswaId),defaultStatusList, thnAjaran);
			List<ReportDTO> listSyahriah = tagihanRepository.listSyahriahLondry(Long.parseLong(siswaId),defaultStatusList, thnAjaran, "SYAHRIAH");
			List<ReportDTO> listLoundry = tagihanRepository.listSyahriahLondry(Long.parseLong(siswaId),defaultStatusList, thnAjaran, "LOUNDRY");
			Long tot = (long) 0;
			
			for (int i = 0; i < data.size(); i++) {
				ReportDTO row = data.get(i);
				int no = i+1;
				int y = i * interval + firstY + int2 + int3;
					String number = NumberUtils.format(no, NumberUtils.FORMATTER_LONG_EN);
					appendPDFContentStream(contentStream, fontRegular, fontSize, 50, y, number, true);
					appendPDFContentStream(contentStream, fontRegular, fontSize, 70, y, row.getItem(), false);
					String rincianSyahriah = "";
					String rincianLoundry = "";
					if("SYAHRIAH".equals(row.getItem())) {
						Long itemSat= (long) 0;
						if(listSyahriah.size()>0) {
							for (ReportDTO syahriah : listSyahriah) {
								rincianSyahriah += syahriah.getItem().split("-")[1].substring(0, 3).toLowerCase() + ", ";
								itemSat = syahriah.getNominalPembayaran();
							}
							int y2 = y + interval;
							int2 = interval;
							String detail = listSyahriah.size() + " bulan x " + NumberUtils.format(itemSat, NumberUtils.FORMATTER_LONG_EN); 
							appendPDFContentStream(contentStream, fontRegular, fontSize, 70, y2, rincianSyahriah, false);
							appendPDFContentStream(contentStream, fontRegular, fontSize, 340, y2, detail, false);

							appendPDFContentStream(contentStream, fontRegular, fontSize, 500, y2, "Rp.", true);
							String harusBayar = NumberUtils.format(row.getNominalPembayaran(), NumberUtils.FORMATTER_LONG_EN);
							appendPDFContentStream(contentStream, fontRegular, fontSize, 560, y2, harusBayar, true);
						}
					} 
					
					if("LOUNDRY".equals(row.getItem())) {
						Long itemSatL= (long) 0;
						if(listLoundry.size()>0) {
							for (ReportDTO loundry : listLoundry) {
								rincianLoundry += loundry.getItem().split("-")[1].substring(0, 3).toLowerCase() + ", ";
								itemSatL = loundry.getNominalPembayaran();
							}
							int y3 = y + interval;
							int3 = interval;
							String detail = listLoundry.size() + " bulan x " + NumberUtils.format(itemSatL, NumberUtils.FORMATTER_LONG_EN); 
							appendPDFContentStream(contentStream, fontRegular, fontSize, 70, y3, rincianLoundry, false);
							appendPDFContentStream(contentStream, fontRegular, fontSize, 340, y3, detail, false);

							appendPDFContentStream(contentStream, fontRegular, fontSize, 500, y3, "Rp.", true);
							String harusBayar = NumberUtils.format(row.getNominalPembayaran(), NumberUtils.FORMATTER_LONG_EN);
							appendPDFContentStream(contentStream, fontRegular, fontSize, 560, y3, harusBayar, true);
						}
					} 
					

					if(!"LOUNDRY".equals(row.getItem())) {
						if (!"SYAHRIAH".equals(row.getItem())) {
							appendPDFContentStream(contentStream, fontRegular, fontSize, 500, y, "Rp.", true);
							String harusBayar = NumberUtils.format(row.getNominalPembayaran(), NumberUtils.FORMATTER_LONG_EN);
							appendPDFContentStream(contentStream, fontRegular, fontSize, 560, y, harusBayar, true);
						}
					}
					tot = tot + row.getNominalPembayaran();
			}


//            AddParagraph.addParagraph(document,0,0,null,"Demikian surat pemberitahuan kami sampaikan, atas perhatiannya diucapkan terima kasih", f_normal);
			
			appendPDFContentStream(contentStream, fontBold, fontSize, 500, 208, "Rp.", true);
			appendPDFContentStream(contentStream, fontBold, fontSize, 500, 208, "Rp.", true);
			String total = NumberUtils.format(tot, NumberUtils.FORMATTER_LONG_EN);
			appendPDFContentStream(contentStream, fontBold, fontSize, 560, 208, total, true);
			
			contentStream.close();
			File resultDirectory = new File(filePath);
			if (!resultDirectory.exists())
				resultDirectory.mkdir();

			result = new File(resultDirectory +"/"+ surat.getSiswa().getNama()+ surat.getNomor() +"(" +siswaId + ").pdf");
			document.save(result);
			document.close();	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result.getCanonicalPath().toString();
	}

	private static void appendPDFContentStream(PDPageContentStream contentStream, PDFont font, float fontSize,
			float lineTx, float lineTy, String text, boolean rightAlign) throws IOException {
		contentStream.beginText();
		contentStream.setFont(font, fontSize);
		if (rightAlign) {
			contentStream.newLineAtOffset(lineTx - ((font.getStringWidth(text) / 1000.0f) * fontSize), lineTy);
		} else {
			contentStream.newLineAtOffset(lineTx, lineTy);
		}
		contentStream.showText(text);
		contentStream.endText();
	}
}
