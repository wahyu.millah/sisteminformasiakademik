package com.akademik.app.service;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.Message;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;
import com.akademik.app.repository.MessageRepository;
import com.akademik.app.service.dto.WaResponDTO;

@Service
@Transactional
public class MessageService {

    private final Logger log = LoggerFactory.getLogger(MessageService.class);

    private final MessageRepository messageRepository;

    private final WaService waService;

    private final NotificationService notificationService;
    
    public MessageService (MessageRepository messageRepository, WaService waService, NotificationService notificationService) {
    	this.messageRepository = messageRepository;
    	this.waService = waService;
    	this.notificationService = notificationService;
    }
    
    public Message newMessage(NotificationChannel channel, String content, String attachment, Instant datetime,
    		Instant receiveDatetime, Instant sendDatetime, String receiver, String sender, NotificationStatus status,
    		String subject, User user) {
    	log.debug("Simpan Pesan");
    	
    	Message message = new Message();
    			message.setChannel(channel);
    			message.setContent(content);
    			message.setAttachment(attachment);
    			message.setDatetime(datetime);
    			message.setReceiveDatetime(receiveDatetime);
    			message.setSendDatetime(sendDatetime);
    			message.setReceiver(receiver);
    			message.setSender(sender);
    			message.setStatus(status);
    			message.setSubject(subject);
    			message.setUser(user);
    			messageRepository.save(message);
    			
    			if (content != null) {
        			WaResponDTO result = waService.sendWhatsapp(receiver, content);
        			if (result != null) 
        			if (result.getSuccess()) {
            			message.setSendDatetime(Instant.now());
            			message.setReceiveDatetime(Instant.now());
            			message.setStatus(NotificationStatus.SENT);
        			} else {
            			message.setSendDatetime(Instant.now());
            			message.setStatus(NotificationStatus.FAILED);
        			}
        			
        			notificationService.sendNotifikasi(receiver, content, subject);
    			}
		return message;
    }
}
