package com.akademik.app.service;

import java.time.Instant;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.TokenNotifikasi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TokenNotifikasiRepository;
import com.akademik.app.repository.UserRepository;

@Service
@Transactional
public class BlashNotifikasiService {

    private final Logger log = LoggerFactory.getLogger(BlashNotifikasiService.class);

    private final TokenNotifikasiRepository tokenNotifikasiRepository;

    private final MessageService messageService;

    private final UserRepository userRepository;

    private final SiswaRepository siswaRepository;
    
    public BlashNotifikasiService(TokenNotifikasiRepository tokenNotifikasiRepository, MessageService messageService, UserRepository userRepository, SiswaRepository siswaRepository) {
    	this.tokenNotifikasiRepository = tokenNotifikasiRepository;
    	this.messageService = messageService;
    	this.userRepository = userRepository;
    	this.siswaRepository = siswaRepository;
    }
    
    public void sendToAllNotif(String content, String titel) {
    	log.debug("Start sending notifikasi. . .");
    	List<TokenNotifikasi> lists = tokenNotifikasiRepository.findAll();
    	
    	for (TokenNotifikasi list : lists) {
            User user = userRepository.findById(siswaRepository.findById(list.getSiswaId()).get().getUser().getId()).get();
            
    		messageService.newMessage(
	        		  NotificationChannel.FIREBASE, content,
	        		  null, Instant.now(), Instant.now(), Instant.now(),
	        		  list.getTokenNotifikasi(), "SYSTEM", NotificationStatus.NEW, titel, user);
		}
    }

}
