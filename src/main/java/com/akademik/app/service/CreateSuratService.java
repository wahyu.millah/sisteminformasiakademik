package com.akademik.app.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Surat;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.SuratRepository;

@Service
public class CreateSuratService {

    private final Logger log = LoggerFactory.getLogger(CreateSuratService.class);
    
    private final SiswaRepository siswaRepository;

    private final SuratRepository suratRepository;
    
	public CreateSuratService(SuratRepository suratRepository, SiswaRepository siswaRepository) {
		this.siswaRepository = siswaRepository;
		this.suratRepository = suratRepository;
	}
	
	public Surat create(String siswaId) {
		log.debug("Create nomor surat");
		
		Integer number = 0;
		
		Siswa siswa = siswaRepository.findById(Long.parseLong(siswaId)).get();
        LocalDate start = LocalDate.now().withDayOfMonth(1);
        LocalDate end = LocalDate.now().plusMonths(1).withDayOfMonth(1).minusDays(1);

		log.debug("start date {} end date {} ", start, end);
        
		List<Surat> listSurat = suratRepository.findByTglKeluarBetween(start, end);
		Surat surat = new Surat();
		
		if (listSurat.size() > 0 && listSurat != null) {
			number = listSurat.size() + 1 ;
		} else {
			number = 1;	
		}
		
		surat.setNomor(number);
		surat.setNama("PPSU");
		surat.setUsedBy("SPbS");
		surat.setTglKeluar(LocalDate.now());
		surat.setSiswa(siswa);
		suratRepository.save(surat);
		
		return surat;
	}

}
