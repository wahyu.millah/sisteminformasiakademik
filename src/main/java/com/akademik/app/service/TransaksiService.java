package com.akademik.app.service;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;

import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.Channel;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.TokenNotifikasi;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.CaraBayar;
import com.akademik.app.domain.enumeration.MetodeTransaksi;
import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.domain.enumeration.StatusTransaksi;
import com.akademik.app.repository.ChannelRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.repository.TokenNotifikasiRepository;
import com.akademik.app.repository.TransaksiRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.dto.PaymentResponseDTO;
import com.akademik.app.service.dto.TagihanDTO;
import com.akademik.app.util.DateUtils;
import com.akademik.app.util.NumberUtils;

@Service
@Transactional
public class TransaksiService {

    private final Logger log = LoggerFactory.getLogger(TransaksiService.class);

    private final TagihanRepository tagihanRepository;

    private final TransaksiRepository transaksiRepository;

    private final UserRepository userRepository;

    private final SiswaRepository siswaRepository;

    private final MessageService messageService;

    private final PaymentService paymentService;

    private final ChannelRepository channelRepository;

    private final TokenNotifikasiRepository tokenNotifikasiRepository;
    
    private final HistoryTransactionService historyTransactionService;
    
    public TransaksiService (HistoryTransactionService historyTransactionService, TokenNotifikasiRepository tokenNotifikasiRepository, SiswaRepository siswaRepository,MessageService messageService, ChannelRepository channelRepository, UserRepository userRepository, PaymentService paymentService, TagihanRepository tagihanRepository, TransaksiRepository transaksiRepository) {
		this.tagihanRepository = tagihanRepository;
		this.transaksiRepository = transaksiRepository;
        this.userRepository = userRepository;
        this.paymentService = paymentService;
        this.channelRepository = channelRepository;
        this.messageService = messageService;
        this.siswaRepository = siswaRepository;
        this.tokenNotifikasiRepository = tokenNotifikasiRepository;
        this.historyTransactionService = historyTransactionService;
	}
    
    public Transaksi createMultiTransaksi (String[] ids, String va) {
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();
        Siswa siswa = siswaRepository.findByUser(user);
        Long currentMilis = System.currentTimeMillis();
        Long totalAmount = (long) 0;
        
        Channel ch = channelRepository.findByName(va.toUpperCase().split("_")[1]);
        
        TagihanDTO tagihanDTO = null;
        Tagihan tagihan = null ;   
        Transaksi transaksi = new Transaksi();
        transaksi.setUpdateOn(Instant.now());
        transaksi.setUpdateBy(user.getFirstName()+" "+user.getLastName());
        transaksi.setNomor("SU"+currentMilis.toString());
        transaksi.setJmlSisa((long) 0);
        transaksi.setStatus(StatusTransaksi.PROSES);
        transaksi.setTanggal(Instant.now());
        transaksi.setMetode(MetodeTransaksi.TRANSFER);
        transaksi.setCaraBayar(CaraBayar.CASH);
        transaksi.setBank(va);
         
//        tagihan.setHarusBayar(tagihanDTO.getJmlTagihan());
        transaksiRepository.save(transaksi);
        
            try {
            	log.debug("id transaksi:{}", transaksi.getId());
            	String tghnId ="";
	            
	        	for (String id : ids) {
	            	log.debug("id tagihan:"+ id,va);
	            	tagihanDTO = tagihanRepository.findTagihanById(Long.parseLong(id)).get();
	            	tagihan = tagihanRepository.findById(Long.parseLong(id)).get();
	            	tagihan.setStatus(StatusBayar.MENUNGGU);
	                tagihan.setTanggalBayar(Instant.now());
	            	totalAmount = totalAmount + tagihanDTO.getNominalPembayaran();
	            	log.debug("totalAmount:{}"+ totalAmount);
		            tagihan.setTransaksi(transaksi);
		            tagihan.setType(1);
		            tagihanRepository.save(tagihan);
		            tghnId += id+",";
	    		}
	        	
	            transaksi.setJmlTagiha(totalAmount); // gabungan
	    	    transaksi.setJmlBayar(totalAmount+ch.getFee());// gabungan
	            historyTransactionService.recordHostoryTransaction(transaksi.getId(), tghnId.substring(0, tghnId.length()-1), va, 
	            		siswa.getId(), tagihan.getBesarBayar().getJenisBayar().getJenis(), tagihan.getHarusBayar());
	        	
            	PaymentResponseDTO result = paymentService.createMultiPaymentV2(transaksi, va, transaksi.getId(), siswa);
            	if (result.getStatus().equals("ACTIVE") &&  result.getPayment_code() != null) {
		            transaksi.setExpiredDate(DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(result.getExpired_time())));
		            transaksi.setTransactionId(result.getTransaction_id());
		            transaksi.setVirtualAccount(result.getPayment_code());
	                transaksi.setJmlSisa(Long.valueOf(0));
		            transaksiRepository.save(transaksi);
            	}
            	
            	for (String id : ids) {
	            	tagihan = tagihanRepository.findById(Long.parseLong(id)).get();
	            	tagihan.setVirtualAccount(result.getPayment_code());
				}
			} catch (Exception e) {
				log.debug("error :", e);
			}
		return transaksi;
	}
    
    public Transaksi createTransaksiMobile (TagihanDTO tagihanDTO, String va) {
        log.debug("REST request to save transaksi service : {}", tagihanDTO.getIdTagihan(), va);

        Tagihan tagihan = tagihanRepository.findById(tagihanDTO.getIdTagihan()).get();
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();
        
        Siswa siswa = siswaRepository.findByUser(user);
        Long currentMilis = System.currentTimeMillis();
        Transaksi transaksi = new Transaksi();
	            transaksi.setUpdateOn(Instant.now());
	            transaksi.setUpdateBy(user.getFirstName()+" "+user.getLastName());
	            transaksi.setNomor("SU"+currentMilis.toString());
	            transaksi.setJmlTagiha(tagihanDTO.getHarusBayar());
	            transaksi.setJmlBayar(tagihanDTO.getJmlBayar());
	            transaksi.setJmlSisa(tagihanDTO.getJmlSisa());
	            transaksi.setStatus(StatusTransaksi.PROSES);
	            transaksi.setTanggal(Instant.now());
	            transaksi.setMetode(MetodeTransaksi.TRANSFER);
	            transaksi.setCaraBayar(tagihanDTO.getCaraBayar());
	            transaksi.setBank(va);
	            tagihan.setHarusBayar(tagihanDTO.getHarusBayar());
	            tagihan.setType(0);
	            tagihan.setIsPublish(0);
	            tagihan.setMulti(false);
	            tagihan.setReportSubmitted(false);
	            transaksiRepository.save(transaksi);
	            log.debug("recordHostoryTransaction tr id: {}" , transaksi.getId());
	            log.debug("recordHostoryTransaction str tg id: {}" , String.valueOf(tagihan.getId()));
	            log.debug("recordHostoryTransaction va: {}" , va );
	            log.debug("recordHostoryTransaction siswa id: {}" ,siswa.getId());
	            log.debug("recordHostoryTransaction tg bb jns: {}" , tagihan.getBesarBayar().getJenisBayar().getJenis());
	            log.debug("recordHostoryTransaction tghb: {}" , tagihan.getHarusBayar());
	            historyTransactionService.recordHostoryTransaction(transaksi.getId(), String.valueOf(tagihan.getId()), va, 
	            		siswa.getId(), tagihan.getBesarBayar().getJenisBayar().getJenis(), tagihan.getHarusBayar());
	            try {
	            	log.debug("id transaksi:{}", transaksi.getId());
	            	PaymentResponseDTO result = paymentService.createPaymentV2(tagihanDTO, va, transaksi.getId());
	            	if (result.getStatus().equals("ACTIVE") &&  result.getPayment_code() != null) {
			            tagihan.setVirtualAccount(result.getPayment_code());
			            tagihan.setTanggalBayar(Instant.now());
			            
			            transaksi.setExpiredDate(DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(result.getExpired_time())));
			            transaksi.setTransactionId(result.getTransaction_id());
			            transaksi.setVirtualAccount(result.getPayment_code());
	
			            
			            if (!tagihan.getBesarBayar().getJenisBayar().getMetode().equals("ANGSURAN")) {
			                log.debug("non angsuran");
			                log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
			                transaksi.setJmlSisa(Long.valueOf(0));
			            	tagihan.setStatus(StatusBayar.MENUNGGU);
			            	transaksi.setJmlBayar(tagihanDTO.getNominalPembayaran());
			                log.debug("jml sisa transaksi: {}", transaksi.getJmlSisa());
				            tagihanRepository.save(tagihan);
			            } else {
			                log.debug("angusran");
			                log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
			                transaksi.setJmlSisa(tagihanDTO.getJmlSisa());
			                transaksi.setJmlBayar(tagihanDTO.getJmlBayar());
			                log.debug("jml sisa transaksi: {}", transaksi.getJmlSisa());
			                if ( transaksi.getJmlSisa() > 0) tagihan.setStatus(StatusBayar.ANGSUR);
			                if (transaksi.getJmlSisa() == 0) tagihan.setStatus(StatusBayar.LUNAS);
			            }
			            
			            transaksiRepository.save(transaksi);
			            tagihan.setTransaksi(transaksi);
			            tagihanRepository.save(tagihan);
			            String bank = transaksi.getBank().toUpperCase().split("_")[1];
			            String exp = DateUtils.formatZonedDateTime(ZonedDateTime.parse(result.getExpired_time()), DateUtils.FORMATTER_ID_LOCAL_DATETIME);
			            String total = "Rp." + NumberUtils.format(transaksi.getJmlBayar(), NumberUtils.FORMATTER_LONG_EN);
			            String utkBayar = tagihan.getTotal();
						
						String content = "*Info Create Virtual Account*" +
								"\n\nAssalamu'alaikum wr. wb. " +
								"\nBerikut informasi virtual account yang telah di buat:" +
								"\nNama	    : " + siswa.getNama() +
								"\nNis		    : " + siswa.getNis() +
								"\nBayar      : " + total +
								"\nUntuk      : " + utkBayar +
								"\nKode VA : " +"*"+ transaksi.getVirtualAccount() +"*"+
								"\nBANK      : " + bank +
								"\nExpired   : " + exp +
								"\n\nHarap Segera melakukan pembayaran di Bank atau outlet terkait sebelum waktu expired yang telah ditentukan." +
								"\n\nTerima Kasih." +
								"\n_Admin Siraajul Ummah._";
//			            if (siswa.getNoTelephone() != null && siswa.getNoTelephone().length() > 0) {
//			            	String[] tlpon = siswa.getNoTelephone().split("/");
//			            	for (String string : tlpon) {
//			            		String tlp = string.trim().replace("-","");
//					            messageService.newMessage(
//					            		NotificationChannel.WA, content,
//					            		null, Instant.now(), Instant.now(), Instant.now(),
//					            		tlp, "SYSTEM", NotificationStatus.NEW, "Notif Create VA", user);
//							}
//			            }
			            
			            if(tokenNotifikasiRepository.findBySiswaId(siswa.getId()) != null) {
					          TokenNotifikasi tokenNotifikasi = tokenNotifikasiRepository.findBySiswaId(siswa.getId());
					          String body = "Virtual account " +transaksi.getVirtualAccount()+ " untuk pembayaran [" + utkBayar+ "] dengan Bank [" + bank + "] Total bayar ["+ total +"] berhasil dibuat." +
					          "Silahkan melakukan pembayaran di Bank atau outlet terkait sebelum waktu expired [" + exp + "]";
					          
					          messageService.newMessage(NotificationChannel.FIREBASE, body, null, Instant.now(), Instant.now(),
					        		  Instant.now(), tokenNotifikasi.getTokenNotifikasi(), "SYSTEM", NotificationStatus.NEW, "NOTIFIKASI", user);
			            }  
	            	}
				} catch (Exception e) {
					log.debug("error :", e);
				}
	            
				return transaksi;
	} 
    
	public ResponseEntity<Tagihan> createTransaksi (TagihanDTO tagihanDTO, String va) {
        log.debug("REST request to save transaksi service : {}", tagihanDTO.getIdTagihan(), va);

        Tagihan tagihan = tagihanRepository.findById(tagihanDTO.getIdTagihan()).get();
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();
        Siswa siswa = siswaRepository.findByUser(user);
        Long currentMilis = System.currentTimeMillis();
        Transaksi transaksi = new Transaksi();
	            transaksi.setUpdateOn(Instant.now());
	            transaksi.setUpdateBy(user.getFirstName()+" "+user.getLastName());
	            transaksi.setNomor("SU"+currentMilis.toString());
	            transaksi.setJmlTagiha(tagihanDTO.getNominalPembayaran());
	            transaksi.setJmlBayar(tagihanDTO.getJmlBayar());
	            transaksi.setJmlSisa(tagihanDTO.getJmlSisa());
	            transaksi.setStatus(StatusTransaksi.PROSES);
	            transaksi.setTanggal(Instant.now());
	            transaksi.setMetode(MetodeTransaksi.TRANSFER);
	            transaksi.setCaraBayar(tagihanDTO.getCaraBayar());
	            transaksi.setBank(va);
	            tagihan.setHarusBayar(tagihanDTO.getNominalPembayaran());
	            tagihan.setType(0);
	            transaksiRepository.save(transaksi);
	            historyTransactionService.recordHostoryTransaction(transaksi.getId(), String.valueOf(tagihan.getId()), va, 
	            		siswa.getId(), tagihan.getBesarBayar().getJenisBayar().getJenis(), tagihan.getHarusBayar());
	            try {
	            	PaymentResponseDTO result = paymentService.createPaymentV2(tagihanDTO, va, transaksi.getId());
	            	log.debug("result:{}", result);
	            	if (result != null && result.getStatus().equals("ACTIVE") &&  result.getPayment_code() != null) {
			            tagihan.setVirtualAccount(result.getPayment_code());
			            tagihan.setTanggalBayar(Instant.now());
			            
			            transaksi.setExpiredDate(DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(result.getExpired_time())));
			            transaksi.setTransactionId(result.getTransaction_id());
			            transaksi.setVirtualAccount(result.getPayment_code());
			            
			            if (!tagihan.getBesarBayar().getJenisBayar().getMetode().equals("ANGSURAN")) {
			                log.debug("non angsuran");
			                log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
			                transaksi.setJmlSisa(Long.valueOf(0));
			            	tagihan.setStatus(StatusBayar.MENUNGGU);
			            	transaksi.setJmlBayar(tagihanDTO.getNominalPembayaran());
			                log.debug("jml sisa transaksi: {}", transaksi.getJmlSisa());
				            tagihanRepository.save(tagihan);
			            } else {
			                log.debug("angusran");
			                log.debug("jml sisa dto: {}", tagihanDTO.getJmlSisa());
			                transaksi.setJmlSisa(tagihanDTO.getJmlSisa());
			                transaksi.setJmlBayar(tagihanDTO.getJmlBayar());
			                log.debug("jml sisa transaksi: {}", transaksi.getJmlSisa());
			                if ( transaksi.getJmlSisa() > 0) tagihan.setStatus(StatusBayar.ANGSUR);
			                if (transaksi.getJmlSisa() == 0) tagihan.setStatus(StatusBayar.LUNAS);
			            }
			            transaksiRepository.save(transaksi);
			            tagihan.setTransaksi(transaksi);
			            tagihanRepository.save(tagihan);
						
						String content = "*Info Create Virtual Account*" +
								"\n\nAssalamu'alaikum wr. wb. " +
								"\nBerikut informasi virtual account yang telah di buat:" +
								"\nNama	    : " + siswa.getNama() +
								"\nNis		    : " + siswa.getNis() +
								"\nBayar      : Rp." + NumberUtils.format(transaksi.getJmlBayar(), NumberUtils.FORMATTER_LONG_EN) +
								"\nUntuk      : " + tagihan.getTotal() +
								"\nKode VA : " +"*"+ transaksi.getVirtualAccount() +"*"+
								"\nBANK      : " + transaksi.getBank().toUpperCase().split("_")[1] +
								"\nExpired   : " + DateUtils.formatZonedDateTime(ZonedDateTime.parse(result.getExpired_time()), DateUtils.FORMATTER_ID_LOCAL_DATETIME) +
								"\n\nHarap Segera melakukan pembayaran di Bank atau outlet terkait sebelum waktu expired yang telah ditentukan." +
								"\n\nTerima Kasih." +
								"\n_Admin Siraajul Ummah._";
//			            if (siswa.getNoTelephone() != null && siswa.getNoTelephone().length() > 0) {
//			            	String[] tlpon = siswa.getNoTelephone().split("/");
//			            	for (String string : tlpon) {
//			            		String tlp = string.trim().replace("-","");
//					            messageService.newMessage(
//					            		NotificationChannel.WA, content,
//					            		null, Instant.now(), Instant.now(), Instant.now(),
//					            		tlp, "SYSTEM", NotificationStatus.NEW, "Notif Create VA", user);
//							}
//			            }
	            	}
				} catch (Exception e) {
					log.debug("error :", e);
				}
				return null;
	}

}
