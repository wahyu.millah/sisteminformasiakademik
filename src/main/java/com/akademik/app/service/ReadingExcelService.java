package com.akademik.app.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.Siswa;
import com.akademik.app.repository.SiswaRepository;

@Service
@Transactional
public class ReadingExcelService {

	private static final Logger log = LoggerFactory.getLogger(ReadingExcelService.class);

	public ReadingExcelService() {
		
	}
	
	public void reading(String file) throws IOException {
		log.debug("reading excel file...");
		
		FileInputStream ExcelFileToRead = new FileInputStream(file);
        XSSFWorkbook  wb = new XSSFWorkbook(ExcelFileToRead);

        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFRow row; 
        XSSFCell cell;

        Iterator<Row> rows = sheet.rowIterator();

        while (rows.hasNext()) {
            row=(XSSFRow) rows.next();
            if (row.getRowNum() == 0) continue;
            
    		Iterator<Cell> cells = row.cellIterator();
            while (cells.hasNext()){
            	Siswa siswa = new Siswa();
                cell=(XSSFCell) cells.next();
                
                if (cell.getColumnIndex() == 1) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 2) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 3) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 4) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 5) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 6) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 7) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 8) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 9) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 10) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 11) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 11) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 13) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 14) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 15) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 16) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 17) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 18) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 19) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 21) siswa.setNama(cell.getStringCellValue());
                if (cell.getColumnIndex() == 22) siswa.setNama(cell.getStringCellValue());
                
                	if (cell.getCellType() == CellType.STRING) {
                		log.debug(cell.getStringCellValue());
                    } else if (cell.getCellType() == CellType.NUMERIC) {
                		if (cell.getColumnIndex() == 2) {
                			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                	          Date date = cell.getDateCellValue();
                  			log.debug(""+df.format(date));
                		} else {
                    		log.debug(String.valueOf(cell.getNumericCellValue()).split("\\.")[0]);
                		}
                    } else {
//                        Date date = cell.getDateCellValue();
//                    	log.debug("col ke:"+cell.getColumnIndex());
//                    	log.debug("date: {}",date);
                    }
            }
        }
        wb.close();
	}
}
