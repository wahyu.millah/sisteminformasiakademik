package com.akademik.app.service;
//
//import java.io.File;
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.List;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class KwitansiService {

    private final Logger log = LoggerFactory.getLogger(KwitansiService.class);
//
	public void generateKwitansi() {
		log.debug("GenerateKwitansi");
//		List<TransactionGroup> transactionGroups = new ArrayList<>();
//		long subTotalAmount = 0;
//		long subTotalAmountNett = 0;
//		if (priceList.isEmpty())
//			initTransactionPrice();
//		
//		for (TransactionPrice p : priceList) {
//			TransactionGroup group = new TransactionGroup(p.getMinQuantity(), p.getMaxQuantity(), p.getPrice(),
//					p.getPriceTax());
//			int quantity = 0;
//			for (Sertifikat s : sertifikats) {
//				TransactionPrice p2 = priceMap.get(s.getBiayaJasa());
//				if (p2 != null && p2.getPrice() == group.getPrice()) {
//					quantity++;
//				}
//			}
//
//			group.setQuantity(quantity);
//			group.setAmount(quantity * p.getPriceTax());
//			subTotalAmount += group.getAmount();
//			subTotalAmountNett += (quantity * p.getPrice());
//			transactionGroups.add(group);
//		}
//
//		long tax = Math.floorDiv(subTotalAmount * 10, 100);
//		long total = subTotalAmount + tax;
//
//		String subDirectory = orderDate.format(DateUtils.FORMATTER_ISO_LOCAL_MONTH);
//		String contentDate = orderDate.format(DateUtils.FORMATTER_ID_LOCAL_DATE);
//
//		String nomor = StringUtils.padLeft(referensiService.getLeasing().getId().intValue(), 2) + "02"
//				+ orderDate.format(DateUtils.FORMATTER_ISO_LOCAL_DATE_V3);
//
		try {
			File file = new File("D:/source/branch/doc/kwitansi.pdf");
            PDDocument document = PDDocument.load(file);
			PDFont fontRegular = PDType1Font.HELVETICA;
			PDFont fontBold = PDType1Font.HELVETICA_BOLD;
			float fontSize = 10;

			PDPage page = document.getPage(0);
			PDPageContentStream contentStream = new PDPageContentStream(document, page,
					PDPageContentStream.AppendMode.APPEND, true, true);

			appendPDFContentStream(contentStream, fontBold, fontSize, 110,750, "SU1603850839708", false);

			appendPDFContentStream(contentStream, fontBold, fontSize, 110,730, "31 Mei 2021", false);
//
//			int interval = -22;
//			int firstY = 452;
//
//			for (int i = 0; i < transactionGroups.size(); i++) {
//				TransactionGroup group = transactionGroups.get(i);
//				if (group.getQuantity() > 0) {
//					int y = i * interval + firstY;
//					if (group.getMaxQuantity() == 1) {
//						appendPDFContentStream(contentStream, fontRegular, fontSize, 183, y, "Biaya Jasa Single Unit",
//								false);
//
//					} else if (group.getMaxQuantity() == 99999) {
//						appendPDFContentStream(contentStream, fontRegular, fontSize, 183, y,
//								"Biaya Jasa Multi Unit > " + group.getMinQuantity(), false);
//
//					} else {
//						appendPDFContentStream(contentStream, fontRegular, fontSize, 183, y,
//								"Biaya Jasa Multi Unit " + group.getMinQuantity() + " s.d. " + group.getMaxQuantity(),
//								false);
//					}
//
//					String groupQuantity = NumberUtils.format(group.getQuantity(), NumberUtils.FORMATTER_LONG_EN);
//					appendPDFContentStream(contentStream, fontRegular, fontSize, 379, y, groupQuantity, true);
//					String groupPrice = NumberUtils.format(group.getPriceTax(), NumberUtils.FORMATTER_LONG_EN);
//					appendPDFContentStream(contentStream, fontRegular, fontSize, 387, y, "IDR", false);
//					appendPDFContentStream(contentStream, fontRegular, fontSize, 453, y, groupPrice, true);
//					String groupAmount = NumberUtils.format(group.getAmount(), NumberUtils.FORMATTER_LONG_EN);
//					appendPDFContentStream(contentStream, fontRegular, fontSize, 461, y, "IDR", false);
//					appendPDFContentStream(contentStream, fontRegular, fontSize, 549, y, groupAmount, true);
//				}
//			}
//
////			appendPDFContentStream(contentStream, fontRegular, fontSize, 379, 342, contentQuantity, true);
//
//			String subTotalAmountStr = NumberUtils.format(subTotalAmount, NumberUtils.FORMATTER_LONG_EN);
//			appendPDFContentStream(contentStream, fontRegular, fontSize, 461, 293, "IDR", false);
//			appendPDFContentStream(contentStream, fontRegular, fontSize, 549, 293, subTotalAmountStr, true);
//
//			String taxStr = NumberUtils.format(tax, NumberUtils.FORMATTER_LONG_EN);
//			appendPDFContentStream(contentStream, fontRegular, fontSize, 461, 272, "IDR", false);
//			appendPDFContentStream(contentStream, fontRegular, fontSize, 549, 272, taxStr, true);
//			String totalAmountStr = NumberUtils.format(total, NumberUtils.FORMATTER_LONG_EN);
//			appendPDFContentStream(contentStream, fontBold, fontSize, 461, 252, "IDR", false);
//			appendPDFContentStream(contentStream, fontBold, fontSize, 549, 252, totalAmountStr, true);
//
			contentStream.close();
//
			File resultDirectory = new File("D:/source/branch/doc/result");
			if (!resultDirectory.exists())
				resultDirectory.mkdir();

			File result = new File(resultDirectory + "/ " + "SU1603850839708" + ".pdf");
//
			document.save(result);
//
			document.close();
//
//			return invoice;
		} catch (Exception e) {
			e.printStackTrace();
		}
//
//		return null;
	}

	private static void appendPDFContentStream(PDPageContentStream contentStream, PDFont font, float fontSize,
			float lineTx, float lineTy, String text, boolean rightAlign) throws IOException {
		contentStream.beginText();
		contentStream.setFont(font, fontSize);
		if (rightAlign) {
			contentStream.newLineAtOffset(lineTx - ((font.getStringWidth(text) / 1000.0f) * fontSize), lineTy);
		} else {
			contentStream.newLineAtOffset(lineTx, lineTy);
		}
		contentStream.showText(text);
		contentStream.endText();
	}
}
