package com.akademik.app.service;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class EncriptSha256Service {

	private static final Logger log = LoggerFactory.getLogger(EncriptSha256Service.class);
	
	public String encript(String input) {
		MessageDigest messageDigest;
		String hashStr = null;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
			byte hashBytes[] = messageDigest.digest(input.getBytes(StandardCharsets.UTF_8));
			BigInteger noHash = new BigInteger(1, hashBytes);
			hashStr = noHash.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		log.debug("Encript 256 from {} to {}",input, hashStr);
		return hashStr;
	}
	
	
	public String toBase64(String data) {
	    return DatatypeConverter.printBase64Binary(data.getBytes());
	}
	

}
