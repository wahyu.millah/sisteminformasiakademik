package com.akademik.app.service.dto;

public class TransaksiDTO {

	private Long idTransaksi;

    private String virtualAccount;

	private String statusTransaksi;

    private String transactionId;
    
    private String statusTransaksiMCP;
    
    private String namaSiswa;
    
    private String tglBayar;
    
    private String statusTagihan;
    
    private String jenisTagihan;

	public String getNamaSiswa() {
		return namaSiswa;
	}

	public void setNamaSiswa(String namaSiswa) {
		this.namaSiswa = namaSiswa;
	}

	public String getTglBayar() {
		return tglBayar;
	}

	public void setTglBayar(String tglBayar) {
		this.tglBayar = tglBayar;
	}

	public String getStatusTagihan() {
		return statusTagihan;
	}

	public void setStatusTagihan(String statusTagihan) {
		this.statusTagihan = statusTagihan;
	}

	public String getJenisTagihan() {
		return jenisTagihan;
	}

	public void setJenisTagihan(String jenisTagihan) {
		this.jenisTagihan = jenisTagihan;
	}

	public Long getIdTransaksi() {
		return idTransaksi;
	}

	public void setIdTransaksi(Long idTransaksi) {
		this.idTransaksi = idTransaksi;
	}

	public String getVirtualAccount() {
		return virtualAccount;
	}

	public void setVirtualAccount(String virtualAccount) {
		this.virtualAccount = virtualAccount;
	}

	public String getStatusTransaksi() {
		return statusTransaksi;
	}

	public void setStatusTransaksi(String statusTransaksi) {
		this.statusTransaksi = statusTransaksi;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getStatusTransaksiMCP() {
		return statusTransaksiMCP;
	}

	public void setStatusTransaksiMCP(String statusTransaksiMCP) {
		this.statusTransaksiMCP = statusTransaksiMCP;
	}

	public TransaksiDTO(Long idTransaksi, String virtualAccount, String statusTransaksi, String transactionId, String statusTransaksiMCP, String namaSiswa, String tglBayar,
			String statusTagihan, String jenisTagihan) {
		super();
		this.idTransaksi = idTransaksi;
		this.virtualAccount = virtualAccount;
		this.statusTransaksi = statusTransaksi;
		this.transactionId = transactionId;
		this.statusTransaksiMCP = statusTransaksiMCP;
		this.namaSiswa = namaSiswa;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
		this.jenisTagihan = jenisTagihan;
	}

	public TransaksiDTO() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTransaksi == null) ? 0 : idTransaksi.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransaksiDTO other = (TransaksiDTO) obj;
		if (idTransaksi == null) {
			if (other.idTransaksi != null)
				return false;
		} else if (!idTransaksi.equals(other.idTransaksi))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TransaksiDTO [idTransaksi=" + idTransaksi + ", virtualAccount=" + virtualAccount + ", statusTransaksi=" + statusTransaksi + ", transactionId=" + transactionId
				+ ", statusTransaksiMCP=" + statusTransaksiMCP + ", namaSiswa=" + namaSiswa + ", tglBayar=" + tglBayar + ", statusTagihan=" + statusTagihan + ", jenisTagihan="
				+ jenisTagihan + "]";
	}

}
