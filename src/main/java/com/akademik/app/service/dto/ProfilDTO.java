package com.akademik.app.service.dto;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Arrays;

import javax.persistence.Column;

import com.akademik.app.domain.enumeration.JenisKelamin;

public class ProfilDTO {

    private Long userId;

    private String firstName;

    private String lastName;
    
    private Long siswaId;
    
    private String nis;

    private String nama;

    private String alamat;

    private JenisKelamin jenisKelamin;

    private String tempatLahir;

    private Instant tanggalLahir;

    private String waliMurid;

    private String noTelephone;

    private String tahunAjaran;

    private String status;
    
    private String kelas;
    
    private String imageUrl;

    private byte[] photo;

    private String photoContentType;

    private String agama;

    private String anakKe;

    private String madrasahAsal;

    private String kelasMasuk;

    private LocalDate tglMasuk;

    private String namaAyah;

    private String namaIbu;

    private String alamatOrangtua;

    private String pekerjaanAyah;

    private String pekerjaanIbu;

    private String namaWali;

    private String alamatWali;

    private String nomorTelpWali;

    private String pekerjaanWali;

    private String login;

    private String password;
    
    private String nip;
    
    private Long jabatanId;
    
    private String jabatanName;
    
    private Long karyawanId;
    
    private Boolean loundry;
    
    private Boolean beasiswa;
    
    private String jenisBeasiswa;
    
    private Long jmlBulan;

	public Boolean getLoundry() {
		return loundry;
	}

	public void setLoundry(Boolean loundry) {
		this.loundry = loundry;
	}

	public Boolean getBeasiswa() {
		return beasiswa;
	}

	public void setBeasiswa(Boolean beasiswa) {
		this.beasiswa = beasiswa;
	}

	public String getJenisBeasiswa() {
		return jenisBeasiswa;
	}

	public void setJenisBeasiswa(String jenisBeasiswa) {
		this.jenisBeasiswa = jenisBeasiswa;
	}

	public Long getJmlBulan() {
		return jmlBulan;
	}

	public void setJmlBulan(Long jmlBulan) {
		this.jmlBulan = jmlBulan;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Long siswaId) {
		this.siswaId = siswaId;
	}

	public String getNis() {
		return nis;
	}

	public void setNis(String nis) {
		this.nis = nis;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public JenisKelamin getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(JenisKelamin jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public Instant getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Instant tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getWaliMurid() {
		return waliMurid;
	}

	public void setWaliMurid(String waliMurid) {
		this.waliMurid = waliMurid;
	}

	public String getNoTelephone() {
		return noTelephone;
	}

	public void setNoTelephone(String noTelephone) {
		this.noTelephone = noTelephone;
	}

	public String getTahunAjaran() {
		return tahunAjaran;
	}

	public void setTahunAjaran(String tahunAjaran) {
		this.tahunAjaran = tahunAjaran;
	}

	public Boolean getStatus() {
		 if("AKTIF".equals(status)) return true;
		 if("NONAKTIF".equals(status)) return false;
		 return false;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getPhotoContentType() {
		return photoContentType;
	}

	public void setPhotoContentType(String photoContentType) {
		this.photoContentType = photoContentType;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public String getAnakKe() {
		return anakKe;
	}

	public void setAnakKe(String anakKe) {
		this.anakKe = anakKe;
	}

	public String getMadrasahAsal() {
		return madrasahAsal;
	}

	public void setMadrasahAsal(String madrasahAsal) {
		this.madrasahAsal = madrasahAsal;
	}

	public String getKelasMasuk() {
		return kelasMasuk;
	}

	public void setKelasMasuk(String kelasMasuk) {
		this.kelasMasuk = kelasMasuk;
	}

	public LocalDate getTglMasuk() {
		return tglMasuk;
	}

	public void setTglMasuk(LocalDate tglMasuk) {
		this.tglMasuk = tglMasuk;
	}

	public String getNamaAyah() {
		return namaAyah;
	}

	public void setNamaAyah(String namaAyah) {
		this.namaAyah = namaAyah;
	}

	public String getNamaIbu() {
		return namaIbu;
	}

	public void setNamaIbu(String namaIbu) {
		this.namaIbu = namaIbu;
	}

	public String getAlamatOrangtua() {
		return alamatOrangtua;
	}

	public void setAlamatOrangtua(String alamatOrangtua) {
		this.alamatOrangtua = alamatOrangtua;
	}

	public String getPekerjaanAyah() {
		return pekerjaanAyah;
	}

	public void setPekerjaanAyah(String pekerjaanAyah) {
		this.pekerjaanAyah = pekerjaanAyah;
	}

	public String getPekerjaanIbu() {
		return pekerjaanIbu;
	}

	public void setPekerjaanIbu(String pekerjaanIbu) {
		this.pekerjaanIbu = pekerjaanIbu;
	}

	public String getNamaWali() {
		return namaWali;
	}

	public void setNamaWali(String namaWali) {
		this.namaWali = namaWali;
	}

	public String getAlamatWali() {
		return alamatWali;
	}

	public void setAlamatWali(String alamatWali) {
		this.alamatWali = alamatWali;
	}

	public String getNomorTelpWali() {
		return nomorTelpWali;
	}

	public void setNomorTelpWali(String nomorTelpWali) {
		this.nomorTelpWali = nomorTelpWali;
	}

	public String getPekerjaanWali() {
		return pekerjaanWali;
	}

	public void setPekerjaanWali(String pekerjaanWali) {
		this.pekerjaanWali = pekerjaanWali;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public Long getJabatanId() {
		return jabatanId;
	}

	public void setJabatanId(Long jabatanId) {
		this.jabatanId = jabatanId;
	}

	public String getJabatanName() {
		return jabatanName;
	}

	public void setJabatanName(String jabatanName) {
		this.jabatanName = jabatanName;
	}

	public Long getKaryawanId() {
		return karyawanId;
	}

	public void setKaryawanId(Long karyawanId) {
		this.karyawanId = karyawanId;
	}

	public ProfilDTO(Long userId, String nama, String alamat, JenisKelamin jenisKelamin, String tempatLahir, Instant tanggalLahir, String noTelephone, String agama,
			String nip, Long jabatanId, String jabatanName, Long karyawanId) {
		this.userId = userId;
		this.nama = nama;
		this.alamat = alamat;
		this.jenisKelamin = jenisKelamin;
		this.tempatLahir = tempatLahir;
		this.tanggalLahir = tanggalLahir;
		this.noTelephone = noTelephone;
		this.agama = agama;
		this.nip = nip;
		this.jabatanId = jabatanId;
		this.jabatanName = jabatanName;
		this.karyawanId = karyawanId;
	}

	public ProfilDTO(Long userId, String firstName, String lastName, Long siswaId, String nis, String nama, String alamat, JenisKelamin jenisKelamin, String tempatLahir,
			Instant tanggalLahir, String waliMurid, String noTelephone, String tahunAjaran, String status, String kelas, String imageUrl) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.siswaId = siswaId;
		this.nis = nis;
		this.nama = nama;
		this.alamat = alamat;
		this.jenisKelamin = jenisKelamin;
		this.tempatLahir = tempatLahir;
		this.tanggalLahir = tanggalLahir;
		this.waliMurid = waliMurid;
		this.noTelephone = noTelephone;
		this.tahunAjaran = tahunAjaran;
		this.status = status;
		this.kelas = kelas;
		this.imageUrl = imageUrl;
	}

	public ProfilDTO(Long userId, String firstName, String lastName, Long siswaId, String nis, String nama, String alamat, JenisKelamin jenisKelamin, String tempatLahir,
			Instant tanggalLahir, String waliMurid, String noTelephone, String tahunAjaran, String status, String kelas, String imageUrl,
			String agama, String anakKe, String madrasahAsal, String kelasMasuk, LocalDate tglMasuk, String namaAyah, String namaIbu,
			String alamatOrangtua, String pekerjaanAyah, String pekerjaanIbu, String namaWali, String alamatWali, String nomorTelpWali, String pekerjaanWali,
			String login, String password, Boolean loundry, Boolean beasiswa, String jenisBeasiswa, Long jmlBulan) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.siswaId = siswaId;
		this.nis = nis;
		this.nama = nama;
		this.alamat = alamat;
		this.jenisKelamin = jenisKelamin;
		this.tempatLahir = tempatLahir;
		this.tanggalLahir = tanggalLahir;
		this.waliMurid = waliMurid;
		this.noTelephone = noTelephone;
		this.tahunAjaran = tahunAjaran;
		this.status = status;
		this.kelas = kelas;
		this.imageUrl = imageUrl;
		this.agama = agama;
		this.anakKe = anakKe;
		this.madrasahAsal = madrasahAsal;
		this.kelasMasuk = kelasMasuk;
		this.tglMasuk = tglMasuk;
		this.namaAyah = namaAyah;
		this.namaIbu = namaIbu;
		this.alamatOrangtua = alamatOrangtua;
		this.pekerjaanAyah = pekerjaanAyah;
		this.pekerjaanIbu = pekerjaanIbu;
		this.namaWali = namaWali;
		this.alamatWali = alamatWali;
		this.nomorTelpWali = nomorTelpWali;
		this.pekerjaanWali = pekerjaanWali;
		this.login = login;
		this.password = password;
		this.loundry = loundry;
		this.beasiswa = beasiswa;
		this.jenisBeasiswa = jenisBeasiswa;
		this.jmlBulan = jmlBulan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "ProfilDTO [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", siswaId=" + siswaId + ", nis=" + nis + ", nama=" + nama + ", alamat="
				+ alamat + ", jenisKelamin=" + jenisKelamin + ", tempatLahir=" + tempatLahir + ", tanggalLahir=" + tanggalLahir + ", waliMurid=" + waliMurid + ", noTelephone="
				+ noTelephone + ", tahunAjaran=" + tahunAjaran + ", status=" + status + ", kelas=" + kelas + ", imageUrl=" + imageUrl + ", photo=" + Arrays.toString(photo)
				+ ", photoContentType=" + photoContentType + ", agama=" + agama + ", anakKe=" + anakKe + ", madrasahAsal=" + madrasahAsal + ", kelasMasuk=" + kelasMasuk
				+ ", tglMasuk=" + tglMasuk + ", namaAyah=" + namaAyah + ", namaIbu=" + namaIbu + ", alamatOrangtua=" + alamatOrangtua + ", pekerjaanAyah=" + pekerjaanAyah
				+ ", pekerjaanIbu=" + pekerjaanIbu + ", namaWali=" + namaWali + ", alamatWali=" + alamatWali + ", nomorTelpWali=" + nomorTelpWali + ", pekerjaanWali="
				+ pekerjaanWali + ", login=" + login + ", password=" + password + ", nip=" + nip + ", jabatanId=" + jabatanId + ", jabatanName=" + jabatanName
				+ ", karyawanId=" + karyawanId + ", loundry=" + loundry + ", beasiswa=" + beasiswa + ", jenisBeasiswa=" + jenisBeasiswa + ", jmlBulan=" + jmlBulan + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfilDTO other = (ProfilDTO) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
}
