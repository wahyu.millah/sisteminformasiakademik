package com.akademik.app.service.dto;

import java.io.Serializable;

public class PaymentDetailsDTO  implements Serializable {

    private String payment_system;

    private String billing_name;

    private String va_number;

    private Long amount;

    private Long total_fee_amount;

    private Long total_promo_amount;

    private Long total_amount;

    private Boolean is_multi_use;

    private Boolean is_customer_paying_fee;

    private String expired_time;

    private String transaction_description;

	public String getPayment_system() {
		return payment_system;
	}

	public void setPayment_system(String payment_system) {
		this.payment_system = payment_system;
	}

	public String getBilling_name() {
		return billing_name;
	}

	public void setBilling_name(String billing_name) {
		this.billing_name = billing_name;
	}

	public String getVa_number() {
		return va_number;
	}

	public void setVa_number(String va_number) {
		this.va_number = va_number;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getTotal_fee_amount() {
		return total_fee_amount;
	}

	public void setTotal_fee_amount(Long total_fee_amount) {
		this.total_fee_amount = total_fee_amount;
	}

	public Long getTotal_promo_amount() {
		return total_promo_amount;
	}

	public void setTotal_promo_amount(Long total_promo_amount) {
		this.total_promo_amount = total_promo_amount;
	}

	public Long getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(Long total_amount) {
		this.total_amount = total_amount;
	}

	public Boolean getIs_multi_use() {
		return is_multi_use;
	}

	public void setIs_multi_use(Boolean is_multi_use) {
		this.is_multi_use = is_multi_use;
	}

	public Boolean getIs_customer_paying_fee() {
		return is_customer_paying_fee;
	}

	public void setIs_customer_paying_fee(Boolean is_customer_paying_fee) {
		this.is_customer_paying_fee = is_customer_paying_fee;
	}

	public String getExpired_time() {
		return expired_time;
	}

	public void setExpired_time(String expired_time) {
		this.expired_time = expired_time;
	}

	public String getTransaction_description() {
		return transaction_description;
	}

	public void setTransaction_description(String transaction_description) {
		this.transaction_description = transaction_description;
	}

	@Override
	public String toString() {
		return "PaymentDetailsDTO [payment_system=" + payment_system + ", billing_name=" + billing_name + ", va_number="
				+ va_number + ", amount=" + amount + ", total_fee_amount=" + total_fee_amount + ", total_promo_amount="
				+ total_promo_amount + ", total_amount=" + total_amount + ", is_multi_use=" + is_multi_use
				+ ", is_customer_paying_fee=" + is_customer_paying_fee + ", expired_time=" + expired_time
				+ ", transaction_description=" + transaction_description + "]";
	}

}
