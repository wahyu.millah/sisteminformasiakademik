package com.akademik.app.service.dto;

public class KeyValueDTO {
	
	private String key;
	
	private String value;

	public KeyValueDTO(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public KeyValueDTO(Long key, String value) {
		this.key = key != null ? key.toString() : null;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyValueDTO other = (KeyValueDTO) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "KeyValueDTO [key=" + key + ", value=" + value + "]";
	}
	
}
