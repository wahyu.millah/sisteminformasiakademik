package com.akademik.app.service.dto;

public class ResponDevicesWaDTO {
    
    private String id;
    
    private String status;
    
    private String create_at;
    
    private String connected_at;
    
    private String disconnected_at;
    
    private String disconnected_reason;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreate_at() {
		return create_at;
	}

	public void setCreate_at(String create_at) {
		this.create_at = create_at;
	}

	public String getConnected_at() {
		return connected_at;
	}

	public void setConnected_at(String connected_at) {
		this.connected_at = connected_at;
	}

	public String getDisconnected_at() {
		return disconnected_at;
	}

	public void setDisconnected_at(String disconnected_at) {
		this.disconnected_at = disconnected_at;
	}

	public String getDisconnected_reason() {
		return disconnected_reason;
	}

	public void setDisconnected_reason(String disconnected_reason) {
		this.disconnected_reason = disconnected_reason;
	}

	@Override
	public String toString() {
		return "ResponDevicesWaDTO [id=" + id + ", status=" + status + ", create_at=" + create_at + ", connected_at="
				+ connected_at + ", disconnected_at=" + disconnected_at + ", disconnected_reason=" + disconnected_reason
				+ "]";
	}
    
}
