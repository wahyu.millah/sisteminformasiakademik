package com.akademik.app.service.dto;

import java.io.Serializable;

import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;

public class MessageDTO implements Serializable  {
	
	private Long id;
	
    private NotificationChannel channel;

    private String title;
    
    private String body;
    
    private NotificationStatus status;
    
    private Long userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NotificationChannel getChannel() {
		return channel;
	}

	public void setChannel(NotificationChannel channel) {
		this.channel = channel;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public NotificationStatus getStatus() {
		return status;
	}

	public void setStatus(NotificationStatus status) {
		this.status = status;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public MessageDTO(Long id, NotificationChannel channel, String title, String body, NotificationStatus status, Long userId) {
		this.id = id;
		this.channel = channel;
		this.title = title;
		this.body = body;
		this.status = status;
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "MessageDTO [id=" + id + ", channel=" + channel + ", title=" + title + ", body=" + body + ", status=" + status + ", userId=" + userId + "]";
	}
}
