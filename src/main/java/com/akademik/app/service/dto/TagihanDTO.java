package com.akademik.app.service.dto;

import java.time.Instant;
import java.util.List;

import javax.persistence.Column;

import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.enumeration.CaraBayar;
import com.akademik.app.domain.enumeration.MetodeTransaksi;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.domain.enumeration.StatusTransaksi;

public class TagihanDTO {

	private String namaSiswa;

    private String nis;

	private String kelas;

    private String jenisPembayaran;

    private Long nominalPembayaran;

    private Long harusBayar;

    private Long idTagihan;

    private String nomorTagihan;

    private String tglTagihan;

    private Instant tglJatuhTempo;

    private Instant tglBayar;

    private StatusBayar statusTagihan;
    
    private Long idTransaksi;

    private String nomorTransaksi;

    private Instant tanggalTransaksi;

    private MetodeTransaksi metodeTransaksi;

    private String bank;

    private String nomorRekening;

    private String virtualAccount;

    private StatusTransaksi statusTransaksi;

    private String updateBy;

    private Instant updateOn;

    private Long jmlTagihan;

    private Long jmlBayar;

    private Long jmlSisa;

    private CaraBayar caraBayar;
    
    private String metodeBayar;

    private Instant expiredDate;

    private String transactionId;
    
    private Long siswaId;
    
    private Integer typeTagihan;
    
    private List<Tagihan> multiTagihan;
    
    private Long biayaAdmin;
    
    private String keterangan;

    private Boolean multi;
    
    private Long id;
    
    private Long name;
    
    private Boolean cicilan;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getName() {
		return name;
	}

	public void setName(Long name) {
		this.name = name;
	}

	public TagihanDTO(Long id, Long name) {
		this.id = id;
		this.name = name;
	}

	// DTO tagihan per siswa
	public TagihanDTO(String namaSiswa, String nis, String kelas, Long siswaId, String tglTagihan) {
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.siswaId = siswaId;
		this.tglTagihan = tglTagihan;
	}

    public TagihanDTO(Long idTransaksi, Instant tanggalTransaksi, String bank, String virtualAccount, Long jmlTagihan, Long jmlBayar, Instant expiredDate) {
		this.idTransaksi = idTransaksi;
		this.tanggalTransaksi = tanggalTransaksi;
		this.bank = bank;
		this.virtualAccount = virtualAccount;
		this.jmlTagihan = jmlTagihan;
		this.jmlBayar = jmlBayar;
		this.expiredDate = expiredDate;
	}

	public TagihanDTO(String namaSiswa, String nis, String kelas, String jenisPembayaran, Long nominalPembayaran, Long harusBayar, Long idTagihan, String nomorTagihan, String tglTagihan,
			Instant tglJatuhTempo, Instant tglBayar, StatusBayar statusTagihan, String metodeBayar, String virtualAccount,  Integer typeTagihan) {
		super();
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.harusBayar = harusBayar;
		this.idTagihan = idTagihan;
		this.nomorTagihan = nomorTagihan;
		this.tglTagihan = tglTagihan;
		this.tglJatuhTempo = tglJatuhTempo;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
		this.metodeBayar = metodeBayar;
		this.virtualAccount = virtualAccount;
		this.typeTagihan = typeTagihan;
	}

	public TagihanDTO(String namaSiswa, String nis, String kelas, String jenisPembayaran, Long nominalPembayaran, Long harusBayar, Long idTagihan, String nomorTagihan, String tglTagihan,
			Instant tglJatuhTempo, Instant tglBayar, StatusBayar statusTagihan, String metodeBayar, String virtualAccount,  Integer typeTagihan, Long idTransaksi, String keterangan) {
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.harusBayar = harusBayar;
		this.idTagihan = idTagihan;
		this.nomorTagihan = nomorTagihan;
		this.tglTagihan = tglTagihan;
		this.tglJatuhTempo = tglJatuhTempo;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
		this.metodeBayar = metodeBayar;
		this.virtualAccount = virtualAccount;
		this.typeTagihan = typeTagihan;
		this.idTransaksi = idTransaksi;
		this.keterangan = keterangan;
	}

	public TagihanDTO(String namaSiswa, String nis, String kelas, String jenisPembayaran, Long nominalPembayaran, Long harusBayar, Long idTagihan, String nomorTagihan, String tglTagihan,
			Instant tglJatuhTempo, Instant tglBayar, StatusBayar statusTagihan, String virtualAccount,  Integer typeTagihan, Long idTransaksi, String keterangan) {
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.harusBayar = harusBayar;
		this.idTagihan = idTagihan;
		this.nomorTagihan = nomorTagihan;
		this.tglTagihan = tglTagihan;
		this.tglJatuhTempo = tglJatuhTempo;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
		this.virtualAccount = virtualAccount;
		this.typeTagihan = typeTagihan;
		this.idTransaksi = idTransaksi;
		this.keterangan = keterangan;
	}

	public TagihanDTO(String namaSiswa, String nis, String kelas, String jenisPembayaran, Long nominalPembayaran, Long harusBayar, Long idTagihan, String nomorTagihan, String tglTagihan,
			Instant tglJatuhTempo, Instant tglBayar, StatusBayar statusTagihan, String virtualAccount,  Integer typeTagihan, Long idTransaksi, String keterangan, Boolean multi, Boolean cicilan) {
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.harusBayar = harusBayar;
		this.idTagihan = idTagihan;
		this.nomorTagihan = nomorTagihan;
		this.tglTagihan = tglTagihan;
		this.tglJatuhTempo = tglJatuhTempo;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
		this.virtualAccount = virtualAccount;
		this.typeTagihan = typeTagihan;
		this.idTransaksi = idTransaksi;
		this.keterangan = keterangan;
		this.multi = multi;
		this.cicilan = cicilan;
	}
	
	public TagihanDTO() {}

	public TagihanDTO(String namaSiswa, String kelas, String jenisPembayaran, Long harusBayar, Long nominalPembayaran) {
		this.namaSiswa = namaSiswa;
		this.kelas = kelas;
		this.jenisPembayaran = jenisPembayaran;
		this.harusBayar = harusBayar;
		this.nominalPembayaran = nominalPembayaran;
	}

	public TagihanDTO(String namaSiswa, String nis, String kelas, String jenisPembayaran, Long nominalPembayaran, Long harusBayar, Long idTagihan, String nomorTagihan, String tglTagihan,
			Instant tglJatuhTempo, Instant tglBayar, StatusBayar statusTagihan, Long idTransaksi, String nomorTransaksi, Instant tanggalTransaksi,
			MetodeTransaksi metodeTransaksi, String bank, String nomorRekening, String virtualAccount, StatusTransaksi statusTransaksi, String updateBy, Instant updateOn,
			Long jmlTagihan, Long jmlBayar, Long jmlSisa, CaraBayar caraBayar, String metodeBayar, Instant expiredDate, String transactionId, Integer typeTagihan) {
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.harusBayar = harusBayar;
		this.idTagihan = idTagihan;
		this.nomorTagihan = nomorTagihan;
		this.tglTagihan = tglTagihan;
		this.tglJatuhTempo = tglJatuhTempo;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
		this.idTransaksi = idTransaksi;
		this.nomorTransaksi = nomorTransaksi;
		this.tanggalTransaksi = tanggalTransaksi;
		this.metodeTransaksi = metodeTransaksi;
		this.bank = bank;
		this.nomorRekening = nomorRekening;
		this.virtualAccount = virtualAccount;
		this.statusTransaksi = statusTransaksi;
		this.updateBy = updateBy;
		this.updateOn = updateOn;
		this.jmlTagihan = jmlTagihan;
		this.jmlBayar = jmlBayar;
		this.jmlSisa = jmlSisa;
		this.caraBayar = caraBayar;
		this.metodeBayar = metodeBayar;
		this.expiredDate = expiredDate;
		this.transactionId = transactionId;
		this.typeTagihan = typeTagihan;
	}

	public TagihanDTO(String namaSiswa, String nis, String kelas, String jenisPembayaran, Long nominalPembayaran, Long harusBayar, Long idTagihan, String nomorTagihan, String tglTagihan,
			Instant tglJatuhTempo, Instant tglBayar, StatusBayar statusTagihan) {
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.harusBayar = harusBayar;
		this.idTagihan = idTagihan;
		this.nomorTagihan = nomorTagihan;
		this.tglTagihan = tglTagihan;
		this.tglJatuhTempo = tglJatuhTempo;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
	}

	public Long getBiayaAdmin() {
		return biayaAdmin;
	}

	public void setBiayaAdmin(Long biayaAdmin) {
		this.biayaAdmin = biayaAdmin;
	}

	public Long getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Long siswaId) {
		this.siswaId = siswaId;
	}

	public Long getIdTransaksi() {
		return idTransaksi;
	}

	public void setIdTransaksi(Long idTransaksi) {
		this.idTransaksi = idTransaksi;
	}

	public String getNomorTransaksi() {
		return nomorTransaksi;
	}

	public void setNomorTransaksi(String nomorTransaksi) {
		this.nomorTransaksi = nomorTransaksi;
	}

	public Instant getTanggalTransaksi() {
		return tanggalTransaksi;
	}

	public void setTanggalTransaksi(Instant tanggalTransaksi) {
		this.tanggalTransaksi = tanggalTransaksi;
	}

	public MetodeTransaksi getMetodeTransaksi() {
		return metodeTransaksi;
	}

	public void setMetodeTransaksi(MetodeTransaksi metodeTransaksi) {
		this.metodeTransaksi = metodeTransaksi;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getNomorRekening() {
		return nomorRekening;
	}

	public void setNomorRekening(String nomorRekening) {
		this.nomorRekening = nomorRekening;
	}

	public String getVirtualAccount() {
		return virtualAccount;
	}

	public void setVirtualAccount(String virtualAccount) {
		this.virtualAccount = virtualAccount;
	}

	public StatusTransaksi getStatusTransaksi() {
		return statusTransaksi;
	}

	public void setStatusTransaksi(StatusTransaksi statusTransaksi) {
		this.statusTransaksi = statusTransaksi;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Instant getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(Instant updateOn) {
		this.updateOn = updateOn;
	}

	public Long getJmlTagihan() {
		return jmlTagihan;
	}

	public void setJmlTagihan(Long jmlTagihan) {
		this.jmlTagihan = jmlTagihan;
	}

	public Long getJmlBayar() {
		return jmlBayar;
	}

	public void setJmlBayar(Long jmlBayar) {
		this.jmlBayar = jmlBayar;
	}

	public Long getJmlSisa() {
		return jmlSisa;
	}

	public void setJmlSisa(Long jmlSisa) {
		this.jmlSisa = jmlSisa;
	}

	public CaraBayar getCaraBayar() {
		return caraBayar;
	}

	public void setCaraBayar(CaraBayar caraBayar) {
		this.caraBayar = caraBayar;
	}

	public String getNamaSiswa() {
		return namaSiswa;
	}

	public void setNamaSiswa(String namaSiswa) {
		this.namaSiswa = namaSiswa;
	}

	public String getNis() {
		return nis;
	}

	public void setNis(String nis) {
		this.nis = nis;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public String getJenisPembayaran() {
		return jenisPembayaran;
	}

	public void setJenisPembayaran(String jenisPembayaran) {
		this.jenisPembayaran = jenisPembayaran;
	}

	public Long getNominalPembayaran() {
		return nominalPembayaran;
	}

	public void setNominalPembayaran(Long nominalPembayaran) {
		this.nominalPembayaran = nominalPembayaran;
	}

	public Long getIdTagihan() {
		return idTagihan;
	}

	public void setIdTagihan(Long idTagihan) {
		this.idTagihan = idTagihan;
	}

	public String getNomorTagihan() {
		return nomorTagihan;
	}

	public void setNomorTagihan(String nomorTagihan) {
		this.nomorTagihan = nomorTagihan;
	}

	public String getTglTagihan() {
		return tglTagihan;
	}

	public void setTglTagihan(String tglTagihan) {
		this.tglTagihan = tglTagihan;
	}

	public Instant getTglJatuhTempo() {
		return tglJatuhTempo;
	}

	public void setTglJatuhTempo(Instant tglJatuhTempo) {
		this.tglJatuhTempo = tglJatuhTempo;
	}

	public Instant getTglBayar() {
		return tglBayar;
	}

	public void setTglBayar(Instant tglBayar) {
		this.tglBayar = tglBayar;
	}

	public StatusBayar getStatusTagihan() {
		return statusTagihan;
	}

	public void setStatusTagihan(StatusBayar statusTagihan) {
		this.statusTagihan = statusTagihan;
	}

    public Long getHarusBayar() {
		return harusBayar;
	}

	public void setHarusBayar(Long harusBayar) {
		this.harusBayar = harusBayar;
	}

	public String getMetodeBayar() {
		return metodeBayar;
	}

	public void setMetodeBayar(String metodeBayar) {
		this.metodeBayar = metodeBayar;
	}

	public Instant getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Instant expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Integer getTypeTagihan() {
		return typeTagihan;
	}

	public void setTypeTagihan(Integer typeTagihan) {
		this.typeTagihan = typeTagihan;
	}

	public List<Tagihan> getMultiTagihan() {
		return multiTagihan;
	}

	public void setMultiTagihan(List<Tagihan> multiTagihan) {
		this.multiTagihan = multiTagihan;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public Boolean getMulti() {
		return multi;
	}

	public void setMulti(Boolean multi) {
		this.multi = multi;
	}

	public Boolean getCicilan() {
		return cicilan;
	}

	public void setCicilan(Boolean cicilan) {
		this.cicilan = cicilan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTagihan == null) ? 0 : idTagihan.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagihanDTO other = (TagihanDTO) obj;
		if (idTagihan == null) {
			if (other.idTagihan != null)
				return false;
		} else if (!idTagihan.equals(other.idTagihan))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TagihanDTO [namaSiswa=" + namaSiswa + ", nis=" + nis + ", kelas=" + kelas + ", jenisPembayaran=" + jenisPembayaran + ", nominalPembayaran=" + nominalPembayaran
				+ ", harusBayar=" + harusBayar + ", idTagihan=" + idTagihan + ", nomorTagihan=" + nomorTagihan + ", tglTagihan=" + tglTagihan + ", tglJatuhTempo="
				+ tglJatuhTempo + ", tglBayar=" + tglBayar + ", statusTagihan=" + statusTagihan + ", idTransaksi=" + idTransaksi + ", nomorTransaksi=" + nomorTransaksi
				+ ", tanggalTransaksi=" + tanggalTransaksi + ", metodeTransaksi=" + metodeTransaksi + ", bank=" + bank + ", nomorRekening=" + nomorRekening
				+ ", virtualAccount=" + virtualAccount + ", statusTransaksi=" + statusTransaksi + ", updateBy=" + updateBy + ", updateOn=" + updateOn + ", jmlTagihan="
				+ jmlTagihan + ", jmlBayar=" + jmlBayar + ", jmlSisa=" + jmlSisa + ", caraBayar=" + caraBayar + ", metodeBayar=" + metodeBayar + ", expiredDate=" + expiredDate
				+ ", transactionId=" + transactionId + ", siswaId=" + siswaId + ", typeTagihan=" + typeTagihan + ", multiTagihan=" + multiTagihan + ", biayaAdmin="
				+ biayaAdmin + ", keterangan=" + keterangan + ", multi=" + multi + ", id=" + id + ", name=" + name + ", cicilan=" + cicilan + "]";
	}
}
