package com.akademik.app.service.dto;

import java.io.Serializable;

public class JenisPembayaranDTO implements Serializable   {
	
	private Long siswaId;
	
	private Long besarBayarId;

    private String pembayaran;
    
    private Long nominal;
	
	private Long value;

    private String label;

	public JenisPembayaranDTO() {}
    
	public Long getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Long siswaId) {
		this.siswaId = siswaId;
	}

	public String getPembayaran() {
		return pembayaran;
	}

	public void setPembayaran(String pembayaran) {
		this.pembayaran = pembayaran;
	}

	public Long getNominal() {
		return nominal;
	}

	public void setNominal(Long nominal) {
		this.nominal = nominal;
	}

	public Long getBesarBayarId() {
		return besarBayarId;
	}

	public void setBesarBayarId(Long besarBayarId) {
		this.besarBayarId = besarBayarId;
	}

	public JenisPembayaranDTO(Long value, String label,Long besarBayarId) {
		this.value = value;
		this.label = label;
		this.besarBayarId = besarBayarId;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((siswaId == null) ? 0 : siswaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JenisPembayaranDTO other = (JenisPembayaranDTO) obj;
		if (siswaId == null) {
			if (other.siswaId != null)
				return false;
		} else if (!siswaId.equals(other.siswaId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JenisPembayaranDTO [siswaId=" + siswaId + ", besarBayarId=" + besarBayarId + ", pembayaran=" + pembayaran + ", nominal=" + nominal + ", value=" + value
				+ ", label=" + label + "]";
	}
}
