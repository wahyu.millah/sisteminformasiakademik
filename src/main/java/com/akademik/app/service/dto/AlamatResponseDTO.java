package com.akademik.app.service.dto;

public class AlamatResponseDTO {

    private String kode;

    private String nama;

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@Override
	public String toString() {
		return "AlamatResponseDTO [kode=" + kode + ", nama=" + nama + "]";
	}

}
