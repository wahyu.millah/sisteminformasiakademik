package com.akademik.app.service.dto;

import java.time.Instant;

public class ReportSyahriahDTO {

	private Long id;

	private String namaSiswa;

    private String nis;

	private String kelas;

	private String tahunAjaran;

    private Long januari;

    private Long februari;

    private Long maret;

    private Long april;

    private Long mei;

    private Long juni;

    private Long juli;

    private Long agustus;

    private Long september;

    private Long oktober;

    private Long november;

    private Long desember;

    private Instant januariOn;

    private Instant februariOn;

    private Instant maretOn;

    private Instant aprilOn;

    private Instant meiOn;

    private Instant juniOn;

    private Instant juliOn;

    private Instant agustusOn;

    private Instant septemberOn;

    private Instant oktoberOn;

    private Instant novemberOn;

    private Instant desemberOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamaSiswa() {
		return namaSiswa;
	}

	public void setNamaSiswa(String namaSiswa) {
		this.namaSiswa = namaSiswa;
	}

	public String getNis() {
		return nis;
	}

	public void setNis(String nis) {
		this.nis = nis;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public String getTahunAjaran() {
		return tahunAjaran;
	}

	public void setTahunAjaran(String tahunAjaran) {
		this.tahunAjaran = tahunAjaran;
	}

	public Long getJanuari() {
		return januari;
	}

	public void setJanuari(Long januari) {
		this.januari = januari;
	}

	public Long getFebruari() {
		return februari;
	}

	public void setFebruari(Long februari) {
		this.februari = februari;
	}

	public Long getMaret() {
		return maret;
	}

	public void setMaret(Long maret) {
		this.maret = maret;
	}

	public Long getApril() {
		return april;
	}

	public void setApril(Long april) {
		this.april = april;
	}

	public Long getMei() {
		return mei;
	}

	public void setMei(Long mei) {
		this.mei = mei;
	}

	public Long getJuni() {
		return juni;
	}

	public void setJuni(Long juni) {
		this.juni = juni;
	}

	public Long getJuli() {
		return juli;
	}

	public void setJuli(Long juli) {
		this.juli = juli;
	}

	public Long getAgustus() {
		return agustus;
	}

	public void setAgustus(Long agustus) {
		this.agustus = agustus;
	}

	public Long getSeptember() {
		return september;
	}

	public void setSeptember(Long september) {
		this.september = september;
	}

	public Long getOktober() {
		return oktober;
	}

	public void setOktober(Long oktober) {
		this.oktober = oktober;
	}

	public Long getNovember() {
		return november;
	}

	public void setNovember(Long november) {
		this.november = november;
	}

	public Long getDesember() {
		return desember;
	}

	public void setDesember(Long desember) {
		this.desember = desember;
	}

	public Instant getJanuariOn() {
		return januariOn;
	}

	public void setJanuariOn(Instant januariOn) {
		this.januariOn = januariOn;
	}

	public Instant getFebruariOn() {
		return februariOn;
	}

	public void setFebruariOn(Instant februariOn) {
		this.februariOn = februariOn;
	}

	public Instant getMaretOn() {
		return maretOn;
	}

	public void setMaretOn(Instant maretOn) {
		this.maretOn = maretOn;
	}

	public Instant getAprilOn() {
		return aprilOn;
	}

	public void setAprilOn(Instant aprilOn) {
		this.aprilOn = aprilOn;
	}

	public Instant getMeiOn() {
		return meiOn;
	}

	public void setMeiOn(Instant meiOn) {
		this.meiOn = meiOn;
	}

	public Instant getJuniOn() {
		return juniOn;
	}

	public void setJuniOn(Instant juniOn) {
		this.juniOn = juniOn;
	}

	public Instant getJuliOn() {
		return juliOn;
	}

	public void setJuliOn(Instant juliOn) {
		this.juliOn = juliOn;
	}

	public Instant getAgustusOn() {
		return agustusOn;
	}

	public void setAgustusOn(Instant agustusOn) {
		this.agustusOn = agustusOn;
	}

	public Instant getSeptemberOn() {
		return septemberOn;
	}

	public void setSeptemberOn(Instant septemberOn) {
		this.septemberOn = septemberOn;
	}

	public Instant getOktoberOn() {
		return oktoberOn;
	}

	public void setOktoberOn(Instant oktoberOn) {
		this.oktoberOn = oktoberOn;
	}

	public Instant getNovemberOn() {
		return novemberOn;
	}

	public void setNovemberOn(Instant novemberOn) {
		this.novemberOn = novemberOn;
	}

	public Instant getDesemberOn() {
		return desemberOn;
	}

	public void setDesemberOn(Instant desemberOn) {
		this.desemberOn = desemberOn;
	}

	public ReportSyahriahDTO(Long id, String namaSiswa, String nis, String kelas, String tahunAjaran, Long januari, Long februari, Long maret, Long april,
			Long mei, Long juni, Long juli, Long agustus, Long september, Long oktober, Long november, Long desember, Instant januariOn,
			Instant februariOn, Instant maretOn, Instant aprilOn, Instant meiOn, Instant juniOn, Instant juliOn, Instant agustusOn,
			Instant septemberOn, Instant oktoberOn, Instant novemberOn, Instant desemberOn) {
		this.id = id;
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.tahunAjaran = tahunAjaran;
		this.januari = januari;
		this.februari = februari;
		this.maret = maret;
		this.april = april;
		this.mei = mei;
		this.juni = juni;
		this.juli = juli;
		this.agustus = agustus;
		this.september = september;
		this.oktober = oktober;
		this.november = november;
		this.desember = desember;
		this.januariOn = januariOn;
		this.februariOn = februariOn;
		this.maretOn = maretOn;
		this.aprilOn = aprilOn;
		this.meiOn = meiOn;
		this.juniOn = juniOn;
		this.juliOn = juliOn;
		this.agustusOn = agustusOn;
		this.septemberOn = septemberOn;
		this.oktoberOn = oktoberOn;
		this.novemberOn = novemberOn;
		this.desemberOn = desemberOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportSyahriahDTO other = (ReportSyahriahDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReportSyahriahDTO [id=" + id + ", namaSiswa=" + namaSiswa + ", nis=" + nis + ", kelas=" + kelas + ", tahunAjaran=" + tahunAjaran + ", januari=" + januari
				+ ", februari=" + februari + ", maret=" + maret + ", april=" + april + ", mei=" + mei + ", juni=" + juni + ", juli=" + juli + ", agustus=" + agustus
				+ ", september=" + september + ", oktober=" + oktober + ", november=" + november + ", desember=" + desember + ", januariOn=" + januariOn + ", februariOn="
				+ februariOn + ", maretOn=" + maretOn + ", aprilOn=" + aprilOn + ", meiOn=" + meiOn + ", juniOn=" + juniOn + ", juliOn=" + juliOn + ", agustusOn=" + agustusOn
				+ ", septemberOn=" + septemberOn + ", oktoberOn=" + oktoberOn + ", novemberOn=" + novemberOn + ", desemberOn=" + desemberOn + "]";
	}
}
