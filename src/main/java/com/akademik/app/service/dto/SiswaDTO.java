package com.akademik.app.service.dto;

import java.time.Instant;

import com.akademik.app.domain.enumeration.JenisKelamin;

public class SiswaDTO {

    private Long userId;

    private String firstName;

    private String lastName;

    private String login;

    private String password;
    
    private Long siswaId;
    
    private String nis;

    private String nama;

    private String alamat;

    private JenisKelamin jenisKelamin;

    private String tempatLahir;

    private Instant tanggalLahir;

    private String waliMurid;

    private String noTelephone;

    private String tahunAjaran;

    private String status;
    
    private String kelas;
    
    private Boolean loundry;
    
    private Boolean beasiswa;
    
    private Long jmlBulan;
    
    private String jenisBeasiswa;
    
    public SiswaDTO() {}

	public SiswaDTO(Long userId, Long siswaId, String nis, String nama, String kelas, String status, Boolean loundry, Boolean beasiswa) {
		this.userId = userId;
		this.siswaId = siswaId;
		this.nis = nis;
		this.nama = nama;
		this.kelas = kelas;
		this.status = status;
		this.loundry = loundry;
		this.beasiswa = beasiswa;
	}

	public SiswaDTO(Long siswaId, String nama, String status, Boolean loundry, Boolean beasiswa) {
		this.siswaId = siswaId;
		this.nama = nama;
		this.status = status;
		this.loundry = loundry;
		this.beasiswa = beasiswa;
	}

	public SiswaDTO(Long userId, String firstName, String lastName, String login, Long siswaId, String nis, String nama, String alamat, JenisKelamin jenisKelamin,
			String tempatLahir, Instant tanggalLahir, String waliMurid, String noTelephone, String tahunAjaran, String status, String kelas) {
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.siswaId = siswaId;
		this.nis = nis;
		this.nama = nama;
		this.alamat = alamat;
		this.jenisKelamin = jenisKelamin;
		this.tempatLahir = tempatLahir;
		this.tanggalLahir = tanggalLahir;
		this.waliMurid = waliMurid;
		this.noTelephone = noTelephone;
		this.tahunAjaran = tahunAjaran;
		this.status = status;
		this.kelas = kelas;
	}

	public SiswaDTO(Long userId, String firstName, String lastName, String login, Long siswaId, String nis, String nama, String alamat, JenisKelamin jenisKelamin,
			String tempatLahir, Instant tanggalLahir, String waliMurid, String noTelephone, String tahunAjaran, String status, String kelas, Boolean loundry, Boolean beasiswa,
			Long jmlBulan, String jenisBeasiswa) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.siswaId = siswaId;
		this.nis = nis;
		this.nama = nama;
		this.alamat = alamat;
		this.jenisKelamin = jenisKelamin;
		this.tempatLahir = tempatLahir;
		this.tanggalLahir = tanggalLahir;
		this.waliMurid = waliMurid;
		this.noTelephone = noTelephone;
		this.tahunAjaran = tahunAjaran;
		this.status = status;
		this.kelas = kelas;
		this.loundry = loundry;
		this.beasiswa = beasiswa;
		this.jmlBulan = jmlBulan;
		this.jenisBeasiswa = jenisBeasiswa;
	}

	public SiswaDTO(Long userId, String firstName, String lastName, String login, String password, Long siswaId, String nis, String nama, String alamat,
			JenisKelamin jenisKelamin, String tempatLahir, Instant tanggalLahir, String waliMurid, String noTelephone, String tahunAjaran, String status, String kelas,
			Boolean loundry, Boolean beasiswa, Long jmlBulan, String jenisBeasiswa) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.login = login;
		this.password = password;
		this.siswaId = siswaId;
		this.nis = nis;
		this.nama = nama;
		this.alamat = alamat;
		this.jenisKelamin = jenisKelamin;
		this.tempatLahir = tempatLahir;
		this.tanggalLahir = tanggalLahir;
		this.waliMurid = waliMurid;
		this.noTelephone = noTelephone;
		this.tahunAjaran = tahunAjaran;
		this.status = status;
		this.kelas = kelas;
		this.loundry = loundry;
		this.beasiswa = beasiswa;
		this.jmlBulan = jmlBulan;
		this.jenisBeasiswa = jenisBeasiswa;
	}

	public Boolean getLoundry() {
		return loundry;
	}

	public void setLoundry(Boolean loundry) {
		this.loundry = loundry;
	}

	public Boolean getBeasiswa() {
		return beasiswa;
	}

	public void setBeasiswa(Boolean beasiswa) {
		this.beasiswa = beasiswa;
	}

	public Long getJmlBulan() {
		return jmlBulan;
	}

	public void setJmlBulan(Long jmlBulan) {
		this.jmlBulan = jmlBulan;
	}

	public String getJenisBeasiswa() {
		return jenisBeasiswa;
	}

	public void setJenisBeasiswa(String jenisBeasiswa) {
		this.jenisBeasiswa = jenisBeasiswa;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Long getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Long siswaId) {
		this.siswaId = siswaId;
	}

	public String getNis() {
		return nis;
	}

	public void setNis(String nis) {
		this.nis = nis;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public JenisKelamin getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(JenisKelamin jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public Instant getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Instant tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getWaliMurid() {
		return waliMurid;
	}

	public void setWaliMurid(String waliMurid) {
		this.waliMurid = waliMurid;
	}

	public String getNoTelephone() {
		return noTelephone;
	}

	public void setNoTelephone(String noTelephone) {
		this.noTelephone = noTelephone;
	}

	public String getTahunAjaran() {
		return tahunAjaran;
	}

	public void setTahunAjaran(String tahunAjaran) {
		this.tahunAjaran = tahunAjaran;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((siswaId == null) ? 0 : siswaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SiswaDTO other = (SiswaDTO) obj;
		if (siswaId == null) {
			if (other.siswaId != null)
				return false;
		} else if (!siswaId.equals(other.siswaId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SiswaDTO [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", login=" + login + ", password=" + password + ", siswaId=" + siswaId
				+ ", nis=" + nis + ", nama=" + nama + ", alamat=" + alamat + ", jenisKelamin=" + jenisKelamin + ", tempatLahir=" + tempatLahir + ", tanggalLahir="
				+ tanggalLahir + ", waliMurid=" + waliMurid + ", noTelephone=" + noTelephone + ", tahunAjaran=" + tahunAjaran + ", status=" + status + ", kelas=" + kelas
				+ ", loundry=" + loundry + ", beasiswa=" + beasiswa + ", jmlBulan=" + jmlBulan + ", jenisBeasiswa=" + jenisBeasiswa + "]";
	}

}
