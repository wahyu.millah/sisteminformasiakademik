package com.akademik.app.service.dto;

import java.time.Instant;
import java.util.List;

import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.enumeration.CaraBayar;
import com.akademik.app.domain.enumeration.MetodeTransaksi;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.domain.enumeration.StatusTransaksi;

public class ReportDTO {

	private Long id;

	private String namaSiswa;

    private String nis;

	private String kelas;

	private String ta;

    private String jenisPembayaran;

    private Long nominalPembayaran;

    private Instant tglBayar;

    private StatusBayar statusTagihan;

    private MetodeTransaksi metodeBayar;

    private String bank;

    private String virtualAccount;

    private String updateBy;

    private String item;

	public ReportDTO(Long id, String item, Long nominalPembayaran) {
		this.id = id;
		this.item = item;
		this.nominalPembayaran = nominalPembayaran;
	}

	public ReportDTO(Long id, String namaSiswa, String nis, String kelas, String ta, String jenisPembayaran, Long nominalPembayaran, Instant tglBayar,
			StatusBayar statusTagihan, MetodeTransaksi metodeBayar, String bank, String virtualAccount, String updateBy) {
		this.id = id;
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.ta = ta;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
		this.metodeBayar = metodeBayar;
		this.bank = bank;
		this.virtualAccount = virtualAccount;
		this.updateBy = updateBy;
	}

	public ReportDTO(Long id, String namaSiswa, String nis, String kelas, String jenisPembayaran, Long nominalPembayaran, Instant tglBayar, StatusBayar statusTagihan,
			MetodeTransaksi metodeBayar, String virtualAccount) {
		this.id = id;
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
		this.metodeBayar = metodeBayar;
		this.virtualAccount = virtualAccount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamaSiswa() {
		return namaSiswa;
	}

	public void setNamaSiswa(String namaSiswa) {
		this.namaSiswa = namaSiswa;
	}

	public String getNis() {
		return nis;
	}

	public void setNis(String nis) {
		this.nis = nis;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public String getJenisPembayaran() {
		return jenisPembayaran;
	}

	public void setJenisPembayaran(String jenisPembayaran) {
		this.jenisPembayaran = jenisPembayaran;
	}

	public Long getNominalPembayaran() {
		return nominalPembayaran;
	}

	public void setNominalPembayaran(Long nominalPembayaran) {
		this.nominalPembayaran = nominalPembayaran;
	}

	public Instant getTglBayar() {
		return tglBayar;
	}

	public void setTglBayar(Instant tglBayar) {
		this.tglBayar = tglBayar;
	}

	public StatusBayar getStatusTagihan() {
		return statusTagihan;
	}

	public void setStatusTagihan(StatusBayar statusTagihan) {
		this.statusTagihan = statusTagihan;
	}

	public MetodeTransaksi getMetodeBayar() {
		return metodeBayar;
	}

	public void setMetodeBayar(MetodeTransaksi metodeBayar) {
		this.metodeBayar = metodeBayar;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getVirtualAccount() {
		return virtualAccount;
	}

	public void setVirtualAccount(String virtualAccount) {
		this.virtualAccount = virtualAccount;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public String getTa() {
		return ta;
	}

	public void setTa(String ta) {
		this.ta = ta;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportDTO other = (ReportDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReportDTO [id=" + id + ", namaSiswa=" + namaSiswa + ", nis=" + nis + ", kelas=" + kelas + ", ta=" + ta + ", jenisPembayaran=" + jenisPembayaran
				+ ", nominalPembayaran=" + nominalPembayaran + ", tglBayar=" + tglBayar + ", statusTagihan=" + statusTagihan + ", metodeBayar=" + metodeBayar + ", bank="
				+ bank + ", virtualAccount=" + virtualAccount + ", updateBy=" + updateBy + ", item=" + item + "]";
	}
}
