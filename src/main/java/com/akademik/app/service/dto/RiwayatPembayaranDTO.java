package com.akademik.app.service.dto;

import java.time.Instant;

import com.akademik.app.domain.enumeration.StatusBayar;

public class RiwayatPembayaranDTO {
	
	private Long id;
	private String namaSiswa;
	private String nis;
	private String kelas;
	private String ta;
	private String jenisPembayaran;
	private Long nominalPembayaran;
	private Instant tglBayar;
	private StatusBayar statusTagihan;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNamaSiswa() {
		return namaSiswa;
	}
	public void setNamaSiswa(String namaSiswa) {
		this.namaSiswa = namaSiswa;
	}
	public String getNis() {
		return nis;
	}
	public void setNis(String nis) {
		this.nis = nis;
	}
	public String getKelas() {
		return kelas;
	}
	public void setKelas(String kelas) {
		this.kelas = kelas;
	}
	public String getTa() {
		return ta;
	}
	public void setTa(String ta) {
		this.ta = ta;
	}
	public String getJenisPembayaran() {
		return jenisPembayaran;
	}
	public void setJenisPembayaran(String jenisPembayaran) {
		this.jenisPembayaran = jenisPembayaran;
	}
	public Long getNominalPembayaran() {
		return nominalPembayaran;
	}
	public void setNominalPembayaran(Long nominalPembayaran) {
		this.nominalPembayaran = nominalPembayaran;
	}
	public Instant getTglBayar() {
		return tglBayar;
	}
	public void setTglBayar(Instant tglBayar) {
		this.tglBayar = tglBayar;
	}
	public StatusBayar getStatusTagihan() {
		return statusTagihan;
	}
	public void setStatusTagihan(StatusBayar statusTagihan) {
		this.statusTagihan = statusTagihan;
	}
	
	public RiwayatPembayaranDTO(Long id, String namaSiswa, String nis, String kelas, String ta, String jenisPembayaran,
			Long nominalPembayaran, Instant tglBayar, StatusBayar statusTagihan) {
		this.id = id;
		this.namaSiswa = namaSiswa;
		this.nis = nis;
		this.kelas = kelas;
		this.ta = ta;
		this.jenisPembayaran = jenisPembayaran;
		this.nominalPembayaran = nominalPembayaran;
		this.tglBayar = tglBayar;
		this.statusTagihan = statusTagihan;
	}
	
	@Override
	public String toString() {
		return "RiwayatPembayaranDTO [id=" + id + ", namaSiswa=" + namaSiswa + ", nis=" + nis + ", kelas=" + kelas
				+ ", ta=" + ta + ", jenisPembayaran=" + jenisPembayaran + ", nominalPembayaran=" + nominalPembayaran
				+ ", tglBayar=" + tglBayar + ", statusTagihan=" + statusTagihan + "]";
	}

}
