package com.akademik.app.service.dto;

import java.time.ZonedDateTime;

public class NotificationDTO { private Long id;

	private String signature;
	
	private String transaction_id;
	
	private Long amount;
	
	private String payment_code;
	
	private String customer_info;
	
	private String payment_info;
	
	private String status;
	
	private String order_id;
	
	private String merchant_id;
	
	private String payment_channel;
	
	private String transaction_time;
	
	private String expired_time;
	
	private String paid_time;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getPayment_code() {
		return payment_code;
	}

	public void setPayment_code(String payment_code) {
		this.payment_code = payment_code;
	}

	public String getCustomer_info() {
		return customer_info;
	}

	public void setCustomer_info(String customer_info) {
		this.customer_info = customer_info;
	}

	public String getPayment_info() {
		return payment_info;
	}

	public void setPayment_info(String payment_info) {
		this.payment_info = payment_info;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getPayment_channel() {
		return payment_channel;
	}

	public void setPayment_channel(String payment_channel) {
		this.payment_channel = payment_channel;
	}

	public String getTransaction_time() {
		return transaction_time;
	}

	public void setTransaction_time(String transaction_time) {
		this.transaction_time = transaction_time;
	}

	public String getExpired_time() {
		return expired_time;
	}

	public void setExpired_time(String expired_time) {
		this.expired_time = expired_time;
	}

	public String getPaid_time() {
		return paid_time;
	}

	public void setPaid_time(String paid_time) {
		this.paid_time = paid_time;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotificationDTO other = (NotificationDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NotificationDTO [id=" + id + ", signature=" + signature + ", transaction_id=" + transaction_id + ", amount=" + amount + ", payment_code=" + payment_code
				+ ", customer_info=" + customer_info + ", payment_info=" + payment_info + ", status=" + status + ", order_id=" + order_id + ", merchant_id=" + merchant_id
				+ ", payment_channel=" + payment_channel + ", transaction_time=" + transaction_time + ", expired_date=" + expired_time + ", paid_date=" + paid_time + "]";
	}
}
