package com.akademik.app.service.dto;

import java.io.Serializable;
import java.time.Instant;

public class PaymentResponseDTO implements Serializable {

    private String status_message;
    
    private Integer status_code;

    private String process_id;

    private String transaction_id;

    private Long order_id;

    private Integer amount;

    private String payment_code;

    private String transaction_time;

    private String expired_time;

    private String status;//REQUEST

    private String payment_channel;
    
    private String transaction_fee_total;
    
    private String paid_date;
    
    private String customer_info;
    
    private String payment_info;
    
    private String merchant_id;
    
    private String response_code;
    
    private String external_id;
    
    private String currency;
    
    private String transaction_status;
    
    private String payment_method;
    
    private String additional_data;
    
    private String callback_url;
    
    private PaymentDetailsDTO payment_details;
    
    private CustomerDetailsDTO customer_details;
    

	public String getStatus_message() {
		return status_message;
	}

	public void setStatus_message(String status_message) {
		this.status_message = status_message;
	}

	public Integer getStatus_code() {
		return status_code;
	}

	public void setStatus_code(Integer status_code) {
		this.status_code = status_code;
	}

	public String getProcess_id() {
		return process_id;
	}

	public void setProcess_id(String process_id) {
		this.process_id = process_id;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public Long getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getPayment_code() {
		return payment_code;
	}

	public void setPayment_code(String payment_code) {
		this.payment_code = payment_code;
	}

	public String getTransaction_time() {
		return transaction_time;
	}

	public void setTransaction_time(String transaction_time) {
		this.transaction_time = transaction_time;
	}

	public String getExpired_time() {
		return expired_time;
	}

	public void setExpired_time(String expired_time) {
		this.expired_time = expired_time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPayment_channel() {
		return payment_channel;
	}

	public void setPayment_channel(String payment_channel) {
		this.payment_channel = payment_channel;
	}

	public String getTransaction_fee_total() {
		return transaction_fee_total;
	}

	public void setTransaction_fee_total(String transaction_fee_total) {
		this.transaction_fee_total = transaction_fee_total;
	}

	public String getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(String paid_date) {
		this.paid_date = paid_date;
	}

	public String getCustomer_info() {
		return customer_info;
	}

	public void setCustomer_info(String customer_info) {
		this.customer_info = customer_info;
	}

	public String getPayment_info() {
		return payment_info;
	}

	public void setPayment_info(String payment_info) {
		this.payment_info = payment_info;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getResponse_code() {
		return response_code;
	}

	public void setResponse_code(String response_code) {
		this.response_code = response_code;
	}

	public String getExternal_id() {
		return external_id;
	}

	public void setExternal_id(String external_id) {
		this.external_id = external_id;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTransaction_status() {
		return transaction_status;
	}

	public void setTransaction_status(String transaction_status) {
		this.transaction_status = transaction_status;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	public String getAdditional_data() {
		return additional_data;
	}

	public void setAdditional_data(String additional_data) {
		this.additional_data = additional_data;
	}

	public String getCallback_url() {
		return callback_url;
	}

	public void setCallback_url(String callback_url) {
		this.callback_url = callback_url;
	}

	public PaymentDetailsDTO getPayment_details() {
		return payment_details;
	}

	public void setPayment_details(PaymentDetailsDTO payment_details) {
		this.payment_details = payment_details;
	}

	public CustomerDetailsDTO getCustomer_details() {
		return customer_details;
	}

	public void setCustomer_details(CustomerDetailsDTO customer_details) {
		this.customer_details = customer_details;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((order_id == null) ? 0 : order_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentResponseDTO other = (PaymentResponseDTO) obj;
		if (order_id == null) {
			if (other.order_id != null)
				return false;
		} else if (!order_id.equals(other.order_id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentResponseDTO [status_message=" + status_message + ", status_code=" + status_code + ", process_id=" + process_id + ", transaction_id=" + transaction_id
				+ ", order_id=" + order_id + ", amount=" + amount + ", payment_code=" + payment_code + ", transaction_time=" + transaction_time + ", expired_time="
				+ expired_time + ", status=" + status + ", payment_channel=" + payment_channel + ", transaction_fee_total=" + transaction_fee_total + ", paid_date="
				+ paid_date + ", customer_info=" + customer_info + ", payment_info=" + payment_info + ", merchant_id=" + merchant_id + "]";
	}
}
