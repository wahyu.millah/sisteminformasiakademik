package com.akademik.app.service.dto;

import java.io.Serializable;

public class WaResponDTO implements Serializable  {

    private Boolean success;
    
    private String message;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "WaResponDTO [success=" + success + ", message=" + message + "]";
	}

}
