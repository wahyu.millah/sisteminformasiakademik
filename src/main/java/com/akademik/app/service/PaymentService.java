package com.akademik.app.service;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.config.ApplicationProperties;
import com.akademik.app.domain.Channel;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.domain.User;
import com.akademik.app.repository.ChannelRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.repository.TransaksiRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.dto.PaymentResponseDTO;
import com.akademik.app.service.dto.TagihanDTO;
import com.akademik.app.util.DateUtils;
import com.itextpdf.text.pdf.codec.Base64.InputStream;

@Service
@Transactional
public class PaymentService {

	private static final Logger log = LoggerFactory.getLogger(PaymentService.class);

    private String connectTimeout = "60000" ;
    
    private String readTimeout = "60000" ;
    
    private final TagihanRepository tagihanRepository;
    
    private final TransaksiRepository transaksiRepository;
    
    private final EncriptSha256Service encriptSha256Service;

    private final ApplicationProperties applicationProperties;

    private final UserRepository userRepository;
    
    private final SiswaRepository siswaRepository;
    
    private final ChannelRepository channelRepository;
    
    public PaymentService(TransaksiRepository transaksiRepository, ChannelRepository channelRepository, SiswaRepository siswaRepository, UserRepository userRepository, TagihanRepository tagihanRepository, EncriptSha256Service encriptSha256Service, ApplicationProperties applicationProperties) {
		this.tagihanRepository = tagihanRepository;
		this.encriptSha256Service = encriptSha256Service;
		this.applicationProperties = applicationProperties;
		this.userRepository = userRepository;
		this.siswaRepository = siswaRepository;
		this.channelRepository = channelRepository;
		this.transaksiRepository = transaksiRepository;
	}

    @Async
    @Transactional(readOnly = true) 
    public PaymentResponseDTO createPayment(TagihanDTO tagihandto, String va, Long id) {
		log.debug("Create payment:{}",tagihandto);

		PaymentResponseDTO response = null;
		try {
			Tagihan tagihan = tagihanRepository.findById(tagihandto.getIdTagihan()).get();
			Channel channelVa = channelRepository.findByVa(va.toUpperCase());
			String siswa = null;
			if(tagihan.getSiswa().getNama().replace("'", "").length() > 19) {
				siswa = tagihan.getSiswa().getNama().replace("'", "").substring(0, 19);
			} else {
				siswa = tagihan.getSiswa().getNama().replace("'", "");
			}
			String paymentInfo = null;
			if (tagihandto.getJenisPembayaran().length() > 15) {
				paymentInfo = tagihandto.getJenisPembayaran().trim().substring(0, 15);
			} else {
				paymentInfo = tagihandto.getJenisPembayaran().trim();
			}
			
			log.debug("exp zdt:{}", ZonedDateTime.now());
			log.debug("exp inst:{}", Instant.now());
			log.debug("exp loca:{}", LocalDateTime.now());
			Long total = tagihandto.getNominalPembayaran()+ channelVa.getFee();
			log.debug("total bayar:{}", total);
			String param = "/va/transactions?payment="+va.toUpperCase();
			String signature = encriptSha256Service.encript(channelVa.getSecretKey()+applicationProperties.getPayment().getMerchantid()+
					total+siswa+paymentInfo+id);
			
    		BufferedReader in = null;

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("amount", total);
            jsonObject.put("customer_info", siswa);
            jsonObject.put("payment_info", paymentInfo);
            jsonObject.put("order_id", id);
            jsonObject.put("mrc_id", applicationProperties.getPayment().getMerchantid());
            jsonObject.put("signature", signature);
            jsonObject.put("expired_time", ZonedDateTime.now().plusMinutes(
            		applicationProperties.getPayment().getExpired_time()));
            
            String postData = jsonObject.toString();

			URL url = new URL(applicationProperties.getPayment().getUrl() + param);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("x-api-key",applicationProperties.getPayment().getApikey());
			conn.setRequestProperty("Host", applicationProperties.getPayment().getHost());
			conn.setRequestProperty("Content-Length", "");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();
			
			   log.debug("postDataBytes :{}, {}",postData, param);
			
			int responseCode = conn.getResponseCode();
			URL res = conn.getURL();
			    log.debug("res :{}",res);
			
			if(responseCode == HttpURLConnection.HTTP_OK) {
				log.debug("Post parameters : {}", postData);
		        log.debug("Response Code : {}", responseCode);
		        
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		        
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				response = parseJson(sb.toString());

		        log.debug("Status message : " + response.getStatus_message());
			} else {
				log.debug("Post parameters : " + postData);
				log.debug("Status : " + response.getStatus_message());
			}
			
		} catch (Exception e) {
            log.error("Failed create payment caused by {}", e.getMessage(), e);
		}
		
		return response;
	}
    
    public void curlResponse(String url, String auth, String sign, String postData) {
    	
    	String curl = "curl --location --request POST 'https://api-live.mcpayment.id/va' \\\r\n"
    			+ "--header 'Content-Type: application/json' \\\r\n"
    			+ "--header 'Authorization: \"Basic TUNQMjAyMTA4MzU5MDoweDAwNWQ1NDBlMmE3ZWIxYzQ1Yg==\" ' \\\r\n"
    			+ "--header 'x-req-signature: \"d29b69dbd5a6e7fd2efcd4ffeb8b15251c9c7e8d8265a3d9714097b7a04a9809\"' \\\r\n"
    			+ "--header 'x-version: v3' \\\r\n"
    			+ "--data-raw '{\r\n"
    			+ "    \"external_id\": \"16926\",\r\n"
    			+ "    \"order_id\": \"16926\",\r\n"
    			+ "    \"currency\": \"IDR\",\r\n"
    			+ "    \"payment_method\": \"bank_transfer\",\r\n"
    			+ "    \"payment_channel\": \"FINPAY\",\r\n"
    			+ "    \"payment_details\": {\r\n"
    			+ "        \"billing_name\": \"Whisnu Rangga Maula\",\r\n"
    			+ "        \"payment_system\": \"CLOSED\",\r\n"
    			+ "        \"va_number\": \"\",\r\n"
    			+ "        \"is_multi_use\" : false,\r\n"
    			+ "        \"amount\": 965000,\r\n"
    			+ "        \"expired_time\": \"2023-10-25T22:28:09.629+07:00\",\r\n"
    			+ "        \"transaction_description\": \"SYAHRIAH-OCTOBE\"\r\n"
    			+ "    },\r\n"
    			+ "    \"customer_details\": {\r\n"
    			+ "        \"email\": \"testing@email.com\",\r\n"
    			+ "        \"full_name\" : \"ponpessiraajulummah@gmail.com\",\r\n"
    			+ "        \"phone\": \"081286381467\"\r\n"
    			+ "    },\r\n"
    			+ "    \"item_details\": \"\",\r\n"
    			+ "    \"shipping_address\": \"\",\r\n"
    			+ "    \"billing_address\": \"\",\r\n"
    			+ "    \"additional_data\": \"\",\r\n"
    			+ "    \"callback_url\": \"https://testing.com\" \r\n"
    			+ "}'";
    	log.debug(curl);
    }

    @Transactional(readOnly = true) 
    public PaymentResponseDTO createPaymentV2(TagihanDTO tagihandto, String va, Long id) {
		log.debug("Create payment v2:{} bank:{} id transaksi:{}",tagihandto, va, id);
		
		String auth = "Basic "+ encriptSha256Service.toBase64(applicationProperties.getPayment().getMerchantid() + ":" 
					+ applicationProperties.getPayment().getSecretUnboundId());
		
		log.debug("Auth Base64 from {} to {}",applicationProperties.getPayment().getMerchantid() + ":" 
				+ applicationProperties.getPayment().getSecretUnboundId(), auth);
		
		String xReqSignature = encriptSha256Service.encript(applicationProperties.getPayment().getHashKey() + id + id);
		
		String callbackUrl = "https://testing.com";
		ZonedDateTime expTime = ZonedDateTime.now().plusMinutes(applicationProperties.getPayment().getExpired_time());
		String exp = DateUtils.parse(expTime, DateUtils.FORMATTER_ISO_OFFSET_DATE_TIME);
				
		PaymentResponseDTO response = null;
		try {
			Tagihan tagihan = tagihanRepository.findById(tagihandto.getIdTagihan()).get();
			Channel channelVa = channelRepository.findByVa(va.toUpperCase());
			String siswa = null;
			if(tagihan.getSiswa().getNama().replace("'", "").length() > 19) {
				siswa = tagihan.getSiswa().getNama().replace("'", "").substring(0, 19);
			} else {
				siswa = tagihan.getSiswa().getNama().replace("'", "");
			}
			String paymentInfo = null;
			if (tagihandto.getJenisPembayaran().length() > 15) {
				paymentInfo = tagihandto.getJenisPembayaran().trim().substring(0, 15);
			} else {
				paymentInfo = tagihandto.getJenisPembayaran().trim();
			}
			
			Long total = tagihandto.getNominalPembayaran() + channelVa.getFee();
			
    		BufferedReader in = null;
            JSONObject paymentDetails = new JSONObject();
	            paymentDetails.put("billing_name", siswa); // Mandatory 100
	            paymentDetails.put("payment_system", "CLOSED"); // Mandatory
	            paymentDetails.put("va_number", ""); // Mandatory dynamic from MCP
	            paymentDetails.put("amount", total); // Mandatory numeric
	            paymentDetails.put("expired_time", exp); // Optionl YYYY-mm-ddTHH:mm:ss.vvv+07:00
	            paymentDetails.put("transaction_description",  paymentInfo); //Mandatory(100)

	        JSONObject customerDetails = new JSONObject();
		        customerDetails.put("email", "ponpessiraajulummah@gmail.com"); // Mandatory
		        customerDetails.put("full_name", "ponpessiraajulummah"); // Mandatory
		        customerDetails.put("phone", "081286381467"); // Mandatory

            JSONObject jsonObject = new JSONObject();
	            jsonObject.put("external_id", id); //Mandatory
	            jsonObject.put("order_id", id); //Mandatory
	            jsonObject.put("currency", "IDR"); //Mandatory
	            jsonObject.put("payment_method", "bank_transfer"); //Mandatory
	            jsonObject.put("payment_channel", channelVa.getName()); //Mandatory
	            jsonObject.put("payment_details", paymentDetails); //Mandatory
	            jsonObject.put("customer_details", customerDetails); //Mandatory
	            jsonObject.put("callback_url", callbackUrl); //Mandatory
            
            String postData = jsonObject.toString();

			log.debug("jsonObject :{}", jsonObject);

			URL url = new URL(applicationProperties.getPayment().getUrl());
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", auth);
			conn.setRequestProperty("x-req-signature", xReqSignature);
			conn.setRequestProperty("x-version", "v3");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();
			
			log.debug("postDataBytes :{}, {}", postData, channelVa.getName());
			
			int responseCode = conn.getResponseCode();
//			URL res = conn.getURL();
			    log.debug("conn.getResponseCode() :{}",conn.getResponseCode());
			    
			curlResponse(applicationProperties.getPayment().getUrl(), auth, xReqSignature, postData);
			
			if(responseCode == HttpURLConnection.HTTP_OK) {
				log.debug("Post parameters : {}", postData);
		        log.debug("Response Code : {}", responseCode);
		        
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		        
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				
				return response = parseJsonV2(sb.toString());
			} else {
				log.debug("Post parameters : " + postData);
//				log.debug("Status : " + response.getStatus_message());
			}
			
		} catch (Exception e) {
            log.error("Failed create payment caused by {}", e.getMessage(), e);
		}
		
		return response;
	}

    @Async
    @Transactional(readOnly = true) 
    public PaymentResponseDTO createMultiPayment(Transaksi transaksi, String va, Long id) {
		log.debug("Create payment:{},va:{},idTransaksi:{}",transaksi,va,id);
        String login = SecurityUtils.getCurrentUserLogin().get();
        User user = userRepository.findOneByLogin(login).get();

		PaymentResponseDTO response = null;
		try {
			String siswa = null;
			if(siswaRepository.findOneByUser(user).getNama().replace("'", "").length() > 18) {
				siswa = siswaRepository.findOneByUser(user).getNama().replace("'", "").substring(0, 19);
			} else {
				siswa = siswaRepository.findOneByUser(user).getNama().replace("'", "");
			}
			Channel channelVa = channelRepository.findByVa(va.toUpperCase());
			
			log.debug("exp zdt:{}", ZonedDateTime.now());
			log.debug("exp inst:{}", Instant.now());
			log.debug("exp loca:{}", LocalDateTime.now());
			String param = "/va/transactions?payment="+va.toUpperCase();
			String info = "multi_invoice";
			log.debug("jml bayar:{}", transaksi.getJmlBayar());
			Long total = transaksi.getJmlBayar();
			log.debug("total:{}", total);
			String signature = encriptSha256Service.encript(channelVa.getSecretKey()+applicationProperties.getPayment().getMerchantid()+
					total + siswa + info + id);
			
    		BufferedReader in = null;

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("amount", total);
            jsonObject.put("customer_info", siswa);
            jsonObject.put("payment_info", info);
            jsonObject.put("order_id", id);
            jsonObject.put("mrc_id", applicationProperties.getPayment().getMerchantid());
            jsonObject.put("signature", signature);
            jsonObject.put("expired_time", ZonedDateTime.now().plusMinutes(
            		applicationProperties.getPayment().getExpired_time()));
            
            String postData = jsonObject.toString();

			URL url = new URL(applicationProperties.getPayment().getUrl() + param);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("x-api-key",applicationProperties.getPayment().getApikey());
			conn.setRequestProperty("Host", applicationProperties.getPayment().getHost());
			conn.setRequestProperty("Content-Length", "");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();
			
			   log.debug("postDataBytes :{}, {}",postData, param);
			
			int responseCode = conn.getResponseCode();
			URL res = conn.getURL();
			    log.debug("res :{}",res);
			
			if(responseCode == HttpURLConnection.HTTP_OK) {
				log.debug("Post parameters : {}", postData);
		        log.debug("Response Code : {}", responseCode);
		        
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		        
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				response = parseJson(sb.toString());

		        log.debug("Status message : " + response.getStatus_message());
			} else {
		        
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		        
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				response = parseJson(sb.toString());
				
				log.debug("Post parameters : " + postData);
		        log.debug("Response Code : {}", responseCode);
				log.debug("status_message : " + response.getStatus_message());
			}
			
		} catch (Exception e) {
            log.error("Failed create payment caused by {}", e.getMessage(), e);
		}
		
		return response;
	}
    
    @Transactional(readOnly = true) 
    public PaymentResponseDTO createMultiPaymentV2(Transaksi transaksi, String va, Long id, Siswa siswa) {
		log.debug("Create payment:{},va:{},idTransaksi:{}",transaksi,va,id);
//        String login = SecurityUtils.getCurrentUserLogin().get();
//        User user = userRepository.findOneByLogin(login).get();
		
		String auth = "Basic "+ encriptSha256Service.toBase64(applicationProperties.getPayment().getMerchantid() + ":" 
					+ applicationProperties.getPayment().getSecretUnboundId());
		
		log.debug("Auth Base64 from {} to {}",applicationProperties.getPayment().getMerchantid() + ":" 
				+ applicationProperties.getPayment().getSecretUnboundId(), auth);
		
		String xReqSignature = encriptSha256Service.encript(applicationProperties.getPayment().getHashKey() + id + id);
		
		String callbackUrl = "https://testing.com";

		PaymentResponseDTO response = null;
		try {
			String billingName = null;
			if(siswa.getNama().replace("'", "").length() > 18) {
				billingName = siswa.getNama().replace("'", "").substring(0, 19);
			} else {
				billingName = siswa.getNama().replace("'", "");
			}
			Channel channelVa = channelRepository.findByVa(va.toUpperCase());
			
			String param = "/va/transactions?payment="+va.toUpperCase();
			String info = "multi_invoice";
			Long total = transaksi.getJmlBayar();
			ZonedDateTime expTime = ZonedDateTime.now().plusMinutes(applicationProperties.getPayment().getExpired_time());
			String exp = DateUtils.parse(expTime, DateUtils.FORMATTER_ISO_OFFSET_DATE_TIME);
			
    		BufferedReader in = null;
    		
            JSONObject paymentDetails = new JSONObject();
            paymentDetails.put("billing_name", billingName); // Mandatory 100
            paymentDetails.put("payment_system", "CLOSED"); // Mandatory
            paymentDetails.put("va_number", ""); // Mandatory dynamic from MCP
            paymentDetails.put("amount", total); // Mandatory numeric
            paymentDetails.put("expired_time", exp); // Optionl YYYY-mm-ddTHH:mm:ss.vvv+07:00
            paymentDetails.put("transaction_description",  info); //Mandatory(100)

	        JSONObject customerDetails = new JSONObject();
		        customerDetails.put("email", "ponpessiraajulummah@gmail.com"); // Mandatory
		        customerDetails.put("full_name", "ponpessiraajulummah"); // Mandatory
		        customerDetails.put("phone", "081286381467"); // Mandatory
	
	        JSONObject jsonObject = new JSONObject();
	            jsonObject.put("external_id", id); //Mandatory
	            jsonObject.put("order_id", id); //Mandatory
	            jsonObject.put("currency", "IDR"); //Mandatory
	            jsonObject.put("payment_method", "bank_transfer"); //Mandatory
	            jsonObject.put("payment_channel", channelVa.getName()); //Mandatory
	            jsonObject.put("payment_details", paymentDetails); //Mandatory
	            jsonObject.put("customer_details", customerDetails); //Mandatory
	            jsonObject.put("callback_url", callbackUrl); //Mandatory
        
            String postData = jsonObject.toString();

			URL url = new URL(applicationProperties.getPayment().getUrl());
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", auth);
			conn.setRequestProperty("x-req-signature", xReqSignature);
			conn.setRequestProperty("x-version", "v3");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();
			
			   log.debug("postDataBytes :{}, {}",postData, param);
			
			int responseCode = conn.getResponseCode();
			URL res = conn.getURL();
			    log.debug("res :{}",res);
			
			if(responseCode == HttpURLConnection.HTTP_OK) {
				log.debug("Post parameters : {}", postData);
		        log.debug("Response Code : {}", responseCode);
		        
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		        
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				return response = parseJsonV2(sb.toString());
			} else {
		        
				in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		        
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				return response = parseJsonV2(sb.toString());
			}
			
		} catch (Exception e) {
            log.error("Failed create payment caused by {}", e.getMessage(), e);
		}
		
		return response;
	}
	
	private static PaymentResponseDTO parseJson(String json) throws Exception {

		log.debug("parseJson:{}",json);
		
		PaymentResponseDTO resp = new PaymentResponseDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject message = new JSONObject(json);
					resp.setStatus_message(message.getString("status_message"));
					resp.setStatus_code(message.getInt("status_code"));
					resp.setTransaction_id(message.getString("transaction_id"));
					resp.setOrder_id(message.getLong("order_id"));
					resp.setAmount((int) message.getLong("amount"));
					resp.setPayment_code(message.getString("payment_code"));
					resp.setTransaction_time(message.getString("transaction_time"));
					resp.setExpired_time(message.getString("expired_time"));
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
	
	private static PaymentResponseDTO parseJsonV2(String json) throws Exception {

		log.debug("parseJsonV2:{}",json);
		
		PaymentResponseDTO resp = new PaymentResponseDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject message = new JSONObject(json);
					resp.setResponse_code(message.getString("response_code"));
					resp.setStatus_code(Integer.valueOf(message.getString("response_code")));
					resp.setTransaction_id(message.getString("transaction_id"));
					resp.setExternal_id(message.getString("external_id"));
					resp.setOrder_id(Long.parseLong(message.getString("order_id")));
					resp.setCurrency(message.getString("currency"));
					resp.setTransaction_status(message.getString("transaction_status"));
					resp.setPayment_method(message.getString("payment_method"));
					resp.setPayment_channel(message.getString("payment_channel"));
					resp.setStatus(message.getString("transaction_status"));
					resp.setCallback_url(message.getString("callback_url"));
					resp.setPayment_code(message.getJSONObject("payment_details").getString("va_number"));
					resp.setExpired_time(message.getJSONObject("payment_details").getString("expired_time"));
					
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
	
	private static PaymentResponseDTO parseJsonCekVa(String json) throws Exception {

		log.debug("parseJson:{}",json);
		
		PaymentResponseDTO resp = new PaymentResponseDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject message = new JSONObject(json);
				resp.setTransaction_id(message.getString("transaction_id"));
				resp.setAmount((int) message.getLong("amount"));
				resp.setPayment_code(message.getString("payment_code"));
				resp.setCustomer_info(message.getString("customer_info"));
				resp.setPayment_info(message.getString("payment_info"));
				resp.setStatus(message.getString("status"));
				resp.setOrder_id(message.getLong("order_id"));
				resp.setMerchant_id(message.getString("merchant_id"));
				resp.setPayment_channel(message.getString("payment_channel"));
				resp.setTransaction_time(message.getString("transaction_time"));
				resp.setExpired_time(message.getString("expired_time"));
				if("SUCCESS".equals(message.getString("status"))) {
					resp.setPaid_date(message.getString("paid_date"));
				}
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
	
//	V2
	private static PaymentResponseDTO parseJsonCekVaV2(String json) throws Exception {

		log.debug("parseJson:{}",json);
		
		PaymentResponseDTO resp = new PaymentResponseDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject message = new JSONObject(json);
				resp.setResponse_code(message.getString("response_code"));
				resp.setTransaction_id(message.getString("transaction_id"));
				resp.setExternal_id(message.getString("external_id"));
				resp.setOrder_id(message.getLong("order_id"));
				resp.setCurrency(message.getString("currency"));
				resp.setPayment_method(message.getString("payment_method"));
				resp.setPayment_channel(message.getString("payment_channel"));
				resp.setStatus(message.getString("transaction_status"));
				resp.setTransaction_time(message.getString("transaction_time"));
				if("PAID".equals(message.getString("transaction_status"))) {
					resp.setPaid_date(message.getJSONArray("payment_history").getJSONObject(0).getString("paid_time"));
					resp.setTransaction_time(message.getJSONArray("payment_history").getJSONObject(0).getString("paid_time"));
				}
					resp.setPayment_code(message.getJSONObject("payment_details").getString("va_number"));
					resp.setExpired_time(message.getJSONObject("payment_details").getString("expired_time"));
					resp.setAmount((int)message.getJSONObject("payment_details").getLong("total_amount"));
					resp.setExpired_time(message.getJSONObject("payment_details").getString("expired_time"));
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
    
    public PaymentResponseDTO deletePayment(String transactionId) {
		log.debug("Checking payment with transaction id:{}",transactionId);

		PaymentResponseDTO response = null;
		try {
			
			String param = "/va/transactions/"+ transactionId;
			
    		BufferedReader in = null;

			URL url = new URL(applicationProperties.getPayment().getUrl() + param);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("DELETE");
			conn.setRequestProperty("x-api-key",applicationProperties.getPayment().getApikey());
			conn.setRequestProperty("Host", applicationProperties.getPayment().getHost());
			conn.setRequestProperty("merchant_id", applicationProperties.getPayment().getMerchantid());
			conn.setRequestProperty("Content-Length", "");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
			int responseCode = conn.getResponseCode();
			URL res = conn.getURL();
			log.debug("res :{}",res);
			Object resp = conn.getContent();
			log.debug("resp :{}",resp.toString());
			
			if(responseCode != HttpStatus.OK.value())
			log.debug("Response Code : " + responseCode);

			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			
			response = parseJson(sb.toString());
			
		} catch (Exception e) {
            log.error("Failed delete payment caused by {}", e.getMessage(), e);
		}
		
		return response;
    	
    }
// detail transaksi    
    public PaymentResponseDTO statusPayment(String transactionId) {
		log.debug("detail payment with transaction id:{}",transactionId);

		PaymentResponseDTO response = null;
		try {
			String param = "/va/transactions/"+transactionId;
			
    		BufferedReader in = null;

			URL url = new URL(applicationProperties.getPayment().getUrl() + param);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("x-api-key",applicationProperties.getPayment().getApikey());
			conn.setRequestProperty("Host", applicationProperties.getPayment().getHost());
			conn.setRequestProperty("merchant_id", applicationProperties.getPayment().getMerchantid());
			conn.setRequestProperty("Content-Length", "");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
			int responseCode = conn.getResponseCode();
			URL res = conn.getURL();
			log.debug("res :{}",res);
			Object resp = conn.getContent();
			log.debug("resp :{}",resp.toString());
			
			if(responseCode != HttpStatus.OK.value())
			log.debug("Response Code : " + responseCode);

			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			
			response = parseJsonCekVa(sb.toString());
			
		} catch (Exception e) {
            log.error("Failed cek status payment caused by {}", e.getMessage(), e);
		}
		
		return response;
    	
    }  
    
//    V2
    public PaymentResponseDTO statusPaymentV2(String transactionId) {
		log.debug("detail payment with transaction id:{}",transactionId);

		PaymentResponseDTO response = null;
		Long id = 0L;
		
		Transaksi  transaksi = transaksiRepository.findByTransactionId(transactionId);
		if (transaksi != null) {
			id = transaksi.getId();
		}
		
		try {
			String auth = "Basic "+ encriptSha256Service.toBase64(applicationProperties.getPayment().getMerchantid() + ":" 
						+ applicationProperties.getPayment().getSecretUnboundId());
			
			log.debug("Auth Base64 from {} to {}",applicationProperties.getPayment().getMerchantid() + ":" 
					+ applicationProperties.getPayment().getSecretUnboundId(), auth);
			
			String xReqSignature = encriptSha256Service.encript(applicationProperties.getPayment().getHashKey() + id.toString() + id.toString());
			
    		BufferedReader in = null;

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("external_id", id.toString());
            jsonObject.put("order_id", id.toString());
            jsonObject.put("transaction_id", transactionId);
            jsonObject.put("payment_method", "BANK_TRANSFER");
            jsonObject.put("payment_channel", transaksi.getBank().split("_")[1].toUpperCase());
            
            String postData = jsonObject.toString();

			URL url = new URL(applicationProperties.getPayment().getUrl() + "/inquiry");
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", auth);
			conn.setRequestProperty("x-req-signature", xReqSignature);
			conn.setRequestProperty("x-version", "v3");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();
			
			int responseCode = conn.getResponseCode();
			URL res = conn.getURL();
			Object resp = conn.getContent();
			log.debug("resp :{}",resp.toString());
			log.debug("res :{}",res);
			
			if(responseCode != HttpStatus.OK.value())
			log.debug("Response Code : " + responseCode);

			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			
			response = parseJsonCekVaV2(sb.toString());
			
		} catch (Exception e) {
            log.error("Failed cek status payment caused by {}", e.getMessage(), e);
		}
		
		return response;
    	
    }
    
    public PaymentResponseDTO cancelPaymentV2(String transactionId) {
		log.debug("detail payment with transaction id:{}",transactionId);

		PaymentResponseDTO response = null;
		Long id = 0L;
		
		Transaksi  transaksi = transaksiRepository.findByTransactionId(transactionId);
		if (transaksi != null) {
			id = transaksi.getId();
		}
		
		try {
			String auth = "Basic "+ encriptSha256Service.toBase64(applicationProperties.getPayment().getMerchantid() + ":" 
						+ applicationProperties.getPayment().getSecretUnboundId());
			
			log.debug("Auth Base64 from {} to {}",applicationProperties.getPayment().getMerchantid() + ":" 
					+ applicationProperties.getPayment().getSecretUnboundId(), auth);
			
			String xReqSignature = encriptSha256Service.encript(applicationProperties.getPayment().getHashKey() + id.toString() + id.toString());
			
    		BufferedReader in = null;

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("external_id", id.toString());
            jsonObject.put("order_id", id.toString());
            jsonObject.put("transaction_id", transactionId);
            jsonObject.put("payment_method", "BANK_TRANSFER");
            jsonObject.put("payment_channel", transaksi.getBank().split("_")[1].toUpperCase());
            
            String postData = jsonObject.toString();

			URL url = new URL(applicationProperties.getPayment().getUrl() + "/cancel");
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", auth);
			conn.setRequestProperty("x-req-signature", xReqSignature);
			conn.setRequestProperty("x-version", "v3");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();
			
			int responseCode = conn.getResponseCode();
			URL res = conn.getURL();
			Object resp = conn.getContent();
			log.debug("resp :{}",resp.toString());
			log.debug("res :{}",res);
			
			if(responseCode != HttpStatus.OK.value())
			log.debug("Response Code : " + responseCode);

			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			
			response = parseJsonCancelVaV2(sb.toString());
			
		} catch (Exception e) {
            log.error("Failed cek status payment caused by {}", e.getMessage(), e);
		}
		
		return response;
    	
    }
    
	private static PaymentResponseDTO parseJsonCancelVaV2(String json) throws Exception {

		log.debug("parseJson:{}",json);
		
		PaymentResponseDTO resp = new PaymentResponseDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject message = new JSONObject(json);
				resp.setResponse_code(message.getString("response_code"));
				resp.setTransaction_id(message.getString("transaction_id"));
				resp.setExternal_id(message.getString("external_id"));
				resp.setOrder_id(message.getLong("order_id"));
				resp.setPayment_method(message.getString("payment_method"));
				resp.setPayment_channel(message.getString("payment_channel"));
				resp.setStatus(message.getString("transaction_status"));
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
	
	private static PaymentResponseDTO parseJsonCekAll(String json) throws Exception {
		PaymentResponseDTO resp = new PaymentResponseDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject message = new JSONObject(json);
				JSONArray content = message.getJSONArray("content");
				
				for (int i=0; i<content.length(); i++) {
					JSONObject e = content.getJSONObject(i);
					resp.setTransaction_id(e.getString("transaction_id"));
					resp.setAmount((int) e.getLong("amount"));
					resp.setPayment_code(e.getString("payment_code"));
					resp.setCustomer_info(e.getString("customer_info"));
					resp.setPayment_info(e.getString("payment_info"));
					resp.setStatus(e.getString("status"));
					resp.setOrder_id(e.getLong("order_id"));
					resp.setMerchant_id(e.getString("merchant_id"));
					resp.setPayment_channel(e.getString("payment_channel"));
					resp.setTransaction_time(e.getString("transaction_time"));
					resp.setExpired_time(e.getString("expired_date"));
					if ("SUCCESS".equals(resp.getStatus())) {
						resp.setPaid_date(e.getString("paid_date"));
					}

					log.debug("customer_info:{},{},{},{}",resp.getPayment_info(), resp.getPayment_code(), resp.getStatus());
					
					}
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
    // all transaksi
    public void allPayment() {
		log.debug(" GET transaction All");
		try {
			
			String param = "/va/transactions?page=all&size=2000";
			
    		BufferedReader in = null;

			URL url = new URL(applicationProperties.getPayment().getUrl() + param);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("x-api-key",applicationProperties.getPayment().getApikey());
			conn.setRequestProperty("Host", applicationProperties.getPayment().getHost());
			conn.setRequestProperty("merchant_id", applicationProperties.getPayment().getMerchantid());
			conn.setRequestProperty("Content-Length", "");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			parseJsonCekAll(sb.toString());
		} catch (Exception e) {
            log.error("Failed cek status payment caused by {}", e.getMessage(), e);
		}
    }
    
    public Tagihan requestPayment(TagihanDTO tagihandto, String va) {
		log.debug("Create payment:{}",tagihandto);

		PaymentResponseDTO response = null;
		try {
			Tagihan tagihan = tagihanRepository.findById(tagihandto.getIdTagihan()).get();
			String siswa = tagihan.getSiswa().getNama();
			String secretKey = null;
			if (va.toUpperCase().equals("VA_MUAMALAT")) {
				secretKey = applicationProperties.getPayment().getSecretkeymuammalat();
			} else {
				secretKey = applicationProperties.getPayment().getSecretkeyfinpay();
			}
			
			String param = "/va/transactions?payment="+va.toUpperCase();
			String signature = encriptSha256Service.encript(secretKey+applicationProperties.getPayment().getMerchantid()+
					tagihandto.getNominalPembayaran()+siswa+tagihandto.getJenisPembayaran()+
					tagihandto.getIdTagihan());
			
    		BufferedReader in = null;

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("amount", tagihandto.getNominalPembayaran());
            jsonObject.put("customer_info", siswa);
            jsonObject.put("payment_info", tagihandto.getJenisPembayaran());
            jsonObject.put("order_id", tagihandto.getIdTagihan());
            jsonObject.put("mrc_id", "sira200801");
            jsonObject.put("signature", signature);
            
            String postData = jsonObject.toString();

			URL url = new URL(applicationProperties.getPayment().getUrl() + param);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("x-api-key",applicationProperties.getPayment().getApikey());
			conn.setRequestProperty("Host", applicationProperties.getPayment().getHost());
			conn.setRequestProperty("Content-Length", "");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
			conn.setConnectTimeout(Integer.parseInt(connectTimeout));
			conn.setReadTimeout(Integer.parseInt(readTimeout));
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(postData);
            wr.flush();
            wr.close();
			
			log.debug("postDataBytes :{}, {}",postData, param);
			
			int responseCode = conn.getResponseCode();
			URL res = conn.getURL();
			log.debug("res :{}",res);
			Object resp = conn.getContent();
			log.debug("resp :{}",resp.toString());
			
			if(responseCode != HttpStatus.OK.value())
				log.debug("Post parameters : " + postData);
			log.debug("Response Code : " + responseCode);

			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String line;
			StringBuffer sb = new StringBuffer();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			
			response = parseJson(sb.toString());

			tagihan.setVirtualAccount(response.getPayment_code());
			tagihanRepository.save(tagihan);
			
		} catch (Exception e) {
            log.error("Failed create payment caused by {}", e.getMessage(), e);
		}
		
		return null ;
	}
}
