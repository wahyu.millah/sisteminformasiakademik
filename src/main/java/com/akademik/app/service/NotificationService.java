package com.akademik.app.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Optional;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.Notification;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.TokenNotifikasi;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.domain.enumeration.StatusTransaksi;
import com.akademik.app.repository.NotificationRepository;
import com.akademik.app.repository.SiswaRepository;
import com.akademik.app.repository.TagihanRepository;
import com.akademik.app.repository.TokenNotifikasiRepository;
import com.akademik.app.repository.TransaksiRepository;
import com.akademik.app.repository.UserRepository;
import com.akademik.app.security.SecurityUtils;
import com.akademik.app.service.dto.NotificationDTO;
import com.akademik.app.service.dto.WaResponDTO;
import com.akademik.app.util.DateUtils;

import javassist.expr.NewExpr;

@Service
@Transactional
public class NotificationService {

	private static final Logger log = LoggerFactory.getLogger(NotificationService.class);
	
	private final NotificationRepository notificationRepository;

    private final UserRepository userRepository;

    private final SiswaRepository siswaRepository;

    private final TokenNotifikasiRepository tokenNotifikasiRepository;
	
	public NotificationService (TokenNotifikasiRepository tokenNotifikasiRepository, NotificationRepository notificationRepository, UserRepository userRepository, SiswaRepository siswaRepository) {
		this.notificationRepository = notificationRepository;
        this.userRepository = userRepository;
        this.siswaRepository = siswaRepository;
        this.tokenNotifikasiRepository = tokenNotifikasiRepository;
	}
	
	public Notification save(NotificationDTO notificationDTO) {
		log.debug("Save notifikasi ...");
		
		Notification notification = new Notification();
		notification.setSignature(notificationDTO.getSignature());
		notification.setTransaction_id(notificationDTO.getTransaction_id());
		notification.setAmount(notificationDTO.getAmount());
		notification.setPayment_code(notificationDTO.getPayment_code());
		notification.setCustomer_info(notificationDTO.getCustomer_info());
		notification.setPayment_info(notificationDTO.getPayment_info());
		notification.setStatus(notificationDTO.getStatus());
		notification.setOrder_id(notificationDTO.getOrder_id());
		notification.setMerchant_id(notificationDTO.getMerchant_id());
		notification.setPayment_channel(notificationDTO.getPayment_channel());
		notification.setTransaction_time(DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(notificationDTO.getTransaction_time())));
		notification.setExpired_date(notificationDTO.getExpired_time() != null ?
				DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(notificationDTO.getExpired_time())): null);
		notification.setPaid_date(notificationDTO.getPaid_time() != null ?
				DateUtils.convertZonedDateTimeToInstant(ZonedDateTime.parse(notificationDTO.getPaid_time())): null);
		notification.setNotifOn(Instant.now());
		notification.setStatusUpdate(false);
		notificationRepository.save(notification);
		
		return notification;
	}
	
	public TokenNotifikasi saveToken(String tokenDevice) {
		log.debug("Save token device :{}", tokenDevice);
		
		String login = SecurityUtils.getCurrentUserLogin().get();
		Optional<User> user = userRepository.findOneByLogin(login);
			Siswa siswa = siswaRepository.findOneByUser(user.get());
			
			TokenNotifikasi tokenNotifikasi = null;
			if (siswa != null) {
				
				if (tokenNotifikasiRepository.findBySiswaId(siswa.getId()) != null && tokenDevice != null) {
					log.debug("Siswa already exist");
					tokenNotifikasi = tokenNotifikasiRepository.findBySiswaId(siswa.getId());
					tokenNotifikasi.setTokenNotifikasi(tokenDevice);
					tokenNotifikasiRepository.save(tokenNotifikasi);
				} else {
					log.debug("new token and siswa");
					tokenNotifikasi = new TokenNotifikasi();
					tokenNotifikasi.setSiswaId(siswa.getId());
					tokenNotifikasi.setTokenNotifikasi(tokenDevice);
					tokenNotifikasiRepository.save(tokenNotifikasi);
				}
			}
		
		return tokenNotifikasi;
	}
	
	public void sendNotifikasi(String to, String body, String title) {
    	log.info("Request to notifikasi to receiver:{}", to);
    	
    	WaResponDTO response = null;
    	
    		BufferedReader in = null;
            try {
            	
            	int connectTimeout = 20000;
            	int readTimeout = 20000;
            	
            	JSONObject jsonObject = new JSONObject();
            	jsonObject.put("to", to);
            	JSONObject notification = new JSONObject();
            	notification.put("body", body);
            	notification.put("title", title);
            	jsonObject.put("notification", notification);

                String postData = jsonObject.toString();

                URL url = new URL("https://fcm.googleapis.com/fcm/send");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("Authorization", "key=AAAA2XHbwC0:APA91bGS-cbnRkCBF7P-C7SpVjXAnijKmxWdghOcOfrEoOrJKHFYjWfk4aT4omTvReeGi8nkbN75HuxnmlN77sCnQHr1Wa-j3LI87uYDhGOx6G4uUOgP7o3h0TKd6pFznUbuch0s8JgZ");
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");

                con.setConnectTimeout(connectTimeout);
                con.setReadTimeout(readTimeout);
                con.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(postData);
                wr.flush();
                wr.close();

    			int responseCode = con.getResponseCode();
    			
    			if(responseCode == HttpStatus.OK.value()) {
    				log.debug("Post parameters : " + postData);
    				log.debug("Response Code : " + responseCode);
    				
    				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        			
        			String line;
        			StringBuffer sb = new StringBuffer();
        			while ((line = in.readLine()) != null) {
        				sb.append(line);
        			}
        			
        			response = parseJson(sb.toString());
    			} else  {
    				log.debug("Post parameters : " + postData);
    				log.debug("Response Code : " + responseCode);
    			}
            } catch (Exception e) {
                log.error("Failed sendMessage caused by {}", e.getMessage(), e);
            } finally {
    			if(in != null) {
    				try {
    					in.close();
    				} catch(Exception e) {}
    			}
    		}
		
	}
	
	private static WaResponDTO parseJson(String json) throws Exception {
		WaResponDTO resp = new WaResponDTO();
		resp.setSuccess(false);
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject obj = new JSONObject(json);
				log.debug(obj.toString());
//				resp.setSuccess(obj.getBoolean("success"));
//				
//				if(resp.getSuccess()) {
//					resp.setMessage(obj.getString("message"));
//				} else {
//					resp.setSuccess(obj.getBoolean("success"));
//					resp.setMessage(obj.getString("message"));
//				}
			} catch (Exception e) {
				throw e;
			}
		}
		
		return resp;
	}
	

}
