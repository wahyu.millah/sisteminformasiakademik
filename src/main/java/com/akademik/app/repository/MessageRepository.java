package com.akademik.app.repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.akademik.app.domain.ApplicationConfig;
import com.akademik.app.domain.Message;
import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;
import com.akademik.app.service.dto.MessageDTO;

/**
 * Spring Data  repository for the Kelas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MessageRepository extends JpaRepository<Message, Long>  {

	@Query("SELECT new com.akademik.app.service.dto.MessageDTO(m.id, m.channel, m.subject, m.content, m.status, u.id)"
			+ " FROM Message m"
			+ " INNER JOIN User u on u.id=m.user.id"
			+ " WHERE m.channel=:channel AND u.id=:userId"
			+ " ORDER BY m.id DESC")
	Page<MessageDTO> findByUserAndChannel(
			@Param("userId") Long userId,
			@Param("channel") NotificationChannel channel,
			Pageable pageable);
	
    @Transactional
    @Modifying
    @Query("UPDATE Message "
        + " SET status = READ "
        + " WHERE id=:id")
    int reading(
   		@Param("id") Long id);

	@Query("SELECT m"
			+ " FROM Message m"
			+ " WHERE m.id=:id")
	Optional<Message> findById(
			@Param("id") Long id);

}
