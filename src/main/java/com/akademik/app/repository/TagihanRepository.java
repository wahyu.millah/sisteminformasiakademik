package com.akademik.app.repository;

import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.service.dto.ReportDTO;
import com.akademik.app.service.dto.TagihanDTO;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the Tagihan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TagihanRepository extends JpaRepository<Tagihan, Long> {
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id")
	Page<TagihanDTO> findAllTagihan(Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type, t.id, t.keterangan)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.id=:id AND t.tahunAjaran=:ta AND t.isPublish='false'"
			+ " ORDER BY t.id DESC")
	Page<TagihanDTO> findAllTagihanByLoginAndTa(
			@Param("id") Long id,
			@Param("ta") String ta,
			Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type, t.id, t.keterangan)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.id=:id AND t.tahunAjaran=:ta AND t.isPublish='false'"
			+ " ORDER BY t.id DESC")
	List<TagihanDTO> findAllTagihanByLoginAndTa(
			@Param("id") Long id,
			@Param("ta") String ta);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type, t.id, t.keterangan)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.id=:id AND t.isPublish='false'"
			+ " ORDER BY t.id DESC")
	Page<TagihanDTO> findAllTagihanByLogin(
			@Param("id") Long id,
			Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE j.jenis='LOUNDRY'AND s.id=:id")
	Page<TagihanDTO> findAllTagihanByLoginAndLoundry(
			@Param("id") Long id,
			Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.id=:id")
	Page<List<TagihanDTO>> findAllTagihanBySiswa(
			@Param("id") Long id,
			Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.nis=:nis")
	Page<TagihanDTO> findTagihanByNis(
			@Param("nis") String nis,
			Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, g.total, g.harusBayar, g.harusBayar, g.id, g.total, g.tahunAjaran, g.jatuhTempo, g.tanggalBayar, g.status, t.id, t.nomor,t.tanggal, t.metode, t.bank, t.nomorRekening, t.virtualAccount, t.status, t.updateBy, t.updateOn, t.jmlTagiha, t.jmlBayar, t.jmlSisa, t.caraBayar, j.metode, t.expiredDate, t.transactionId, g.type)"
			+ " FROM Transaksi t"
			+ " INNER JOIN Tagihan g ON g.transaksi.id = t.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = g.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE s.nama LIKE %:nis%")
	Page<TagihanDTO> findTransaksiByNis(
			@Param("nis") String nis,
			Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, g.total, g.harusBayar, g.harusBayar, g.id, g.total, g.tahunAjaran, g.jatuhTempo, g.tanggalBayar, g.status, t.id, t.nomor,t.tanggal, t.metode, t.bank, t.nomorRekening, t.virtualAccount, t.status, t.updateBy, t.updateOn, t.jmlTagiha, t.jmlBayar, t.jmlSisa, t.caraBayar, j.metode, t.expiredDate, t.transactionId, g.type)"
			+ " FROM Transaksi t"
			+ " INNER JOIN Tagihan g ON g.transaksi.id = t.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = g.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
    		+ " WHERE t.tanggal BETWEEN :startDate AND :endDate"
			+ " ORDER BY t.id DESC")
	Page<TagihanDTO> findAllTransaksi(
			@Param("startDate") Instant startDate,
			@Param("endDate") Instant endDate,
			Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, g.total, g.harusBayar, g.harusBayar, g.id, g.total, g.tahunAjaran, g.jatuhTempo, g.tanggalBayar, g.status, t.id, t.nomor,t.tanggal, t.metode, t.bank, t.nomorRekening, t.virtualAccount, t.status, t.updateBy, t.updateOn, t.jmlTagiha, t.jmlBayar, t.jmlSisa, t.caraBayar, j.metode, t.expiredDate, t.transactionId, g.type)"
			+ " FROM Transaksi t"
			+ " INNER JOIN Tagihan g ON g.transaksi.id = t.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = g.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE t.virtualAccount=:va")
	Optional<TagihanDTO> findTransaksiByVa(
			@Param("va") String va);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(t.id, t.tanggal, t.bank, t.virtualAccount, t.jmlTagiha, jmlBayar, t.expiredDate)"
			+ " FROM Transaksi t"
			+ " WHERE t.virtualAccount=:va")
	Optional<TagihanDTO> findByTransaksiMultiByVa(
			@Param("va") String va);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, g.total, g.harusBayar, g.harusBayar, g.id, g.total, g.tahunAjaran, g.jatuhTempo, g.tanggalBayar, g.status)"
			+ " FROM Tagihan g"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE g.id=:id")
	Optional<TagihanDTO> findTagihanById(
			@Param("id") Long id);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, k.nama, g.total, g.harusBayar, g.harusBayar)"
			+ " FROM Tagihan g"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " LEFT JOIN Tagihan g1 ON g1.id = g.id AND g1.virtualAccount is not null"
			+ " LEFT JOIN Tagihan g2 ON g2.id = g.id AND g2.virtualAccount is null"
			+ " WHERE g.tanggalBayar=:tanggalBayar AND g.status=:status")
	Page<TagihanDTO> findTagihanByDateAndStatus(
			@Param("tanggalBayar") Instant tanggalBayar,
			@Param("status") StatusBayar status,
			Pageable pageable);

	// query loundry	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE j.jenis='LOUNDRY'")
	Page<TagihanDTO> findAllTagihanByAccLoundry(
			Pageable pageable);
	
	// query loundry and nis	
		@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, j.metode, t.virtualAccount, t.type)"
				+ " FROM Tagihan t"
				+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
				+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
				+ " INNER JOIN BesarBayar b ON b.id = t.besarBayar.id"
				+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
				+ " WHERE j.jenis='LOUNDRY'AND s.nis=:nis")
		Page<TagihanDTO> findAllTagihanByAccLoundryAndNis(
				@Param("nis") String nis,
				Pageable pageable);
	
	//query tagihan by group by siswa
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, s.id, s.tahunAjaran)"
			+ " FROM Siswa s"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE k.id=:kelasId")
	Page<TagihanDTO> findAllTagihanByGroupKelas(
			@Param("kelasId") Long kelasId,
			Pageable pageable);
	
// searching siswa di tagihan	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, s.id, s.tahunAjaran)"
			+ " FROM Siswa s"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE s.nama LIKE %:nis%")
	Page<TagihanDTO> findAllTagihanByGroupSiswaId(
			@Param("nis") String nis,
			Pageable pageable);
	
//query tagihan by group by siswa
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, s.id, s.tahunAjaran)"
			+ " FROM Siswa s"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE s.loundry = true")
	Page<TagihanDTO> findAllTagihanByGroupSiswaLoundry(
			Pageable pageable);
	
// searching siswa di tagihan	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, s.id, s.tahunAjaran)"
			+ " FROM Siswa s"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE  s.nama LIKE %:nis% AND s.loundry='true'")
	Page<TagihanDTO> findAllTagihanByGroupSiswaIdLoundry(
			@Param("nis") String nis,
			Pageable pageable);

	List<Tagihan> findByTransaksiId(Long id);
	
	static final String REPORT_DTO  = "SELECT new com.akademik.app.service.dto.ReportDTO(g.id, s.nama, s.nis, k.nama, g.tahunAjaran, g.total, g.harusBayar, t.tanggal, g.status, t.metode, t.bank, t.virtualAccount, t.updateBy)"
			+ " FROM Tagihan g"
			+ " LEFT JOIN Transaksi t ON g.transaksi.id = t.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = g.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id";
	
	@Query(REPORT_DTO + " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateAllReport();
	
	@Query(REPORT_DTO + " WHERE k.id =?1 AND j.id =?2 AND g.tahunAjaran =?3"
			+ " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateReportByKelasAndJenisAndThnAjaran(Long idKelas, Long idJenis, String ta);
	
	@Query(REPORT_DTO + " WHERE k.id =?1 AND g.tahunAjaran =?2"
			+ " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateReportByKelasAndThnAjaran(Long idKelas, String ta);
	
	@Query(REPORT_DTO + " WHERE k.id =?1 AND j.id =?2"
			+ " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateReportByKelasAndJenis(Long idKelas, Long idJenis);
	
	@Query(REPORT_DTO + " WHERE j.id =?1 AND g.tahunAjaran =?2"
			+ " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateReportByJenisAndThnAjaran(Long idJenis, String ta);
	
	@Query(REPORT_DTO + " WHERE j.id =?1"
			+ " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateReportByJenis(Long idJenis);
	
	@Query(REPORT_DTO + " WHERE g.tahunAjaran =?1"
			+ " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateReportByTahunAjaran(String ta);
	
	@Query(REPORT_DTO + " WHERE s.id =?1"
			+ " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateReportBySiswa(Long id);
	
	@Query(REPORT_DTO + " WHERE k.id =?1"
			+ " ORDER BY k.id, s.id, g.id DESC")
	List<ReportDTO> generateReportByKelas(Long id);
	
    @Transactional
    @Modifying
    @Query("UPDATE Tagihan " +
        " SET tanggalBayar =:tanggal " +
        " WHERE id=:id")
    int updateNote(
   		@Param("tanggal") Instant tanggal,
        @Param("id") Long id);

	List<Tagihan> findByStatus(StatusBayar lunas);
	
	List<Tagihan> findByStatusAndReportSubmitted(StatusBayar lunas, boolean b);
	
	@Query("SELECT t"
			+ " FROM Tagihan t"
			+ " WHERE t.total LIKE %:jenis% AND t.status=:status AND t.reportSubmitted=:reportSubmitted")
	List<Tagihan> findByTotal(
			@Param("jenis") String jenis,
			@Param("status") StatusBayar status,
			@Param("reportSubmitted") Boolean submitted);
// mobile repo	
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, t.virtualAccount, t.type, t.id, t.keterangan, t.multi, t.cicilan)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE s.id=:id AND t.isPublish='false' AND t.status in :statusList AND t.tahunAjaran=:ta"
			+ " ORDER BY t.id DESC")
	Page<TagihanDTO> findAllTagihanByLoginAndStatus(
			@Param("id") Long id,
			@Param("statusList") List<StatusBayar> defaultStatusList,
			@Param("ta") String ta,
			Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, t.virtualAccount, t.type, t.id, t.keterangan, t.multi, t.cicilan)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE s.id=:id AND t.isPublish='false' AND t.status in :statusList AND t.tahunAjaran=:ta"
			+ " ORDER BY t.id DESC")
	List<TagihanDTO> findAllTagihanByLoginAndStatus(
			@Param("id") Long id,
			@Param("statusList") List<StatusBayar> defaultStatusList,
			@Param("ta") String ta);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, t.virtualAccount, t.type, t.id, t.keterangan)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE s.id=:id AND t.isPublish='false' AND t.tahunAjaran=:ta"
			+ " ORDER BY t.id DESC")
	List<TagihanDTO> findAllTagihanBySiswaId(
			@Param("id") Long id,
			@Param("ta") String ta);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, t.virtualAccount, t.type, t.id, t.keterangan)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE t.id=:id")
	Optional<TagihanDTO> findDetailTagihanByLoginAndStatusNotLunas(
			@Param("id") Long id);

	List<Tagihan> findByVirtualAccount(String virtualAccount);

	Tagihan findBySiswa(Siswa siswa);
	
	@Query("SELECT new com.akademik.app.service.dto.TagihanDTO(s.nama, s.nis, k.nama, t.total, t.harusBayar, t.harusBayar, t.id, t.total, t.tahunAjaran, t.jatuhTempo, t.tanggalBayar, t.status, t.virtualAccount, t.type, t.id, t.keterangan, t.multi, t.cicilan)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE s.id=:id AND t.isPublish='false' AND t.status in :statusList AND t.tahunAjaran=:ta AND t.multi=true"
			+ " ORDER BY t.id DESC")
	Page<TagihanDTO> findAllMulti(
			@Param("id") Long id,
			@Param("statusList") List<StatusBayar> defaultStatusList,
			@Param("ta") String ta,
			Pageable pageable);

	List<Tagihan> findBySiswaIdAndTahunAjaran(Long idSiswa, String tahunAjaran);

	List<Tagihan> findBySiswaIdAndTahunAjaranAndStatusNot(Long i, String string, StatusBayar Status);
	
	@Query("SELECT new com.akademik.app.service.dto.ReportDTO(t.id, t.itemTagihan, SUM(t.harusBayar))"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE s.id=:id AND t.isPublish='false' AND t.status in :statusList AND t.tahunAjaran=:ta AND t.isPublish='false' GROUP BY t.itemTagihan ORDER BY t.id ASC")
	List<ReportDTO> generateReportByDateAndItem2(
			@Param("id") Long id,
			@Param("statusList") List<StatusBayar> defaultStatusList,
			@Param("ta") String ta);
	
	@Query("SELECT new com.akademik.app.service.dto.ReportDTO(t.id, t.total, t.harusBayar)"
			+ " FROM Tagihan t"
			+ " INNER JOIN Siswa s ON s.id = t.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE s.id=:id AND t.isPublish='false' AND t.status in :statusList AND t.tahunAjaran=:ta AND t.itemTagihan=:itemTagihan AND t.isPublish='false' ORDER BY t.id ASC")
	List<ReportDTO> listSyahriahLondry(
			@Param("id") Long id,
			@Param("statusList") List<StatusBayar> defaultStatusList,
			@Param("ta") String ta,
			@Param("itemTagihan") String itemTagihan);

}
