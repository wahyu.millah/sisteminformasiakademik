package com.akademik.app.repository;

import com.akademik.app.domain.Notification;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Kelas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

	List<Notification> findByStatusUpdate(boolean b);
}
