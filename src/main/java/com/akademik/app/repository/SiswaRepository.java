package com.akademik.app.repository;

import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.User;
import com.akademik.app.service.dto.KeyValueDTO;
import com.akademik.app.service.dto.ProfilDTO;
import com.akademik.app.service.dto.SiswaDTO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the Siswa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiswaRepository extends JpaRepository<Siswa, Long> {
	
	static final String SISWA_DTO  = "SELECT new com.akademik.app.service.dto.SiswaDTO(u.id, u.firstName, u.lastName, u.login, s.id, s.nis, s.nama, s.alamat, s.jenisKelamin, s.tempatLahir, s.tanggalLahir, s.waliMurid, s.noTelephone, s.tahunAjaran, s.status, k.nama, s.loundry, s.beasiswa, s.jmlBulan, s.jenisBeasiswa)"
			+ " FROM Siswa s"
			+ " INNER JOIN User u ON u.id = s.user.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id";
	
	static final String SISWA_DTO_MOBILE  = "SELECT new com.akademik.app.service.dto.SiswaDTO(u.id, s.id, s.nis, s.nama, k.nama, s.status, s.loundry, s.beasiswa)"
			+ " FROM Siswa s"
			+ " INNER JOIN User u ON u.id = s.user.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id";
	
	static final String SISWA_DTO_MOBILE_SET  = "SELECT new com.akademik.app.service.dto.SiswaDTO(s.id, s.nama, s.status, s.loundry, s.beasiswa)"
			+ " FROM Siswa s";
	
	Page<Siswa> findAll(Pageable pageable);
	
	@Query("SELECT new com.akademik.app.service.dto.ProfilDTO(u.id, u.firstName, u.lastName, s.id, s.nis, s.nama, s.alamat, s.jenisKelamin, s.tempatLahir, s.tanggalLahir, s.waliMurid, s.noTelephone, s.tahunAjaran, s.status, k.nama, u.imageUrl, s.agama, s.anakKe, s. madrasahAsal, s.kelasMasuk, s.tglMasuk, s.namaAyah, s.namaIbu, s.alamatOrangtua, s.pekerjaanAyah, s.pekerjaanIbu, s.namaWali, s.alamatWali, s.nomorTelpWali, s.pekerjaanWali, u.login, u.password, s.loundry, s.beasiswa, s.jenisBeasiswa, s.jmlBulan)"
			+ " FROM Siswa s"
			+ " INNER JOIN User u ON u.id = s.user.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " WHERE u.id=:id")
	Optional<ProfilDTO> userProfil(
			@Param("id") Long id);
	
	static final String QUERY_KEY_VALUE_DTO = 
			"SELECT new com.akademik.app.service.dto.KeyValueDTO(s.id, s.nama)"
			+ " FROM Siswa s";
	
	@Query(QUERY_KEY_VALUE_DTO)
	List<KeyValueDTO> findSiswaKeyValueAll();
	
	@Query(SISWA_DTO + " WHERE k.id =:kelasId")
	Page<SiswaDTO> findAllSiswaAndKelas (@Param("kelasId") Long kelasId, Pageable pageable);
	
	@Query(SISWA_DTO_MOBILE + " WHERE k.id =:kelasId")
	Page<SiswaDTO> findAllSiswaMobile (@Param("kelasId") Long kelasId, Pageable pageable);
	
	@Query(SISWA_DTO + " WHERE s.nama LIKE %:nama%")
	Page<SiswaDTO> findByNameSiswaDto (
			@Param("nama") String nama, Pageable pageable);
	
	@Query(SISWA_DTO_MOBILE + " WHERE s.nama LIKE %:nama%")
	Page<SiswaDTO> findByNameSiswaMobile (
			@Param("nama") String nama, Pageable pageable);
	
	@Query(SISWA_DTO_MOBILE + " WHERE s.id LIKE %:id%")
	ProfilDTO findByIdSiswaMobile (
			@Param("id") Long id);
	
	List<Siswa> findByKelasId(Long kelasId);

	Page<Siswa> findByKelasId(Long svalueKelas, Pageable pageable);

	Page<Siswa> findByKelasIdAndNis(Long kelasId, String svalue, Pageable pageable);

	Page<Siswa> findByKelasIdAndNama(Long kelasId, String svalue, Pageable pageable);

	Page<Siswa> findOneByNisAndKelas(String svalue, Long kelasId, Pageable pageable);

	List<Siswa> findByLoundry(boolean b);

	List<Siswa> findByStatus(String string);
	
	Siswa findOneByUser(User user);

	Page<Siswa> findOneByNis(String svalue, Pageable pageable);
	
	@Query("SELECT s"
			+ " FROM Siswa s"
			+ " WHERE s.nama LIKE %:nama%")
	Page<Siswa> findByNama(
			@Param("nama") String nama,
			Pageable pageable);
	
	@Query("SELECT s"
			+ " FROM Siswa s"
			+ " WHERE s.nama LIKE %:nama% AND s.kelas.id=:kelasId")
	Page<Siswa> findByNamaAndKelas(
			@Param("nama") String nama,
			@Param("kelasId") Long kelasId,
			Pageable pageable);
	
    @Transactional
    @Modifying
    @Query("UPDATE Siswa " +
        " SET beasiswa =:beasiswa" +
        " WHERE id=:id")
    int updateBeasiswa(
        @Param("id") Long id,
        @Param("beasiswa") Boolean beasiswa);
	
    @Transactional
    @Modifying
    @Query("UPDATE Siswa " +
        " SET loundry =:loundry" +
        " WHERE id=:id")
    int updateLoundry(
        @Param("id") Long id,
        @Param("loundry") Boolean loundry);

	Siswa findByNis(String nis);

	Siswa findByUser(User user);
	
	@Query(SISWA_DTO_MOBILE_SET
			+ " WHERE s.id=:siswaId AND s.kelas.id=:kelasId")
	SiswaDTO findBySiswaId(
			@Param("siswaId") String siswaId);
}
