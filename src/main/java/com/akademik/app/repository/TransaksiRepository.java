package com.akademik.app.repository;

import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.service.dto.TagihanDTO;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Transaksi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransaksiRepository extends JpaRepository<Transaksi, Long> {

//	Optional<Transaksi> findByTagihanId(Long id);

//	Transaksi findByTagihan(Tagihan tagihan);

	List<Transaksi> findByExpiredDateAndStatus(ZonedDateTime time, String string);

	Transaksi findByVirtualAccount(String va);
	Transaksi findByTransactionId(String transactionId);
	
//	@Query("SELECT * FROM Transaksi h WHERE h.expiredDate > time AND h.status=:status")
//    List<Transaksi> findByExpiredDateAndStatus(
//	    	@Param("time") String time,
//	    	@Param("status") String status);
}
