package com.akademik.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.akademik.app.domain.Karyawan;
import com.akademik.app.service.dto.ProfilDTO;

/**
 * Spring Data  repository for the Kelas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KaryawanRepository extends JpaRepository<Karyawan, Long>  {
	
	@Query("SELECT new com.akademik.app.service.dto.ProfilDTO(u.id, k.nama,k.alamat, k.jenisKelamin, k.tempatLahir, k.tanggalLahir, k.noTelephone, k.agama, k.nip, j.id, j.nama, k.id)"
			+ " FROM Karyawan k"
			+ " INNER JOIN User u ON u.id = k.user.id"
			+ " INNER JOIN Jabatan j ON j.id = k.jabatan.id"
			+ " WHERE u.id=:id")
	Optional<ProfilDTO> profil(
			@Param("id") Long id);

}
