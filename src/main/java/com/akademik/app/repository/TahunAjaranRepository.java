package com.akademik.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import com.akademik.app.domain.TahunAjaran;

/**
 * Spring Data  repository for the Kelas TahunAjaran.
 */
@SuppressWarnings("unused")
@Repository
public interface TahunAjaranRepository extends JpaRepository<TahunAjaran, Long> {
}
