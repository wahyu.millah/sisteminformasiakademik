package com.akademik.app.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akademik.app.domain.Surat;

@SuppressWarnings("unused")
@Repository
public interface SuratRepository extends JpaRepository<Surat, Long>  {

	List<Surat> findByTglKeluarBetween(LocalDate start, LocalDate end);

}
