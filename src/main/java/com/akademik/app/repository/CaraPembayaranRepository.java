package com.akademik.app.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akademik.app.domain.CaraPembayaran;
import com.akademik.app.domain.Channel;

@SuppressWarnings("unused")
@Repository
public interface CaraPembayaranRepository extends JpaRepository<CaraPembayaran, Long>  {

	List<CaraPembayaran> findByChannel(Channel channel);

}
