package com.akademik.app.repository;

import com.akademik.app.domain.Kelas;

import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Kelas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KelasRepository extends JpaRepository<Kelas, Long> {

	Optional<Kelas> findByNama(String kelas);
}
