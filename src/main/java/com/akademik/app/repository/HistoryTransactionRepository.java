package com.akademik.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akademik.app.domain.HistoryTransaction;

@SuppressWarnings("unused")
@Repository
public interface HistoryTransactionRepository extends JpaRepository<HistoryTransaction, Long>  {

	HistoryTransaction findByTransaksiId(Long id);

}
