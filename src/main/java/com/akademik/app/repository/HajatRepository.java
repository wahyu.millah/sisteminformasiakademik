package com.akademik.app.repository;

import com.akademik.app.domain.Hajat;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Hajat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HajatRepository extends JpaRepository<Hajat, Long> {

}
