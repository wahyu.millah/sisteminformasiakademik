package com.akademik.app.repository;

import com.akademik.app.domain.ReportSyahriah;
import com.akademik.app.domain.Siswa;
import com.akademik.app.domain.Transaksi;
import com.akademik.app.domain.User;
import com.akademik.app.domain.enumeration.StatusBayar;
import com.akademik.app.service.dto.ProfilDTO;
import com.akademik.app.service.dto.ReportDTO;
import com.akademik.app.service.dto.ReportSyahriahDTO;
import com.akademik.app.service.dto.RiwayatPembayaranDTO;
import com.akademik.app.service.dto.SiswaDTO;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the Siswa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReportSyahriahRepository extends JpaRepository<ReportSyahriah, Long>  {
	
	static final String R_SYAHRIAH_DTO  = "SELECT new com.akademik.app.service.dto.ReportSyahriahDTO("
			+ " r.id, s.nama, s.nis, r.kelas, r.tahunAjaran, r.januari, r.februari, r.maret, r.april, r.mei, r.juni, r.juli, r.agustus,"
			+ " r.september, r.oktober, r.november, r.desember, r.januariOn, r.februariOn, r.maretOn, r.aprilOn, r.meiOn, r.juniOn," 
			+ " r.juliOn, r.agustusOn, r.septemberOn, r.oktoberOn, r.novemberOn, r.desemberOn)"
			+ " FROM ReportSyahriah r"
			+ " INNER JOIN Siswa s ON s.id = r.siswa.id";
	
	static final String REPORT_DTO  = "SELECT new com.akademik.app.service.dto.ReportDTO(g.id, s.nama, s.nis, k.nama, g.tahunAjaran, g.total, b.nominal, t.tanggal, g.status, t.metode, t.bank, t.virtualAccount, t.updateBy)"
			+ " FROM Tagihan g"
			+ " LEFT JOIN Transaksi t ON g.transaksi.id = t.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id"
			+ " INNER JOIN BesarBayar b ON b.id = g.besarBayar.id"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id";
	
	static final String REPORT_DTO_DATE  = "SELECT new com.akademik.app.service.dto.ReportDTO(g.id, s.nama, s.nis, k.nama, g.total, g.harusBayar, t.tanggal, g.status, t.metode, t.virtualAccount)"
			+ " FROM Tagihan g"
			+ " LEFT JOIN Transaksi t ON g.transaksi.id = t.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id";
	
	static final String REPORT_DTO_DATE_ITEM  = "SELECT new com.akademik.app.service.dto.ReportDTO(g.id, g.itemTagihan, SUM(g.harusBayar))"
			+ " FROM Tagihan g"
			+ " LEFT JOIN Transaksi t ON g.transaksi.id = t.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id";
	
	static final String REPORT_DTO_RIWAYAT  = "SELECT new com.akademik.app.service.dto.RiwayatPembayaranDTO(g.id, s.nama, s.nis, k.nama, g.tahunAjaran, g.total, g.harusBayar, g.tanggalBayar, g.status)"
			+ " FROM Tagihan g"
			+ " LEFT JOIN Transaksi t ON g.transaksi.id = t.id"
			+ " INNER JOIN Siswa s ON s.id = g.siswa.id"
			+ " INNER JOIN Kelas k ON k.id = s.kelas.id";
	
	@Query(R_SYAHRIAH_DTO)
	Page<ReportSyahriahDTO> findAllSyahriah (Pageable pageable);
	
	@Query(R_SYAHRIAH_DTO + " WHERE s.nama LIKE %:nama%")
	Page<ReportSyahriahDTO> findByNameSiswaDto (
	@Param("nama") String nama, Pageable pageable);
	
	@Query(R_SYAHRIAH_DTO + " WHERE r.kelas=:kelas")
	Page<ReportSyahriahDTO> findByKelas (
	@Param("kelas") String kelas, Pageable pageable);
	
	@Query(R_SYAHRIAH_DTO)
	List<ReportSyahriahDTO> findAllSyahriah ();
	
	@Query(R_SYAHRIAH_DTO + " WHERE s.nama LIKE %:nama%")
	List<ReportSyahriahDTO> findByNameSiswaDto (
	@Param("nama") String nama);
	
	@Query(R_SYAHRIAH_DTO + " WHERE r.kelas=:kelas")
	List<ReportSyahriahDTO> findByKelas (
	@Param("kelas") String kelas);
	
	@Query(R_SYAHRIAH_DTO + " WHERE r.tahunAjaran=:ta")
	List<ReportSyahriahDTO> findByTa (
	@Param("ta") String ta);
	
	@Query(REPORT_DTO + " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateAllReport(Pageable pageable);
	
	@Query(REPORT_DTO + " WHERE k.id =?1 AND j.id =?2 AND g.tahunAjaran =?3"
			+ " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateReportByKelasAndJenisAndThnAjaran(Long idKelas, Long idJenis, String ta, Pageable pageable);
	
	@Query(REPORT_DTO + " WHERE k.id =?1 AND g.tahunAjaran =?2"
			+ " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateReportByKelasAndThnAjaran(Long idKelas, String ta, Pageable pageable);
	
	@Query(REPORT_DTO + " WHERE k.id =?1 AND j.id =?2"
			+ " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateReportByKelasAndJenis(Long idKelas, Long idJenis, Pageable pageable);
	
	@Query(REPORT_DTO + " WHERE j.id =?1 AND g.tahunAjaran =?2"
			+ " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateReportByJenisAndThnAjaran(Long idJenis, String ta, Pageable pageable);
	
	@Query(REPORT_DTO + " WHERE j.id =?1"
			+ " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateReportByJenis(Long idJenis, Pageable pageable);
	
	@Query(REPORT_DTO + " WHERE g.tahunAjaran =?1"
			+ " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateReportByTahunAjaran(String ta, Pageable pageable);
	
	@Query(REPORT_DTO + " WHERE s.id =?1"
			+ " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateReportBySiswa(Long id, Pageable pageable);
	
	@Query(REPORT_DTO + " WHERE k.id =?1"
			+ " ORDER BY k.id, s.id, g.id DESC")
	Page<ReportDTO> generateReportByKelas(Long id, Pageable pageable);
	
	@Query(REPORT_DTO_DATE + " WHERE g.status ='LUNAS' AND t.tanggal BETWEEN ?1 AND ?2 ORDER BY t.tanggal DESC")
	Page<ReportDTO> generateReportByDate(Instant startDate,Instant endDate, Pageable pageable);
	
	@Query(REPORT_DTO_DATE + " WHERE g.status ='LUNAS' AND t.tanggal BETWEEN ?1 AND ?2 ORDER BY t.tanggal DESC")
	List<ReportDTO> generateReportByDate2(Instant startDate,Instant endDate);
	
	@Query(REPORT_DTO_DATE_ITEM + " WHERE g.status ='LUNAS' AND t.tanggal BETWEEN ?1 AND ?2 GROUP BY g.itemTagihan ORDER BY t.tanggal DESC")
	Page<ReportDTO> generateReportByDateAndItem(Instant startDate,Instant endDate, Pageable pageable);
	
	@Query(REPORT_DTO_DATE_ITEM + " WHERE g.status ='LUNAS' AND t.tanggal BETWEEN ?1 AND ?2 GROUP BY g.itemTagihan ORDER BY t.tanggal DESC")
	List<ReportDTO> generateReportByDateAndItem2(Instant startDate,Instant endDate);

	ReportSyahriah findBySiswa(Siswa siswa);
	
	@Query(REPORT_DTO_RIWAYAT + " WHERE s.id =?1 AND g.tahunAjaran =?2 AND g.status=?3 AND g.isPublish='false' ")
	Page<RiwayatPembayaranDTO> generateReportHistory(Long siswaId, String ta, StatusBayar status, Pageable pageable);
	
}
