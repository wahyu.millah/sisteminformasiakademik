package com.akademik.app.repository;

import com.akademik.app.domain.BesarBayar;
import com.akademik.app.domain.JenisPembayaran;
import com.akademik.app.domain.Kelas;
import com.akademik.app.service.dto.JenisPembayaranDTO;
import com.akademik.app.service.dto.TagihanDTO;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BesarBayar entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BesarBayarRepository extends JpaRepository<BesarBayar, Long> {

	List<BesarBayar> findByKelasId(Long kelasId);

	List<BesarBayar> findByJenisBayar(JenisPembayaran jp);

	List<BesarBayar> findByJenisBayarMetode(String string);

	List<BesarBayar> findByJenisBayarMetodeAndJenisBayarJenis(String string, String string2);

	Page<BesarBayar> findByKelas(Kelas kelas, Pageable pageable);

	List<BesarBayar> findByKelas(Kelas kelas);
	
	@Query("SELECT new com.akademik.app.service.dto.JenisPembayaranDTO(b.nominal, j.jenis, b.id)"
			+ " FROM BesarBayar b"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " WHERE b.kelas.id=:id"
			+ " ORDER BY b.id DESC")
	List<JenisPembayaranDTO> findAllJenisPembayaran(
			@Param("id") Long id);
	
	@Query("SELECT new com.akademik.app.service.dto.JenisPembayaranDTO(b.nominal, j.jenis, b.id)"
			+ " FROM BesarBayar b"
			+ " INNER JOIN JenisPembayaran j ON j.id = b.jenisBayar.id"
			+ " ORDER BY b.id DESC")
	List<JenisPembayaranDTO> findAllJenisPembayaran2();
}
