package com.akademik.app.repository;

import com.akademik.app.domain.Channel;
import com.akademik.app.domain.Kelas;
import com.akademik.app.domain.enumeration.StatusBank;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Kelas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChannelRepository extends JpaRepository<Channel, Long> {

	Channel findByName(String name);

	Page<Channel> findByStatus(StatusBank active, Pageable pageable);

	Channel findByVa(String upperCase);
}
