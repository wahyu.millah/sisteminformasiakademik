package com.akademik.app.repository;

import com.akademik.app.domain.TagihanCicilan;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data  repository for the Tagihan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TagihanCicilanRepository extends JpaRepository<TagihanCicilan, Long> {

}
