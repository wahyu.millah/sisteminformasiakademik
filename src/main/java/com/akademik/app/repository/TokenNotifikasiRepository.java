package com.akademik.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.akademik.app.domain.Tagihan;
import com.akademik.app.domain.TokenNotifikasi;
import com.akademik.app.domain.enumeration.StatusBayar;

/**
 * Spring Data  repository for the Kelas TahunAjaran.
 */
@SuppressWarnings("unused")
@Repository
public interface TokenNotifikasiRepository extends JpaRepository<TokenNotifikasi, Long> {

	@Query("SELECT t"
			+ " FROM TokenNotifikasi t"
			+ " WHERE t.siswaId=:siswaId ")
	TokenNotifikasi findBySiswaId(
			@Param("siswaId") Long siswaId);
}
