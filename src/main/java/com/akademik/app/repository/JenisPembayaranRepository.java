package com.akademik.app.repository;

import com.akademik.app.domain.JenisPembayaran;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the JenisPembayaran entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JenisPembayaranRepository extends JpaRepository<JenisPembayaran, Long> {

	JenisPembayaran findByJenis(String jenisTagihan);
}
