package com.akademik.app.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "history_transaction")
public class HistoryTransaction implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "transaksi_id")
    private Long transaksiId;
    
    @Column(name = "tagihan_id")
    private String tagihanId;
    
    @Column(name = "siswa_id")
    private Long siswaId;
    
    @Column(name = "channel_id")
    private Long channelId;
    
    @Column(name = "jenis")
    private String jenis;
    
    @Column(name = "nominal")
    private Long nominal;
    
    @Column(name = "transaction_by")
    private String transactionBy;
    
    @Column(name = "transaction_on")
    private LocalDateTime transactionOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTransaksiId() {
		return transaksiId;
	}

	public void setTransaksiId(Long transaksiId) {
		this.transaksiId = transaksiId;
	}

	public String getTagihanId() {
		return tagihanId;
	}

	public void setTagihanId(String tagihanId) {
		this.tagihanId = tagihanId;
	}

	public Long getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Long siswaId) {
		this.siswaId = siswaId;
	}

	public Long getChannelId() {
		return channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public Long getNominal() {
		return nominal;
	}

	public void setNominal(Long nominal) {
		this.nominal = nominal;
	}

	public String getTransactionBy() {
		return transactionBy;
	}

	public void setTransactionBy(String transactionBy) {
		this.transactionBy = transactionBy;
	}

	public LocalDateTime getTransactionOn() {
		return transactionOn;
	}

	public void setTransactionOn(LocalDateTime transactionOn) {
		this.transactionOn = transactionOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoryTransaction other = (HistoryTransaction) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HistoryTransaction [id=" + id + ", transaksiId=" + transaksiId + ", tagihanId=" + tagihanId + ", siswaId=" + siswaId + ", channelId=" + channelId + ", jenis="
				+ jenis + ", nominal=" + nominal + ", transactionBy=" + transactionBy + ", transactionOn=" + transactionOn + "]";
	}

}
