package com.akademik.app.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.akademik.app.domain.enumeration.NotificationChannel;
import com.akademik.app.domain.enumeration.NotificationStatus;

@Entity
@Table(name = "message")
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "datetime", nullable = false)
    private Instant datetime;

    @Enumerated(EnumType.STRING)
    @Column(name = "channel")
    private NotificationChannel channel;

    @NotNull
    @Size(max = 100)
    @Column(name = "sender", length = 100)
    private String sender;

    @NotNull
    @Size(max = 200)
    @Column(name = "receiver", length = 200)
    private String receiver;

    @Size(max = 100)
    @Column(name = "subject", length = 100)
    private String subject;

    @NotNull
    @Size(max = 2000)
    @Column(name = "content", length = 2000)
    private String content;

    @Size(max = 2000)
    @Column(name = "attachment", length = 2000)
    private String attachment;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private NotificationStatus status;

    @Column(name = "send_datetime")
    private Instant sendDatetime;

    @Column(name = "receive_datetime")
    private Instant receiveDatetime;

    @ManyToOne
    private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getDatetime() {
		return datetime;
	}

	public void setDatetime(Instant datetime) {
		this.datetime = datetime;
	}

	public NotificationChannel getChannel() {
		return channel;
	}

	public void setChannel(NotificationChannel channel) {
		this.channel = channel;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public NotificationStatus getStatus() {
		return status;
	}

	public void setStatus(NotificationStatus status) {
		this.status = status;
	}

	public Instant getSendDatetime() {
		return sendDatetime;
	}

	public void setSendDatetime(Instant sendDatetime) {
		this.sendDatetime = sendDatetime;
	}

	public Instant getReceiveDatetime() {
		return receiveDatetime;
	}

	public void setReceiveDatetime(Instant receiveDatetime) {
		this.receiveDatetime = receiveDatetime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Notification [id=" + id + ", datetime=" + datetime + ", channel=" + channel + ", sender=" + sender + ", receiver=" + receiver
				+ ", subject=" + subject + ", content=" + content + ", attachment=" + attachment + ", status=" + status + ", sendDatetime=" + sendDatetime
				+ ", receiveDatetime=" + receiveDatetime + ", user.id=" + (user != null ? user.getId() : "null") + "]";
	}

}
