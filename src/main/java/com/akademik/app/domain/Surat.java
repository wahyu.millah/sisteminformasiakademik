package com.akademik.app.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "surat")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Surat implements Serializable  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nomor", nullable = false)
    private Integer nomor;

    @Column(name = "tgl_keluar")
    private LocalDate tglKeluar;
    
    @Column(name = "used_by", nullable = false)
    private String usedBy;
    
    @Column(name = "nama", nullable = false)
    private String nama;

    @ManyToOne
    @JsonIgnoreProperties("surats")
    private Siswa siswa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNomor() {
		return nomor;
	}

	public void setNomor(Integer nomor) {
		this.nomor = nomor;
	}

	public LocalDate getTglKeluar() {
		return tglKeluar;
	}

	public void setTglKeluar(LocalDate tglKeluar) {
		this.tglKeluar = tglKeluar;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Siswa getSiswa() {
		return siswa;
	}

	public void setSiswa(Siswa siswa) {
		this.siswa = siswa;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Surat other = (Surat) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Surat [id=" + id + ", nomor=" + nomor + ", tglKeluar=" + tglKeluar + ", usedBy=" + usedBy + ", nama="
				+ nama + ", siswa=" + siswa + "]";
	}

}
