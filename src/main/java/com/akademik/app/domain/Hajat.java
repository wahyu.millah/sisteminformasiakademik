package com.akademik.app.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Hajat.
 */
@Entity
@Table(name = "hajat")
public class Hajat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pengantin_pria")
    private String pengantinPria;

    @Column(name = "image_pria")
    private String imagePria;

    @Column(name = "image_wanita")
    private String imageWanita;

    @Column(name = "pengantin_wanita")
    private String pengantinWanita;

    @Column(name = "wali_pria")
    private String waliPria;

    @Column(name = "wali_wanita")
    private String waliWanita;

    @Column(name = "tgl_akad")
    private String tglAkad;

    @Column(name = "hari_akad")
    private String hariAkad;

    @Column(name = "jam_akad")
    private String jamAkad;

    @Column(name = "lokasi_akad")
    private String lokasiAkad;

    @Column(name = "lokasi_akad_map")
    private String lokasiAkadMap;

    @Column(name = "tgl_resepsi")
    private String tglResepsi;

    @Column(name = "hari_resepsi")
    private String hariResepsi;

    @Column(name = "jam_mulai_resepsi")
    private String jamMulaiResepsi;

    @Column(name = "jam_selesai")
    private String jamSelesai;

    @Column(name = "lokasi_resepsi")
    private String lokasiResepsi;

    @Column(name = "lokasi_resepsi_map")
    private String lokasiResepsiMap;

    @Column(name = "catatan")
    private String catatan;

    @Column(name = "image_1")
    private String image1;

    @Column(name = "image_2")
    private String image2;

    @Column(name = "image_3")
    private String image3;

    @Column(name = "image_4")
    private String image4;

    @Column(name = "image_5")
    private String image5;

    @Column(name = "image_6")
    private String image6;

    @Column(name = "image_7")
    private String image7;

    @Column(name = "image_8")
    private String image8;

    @Column(name = "image_9")
    private String image9;

    @Column(name = "image_10")
    private String image10;

    @Column(name = "history_1")
    private String history1;

    @Column(name = "history_2")
    private String history2;

    @Column(name = "history_3")
    private String history3;

    @Column(name = "hstory_4")
    private String hstory4;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPengantinPria() {
        return pengantinPria;
    }

    public Hajat pengantinPria(String pengantinPria) {
        this.pengantinPria = pengantinPria;
        return this;
    }

    public void setPengantinPria(String pengantinPria) {
        this.pengantinPria = pengantinPria;
    }

    public String getImagePria() {
        return imagePria;
    }

    public Hajat imagePria(String imagePria) {
        this.imagePria = imagePria;
        return this;
    }

    public void setImagePria(String imagePria) {
        this.imagePria = imagePria;
    }

    public String getImageWanita() {
        return imageWanita;
    }

    public Hajat imageWanita(String imageWanita) {
        this.imageWanita = imageWanita;
        return this;
    }

    public void setImageWanita(String imageWanita) {
        this.imageWanita = imageWanita;
    }

    public String getPengantinWanita() {
        return pengantinWanita;
    }

    public Hajat pengantinWanita(String pengantinWanita) {
        this.pengantinWanita = pengantinWanita;
        return this;
    }

    public void setPengantinWanita(String pengantinWanita) {
        this.pengantinWanita = pengantinWanita;
    }

    public String getWaliPria() {
        return waliPria;
    }

    public Hajat waliPria(String waliPria) {
        this.waliPria = waliPria;
        return this;
    }

    public void setWaliPria(String waliPria) {
        this.waliPria = waliPria;
    }

    public String getWaliWanita() {
        return waliWanita;
    }

    public Hajat waliWanita(String waliWanita) {
        this.waliWanita = waliWanita;
        return this;
    }

    public void setWaliWanita(String waliWanita) {
        this.waliWanita = waliWanita;
    }

    public String getTglAkad() {
        return tglAkad;
    }

    public Hajat tglAkad(String tglAkad) {
        this.tglAkad = tglAkad;
        return this;
    }

    public void setTglAkad(String tglAkad) {
        this.tglAkad = tglAkad;
    }

    public String getHariAkad() {
        return hariAkad;
    }

    public Hajat hariAkad(String hariAkad) {
        this.hariAkad = hariAkad;
        return this;
    }

    public void setHariAkad(String hariAkad) {
        this.hariAkad = hariAkad;
    }

    public String getJamAkad() {
        return jamAkad;
    }

    public Hajat jamAkad(String jamAkad) {
        this.jamAkad = jamAkad;
        return this;
    }

    public void setJamAkad(String jamAkad) {
        this.jamAkad = jamAkad;
    }

    public String getLokasiAkad() {
        return lokasiAkad;
    }

    public Hajat lokasiAkad(String lokasiAkad) {
        this.lokasiAkad = lokasiAkad;
        return this;
    }

    public void setLokasiAkad(String lokasiAkad) {
        this.lokasiAkad = lokasiAkad;
    }

    public String getLokasiAkadMap() {
        return lokasiAkadMap;
    }

    public Hajat lokasiAkadMap(String lokasiAkadMap) {
        this.lokasiAkadMap = lokasiAkadMap;
        return this;
    }

    public void setLokasiAkadMap(String lokasiAkadMap) {
        this.lokasiAkadMap = lokasiAkadMap;
    }

    public String getTglResepsi() {
        return tglResepsi;
    }

    public Hajat tglResepsi(String tglResepsi) {
        this.tglResepsi = tglResepsi;
        return this;
    }

    public void setTglResepsi(String tglResepsi) {
        this.tglResepsi = tglResepsi;
    }

    public String getHariResepsi() {
        return hariResepsi;
    }

    public Hajat hariResepsi(String hariResepsi) {
        this.hariResepsi = hariResepsi;
        return this;
    }

    public void setHariResepsi(String hariResepsi) {
        this.hariResepsi = hariResepsi;
    }

    public String getJamMulaiResepsi() {
        return jamMulaiResepsi;
    }

    public Hajat jamMulaiResepsi(String jamMulaiResepsi) {
        this.jamMulaiResepsi = jamMulaiResepsi;
        return this;
    }

    public void setJamMulaiResepsi(String jamMulaiResepsi) {
        this.jamMulaiResepsi = jamMulaiResepsi;
    }

    public String getJamSelesai() {
        return jamSelesai;
    }

    public Hajat jamSelesai(String jamSelesai) {
        this.jamSelesai = jamSelesai;
        return this;
    }

    public void setJamSelesai(String jamSelesai) {
        this.jamSelesai = jamSelesai;
    }

    public String getLokasiResepsi() {
        return lokasiResepsi;
    }

    public Hajat lokasiResepsi(String lokasiResepsi) {
        this.lokasiResepsi = lokasiResepsi;
        return this;
    }

    public void setLokasiResepsi(String lokasiResepsi) {
        this.lokasiResepsi = lokasiResepsi;
    }

    public String getLokasiResepsiMap() {
        return lokasiResepsiMap;
    }

    public Hajat lokasiResepsiMap(String lokasiResepsiMap) {
        this.lokasiResepsiMap = lokasiResepsiMap;
        return this;
    }

    public void setLokasiResepsiMap(String lokasiResepsiMap) {
        this.lokasiResepsiMap = lokasiResepsiMap;
    }

    public String getCatatan() {
        return catatan;
    }

    public Hajat catatan(String catatan) {
        this.catatan = catatan;
        return this;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }

    public String getImage1() {
        return image1;
    }

    public Hajat image1(String image1) {
        this.image1 = image1;
        return this;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public Hajat image2(String image2) {
        this.image2 = image2;
        return this;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public Hajat image3(String image3) {
        this.image3 = image3;
        return this;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public Hajat image4(String image4) {
        this.image4 = image4;
        return this;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public Hajat image5(String image5) {
        this.image5 = image5;
        return this;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getImage6() {
        return image6;
    }

    public Hajat image6(String image6) {
        this.image6 = image6;
        return this;
    }

    public void setImage6(String image6) {
        this.image6 = image6;
    }

    public String getImage7() {
        return image7;
    }

    public Hajat image7(String image7) {
        this.image7 = image7;
        return this;
    }

    public void setImage7(String image7) {
        this.image7 = image7;
    }

    public String getImage8() {
        return image8;
    }

    public Hajat image8(String image8) {
        this.image8 = image8;
        return this;
    }

    public void setImage8(String image8) {
        this.image8 = image8;
    }

    public String getImage9() {
        return image9;
    }

    public Hajat image9(String image9) {
        this.image9 = image9;
        return this;
    }

    public void setImage9(String image9) {
        this.image9 = image9;
    }

    public String getImage10() {
        return image10;
    }

    public Hajat image10(String image10) {
        this.image10 = image10;
        return this;
    }

    public void setImage10(String image10) {
        this.image10 = image10;
    }

    public String getHistory1() {
        return history1;
    }

    public Hajat history1(String history1) {
        this.history1 = history1;
        return this;
    }

    public void setHistory1(String history1) {
        this.history1 = history1;
    }

    public String getHistory2() {
        return history2;
    }

    public Hajat history2(String history2) {
        this.history2 = history2;
        return this;
    }

    public void setHistory2(String history2) {
        this.history2 = history2;
    }

    public String getHistory3() {
        return history3;
    }

    public Hajat history3(String history3) {
        this.history3 = history3;
        return this;
    }

    public void setHistory3(String history3) {
        this.history3 = history3;
    }

    public String getHstory4() {
        return hstory4;
    }

    public Hajat hstory4(String hstory4) {
        this.hstory4 = hstory4;
        return this;
    }

    public void setHstory4(String hstory4) {
        this.hstory4 = hstory4;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Hajat)) {
            return false;
        }
        return id != null && id.equals(((Hajat) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Hajat{" +
            "id=" + getId() +
            ", pengantinPria='" + getPengantinPria() + "'" +
            ", imagePria='" + getImagePria() + "'" +
            ", imageWanita='" + getImageWanita() + "'" +
            ", pengantinWanita='" + getPengantinWanita() + "'" +
            ", waliPria='" + getWaliPria() + "'" +
            ", waliWanita='" + getWaliWanita() + "'" +
            ", tglAkad='" + getTglAkad() + "'" +
            ", hariAkad='" + getHariAkad() + "'" +
            ", jamAkad='" + getJamAkad() + "'" +
            ", lokasiAkad='" + getLokasiAkad() + "'" +
            ", lokasiAkadMap='" + getLokasiAkadMap() + "'" +
            ", tglResepsi='" + getTglResepsi() + "'" +
            ", hariResepsi='" + getHariResepsi() + "'" +
            ", jamMulaiResepsi='" + getJamMulaiResepsi() + "'" +
            ", jamSelesai='" + getJamSelesai() + "'" +
            ", lokasiResepsi='" + getLokasiResepsi() + "'" +
            ", lokasiResepsiMap='" + getLokasiResepsiMap() + "'" +
            ", catatan='" + getCatatan() + "'" +
            ", image1='" + getImage1() + "'" +
            ", image2='" + getImage2() + "'" +
            ", image3='" + getImage3() + "'" +
            ", image4='" + getImage4() + "'" +
            ", image5='" + getImage5() + "'" +
            ", image6='" + getImage6() + "'" +
            ", image7='" + getImage7() + "'" +
            ", image8='" + getImage8() + "'" +
            ", image9='" + getImage9() + "'" +
            ", image10='" + getImage10() + "'" +
            ", history1='" + getHistory1() + "'" +
            ", history2='" + getHistory2() + "'" +
            ", history3='" + getHistory3() + "'" +
            ", hstory4='" + getHstory4() + "'" +
            "}";
    }
}
