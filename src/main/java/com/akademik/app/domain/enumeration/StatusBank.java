package com.akademik.app.domain.enumeration;

/**
 * The Bank enumeration.
 */
public enum StatusBank {
    ACTIVE, INACTIVE
}
