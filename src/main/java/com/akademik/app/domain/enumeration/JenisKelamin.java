package com.akademik.app.domain.enumeration;

/**
 * The JenisKelamin enumeration.
 */
public enum JenisKelamin {
    LAKILAKI, PEREMPUAN
}
