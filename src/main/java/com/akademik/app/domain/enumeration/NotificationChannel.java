package com.akademik.app.domain.enumeration;

public enum NotificationChannel {
	APP, WA, SMS, EMAIL, FIREBASE
}
