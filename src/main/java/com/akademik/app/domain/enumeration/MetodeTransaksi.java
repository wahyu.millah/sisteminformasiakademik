package com.akademik.app.domain.enumeration;

/**
 * The MetodeTransaksi enumeration.
 */
public enum MetodeTransaksi {
    TUNAI, TRANSFER
}
