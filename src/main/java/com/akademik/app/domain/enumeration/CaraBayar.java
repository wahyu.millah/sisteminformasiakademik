package com.akademik.app.domain.enumeration;

/**
 * The MetodeTransaksi enumeration.
 */
public enum CaraBayar {
    CASH, ANGSURAN
}
