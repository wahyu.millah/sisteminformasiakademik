package com.akademik.app.domain.enumeration;

/**
 * The StatusBayar enumeration.
 */
public enum StatusBayar {
    LUNAS, BELUMBAYAR, NUNGGAK, BEASISWA, MENUNGGU, ANGSUR
}
