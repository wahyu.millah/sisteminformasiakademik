package com.akademik.app.domain.enumeration;

public enum NotificationStatus {
	NEW, SENT, FAILED, ERROR, READ
}
