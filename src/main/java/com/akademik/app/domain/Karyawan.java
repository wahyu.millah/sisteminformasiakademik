package com.akademik.app.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.akademik.app.domain.enumeration.JenisKelamin;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Guru.
 */
@Entity
@Table(name = "karyawan")
public class Karyawan  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nip", nullable = false)
    private String nip;

    @NotNull
    @Column(name = "nama", nullable = false)
    private String nama;

    @Column(name = "alamat", nullable = false)
    private String alamat;

    @Enumerated(EnumType.STRING)
    @Column(name = "jenis_kelamin")
    private JenisKelamin jenisKelamin;

    @Column(name = "tempat_lahir")
    private String tempatLahir;

    @Column(name = "tanggal_lahir")
    private Instant tanggalLahir;

    @Column(name = "no_telephone")
    private String noTelephone;

    @Column(name = "agama")
    private String agama;

    @OneToOne
    @JsonIgnoreProperties("karyawans")
    private PhotoProfil photoProfil;

	@OneToOne
    @JoinColumn(unique = true)
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("karyawans")
    private Jabatan jabatan;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public JenisKelamin getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(JenisKelamin jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public Instant getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Instant tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getNoTelephone() {
		return noTelephone;
	}

	public void setNoTelephone(String noTelephone) {
		this.noTelephone = noTelephone;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public PhotoProfil getPhotoProfil() {
		return photoProfil;
	}

	public void setPhotoProfil(PhotoProfil photoProfil) {
		this.photoProfil = photoProfil;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Jabatan getJabatan() {
		return jabatan;
	}

	public void setJabatan(Jabatan jabatan) {
		this.jabatan = jabatan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Karyawan other = (Karyawan) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Guru [id=" + id + ", nip=" + nip + ", nama=" + nama + ", alamat=" + alamat + ", jenisKelamin=" + jenisKelamin + ", tempatLahir=" + tempatLahir
				+ ", tanggalLahir=" + tanggalLahir + ", noTelephone=" + noTelephone + ", agama=" + agama + ", photoProfil=" + photoProfil + ", user=" + user + ", jabatan="
				+ jabatan + "]";
	}

}
