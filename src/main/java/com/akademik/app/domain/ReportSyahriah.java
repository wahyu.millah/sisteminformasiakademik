package com.akademik.app.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.Instant;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Report Syahriah.
 */
@Entity
@Table(name = "report_syahriah")
public class ReportSyahriah implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("reportSyahriahs")
    private Siswa siswa;

    @Column(name = "tahun_ajaran")
    private String tahunAjaran;

    @Column(name = "kelas")
    private String kelas;

    @Column(name = "januari")
    private Long januari;

    @Column(name = "februari")
    private Long februari;

    @Column(name = "maret")
    private Long maret;

    @Column(name = "april")
    private Long april;

    @Column(name = "mei")
    private Long mei;

    @Column(name = "juni")
    private Long juni;

    @Column(name = "juli")
    private Long juli;

    @Column(name = "agustus")
    private Long agustus;

    @Column(name = "september")
    private Long september;

    @Column(name = "oktober")
    private Long oktober;

    @Column(name = "november")
    private Long november;

    @Column(name = "desember")
    private Long desember;

    @Column(name = "januari_on")
    private Instant januariOn;

    @Column(name = "februari_on")
    private Instant februariOn;

    @Column(name = "maret_on")
    private Instant maretOn;

    @Column(name = "april_on")
    private Instant aprilOn;

    @Column(name = "mei_on")
    private Instant meiOn;

    @Column(name = "juni_on")
    private Instant juniOn;

    @Column(name = "juli_on")
    private Instant juliOn;

    @Column(name = "agustus_on")
    private Instant agustusOn;

    @Column(name = "september_on")
    private Instant septemberOn;

    @Column(name = "oktober_on")
    private Instant oktoberOn;

    @Column(name = "november_on")
    private Instant novemberOn;

    @Column(name = "desember_on")
    private Instant desemberOn;

    @Column(name = "jenis_pembayaran")
    private Instant jenisPembayaran;

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public Instant getJenisPembayaran() {
		return jenisPembayaran;
	}

	public void setJenisPembayaran(Instant jenisPembayaran) {
		this.jenisPembayaran = jenisPembayaran;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Siswa getSiswa() {
		return siswa;
	}

	public void setSiswa(Siswa siswa) {
		this.siswa = siswa;
	}

	public String getTahunAjaran() {
		return tahunAjaran;
	}

	public void setTahunAjaran(String tahunAjaran) {
		this.tahunAjaran = tahunAjaran;
	}

	public Long getJanuari() {
		return januari;
	}

	public void setJanuari(Long januari) {
		this.januari = januari;
	}

	public Long getFebruari() {
		return februari;
	}

	public void setFebruari(Long februari) {
		this.februari = februari;
	}

	public Long getMaret() {
		return maret;
	}

	public void setMaret(Long maret) {
		this.maret = maret;
	}

	public Long getApril() {
		return april;
	}

	public void setApril(Long april) {
		this.april = april;
	}

	public Long getMei() {
		return mei;
	}

	public void setMei(Long mei) {
		this.mei = mei;
	}

	public Long getJuni() {
		return juni;
	}

	public void setJuni(Long juni) {
		this.juni = juni;
	}

	public Long getJuli() {
		return juli;
	}

	public void setJuli(Long juli) {
		this.juli = juli;
	}

	public Long getAgustus() {
		return agustus;
	}

	public void setAgustus(Long agustus) {
		this.agustus = agustus;
	}

	public Long getSeptember() {
		return september;
	}

	public void setSeptember(Long september) {
		this.september = september;
	}

	public Long getOktober() {
		return oktober;
	}

	public void setOktober(Long oktober) {
		this.oktober = oktober;
	}

	public Long getNovember() {
		return november;
	}

	public void setNovember(Long november) {
		this.november = november;
	}

	public Long getDesember() {
		return desember;
	}

	public void setDesember(Long desember) {
		this.desember = desember;
	}

	public Instant getJanuariOn() {
		return januariOn;
	}

	public void setJanuariOn(Instant januariOn) {
		this.januariOn = januariOn;
	}

	public Instant getFebruariOn() {
		return februariOn;
	}

	public void setFebruariOn(Instant februariOn) {
		this.februariOn = februariOn;
	}

	public Instant getMaretOn() {
		return maretOn;
	}

	public void setMaretOn(Instant maretOn) {
		this.maretOn = maretOn;
	}

	public Instant getAprilOn() {
		return aprilOn;
	}

	public void setAprilOn(Instant aprilOn) {
		this.aprilOn = aprilOn;
	}

	public Instant getMeiOn() {
		return meiOn;
	}

	public void setMeiOn(Instant meiOn) {
		this.meiOn = meiOn;
	}

	public Instant getJuniOn() {
		return juniOn;
	}

	public void setJuniOn(Instant juniOn) {
		this.juniOn = juniOn;
	}

	public Instant getJuliOn() {
		return juliOn;
	}

	public void setJuliOn(Instant instant) {
		this.juliOn = instant;
	}

	public Instant getAgustusOn() {
		return agustusOn;
	}

	public void setAgustusOn(Instant agustusOn) {
		this.agustusOn = agustusOn;
	}

	public Instant getSeptemberOn() {
		return septemberOn;
	}

	public void setSeptemberOn(Instant septemberOn) {
		this.septemberOn = septemberOn;
	}

	public Instant getOktoberOn() {
		return oktoberOn;
	}

	public void setOktoberOn(Instant oktoberOn) {
		this.oktoberOn = oktoberOn;
	}

	public Instant getNovemberOn() {
		return novemberOn;
	}

	public void setNovemberOn(Instant novemberOn) {
		this.novemberOn = novemberOn;
	}

	public Instant getDesemberOn() {
		return desemberOn;
	}

	public void setDesemberOn(Instant desemberOn) {
		this.desemberOn = desemberOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReportSyahriah other = (ReportSyahriah) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ReportSyahriah [id=" + id + ", siswa=" + siswa + ", tahunAjaran=" + tahunAjaran + ", kelas=" + kelas + ", januari=" + januari + ", februari=" + februari
				+ ", maret=" + maret + ", april=" + april + ", mei=" + mei + ", juni=" + juni + ", juli=" + juli + ", agustus=" + agustus + ", september=" + september
				+ ", oktober=" + oktober + ", november=" + november + ", desember=" + desember + ", januariOn=" + januariOn + ", februariOn=" + februariOn + ", maretOn="
				+ maretOn + ", aprilOn=" + aprilOn + ", meiOn=" + meiOn + ", juniOn=" + juniOn + ", juliOn=" + juliOn + ", agustusOn=" + agustusOn + ", septemberOn="
				+ septemberOn + ", oktoberOn=" + oktoberOn + ", novemberOn=" + novemberOn + ", desemberOn=" + desemberOn + ", jenisPembayaran=" + jenisPembayaran + "]";
	}

}
