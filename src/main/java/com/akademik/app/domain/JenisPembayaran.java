package com.akademik.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A JenisPembayaran.
 */
@Entity
@Table(name = "jenis_pembayaran")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class JenisPembayaran implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jenis")
    private String jenis;

    @Column(name = "metode")
    private String metode;

    @Column(name = "keterangan")
    private String keterangan;
    
    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJenis() {
        return jenis;
    }

    public JenisPembayaran jenis(String jenis) {
        this.jenis = jenis;
        return this;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getMetode() {
        return metode;
    }

    public JenisPembayaran metode(String metode) {
        this.metode = metode;
        return this;
    }

    public void setMetode(String metode) {
        this.metode = metode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JenisPembayaran)) {
            return false;
        }
        return id != null && id.equals(((JenisPembayaran) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "JenisPembayaran{" +
            "id=" + getId() +
            ", jenis='" + getJenis() + "'" +
            ", metode='" + getMetode() + "'" +
            ", keterangan='" + getKeterangan() + "'" +
            "}";
    }
}
