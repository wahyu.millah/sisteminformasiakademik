package com.akademik.app.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Notification.
 */
@Entity
@Table(name = "notification")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Notification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "signature")
    private String signature;

    @Column(name = "transaction_id")
    private String transaction_id;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "payment_code")
    private String payment_code;

    @Column(name = "customer_info")
    private String customer_info;

    @Column(name = "payment_info")
    private String payment_info;

    @Column(name = "status")
    private String status;

    @Column(name = "order_id")
    private String order_id;

    @Column(name = "merchant_id")
    private String merchant_id;

    @Column(name = "payment_channel")
    private String payment_channel;

    @Column(name = "transaction_time")
    private Instant transaction_time;

    @Column(name = "expired_date")
    private Instant expired_date;

    @Column(name = "paid_date")
    private Instant paid_date;

    @Column(name = "notif_on")
    private Instant notifOn;

    @Column(name = "status_update")
    private Boolean statusUpdate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getPayment_code() {
		return payment_code;
	}

	public void setPayment_code(String payment_code) {
		this.payment_code = payment_code;
	}

	public String getCustomer_info() {
		return customer_info;
	}

	public void setCustomer_info(String customer_info) {
		this.customer_info = customer_info;
	}

	public String getPayment_info() {
		return payment_info;
	}

	public void setPayment_info(String payment_info) {
		this.payment_info = payment_info;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getPayment_channel() {
		return payment_channel;
	}

	public void setPayment_channel(String payment_channel) {
		this.payment_channel = payment_channel;
	}

	public Instant getTransaction_time() {
		return transaction_time;
	}

	public void setTransaction_time(Instant transaction_time) {
		this.transaction_time = transaction_time;
	}

	public Instant getExpaired_date() {
		return expired_date;
	}

	public void setExpired_date(Instant expired_date) {
		this.expired_date = expired_date;
	}

	public Instant getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(Instant paid_date) {
		this.paid_date = paid_date;
	}

	public Instant getNotifOn() {
		return notifOn;
	}

	public void setNotifOn(Instant notifOn) {
		this.notifOn = notifOn;
	}

	public Instant getExpired_date() {
		return expired_date;
	}

	public Boolean getStatusUpdate() {
		return statusUpdate;
	}

	public void setStatusUpdate(Boolean statusUpdate) {
		this.statusUpdate = statusUpdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notification other = (Notification) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Notification [id=" + id + ", signature=" + signature + ", transaction_id=" + transaction_id + ", amount=" + amount + ", payment_code=" + payment_code
				+ ", customer_info=" + customer_info + ", payment_info=" + payment_info + ", status=" + status + ", order_id=" + order_id + ", merchant_id=" + merchant_id
				+ ", payment_channel=" + payment_channel + ", transaction_time=" + transaction_time + ", expired_date=" + expired_date + ", paid_date=" + paid_date
				+ ", notifOn=" + notifOn + ", statusUpdate=" + statusUpdate + "]";
	}
}
