package com.akademik.app.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "titipan_dana")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TitipanDana implements Serializable  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "status")
    private String status;
    
    @Column(name = "nominal")
    private Long nominal;

    @Column(name = "tgl_kirim")
    private Instant tglKirim;
    
    @Column(name = "virtual_account")
    private String virtualAccount;
    
    @ManyToOne
    @JsonIgnoreProperties("titipanDana")
    private Channel channel;
    
    @ManyToOne
    @JsonIgnoreProperties("titipanDana")
    private Transaksi transaksi;

    @ManyToOne
    @JsonIgnoreProperties("titipanDana")
    private Siswa siswa;
	

}
