package com.akademik.app.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

import com.akademik.app.domain.enumeration.StatusBayar;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Tagihan Cicilan.
 */
@Entity
@Table(name = "tagihan_cicilan")
public class TagihanCicilan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    @JsonIgnoreProperties("tagihans")
    private Tagihan tagihan;

    @Column(name = "nama_tagihan")
    private String namaTagihan;

    @Column(name = "jml_bayar")
    private Long jmlBayar;

    @Column(name = "tgl_bayar")
    private Instant tglBayar;

    @Column(name = "virtual_account")
    private String virtualAccount;

    @Column(name = "status_bayar")
    private String statusBayar;

    @Column(name = "keterangan")
    private String keterangan;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Tagihan getTagihan() {
		return tagihan;
	}

	public void setTagihan(Tagihan tagihan) {
		this.tagihan = tagihan;
	}

	public String getNamaTagihan() {
		return namaTagihan;
	}

	public void setNamaTagihan(String namaTagihan) {
		this.namaTagihan = namaTagihan;
	}

	public Long getJmlBayar() {
		return jmlBayar;
	}

	public void setJmlBayar(Long jmlBayar) {
		this.jmlBayar = jmlBayar;
	}

	public Instant getTglBayar() {
		return tglBayar;
	}

	public void setTglBayar(Instant tglBayar) {
		this.tglBayar = tglBayar;
	}

	public String getVirtualAccount() {
		return virtualAccount;
	}

	public void setVirtualAccount(String virtualAccount) {
		this.virtualAccount = virtualAccount;
	}

	public String getStatusBayar() {
		return statusBayar;
	}

	public void setStatusBayar(String statusBayar) {
		this.statusBayar = statusBayar;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagihanCicilan other = (TagihanCicilan) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TagihanCicilan [id=" + id + ", tagihan=" + tagihan + ", namaTagihan=" + namaTagihan + ", jmlBayar="
				+ jmlBayar + ", tglBayar=" + tglBayar + ", virtualAccount=" + virtualAccount + ", statusBayar="
				+ statusBayar + ", keterangan=" + keterangan + "]";
	}
}
