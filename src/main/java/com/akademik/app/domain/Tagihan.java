package com.akademik.app.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

import com.akademik.app.domain.enumeration.StatusBayar;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Tagihan.
 */
@Entity
@Table(name = "tagihan")
public class Tagihan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nomor", nullable = false)
    private String nomor;

    @Column(name = "tahun_ajaran", nullable = false)
    private String tahunAjaran;

    @Column(name = "total", nullable = false)
    private String total;

    @Column(name = "harus_bayar", nullable = false)
    private Long harusBayar;

    @Column(name = "jatuh_tempo", nullable = false)
    private Instant jatuhTempo;

    @Column(name = "tanggal_bayar")
    private Instant tanggalBayar;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusBayar status;

    @OneToOne
    private BesarBayar besarBayar;

    @ManyToOne
    @JsonIgnoreProperties("tagihans")
    private Siswa siswa;
    
    @Column(name = "virtual_account")
    private String virtualAccount;
    
    @ManyToOne
    @JsonIgnoreProperties("tagihans")
    private Transaksi transaksi;

    @Column(name = "type", nullable = false)
    private Integer type;

    @Column(name = "is_publish")
    private Integer isPublish;

    @Column(name = "report_submitted")
    private Boolean reportSubmitted;

    @Column(name = "keterangan")
    private String keterangan;

    @Column(name = "multi")
    private Boolean multi;

    @Column(name = "item_tagihan")
    private String itemTagihan;

    @Column(name = "cicilan")
    private Boolean cicilan;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public Siswa getSiswa() {
		return siswa;
	}

	public void setSiswa(Siswa siswa) {
		this.siswa = siswa;
	}

	public void setId(Long id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public Tagihan nomor(String nomor) {
        this.nomor = nomor;
        return this;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getTahunAjaran() {
        return tahunAjaran;
    }

    public Tagihan tahunAjaran(String tahunAjaran) {
        this.tahunAjaran = tahunAjaran;
        return this;
    }

    public void setTahunAjaran(String tahunAjaran) {
        this.tahunAjaran = tahunAjaran;
    }

    public String getTotal() {
        return total;
    }

    public Tagihan total(String total) {
        this.total = total;
        return this;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Instant getJatuhTempo() {
        return jatuhTempo;
    }

    public Tagihan jatuhTempo(Instant jatuhTempo) {
        this.jatuhTempo = jatuhTempo;
        return this;
    }

    public void setJatuhTempo(Instant jatuhTempo) {
        this.jatuhTempo = jatuhTempo;
    }

    public Instant getTanggalBayar() {
        return tanggalBayar;
    }

    public Tagihan tanggalBayar(Instant tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
        return this;
    }

    public void setTanggalBayar(Instant tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
    }

    public StatusBayar getStatus() {
        return status;
    }

    public Tagihan status(StatusBayar status) {
        this.status = status;
        return this;
    }

    public void setStatus(StatusBayar status) {
        this.status = status;
    }

    public BesarBayar getBesarBayar() {
        return besarBayar;
    }

    public Tagihan besarBayar(BesarBayar besarBayar) {
        this.besarBayar = besarBayar;
        return this;
    }

    public void setBesarBayar(BesarBayar besarBayar) {
        this.besarBayar = besarBayar;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Long getHarusBayar() {
		return harusBayar;
	}

	public void setHarusBayar(Long harusBayar) {
		this.harusBayar = harusBayar;
	}

	public String getVirtualAccount() {
		return virtualAccount;
	}

	public void setVirtualAccount(String virtualAccount) {
		this.virtualAccount = virtualAccount;
	}

	public Transaksi getTransaksi() {
		return transaksi;
	}

	public void setTransaksi(Transaksi transaksi) {
		this.transaksi = transaksi;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIsPublish() {
		return isPublish;
	}

	public void setIsPublish(Integer isPublish) {
		this.isPublish = isPublish;
	}

	public Boolean getReportSubmitted() {
		return reportSubmitted;
	}

	public void setReportSubmitted(Boolean reportSubmitted) {
		this.reportSubmitted = reportSubmitted;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public Boolean getMulti() {
		return multi;
	}

	public void setMulti(Boolean multi) {
		this.multi = multi;
	}

	public String getItemTagihan() {
		return itemTagihan;
	}

	public void setItemTagihan(String itemTagihan) {
		this.itemTagihan = itemTagihan;
	}

	public Boolean getCicilan() {
		return cicilan;
	}

	public void setCicilan(Boolean cicilan) {
		this.cicilan = cicilan;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tagihan)) {
            return false;
        }
        return id != null && id.equals(((Tagihan) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

	@Override
	public String toString() {
		return "Tagihan [id=" + id + ", nomor=" + nomor + ", tahunAjaran=" + tahunAjaran + ", total=" + total
				+ ", harusBayar=" + harusBayar + ", jatuhTempo=" + jatuhTempo + ", tanggalBayar=" + tanggalBayar
				+ ", status=" + status + ", besarBayar=" + besarBayar + ", siswa=" + siswa + ", virtualAccount="
				+ virtualAccount + ", transaksi=" + transaksi + ", type=" + type + ", isPublish=" + isPublish
				+ ", reportSubmitted=" + reportSubmitted + ", keterangan=" + keterangan + ", multi=" + multi
				+ ", itemTagihan=" + itemTagihan + ", cicilan=" + cicilan + "]";
	}
    
//	@Override
//	public String toString() {
//		return "Tagihan [id=" + id + ", nomor=" + nomor + ", tahunAjaran=" + tahunAjaran + ", total=" + total + ", harusBayar=" + harusBayar + ", jatuhTempo=" + jatuhTempo
//				+ ", tanggalBayar=" + tanggalBayar + ", status=" + status + ", besarBayar=" + besarBayar + ", siswa=" + siswa + ", virtualAccount=" + virtualAccount
//				+ ", transaksi=" + transaksi + ", type=" + type + ", isPublish=" + isPublish + ", reportSubmitted=" + reportSubmitted + ", keterangan=" + keterangan
//				+ ", multi=" + multi + ", itemTagihan=" + itemTagihan + "]";
//	}
}
