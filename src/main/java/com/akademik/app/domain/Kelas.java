package com.akademik.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.akademik.app.domain.enumeration.Jenjang;

/**
 * A Kelas.
 */
@Entity
@Table(name = "kelas")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Kelas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nama", nullable = false)
    private String nama;

    @Enumerated(EnumType.STRING)
    @Column(name = "jenjang")
    private Jenjang jenjang;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public Kelas nama(String nama) {
        this.nama = nama;
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Jenjang getJenjang() {
        return jenjang;
    }

    public Kelas jenjang(Jenjang jenjang) {
        this.jenjang = jenjang;
        return this;
    }

    public void setJenjang(Jenjang jenjang) {
        this.jenjang = jenjang;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kelas)) {
            return false;
        }
        return id != null && id.equals(((Kelas) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Kelas{" +
            "id=" + getId() +
            ", nama='" + getNama() + "'" +
            ", jenjang='" + getJenjang() + "'" +
            "}";
    }
}
