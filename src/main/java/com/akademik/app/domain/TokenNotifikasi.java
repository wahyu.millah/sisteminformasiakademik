package com.akademik.app.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "token_notifikasi")
public class TokenNotifikasi implements Serializable  {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "siswa_id")
    private Long siswaId;

    @Column(name = "token_notifikasi")
    private String tokenNotifikasi;

	public Long getSiswaId() {
		return siswaId;
	}

	public void setSiswaId(Long siswaId) {
		this.siswaId = siswaId;
	}

	public String getTokenNotifikasi() {
		return tokenNotifikasi;
	}

	public void setTokenNotifikasi(String tokenNotifikasi) {
		this.tokenNotifikasi = tokenNotifikasi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((siswaId == null) ? 0 : siswaId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TokenNotifikasi other = (TokenNotifikasi) obj;
		if (siswaId == null) {
			if (other.siswaId != null)
				return false;
		} else if (!siswaId.equals(other.siswaId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TokenNotifikasi [siswaId=" + siswaId + ", tokenNotifikasi=" + tokenNotifikasi + "]";
	}

}
